//
//  PrivacyViewController.m
//  LYK
//
//  Created by Gouranga Sasmal on 20/07/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import "PrivacyViewController.h"

#define privacyURL                @"http://www.lykapp.com/privacy-policy/"

@interface PrivacyViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webview;

@end

@implementation PrivacyViewController
{
    UIActivityIndicatorView *activityIndicator;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(!activityIndicator){
        activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
        activityIndicator.layer.cornerRadius = 05;
        activityIndicator.opaque = NO;
        activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        activityIndicator.center = self.view.center;
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview: activityIndicator];
        [activityIndicator startAnimating];
    }
    
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:privacyURL];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    //Load the request in the UIWebView.
    [_webview loadRequest:requestObj];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavHeader"]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return  YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [activityIndicator stopAnimating];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error{
    [activityIndicator stopAnimating];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
