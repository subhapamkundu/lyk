//
//  AnalyticsViewController.m
//  LYK
//
//  Created by Subhapam on 16/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import "AnalyticsViewController.h"
#import "MPFlipViewController.h"
#import "ContentViewController.h"
#import "HomeNavController.h"
#import "DetailContentViewController.h"
#import "SelectRelationTableViewCell.h"

#define MOVIE_MIN		-1

@interface AnalyticsFlipDataSet : NSObject

@property (nonatomic) NSInteger maxAnalyticsId;
@property (nonatomic) NSMutableArray *contents;

-(BOOL)parseDataSetWithDetails:(NSDictionary *)json;

@end

@interface AnalyticsFlipDataSet ()
{
    
}
@end

@implementation AnalyticsFlipDataSet

-(BOOL)parseDataSetWithDetails:(NSDictionary *)json{
    _maxAnalyticsId = [[json objectForKey:@"maxAnalyticsId"] integerValue];
    for (NSDictionary *details in [json objectForKey:@"contents"]) {
        NSDictionary *parsedDetails = nil;
        if([[details objectForKey:@"type"] isEqualToString:@"analytics"]){
            parsedDetails = [NSDictionary dictionaryWithObjectsAndKeys:
                             [[details objectForKey:@"content"] objectForKey:@"analyticsId"]?:@"", @"id",
                             [[details objectForKey:@"content"] objectForKey:@"analyticsAge"]?:@"", @"userId",
                             [[details objectForKey:@"content"] objectForKey:@"analyticsTitle"]?:@"", @"statement",
                             [[details objectForKey:@"content"] objectForKey:@"analyticsImageUrl"]?:@"", @"imageURL",
                             [[details objectForKey:@"content"] objectForKey:@"analyticsDescription"]?:@"", @"desc",
                             [[details objectForKey:@"content"] objectForKey:@"analyticsLink"]?:@"", @"linkURL",
                             @"analytics", @"type",nil];
        }
        if(!_contents)
            _contents = [NSMutableArray array];
        [_contents insertObject:parsedDetails atIndex:[_contents count]];
    }
    return YES;
}

@end

@interface AnalyticsViewController ()<MPFlipViewControllerDelegate, MPFlipViewControllerDataSource>
{
    UIActivityIndicatorView *activityIndicator;
    NSMutableDictionary *socialUserDetails;
    NSUInteger lastAnalyticsId;
    NSTimer *autoNextPageTimer;
    ContentViewController *previousPage;
    ContentViewController *currentPage;
    ContentViewController *nextPage;
    AnalyticsFlipDataSet *dataSet;
    BOOL shouldStopOrStopFlipping;
    NSMutableDictionary *selectedUsers;
}

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (assign, nonatomic) int previousIndex;
@property (assign, nonatomic) int tentativeIndex;
@property (strong, nonatomic) MPFlipViewController *flipViewController;

@end


@implementation AnalyticsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.hidesBackButton = YES;
    //    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"Back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(goBack)];
    
    //    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:((HomeNavController *)self.navigationController) action:@selector(toggleMenuWithDelegate)];
    
    [self getDataSet];
    
    self.previousIndex = MOVIE_MIN;
    
    // Configure the page view controller and add it as a child view controller.
    CGRect pageViewRect = self.contentView.bounds;
    self.flipViewController = [[MPFlipViewController alloc] initWithOrientation:[self flipViewController:nil orientationForInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation]];
    self.flipViewController.delegate = self;
    self.flipViewController.dataSource = self;
    self.flipViewController.view.frame = pageViewRect;
    [self addChildViewController:self.flipViewController];
    [self.contentView addSubview:self.flipViewController.view];
    [self.flipViewController didMoveToParentViewController:self];
    
    UISwipeGestureRecognizer *moveToTabBarControllerSwipegesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(goToTabBarController)];
    
    [_contentView addGestureRecognizer:moveToTabBarControllerSwipegesture];
    
    
//    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]
//                                                          initWithTarget:self action:@selector(handleSingleTap:)];
//    
//    singleTapGestureRecognizer.numberOfTapsRequired = 1;
//    [_flipViewController.view addGestureRecognizer:singleTapGestureRecognizer];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(!shouldStopOrStopFlipping)
        autoNextPageTimer = [NSTimer scheduledTimerWithTimeInterval:6.0f target:self selector:@selector(flipArticles) userInfo:nil repeats:YES];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    shouldStopOrStopFlipping = NO;
    if([autoNextPageTimer isValid]){
        [autoNextPageTimer invalidate];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 if([segue.identifier isEqualToString:AnalyticsToDetailSegue]){
     NSDictionary *currentContentDetails = [dataSet.contents objectAtIndex:currentPage.movieIndex];
     [((DetailContentViewController *)segue.destinationViewController) setURL:[NSURL URLWithString:[currentContentDetails objectForKey:@"linkURL"]]];
     // pass details to view controller.
     }
 }

-(void) handleSingleTap:(UITapGestureRecognizer *)sender{
    NSLog(@"Touched");
    NSDictionary *currentContentDetails = [dataSet.contents objectAtIndex:currentPage.movieIndex];
    if([currentContentDetails objectForKey:@"linkURL"] && ![allTrim([currentContentDetails objectForKey:@"linkURL"]) isEqualToString:@""]){
        [self performSegueWithIdentifier:AnalyticsToDetailSegue sender:self];
        [self pauseFlipping];
    }
}

-(void)goBack{
    //[self disableTextFieldValidation];
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}


- (void)contentViewWithIndex:(int)index
{
    NSLog(@"Present index:%d",index);
    if(index > 0){
        if(_previousIndex < _tentativeIndex){
            if(index > [dataSet.contents count]-5){
                [self getDataSet];
            }
            previousPage = currentPage;
            currentPage = nextPage;
            [self createNextPageForIndex:index];
        } else {
            nextPage = currentPage;
            currentPage = previousPage;
            [self createPreviousPageForIndex:index];
            //            UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]
            //                                                                  initWithTarget:self action:@selector(handleSingleTap:)];
            //
            //            singleTapGestureRecognizer.numberOfTapsRequired = 1;
            //            [_contentView addGestureRecognizer:singleTapGestureRecognizer];
        }
    } else if(index == 0){
        previousPage = nil;
        [self createCurrentPageForIndex:index];
        [self createNextPageForIndex:index];
    }
    //page.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

-(void)goToTabBarController{
    
}

-(void)pauseFlipping{
    shouldStopOrStopFlipping = YES;
    if([autoNextPageTimer isValid]){
        [autoNextPageTimer invalidate];
    }
    
}

-(void)startFlipping{
    if(![autoNextPageTimer isValid])
        autoNextPageTimer = [NSTimer scheduledTimerWithTimeInterval:6.0f target:_flipViewController selector:@selector(gotoNextPage) userInfo:nil repeats:YES];
}


#pragma mark - MPFlipViewControllerDelegate protocol

- (void)flipViewController:(MPFlipViewController *)flipViewController didFinishAnimating:(BOOL)finished previousViewController:(UIViewController *)previousViewController transitionCompleted:(BOOL)completed
{
    if (completed)
    {
        self.previousIndex = self.tentativeIndex;
    }
}

- (MPFlipViewControllerOrientation)flipViewController:(MPFlipViewController *)flipViewController orientationForInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        return UIInterfaceOrientationIsPortrait(orientation)? MPFlipViewControllerOrientationVertical : MPFlipViewControllerOrientationHorizontal;
    else
        return MPFlipViewControllerOrientationHorizontal;
}

#pragma mark - MPFlipViewControllerDataSource protocol

-(void)didReceiveUserInteractionInForwardDirection{
    if(![autoNextPageTimer isValid])
        [self startFlipping];
    else
        [self pauseFlipping];
}

-(void)didReceiveUserInteractionInBackwardDirection{
    [self pauseFlipping];
}


- (UIViewController *)flipViewController:(MPFlipViewController *)flipViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    int index = self.previousIndex;
    index--;
    if (index < 0)
        return nil; // reached beginning, don't wrap
    self.tentativeIndex = index;
    [self contentViewWithIndex:index];
    return currentPage;
}

- (UIViewController *)flipViewController:(MPFlipViewController *)flipViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    int index = self.previousIndex;
    index++;
    if (index >= [dataSet.contents count])
        return nil;
    self.tentativeIndex = index;
    [self contentViewWithIndex:index];
    return currentPage;
}

-(void)getDataSet{
    if(!activityIndicator){
        activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
        activityIndicator.layer.cornerRadius = 05;
        activityIndicator.opaque = NO;
        activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        activityIndicator.center = self.view.center;
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview: activityIndicator];
    }
    self.view.userInteractionEnabled = false;
    [activityIndicator startAnimating];
    [[RequestController sharedInstance] getAnalyiticsListWithMaxCredentials:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [[UserData sharedInstance] getUserId],@"userId",
      [NSNumber numberWithUnsignedInteger:15],@"setLength",
      [NSNumber numberWithUnsignedInteger:lastAnalyticsId],@"lastAnalyticsId",nil] successHandler:^(NSDictionary *response) {
         dispatch_async(dispatch_get_main_queue(), ^{
             self.view.userInteractionEnabled = true;
             [activityIndicator stopAnimating];
             NSLog(@"data set: %@", response);
             if([response objectForKey:@"token"]){
                 [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:@"token"]];
             } else {
                 //[[HelperModule sharedInstance] showNetworkToast:YES message:@"Token not validated" animated:YES];
             }
             [self parseFlipDataSetWithJSON:response];
             lastAnalyticsId = [dataSet maxAnalyticsId];
             if(!currentPage){
                 self.previousIndex = 0;
                 [self contentViewWithIndex:0];
                 [self.flipViewController setViewController:currentPage direction:MPFlipViewControllerDirectionForward animated:NO completion:nil];
                 self.view.gestureRecognizers = self.flipViewController.gestureRecognizers;
             }
         });
     } failureHandler:^(NSError *error) {
         dispatch_async(dispatch_get_main_queue(), ^{
             self.view.userInteractionEnabled = true;
             [activityIndicator stopAnimating];
             NSLog(@"%@",error);
         });
     }];
    
}

-(BOOL)parseFlipDataSetWithJSON:(NSDictionary *)json{
    BOOL flag = NO;
    if(!dataSet)
        dataSet= [[AnalyticsFlipDataSet alloc] init];
    flag = [dataSet parseDataSetWithDetails:[json objectForKey:@"response"]];
    return flag;
}

-(void)createNextPageForIndex:(NSInteger)index{
    if((index + 1) >= [dataSet.contents count]){
        return;
    }
    nextPage = (ContentViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    NSDictionary *contentDetails = [dataSet.contents objectAtIndex:index+1];
    nextPage.movieIndex = index+1;
    if([contentDetails objectForKey:@"imageURL"]){
        if([[contentDetails objectForKey:@"imageURL"] isEqualToString:@""])
            [nextPage setContentImageWithURL:[NSURL URLWithString:@"http://topwalls.net/wallpapers/2012/06/Parent-and-Child-Family-Children-Grassland-Chilean-960x640.jpg"]];
        else
            [nextPage setContentImageWithURL:[NSURL URLWithString:[contentDetails objectForKey:@"imageURL"]]];
    }
    if([[contentDetails objectForKey:@"desc"] isEqualToString:@""])
        [nextPage setContentDescription:[contentDetails objectForKey:@"Welcome to LyK"]];
    else
        [nextPage setContentDescription:[contentDetails objectForKey:@"desc"]];
}

-(void)createCurrentPageForIndex:(NSInteger)index{
    currentPage = (ContentViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    NSDictionary *currentContentDetails = [dataSet.contents objectAtIndex:index];
    currentPage.movieIndex = index;
    if([currentContentDetails objectForKey:@"imageURL"]){
        if([[currentContentDetails objectForKey:@"imageURL"] isEqualToString:@""])
            [currentPage setContentImageWithURL:[NSURL URLWithString:@"http://topwalls.net/wallpapers/2012/06/Parent-and-Child-Family-Children-Grassland-Chilean-960x640.jpg"]];
        else
            [currentPage setContentImageWithURL:[NSURL URLWithString:[currentContentDetails objectForKey:@"imageURL"]]];
    }
    if([[currentContentDetails objectForKey:@"desc"] isEqualToString:@""])
        [currentPage setContentDescription:[currentContentDetails objectForKey:@"Welcome to LyK"]];
    else
        [currentPage setContentDescription:[currentContentDetails objectForKey:@"desc"]];
//    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]
//                                                          initWithTarget:self action:@selector(handleSingleTap:)];
//    
//    singleTapGestureRecognizer.numberOfTapsRequired = 1;
//    [_contentView addGestureRecognizer:singleTapGestureRecognizer];
}

-(void)createPreviousPageForIndex:(NSInteger)index{
    if((index - 1) < 0){
        previousPage = nil;
        return;
    }
    previousPage = (ContentViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    NSDictionary *contentDetails = [dataSet.contents objectAtIndex:index-1];
    previousPage.movieIndex = index-1;
    if([contentDetails objectForKey:@"imageURL"]){
        if([[contentDetails objectForKey:@"imageURL"] isEqualToString:@""])
            [previousPage setContentImageWithURL:[NSURL URLWithString:@"http://topwalls.net/wallpapers/2012/06/Parent-and-Child-Family-Children-Grassland-Chilean-960x640.jpg"]];
        else
            [previousPage setContentImageWithURL:[NSURL URLWithString:[contentDetails objectForKey:@"imageURL"]]];
    }
    if([[contentDetails objectForKey:@"desc"] isEqualToString:@""])
        [previousPage setContentDescription:[contentDetails objectForKey:@"Welcome to LyK"]];
    else
        [previousPage setContentDescription:[contentDetails objectForKey:@"desc"]];
}

-(void)flipArticles{
    if(!shouldStopOrStopFlipping){
        [_flipViewController gotoNextPage];
    }
}


- (IBAction)viewDetails:(id)sender {
    NSLog(@"Touched");
    NSDictionary *currentContentDetails = [dataSet.contents objectAtIndex:currentPage.movieIndex];
    if([currentContentDetails objectForKey:@"linkURL"] && ![allTrim([currentContentDetails objectForKey:@"linkURL"]) isEqualToString:@""]){
        [self performSegueWithIdentifier:AnalyticsToDetailSegue sender:self];
        [self pauseFlipping];
    }
}

@end
