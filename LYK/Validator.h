//
//  Validator.h
//  LYK
//
//  Created by Subhapam on 18/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Validator : NSObject


/**
 Validates the email id passed as parameter and returns formated email Id (if any).
 @param emailId The emailId to validate.
 @returns boolean based on whether the email-id is valid or invalid.
 */
+ (BOOL) validateEmailId:(NSString *)emailId;

/*
 Validates the phone number passed as parameter and returns formated phone number with country code prefixed.
 @param phoneNumber The phone number to validate.
 @param countryCode The countryCode for the mobile number.
 @returns phone number after validation and prefixing the country code.ß
 */
+ (NSString *) validatePhoneNumber:(NSString *)phoneNumber withCountryCode:(NSString *)countryCode;

/**
 Validates the Zipcode passed as parameter.
 @param zipCode The zip code to validate.
 @returns boolean based on whether the zip code is valid or invalid.
 */
+ (BOOL) validateZipcode:(NSString *)zipCode;


/**
 Validates the _inputString passed as parameter.
 @param _inputString The String to validate.
 @returns boolean based on whether the _inputString is null or not.
 */
+(BOOL)isNullString:(NSString*)_inputString;


@end
