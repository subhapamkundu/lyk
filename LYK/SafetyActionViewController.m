//
//  SafetyActionViewController.m
//  LYK
//
//  Created by Subhapam on 01/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import "SafetyActionViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "SelectRelationTableViewCell.h"
#import <GoogleMaps/GoogleMaps.h>
#import <UIImageView+AFNetworking.h>
#import "Validator.h"
#import "MVPlaceSearchTextField/MVPlaceSearchTextField.h"



@interface SafetyActionViewController ()<MKMapViewDelegate, ABPeoplePickerNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate,GMSMapViewDelegate,PlaceSearchTextFieldDelegate>
{
    
    ActionDisplayState actionDisplayState;
    NSMutableArray *selectedCoordinates;
    NSMutableArray *routeLines; //your line
    MKPolyline *routeLine;
    MKPolylineView *routeLineView; //overlay view
    NSMutableDictionary *selectedUsers;
    ABPeoplePickerNavigationController *addressBookController;
    NSArray *relations;
    UIActivityIndicatorView *activityIndicator;
    BOOL userLocationIsSet;
    CLLocationManager *locationManager;
    NSUInteger selectedMarker;
    
    /*Variable is required for ActionDisplayNewRegion type*/
    GMSMutablePath *mutablePath;
    GMSPolygon *polygon;
   /// NSMutableArray *markerPoints;
    BOOL shouldMoveToUserLocation;
    BOOL ismarkerInfoWindowShowing;
    GMSMarker *tappedMarker;
    NSString *fistMarkerAddress;
    
    
}

#define OFFSET 268435456
#define RADIUS 85445659.44705395

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet GMSMapView *gMapView;
@property (weak, nonatomic) IBOutlet UIView *actionView;
@property (weak, nonatomic) IBOutlet LYKTextField *nameTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *numberTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *emailTextField;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *descriptionTextView;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightFromBottom;
@property (weak, nonatomic) IBOutlet UITextField *tfAddress;


- (IBAction)reset:(id)sender;
- (IBAction)save:(id)sender;


@end

@implementation SafetyActionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    shouldMoveToUserLocation = true;
    if(!_detailsToEdit){
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleLongPress:)];
        lpgr.minimumPressDuration = 1.0; //user needs to press for 1 seconds
        [_mapView addGestureRecognizer:lpgr];
    }
    selectedCoordinates = [NSMutableArray array];
    
    UIButton *magnifyingGlass = [[UIButton alloc] init];
    [magnifyingGlass setTitle:[[NSString alloc] initWithUTF8String:"\xF0\x9F\x94\x8D"] forState:UIControlStateNormal];
    [magnifyingGlass sizeToFit];
    [magnifyingGlass addTarget:self action:@selector(openAddressBook:) forControlEvents:UIControlEventTouchUpInside];
    
    [_numberTextField setRightView:magnifyingGlass];
    [_numberTextField setRightViewMode:UITextFieldViewModeAlways];
    
    relations = [[UserData sharedInstance] getMembersArrayWithSelf];
    
    [self setupActionView];
    CLLocationCoordinate2D location = _mapView.userLocation.coordinate;//CLLocationCoordinate2DMake(<LATITUDE>, <LONGITUDE>);
    MKCoordinateRegion region;
    // <LATITUDE> and <LONGITUDE> for Cupertino, CA.
    
    region = MKCoordinateRegionMake(location, MKCoordinateSpanMake(0.2, 0.2));
    // 0.5 is spanning value for region, make change if you feel to adjust bit more
    
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:region];
    [self.mapView setRegion:adjustedRegion animated:NO];
    
    if(!activityIndicator){
        activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
        activityIndicator.layer.cornerRadius = 05;
        activityIndicator.opaque = NO;
        activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        activityIndicator.center = self.view.center;
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview: activityIndicator];
    }
    _descriptionTextView.layer.cornerRadius = 3.0f;
    _descriptionTextView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _descriptionTextView.layer.borderWidth = 1.0f;
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    [locationManager startUpdatingLocation];
    
    _descriptionTextView.strApiKey=GoogleAPIKey;
    _descriptionTextView.placeSearchDelegate=self;
    _tfAddress.hidden=YES;
    
    /*Initialize selected marker index as NSNotFound*/
    selectedMarker=NSNotFound;
    ismarkerInfoWindowShowing=NO;
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavHeader"]];
    
}

/*-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    *To move user in his current location add observer*
    [_gMapView addObserver:self forKeyPath:@"myLocation" options:NSKeyValueObservingOptionNew context: nil];
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    if ([keyPath isEqualToString:@"myLocation"] && [object isKindOfClass:[GMSMapView class]])
    {
        [_gMapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:_gMapView.myLocation.coordinate.latitude longitude:_gMapView.myLocation.coordinate.longitude zoom:_gMapView.maxZoom]];
        *[_gMapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:_gMapView.myLocation.coordinate.latitude
                                                                                 longitude:_gMapView.myLocation.coordinate.longitude
                                                                                      zoom:_gMapView.projection.zoom]];*
    }
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    *Remove observer*
    [_gMapView removeObserver:self forKeyPath:@"myLocation"];
}*/

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    CLLocationCoordinate2D location = _mapView.userLocation.coordinate;//CLLocationCoordinate2DMake(<LATITUDE>, <LONGITUDE>);
    MKCoordinateRegion region;
    // <LATITUDE> and <LONGITUDE> for Cupertino, CA.
    
    region = MKCoordinateRegionMake(location, MKCoordinateSpanMake(2, 2));
    // 0.5 is spanning value for region, make change if you feel to adjust bit more
    
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:region];
    [self.mapView setRegion:adjustedRegion animated:NO];
    if(actionDisplayState != ActionDisplayNewRegion){
        [self zoomInForCoordinates:[_detailsToEdit objectForKey:@"latLong"]];
    }
    
    
    /*Set camera to the co-ordinate of first index of _detailsToEdit array*/
    NSDictionary *firstDict=[[_detailsToEdit objectForKey:@"latLong"]objectAtIndex:0];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[firstDict objectForKey:@"lat"]doubleValue]
                                                            longitude:[[firstDict objectForKey:@"longi"]doubleValue]
                                                                 zoom:12];
    [_gMapView moveCamera:[GMSCameraUpdate setCamera:camera]];

    
    selectedCoordinates=[[NSMutableArray alloc]init];
    
    switch (actionDisplayState) {
        case ActionDisplayAddedRegionByMe:{
            [_gMapView setFrame:CGRectMake(_gMapView.frame.origin.x, _gMapView.frame.origin.y, _gMapView.frame.size.width, _gMapView.frame.size.height + _resetButton.frame.size.height)];
            _layoutConstraintHeightFromBottom.constant = _layoutConstraintHeightFromBottom.constant - 50;
            [selectedCoordinates setArray:[_detailsToEdit objectForKey:@"latLong"]];
            _gMapView.delegate=self;
            [self addPolylineIn:_gMapView usingCoordinates:selectedCoordinates];

        } break;
        case ActionDisplayAddedRegionByOther:{
            [_gMapView setFrame:CGRectMake(_gMapView.frame.origin.x, _gMapView.frame.origin.y, _gMapView.frame.size.width, _gMapView.frame.size.height + _resetButton.frame.size.height)];
            _layoutConstraintHeightFromBottom.constant = _layoutConstraintHeightFromBottom.constant - 50;
            [selectedCoordinates setArray:[_detailsToEdit objectForKey:@"latLong"]];
            _gMapView.delegate=self;
            [self addPolylineIn:_gMapView usingCoordinates:selectedCoordinates];
        } break;
        case ActionDisplayNewRegion:{
            
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"Add at least 3 points to create region" animated:YES];
            
            /*Add double tap to recognize double tap*/
            
//            UILongPressGestureRecognizer *gesture=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(recognizelongPress)];
//            gesture.numberOfTapsRequired=1;
//            [_gMapView addGestureRecognizer:gesture];
            
            _tfAddress.hidden=NO;
            
            
            _gMapView.delegate=self;
            /*Set  camera position*/
            [_gMapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:22.265484564 longitude:88.3215646 zoom:9]];
        }break;
        default:{
            
        } break;
            
    }
    [self.view layoutIfNeeded];
}
-(void)recognizelongPress{
    
    NSLog(@"sgjkhg");
}

/*Format the coordinate in a way that , they could fom a optimal/valid region in Google Map*/
-(NSMutableArray *)getformattedArray{
    return selectedCoordinates;
}
/*
 *
 */

-(void)addPolylineIn:(GMSMapView *)view usingCoordinates:(NSArray *)arrCordinate{
    
//    view.delegate=self;
    
    [self getformattedArray];
    
    /*Clear all the marker from map*/
    [_gMapView clear];
    
    /*Remove all co-ordinate from path*/
    [mutablePath removeAllCoordinates];
    
    /*Initialize mutablePath*/
    mutablePath = [GMSMutablePath path];
    
    /*Add marker in the map as well as in path*/
    for (NSDictionary *dict in arrCordinate) {
        
        //if (actionDisplayState==ActionDisplayAddedRegionByMe) {
            GMSMarker *marker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake([[dict objectForKey:@"lat"]doubleValue], [[dict objectForKey:@"longi"]doubleValue])];
            //marker.title = @"Hello World";
            if(actionDisplayState == ActionDisplayNewRegion){
                marker.draggable=YES;
                marker.title=@"Delete";
            } else {
                marker.draggable = NO;
            }
            marker.map = view;
       // }
        
        [mutablePath addLatitude:[[dict objectForKey:@"lat"]doubleValue] longitude:[[dict objectForKey:@"longi"]doubleValue]];
        
    }
    
    //UILongPressGestureRecognizer
    
    if ([self isPossibleToDrawRegion:mutablePath]) {
        
        /*Draw polygon with above co-ordinate*/
        polygon = [GMSPolygon polygonWithPath:mutablePath];
        polygon.fillColor = [UIColor colorWithRed:0.25 green:0 blue:0 alpha:0.05];
        polygon.strokeColor = [UIColor blackColor];
        polygon.strokeWidth = 2;
        polygon.map = view;
   
    }
    
}

/*Check if region is possible to draw, */
-(BOOL)isPossibleToDrawRegion:(GMSMutablePath *)path{
    
    if (path.count>=3)
        return YES;
    else
        return NO;
    
}

-(NSNumber *)distanceFrom:(CLLocationCoordinate2D)fromCord to:(CLLocationCoordinate2D)toCord {
    
    CLLocation *fromLoc=[[CLLocation alloc]initWithLatitude:fromCord.latitude longitude:fromCord.longitude];
    CLLocation *toLoc=[[CLLocation alloc]initWithLatitude:toCord.latitude longitude:toCord.longitude];
    
    double distance=[toLoc distanceFromLocation:fromLoc];
    
    return [NSNumber numberWithDouble:distance];
}

-(NSUInteger)getMarkerIndexNearestOfCoordinate:(CLLocationCoordinate2D)coordinate{
 
    NSMutableArray *arrDistance=[NSMutableArray array];
    for (NSDictionary *dict in selectedCoordinates)
        [arrDistance addObject:[self distanceFrom:coordinate to:CLLocationCoordinate2DMake([[dict objectForKey:@"lat"]doubleValue], [[dict objectForKey:@"longi"]doubleValue])]];
    NSNumber* min = [arrDistance valueForKeyPath:@"@min.self"];
    
    return [arrDistance indexOfObject:min];
}

#pragma mark GMSMapView Delegate Method

//Working fine, this method will clash with when long press in marker.
//http://stackoverflow.com/questions/27397270/ios-google-maps-marker-drag-event  (we can use taht)
- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    
    if (actionDisplayState==ActionDisplayNewRegion){
        
    if (mutablePath.count==0)
        return;
    else if (mutablePath.count==1)
        selectedMarker=1;
    else
        selectedMarker=[self getMarkerIndexNearestOfCoordinate:coordinate];
    
    }
    
}
- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    if (actionDisplayState==ActionDisplayNewRegion){
        
        
        if (ismarkerInfoWindowShowing){
            ismarkerInfoWindowShowing=NO;
            return;
        }
        
        /*Create marker position and add it to array*/
        NSDictionary *point=@{@"lat":[NSString stringWithFormat:@"%lf",coordinate.latitude],
                              @"longi":[NSString stringWithFormat:@"%lf",coordinate.longitude]};
        
        [selectedCoordinates addObject:point];
        
        /*Show toast msg to user*/
        switch (selectedCoordinates.count) {
            case 1:
                [[HelperModule sharedInstance] showNetworkToast:YES message:@"Add two(2) more points to create region" animated:YES];
                break;
            case 2:
                [[HelperModule sharedInstance] showNetworkToast:YES message:@"Add one(1) more points to create region" animated:YES];
                break;
            default:
                break;
        }
        
        /*Create region*/
        [self addPolylineIn:_gMapView usingCoordinates:selectedCoordinates];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [self showAddressInTheTextfileldWith:coordinate];
        });
        
    }
    
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    
    if ([self getMarkerIndexNearestOfCoordinate:marker.position]!=NSNotFound) {
        
         [selectedCoordinates removeObjectAtIndex:[self getMarkerIndexNearestOfCoordinate:marker.position]];
    }
    ismarkerInfoWindowShowing=NO;
    
    /*Create region*/
    [self addPolylineIn:_gMapView usingCoordinates:selectedCoordinates];
    
}


- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    
    if (marker==tappedMarker) {
        ismarkerInfoWindowShowing=NO;
    }else{
        tappedMarker=marker;
        ismarkerInfoWindowShowing=YES;
    }
   // marker=tappedMarker;
    return NO;
}

- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    
    //isDragging = NO;
    
    if (selectedMarker==NSNotFound)
        return;

    
    NSDictionary *point=@{@"lat":[NSString stringWithFormat:@"%lf",marker.position.latitude],
                          @"longi":[NSString stringWithFormat:@"%lf",marker.position.longitude]};
    [selectedCoordinates replaceObjectAtIndex:selectedMarker withObject:point];
    
    
     [self addPolylineIn:_gMapView usingCoordinates:selectedCoordinates];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self showAddressInTheTextfileldWith:marker.position];
    });

    
    
}

-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{
    
}

-(void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
    if(gesture){
        shouldMoveToUserLocation = false;
    }
}


#pragma mark -
#pragma mark Place search Textfield Delegates
#pragma mark -

-(void)placeSearchResponseForSelectedPlace:(NSMutableDictionary*)responseDict{
    [self.view endEditing:YES];
    NSLog(@"%@",responseDict);
    
    //    NSDictionary *aDictLocation=[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"];
    //    if(areaTag == 300){
    //
    //        [orderDetails setObject:aDictLocation forKey:@"pickUpOrdinates"];
    //        NSLog(@"PickUp location lat-long :: %@",orderDetails);
    //    }
    //    else{
    //        [orderDetails setObject:aDictLocation forKey:@"deliveryOrdinates"];
    //        NSLog(@"delivery location lat-long :: %@",orderDetails);
    //    }
    //    if(orderDetails[@"pickUpOrdinates"] && orderDetails[@"deliveryOrdinates"]){
    //        //[self getOrderCredit];
    //    }
    //NSLog(@"SELECTED ADDRESS :%@",aDictLocation);
}

-(void)placeSearchWillShowResult{
    
}
-(void)placeSearchWillHideResult{
    
}
-(void)placeSearchResultCell:(UITableViewCell *)cell withPlaceObject:(PlaceObject *)placeObject atIndex:(NSInteger)index{
    
    if(index%2==0){
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }
}


#pragma mark
#pragma mark


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showAddressInTheTextfileldWith:(CLLocationCoordinate2D )coordinate{
    
    /*[[GMSGeocoder geocoder]reverseGeocodeCoordinate:coordinate completionHandler:^(GMSReverseGeocodeResponse * _Nullable response, NSError * _Nullable error) {
        
        GMSAddress *address=[response.results lastObject];
        NSString *str=[NSString stringWithFormat:@"%@",[address.lines componentsJoinedByString:@","]];
        self.tfAddress.text=str;
        
        
        
    }];*/
    
    CLGeocoder *geocoder;
    if (!geocoder)
        geocoder = [[CLGeocoder alloc] init];
    
    [geocoder reverseGeocodeLocation:[[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude] completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            CLPlacemark *place=(CLPlacemark *)[placemarks objectAtIndex:0];
            
            NSString *address=[NSString stringWithFormat:@"%@,%@,%@",place.subLocality?:@"",place.locality?:@"",place.country?:@""];
            if (!fistMarkerAddress) {
                 fistMarkerAddress=address;
            }
           
            self.tfAddress.text=address;
        });
    }];
    
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setActionViewState:(ActionDisplayState)state{
    actionDisplayState = state;
    if(actionDisplayState == ActionDisplayAddedSOSByMe || actionDisplayState == ActionDisplayAddedRegionByMe || actionDisplayState == ActionDisplayAddedSOSByOther || actionDisplayState == ActionDisplayAddedRegionByOther){
        NSArray *actionButtons = @[
                                   [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"ExternalShare"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(externalShare)],
                                   [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"InternalShare"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(internalShare)]
                                   ];
        
        self.navigationItem.rightBarButtonItems = actionButtons;
    }
}

-(void)setupActionView{
    switch (actionDisplayState) {
        case ActionDisplayNone:{
            NSLog(@"ActionDisplayNone");
            _mapView.hidden = true;
            _actionView.hidden = true;
            _descriptionTextView.hidden = true;
        }break;
        case ActionDisplayNewSOS:{
            NSLog(@"ActionDisplayNewSOS");
            _mapView.hidden = true;
            _actionView.hidden = false;
            _descriptionTextView.hidden = true;
        }break;
        case ActionDisplayAddedSOSByMe:{
            NSLog(@"ActionDisplayAddedSOSByMe");
            _mapView.hidden = true;
            _actionView.hidden = false;
            _saveButton.enabled = false;
            _resetButton.enabled = false;
            _saveButton.hidden = true;
            _resetButton.hidden = true;
            [_gMapView setFrame:CGRectMake(_gMapView.frame.origin.x, _gMapView.frame.origin.y, _gMapView.frame.size.width, _gMapView.frame.size.height + _resetButton.frame.size.height)];
            _descriptionTextView.hidden = true;
            _nameTextField.text = [_detailsToEdit objectForKey:@"sosName"];
            _emailTextField.text = [_detailsToEdit objectForKey:@"sosMail"];
            _numberTextField.text = [_detailsToEdit objectForKey:@"mobileNo"];
            UIButton *callButton = [[UIButton alloc] init];
            [callButton setTitle:nil forState:UIControlStateNormal];
            [callButton setBackgroundImage:[UIImage imageNamed:@"CallActive"] forState:UIControlStateNormal];
            [callButton sizeToFit];
            [callButton addTarget:self action:@selector(callAction) forControlEvents:UIControlEventTouchUpInside];
            
            [_numberTextField setRightView:callButton];
            [_numberTextField setRightViewMode:UITextFieldViewModeAlways];
            _tableview.hidden = true;
        }break;
        case ActionDisplayAddedSOSByOther:{
            NSLog(@"ActionDisplayAddedSOSByOther");
            _mapView.hidden = true;
            _actionView.hidden = false;
            _saveButton.enabled = false;
            _resetButton.enabled = false;
            _saveButton.hidden = true;
            _resetButton.hidden = true;
            [_gMapView setFrame:CGRectMake(_gMapView.frame.origin.x, _gMapView.frame.origin.y, _gMapView.frame.size.width, _gMapView.frame.size.height + _resetButton.frame.size.height)];
            _descriptionTextView.hidden = true;
            _nameTextField.text = [_detailsToEdit objectForKey:@"sosName"];
            _emailTextField.text = [_detailsToEdit objectForKey:@"sosMail"];
            _numberTextField.text = [_detailsToEdit objectForKey:@"mobileNo"];
            _tableview.hidden = true;
            UIButton *callButton = [[UIButton alloc] init];
            [callButton setTitle:nil forState:UIControlStateNormal];
            [callButton setBackgroundImage:[UIImage imageNamed:@"CallActive"] forState:UIControlStateNormal];
            [callButton sizeToFit];
            [callButton addTarget:self action:@selector(callAction) forControlEvents:UIControlEventTouchUpInside];
            [_numberTextField setRightView:callButton];
            [_numberTextField setRightViewMode:UITextFieldViewModeAlways];
        }break;
        case ActionDisplayNewRegion:{
            NSLog(@"ActionDisplayNewRegion");
//            _mapView.hidden = false;
            _actionView.hidden = true;
            _emailTextField.hidden = true;
            _numberTextField.hidden = true;
            _descriptionTextView.hidden = false;
            _mapView.region = MKCoordinateRegionMakeWithDistance(_mapView.userLocation.coordinate, 2000, 2000);
            [_saveButton setTitle:@"Next" forState:UIControlStateNormal];
        }break;
        case ActionDisplayAddedRegionByMe:{
            NSLog(@"ActionDisplayAddedRegionByMe");
//            _mapView.hidden = false;
            _actionView.hidden = true;
            _emailTextField.hidden = true;
            _numberTextField.hidden = true;
            _descriptionTextView.hidden = false;
            if(_detailsToEdit){
                [self zoomInForCoordinates:[_detailsToEdit objectForKey:@"latLong"]];
                [self plotMapWithCoordinates:[_detailsToEdit objectForKey:@"latLong"]];
            }
            _saveButton.enabled = false;
            _resetButton.enabled = false;
            _saveButton.hidden = true;
            _resetButton.hidden = true;
            [_gMapView setFrame:CGRectMake(_gMapView.frame.origin.x, _gMapView.frame.origin.y, _gMapView.frame.size.width, _gMapView.frame.size.height + _resetButton.frame.size.height)];
        }break;
        case ActionDisplayAddedRegionByOther:{
            NSLog(@"ActionDisplayAddedRegionByOther");
//            _mapView.hidden = false;
            _actionView.hidden = true;
            _emailTextField.hidden = true;
            _numberTextField.hidden = true;
            _descriptionTextView.hidden = false;
            if(_detailsToEdit){
//                [self zoomInForCoordinates:[_detailsToEdit objectForKey:@"latLong"]];
                [self plotMapWithCoordinates:[_detailsToEdit objectForKey:@"latLong"]];
            }
            _saveButton.enabled = false;
            _resetButton.enabled = false;
            _saveButton.hidden = true;
            _resetButton.hidden = true;
            [_gMapView setFrame:CGRectMake(_gMapView.frame.origin.x, _gMapView.frame.origin.y, _gMapView.frame.size.width, _gMapView.frame.size.height + _resetButton.frame.size.height)];
        }break;
        default:
            break;
    }
}

-(void)zoomInForCoordinates:(NSArray *)points{
    CLLocationDegrees minLat,minLng,maxLat,maxLng;
    if([points count]){
        for(NSDictionary *point in points) {
            double lat = [[point objectForKey:@"lat"] doubleValue];
            double longi = [[point objectForKey:@"longi"] doubleValue];
            CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat, longi);
            minLat = MIN(minLat, coordinate.latitude);
            minLng = MIN(minLng, coordinate.longitude);
            
            maxLat = MAX(maxLat, coordinate.latitude);
            maxLng = MAX(maxLng, coordinate.longitude);
        }
        
        CLLocationCoordinate2D coordinateOrigin = CLLocationCoordinate2DMake(minLat, minLng);
        CLLocationCoordinate2D coordinateMax = CLLocationCoordinate2DMake(maxLat, maxLng);
        
        MKMapPoint upperLeft = MKMapPointForCoordinate(coordinateOrigin);
        MKMapPoint lowerRight = MKMapPointForCoordinate(coordinateMax);
        
        //Create the map rect
        MKMapRect mapRect = MKMapRectMake(upperLeft.x,
                                          upperLeft.y,
                                          lowerRight.x - upperLeft.x,
                                          lowerRight.y - upperLeft.y);
        
        //Create the region
        MKCoordinateRegion region = MKCoordinateRegionForMapRect(mapRect);
        
        //THIS HAS THE CENTER, it should include spread
        double lat = [[points[0] objectForKey:@"lat"] doubleValue];
        double longi = [[points[0] objectForKey:@"longi"] doubleValue];
        CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(lat, longi);//region.center;
//        region.span.latitudeDelta = 0.008388;
//        region.span.longitudeDelta = 0.016243;
//        
//        [_mapView setRegion:region animated:YES];
        _mapView.region = MKCoordinateRegionMakeWithDistance(centerCoordinate, 2000, 2000);
        [_mapView setCenterCoordinate:centerCoordinate animated:false];
    }
}

-(void)plotMapWithCoordinates:(NSArray *)points{
    for (NSDictionary *point in points) {
        double lat = [[point objectForKey:@"lat"] doubleValue];
        double longi = [[point objectForKey:@"longi"] doubleValue];
        CLLocationCoordinate2D touchMapCoordinate = CLLocationCoordinate2DMake(lat, longi);
        MKPointAnnotation *annot = [[MKPointAnnotation alloc] init];
        annot.coordinate = touchMapCoordinate;
        [_mapView addAnnotation:annot];
        [selectedCoordinates addObject:annot];
    }
    [self drawBoundary];
}

- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    NSUInteger zoomLevel = [self getZoomLevel];
    
    if(zoomLevel > 13){
        if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
            return;
        CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
        CLLocationCoordinate2D touchMapCoordinate =
        [_mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
        MKPointAnnotation *annot = [[MKPointAnnotation alloc] init];
        annot.coordinate = touchMapCoordinate;
        [_mapView addAnnotation:annot];
        [selectedCoordinates addObject:annot];
    } else {
        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Please zoom to street view for marking points." animated:YES];
    }

    [self drawBoundary];
}

- (NSUInteger)getZoomLevel {
    MKCoordinateRegion region = _mapView.region;
    double centerPixelX = [self longitudeToPixelSpaceX:region.center.longitude];
    double topLeftPixelX = [self longitudeToPixelSpaceX:region.center.longitude - region.span.longitudeDelta / 2];
    double scaledMapWidth = (centerPixelX - topLeftPixelX) * 2;
    CGSize mapSizeInPixels = _mapView.bounds.size;
    double zoomScale = scaledMapWidth / mapSizeInPixels.width;
    double zoomExponent = log(zoomScale) / log(2);
    double zoomLevel = 20 - zoomExponent;
    return zoomLevel;
}

- (double)longitudeToPixelSpaceX:(double)longitude
{
    return round(OFFSET + RADIUS * longitude * M_PI / 180.0);
}

-(void)drawBoundary{
    if([selectedCoordinates count]>2){
        MKMapPoint* pointArr = malloc(sizeof(CLLocationCoordinate2D) * [selectedCoordinates count]);
        for (int index = 0; index < [selectedCoordinates count]; index++) {
            MKPointAnnotation *annot = [selectedCoordinates objectAtIndex:index];
            MKMapPoint point = MKMapPointForCoordinate(annot.coordinate);
            pointArr[index] = point;
        }
        routeLine = [MKPolyline polylineWithPoints:pointArr count:[selectedCoordinates count]];
        [_mapView setVisibleMapRect:[routeLine boundingMapRect]]; //If you want the route to be visible
        NSArray *arr = _mapView.overlays;
        [_mapView removeOverlays:arr];
        [_mapView addOverlay:routeLine];
    }
}

-(MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
    MKOverlayView* overlayView = nil;
    if(overlay == routeLine)
    {
        routeLineView = [[MKPolylineView alloc] initWithPolyline:routeLine];
        routeLineView.fillColor = [UIColor colorWithRed:0.945 green:0.027 blue:0.957  alpha:1];
        routeLineView.strokeColor = [UIColor colorWithRed:0.945 green:0.027 blue:0.957 alpha:1];
        routeLineView.lineWidth = 4;
        overlayView = routeLineView;
    }
    
    return overlayView;
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    if(!userLocationIsSet){
        CLLocationCoordinate2D location = _mapView.userLocation.coordinate;//CLLocationCoordinate2DMake(<LATITUDE>, <LONGITUDE>);
        MKCoordinateRegion region;
        if(actionDisplayState == ActionDisplayNewRegion)
            region = MKCoordinateRegionMake(location, MKCoordinateSpanMake(0.01, 0.01));
        else
            region = MKCoordinateRegionMake(location, MKCoordinateSpanMake(2, 2));
        // 0.5 is spanning value for region, make change if you feel to adjust bit more
        
        MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:region];
        [self.mapView setRegion:adjustedRegion animated:NO];
        [activityIndicator stopAnimating];
        userLocationIsSet = true;
    }
}

-(void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error{
    [activityIndicator stopAnimating];
}




- (IBAction)reset:(id)sender {
    switch (actionDisplayState) {
        case ActionDisplayNone:{
            
        }break;
        case ActionDisplayNewSOS:{
            [_nameTextField setText:@""];
            [_numberTextField setText:@""];
            [_emailTextField setText:@""];
            [selectedUsers removeAllObjects];
            [_tableview reloadData];
        }break;
        case ActionDisplayAddedSOSByMe:{
            
        }break;
        case ActionDisplayAddedSOSByOther:{
            
        }break;
        case ActionDisplayNewRegion:{
            [selectedCoordinates removeAllObjects];
            routeLine = nil;
            routeLines = nil;
            routeLineView = nil;
            NSArray *arr = _mapView.overlays;
            [_mapView removeOverlays:arr];
            arr = _mapView.annotations;
            [_mapView removeAnnotations:arr];
            _actionView.hidden = true;
            _mapView.hidden = false;
        }break;
        case ActionDisplayAddedRegionByMe:{
            
        }break;
        case ActionDisplayAddedRegionByOther:{
            
        }break;
        default:
            break;
    }
}

- (IBAction)save:(id)sender {
    
    
    switch (actionDisplayState) {
        case ActionDisplayNone:{
            
        }break;
        case ActionDisplayNewSOS:{
            if(_nameTextField.text == nil || [allTrim(_nameTextField.text) isEqualToString:BlankText]){
                UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle:nil message: @"Please enter a SOS name" delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
                [cantAddContactAlert show];
                return;
            }
            if(_numberTextField.text == nil || [allTrim(_numberTextField.text) isEqualToString:@""]){
                UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle:nil message: @"Please enter a SOS number" delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
                [cantAddContactAlert show];
                return;
            }
            if(_emailTextField.text == nil || [allTrim(_emailTextField.text) isEqualToString:@""]){
                UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle:nil message: @"Please enter a SOS number" delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
                [cantAddContactAlert show];
                return;
            }
            if([selectedUsers count] <= 0){
                UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Select relations" message: @"Please connect atleast 1 member with this SOS number" delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
                [cantAddContactAlert show];
                return;
            }
            if(!activityIndicator){
                activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
                activityIndicator.layer.cornerRadius = 05;
                activityIndicator.opaque = NO;
                activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
                activityIndicator.center = self.view.center;
                activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
                [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
                [self.view addSubview: activityIndicator];
            }
            self.view.userInteractionEnabled = false;
            [activityIndicator startAnimating];
            
            NSDictionary *newSOSData = [NSDictionary dictionaryWithObjectsAndKeys:
                                        _numberTextField.text?:@"", @"mobileNo",
                                        _nameTextField.text?:@"",@"sosName",
                                        _emailTextField.text?:@"", @"sosMail",
                                        [selectedUsers allKeys]?:@[], @"userSet",
                                        @"1", @"status", nil];
            [[RequestController sharedInstance] addSOSNumberWithDetails:newSOSData successHandler:^(NSDictionary *response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    NSLog(@"data set: %@", response);
                    [[HelperModule sharedInstance] showNetworkToast:YES message:@"SOS added successfully" animated:YES];
                    [self.navigationController popViewControllerAnimated:YES];
                });
            } failureHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    NSLog(@"%@",error);
                    [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
                });
            }];
        }break;
        case ActionDisplayAddedSOSByMe:{
            
        }break;
        case ActionDisplayAddedSOSByOther:{
            
        }break;
        case ActionDisplayNewRegion:{
            
            
            if([_saveButton.currentTitle isEqualToString:@"Next"]){
                if([selectedCoordinates count] <= 2){
                    UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Inadequate" message: @"Please select atleast 3 points to indicate a unsafe region." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
                    [cantAddContactAlert show];
                    return;
                }
                _mapView.hidden = true;
                _actionView.hidden = false;
                [_saveButton setTitle:@"Save" forState:UIControlStateNormal];
                _descriptionTextView.text=fistMarkerAddress;
                [selectedCoordinates addObject:[selectedCoordinates objectAtIndex:0]];
            } else {
                if([selectedUsers count] <= 0){
                    UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Select relations" message: @"Please connect atleast 1 member with this unsafe region" delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
                    [cantAddContactAlert show];
                    return;
                }
                if(!activityIndicator){
                    activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
                    activityIndicator.layer.cornerRadius = 05;
                    activityIndicator.opaque = NO;
                    activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
                    activityIndicator.center = self.view.center;
                    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
                    [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
                    [self.view addSubview: activityIndicator];
                }
                self.view.userInteractionEnabled = false;
                [activityIndicator startAnimating];
                NSMutableArray *locations = [NSMutableArray array];
                [locations setArray:selectedCoordinates];
                /*for (MKPointAnnotation *annotation in selectedCoordinates) {
                    [locations addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                         [NSNumber numberWithDouble:annotation.coordinate.latitude], @"lat",
                                         [NSNumber numberWithDouble:annotation.coordinate.longitude], @"longi", nil]];
                }*/
                NSDictionary *places = [NSDictionary dictionaryWithObjectsAndKeys:
                                        _nameTextField.text?:@"", @"placeName",
                                        locations, @"locations",
                                        _descriptionTextView.text?:@"", @"details", nil];
                NSDictionary *safetyRegionData = [NSDictionary dictionaryWithObjectsAndKeys:
                                            @[places], @"places",
                                            [selectedUsers allKeys]?:@[], @"childs", nil];
                [[RequestController sharedInstance] addSafetyRegionWithDetails:safetyRegionData successHandler:^(NSDictionary *response) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.view.userInteractionEnabled = true;
                        [activityIndicator stopAnimating];
                        NSLog(@"data set: %@", response);
                        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Safety region added successfully" animated:YES];
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                } failureHandler:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.view.userInteractionEnabled = true;
                        [activityIndicator stopAnimating];
                        NSLog(@"%@",error);
                        [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
                    });
                }];
            }
        }break;
        case ActionDisplayAddedRegionByMe:{
            
        }break;
        case ActionDisplayAddedRegionByOther:{
            
        }break;
        default:
            break;
    }
}

-(void)openAddressBook:(id)sender{
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
        //1
        NSLog(@"Denied");
        UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Cannot Load Contact" message: @"You must give the app permission to load the contact first." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [cantAddContactAlert show];
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        //2
        NSLog(@"Authorized");
        addressBookController = [[ABPeoplePickerNavigationController alloc] init];
        [addressBookController setPeoplePickerDelegate:self];
        [self presentViewController:addressBookController animated:YES completion:nil];
    } else{ //ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined
        //3
        NSLog(@"Not determined");
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            if (!granted){
                //4
                NSLog(@"Just denied");
                return;
            } else {
                addressBookController = [[ABPeoplePickerNavigationController alloc] init];
                [addressBookController setPeoplePickerDelegate:self];
                [self presentViewController:addressBookController animated:YES completion:nil];
            }
            //5
            NSLog(@"Just authorized");
        });
    }
}

-(void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person{
    [_nameTextField setText:@""];
    [_numberTextField setText:@""];
    [_emailTextField setText:@""];
    NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString *middleName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonMiddleNameProperty);
    NSString *lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    NSArray *phones = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(ABRecordCopyValue(person, kABPersonPhoneProperty));
    NSArray *emails = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(ABRecordCopyValue(person, kABPersonEmailProperty));
    NSMutableArray *nameSegments = [NSMutableArray array];
    if(firstName)
        [nameSegments addObject:firstName];
    if(middleName)
        [nameSegments addObject:middleName];
    if(lastName)
        [nameSegments addObject:lastName];
    [_nameTextField setText:[nameSegments componentsJoinedByString:@" "]];
    if(phones)
        [_numberTextField setText:[phones objectAtIndex:0]?:@""];
    if(emails)
        [_emailTextField setText:[emails objectAtIndex:0]?:@""];
}

-(void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
    
}

-(void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker{
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [relations count];
}

-(SelectRelationTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectRelationTableViewCell *cell = (SelectRelationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"selectRelationCellIdentifier"];
    if(!cell){
        cell = [[SelectRelationTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"selectRelationCellIdentifier"];
    }
    Relationship *relation = [relations objectAtIndex:indexPath.row];
    if([selectedUsers objectForKey:[relation getRelationshipId]]){
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleSelected"]];
    } else {
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleDeselected"]];
    }
    [cell.nameLabel setText:[relation getUserName]];
    [cell.detailsLabel setText:[relation getEmail]];
    cell.profileImageView.layer.cornerRadius = cell.profileImageView.bounds.size.width/2;
    cell.profileImageView.layer.borderColor = [UIColor clearColor].CGColor;
    cell.profileImageView.layer.borderWidth = 1.0f;
    [cell.profileImageView setImageWithURL:[NSURL URLWithString:[relation getRelationImageURL]] placeholderImage:[UIImage imageNamed:@"Avatar.png"]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectRelationTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Relationship *relation = [relations objectAtIndex:indexPath.row];
    if(!selectedUsers)
        selectedUsers = [NSMutableDictionary dictionary];
    if(![selectedUsers objectForKey:[relation getRelationshipId]]){
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleSelected"]];
        [selectedUsers setObject:relation forKey:[relation getRelationshipId]];
    } else {
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleDeselected"]];
        [selectedUsers removeObjectForKey:[relation getRelationshipId]];
    }
    
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"Select Family Member";
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(void)callAction{
    NSString *phoneNumber = [@"tel://" stringByAppendingString:_numberTextField.text];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    
}

-(void)externalShare{
    NSString *shareText = @"";
    NSArray *itemsToShare = @[shareText,[NSURL URLWithString:@"http://www.google.com"]];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[];
    activityVC.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Activity Status: %@ : %@", activityType, @"OK");
        });
        if (completed){
            NSLog(@"The Activity: %@ was completed", activityType);
        } else {
            NSLog(@"The Activity: %@ was NOT completed", activityType);
        }
        
    };
    [self presentViewController:activityVC animated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    }];
    
}

#pragma mark -
#pragma mark CLLocation Delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(nonnull CLLocation *)newLocation fromLocation:(nonnull CLLocation *)oldLocation{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude zoom:17.0];
    if(shouldMoveToUserLocation){
        [_gMapView animateToCameraPosition:camera];
        _gMapView.myLocationEnabled = true;
    }
}

-(void)internalShare{
    
}

@end
