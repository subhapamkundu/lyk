//
//  OrganizerActionViewController.h
//  LYK
//
//  Created by Subhapam on 02/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OrganizerActionDelegate <NSObject>

-(void)didSaveEvent;

@end

@interface OrganizerActionViewController : UIViewController

@property (nonatomic, weak) NSDictionary *selectedActivity;
@property (nonatomic, weak) id<OrganizerActionDelegate> delegate;

@end
