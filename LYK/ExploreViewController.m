//
//  ExploreViewController.m
//  LYK
//
//  Created by Subhapam on 12/12/15.
//  Copyright © 2015 Subhapam. All rights reserved.
//

#import "ExploreViewController.h"
#import "MPFlipViewController.h"
#import "ContentViewController.h"
#import "ExploreCategoryCollectionCell.h"
#import "ExploreProductListViewController.h"


#define MOVIE_MIN		-1

@interface ExploreFlipDataSet : NSObject

@property (nonatomic) NSMutableArray *contents;

-(BOOL)parseDataSetWithDetails:(NSDictionary *)json;

@end

@interface ExploreFlipDataSet ()
{
    
}
@end

@implementation ExploreFlipDataSet

-(BOOL)parseDataSetWithDetails:(NSDictionary *)json{
    for (NSDictionary *details in [json objectForKey:@"contents"]) {
        NSDictionary *parsedDetails = nil;
        parsedDetails = [NSDictionary dictionaryWithObjectsAndKeys:
                         [details objectForKey:@"productId"]?:@"", @"id",
                         [details objectForKey:@"productName"]?:@"", @"productName",
                         [details objectForKey:@"itemDesc"]?:@"", @"statement",
                         [details objectForKey:@"productImageUrl"]?:@"", @"imageURL",
                         [details objectForKey:@"itemDesc"]?:@"", @"desc",
                         [details objectForKey:@"productItemUrl"]?:@"", @"linkURL", nil];
        if(!_contents)
            _contents = [NSMutableArray array];
        [_contents insertObject:parsedDetails atIndex:[_contents count]];
    }
    return YES;
}

@end


@interface ExploreViewController ()<UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    UIActivityIndicatorView *activityIndicator;
    NSMutableDictionary *socialUserDetails;
    UIRefreshControl *refreshControl;
    NSMutableArray *arrayOriginal;
    NSMutableArray *arForTable;
    NSArray *subCategoryArray;
}

@property (weak, nonatomic) IBOutlet UITableView *exploreTableView;
@property (weak, nonatomic) IBOutlet UICollectionView *exploreCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *categoryHeader;



@end

@implementation ExploreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.hidesBackButton = YES;
    //    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"Back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(goBack)];
    
    [self getCategories];    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if([segue.identifier isEqualToString:ExploreToProductListSegue]){
         // pass details to view controller.
         ExploreProductListViewController *vc = (ExploreProductListViewController *)segue.destinationViewController;
         vc.info = sender;
     }
 }

-(void)goBack{
    //[self disableTextFieldValidation];
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}


-(void)goToTabBarController{
    
}



-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arForTable.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ExploreCategoryCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"xploreCollectionViewCell" forIndexPath:indexPath];
    [cell.categoryName setText:[[arForTable objectAtIndex:indexPath.item] objectForKey:@"categoryName"]];
    if([[[arForTable objectAtIndex:indexPath.item] objectForKey:@"categoryName"] isEqualToString:@"doctor"])
        [cell.categoryImageView setImage:[UIImage imageNamed:@"Doctor"]];
    else if ([[[arForTable objectAtIndex:indexPath.item] objectForKey:@"categoryName"] isEqualToString:@"tutorial"])
        [cell.categoryImageView setImage:[UIImage imageNamed:@"School"]];
    else
        [cell.categoryImageView setImage:[UIImage imageNamed:@"Shopping"]];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Collection view selected item: %@",indexPath);
    subCategoryArray = [[arForTable objectAtIndex:indexPath.item] objectForKey:@"subCategories"];
    NSLog(@"%@",subCategoryArray);
    [_categoryHeader setText:[[arForTable objectAtIndex:indexPath.item] objectForKey:@"categoryName"]];
    [_exploreTableView reloadData];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((collectionView.frame.size.width-8)/3, 110.0f);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 4.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 4.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}





// Customize the number of sections in the table view.
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [subCategoryArray count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text=[[subCategoryArray objectAtIndex:indexPath.row] valueForKey:@"categoryName"];
//    [cell setIndentationLevel:[[[arForTable objectAtIndex:indexPath.row] valueForKey:@"level"] intValue]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *d=[subCategoryArray objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:ExploreToProductListSegue sender:d];
//    if([d valueForKey:@"subCategories"]) {
//        NSArray *ar=[d valueForKey:@"subCategories"];
//        
//        BOOL isAlreadyInserted=NO;
//        
//        for(NSDictionary *dInner in ar ){
//            NSInteger index=[arForTable indexOfObjectIdenticalTo:dInner];
//            isAlreadyInserted=(index>0 && index!=NSIntegerMax);
//            if(isAlreadyInserted) break;
//        }
//        
//        if(isAlreadyInserted) {
//            [self miniMizeThisRows:ar];
//        } else {
//            NSUInteger count=indexPath.row+1;
//            NSMutableArray *arCells=[NSMutableArray array];
//            for(NSDictionary *dInner in ar ) {
//                [arCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
//                [arForTable insertObject:dInner atIndex:count++];
//            }
//            [tableView insertRowsAtIndexPaths:arCells withRowAnimation:UITableViewRowAnimationLeft];
//        }
//    } else {
//        [self performSegueWithIdentifier:ExploreToProductListSegue sender:d];
//    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1)];
}

-(void)miniMizeThisRows:(NSArray*)ar{
    
    for(NSDictionary *dInner in ar ) {
        NSUInteger indexToRemove=[arForTable indexOfObjectIdenticalTo:dInner];
        NSArray *arInner=[dInner valueForKey:@"subCategories"];
        if(arInner && [arInner count]>0){
            [self miniMizeThisRows:arInner];
        }
        
        if([arForTable indexOfObjectIdenticalTo:dInner]!=NSNotFound) {
            [arForTable removeObjectIdenticalTo:dInner];
            [_exploreTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:
                                                    [NSIndexPath indexPathForRow:indexToRemove inSection:0]
                                                    ]
                                  withRowAnimation:UITableViewRowAnimationRight];
        }
    }
}



-(void)getCategories{
    if(!activityIndicator){
        activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
        activityIndicator.layer.cornerRadius = 05;
        activityIndicator.opaque = NO;
        activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        activityIndicator.center = self.view.center;
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview: activityIndicator];
    }
    self.view.userInteractionEnabled = false;
    [activityIndicator startAnimating];
    [[RequestController sharedInstance] getExploreCategoriesWithSuccessHandler:^(NSDictionary *response) {
         dispatch_async(dispatch_get_main_queue(), ^{
             self.view.userInteractionEnabled = true;
             [activityIndicator stopAnimating];
             NSLog(@"data set: %@", response);
             if([response objectForKey:@"token"]){
                 [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:@"token"]];
             } else {
                 //[[HelperModule sharedInstance] showNetworkToast:YES message:@"Token not validated" animated:YES];
             }
             arrayOriginal = [NSMutableArray array];
             for (NSDictionary *category in [response objectForKey:Key_Response]) {
                 NSMutableDictionary *editedCat = [NSMutableDictionary dictionaryWithDictionary:category];
                 [editedCat setObject:@"1" forKey:@"level"];
                 if([editedCat objectForKey:@"subCategories"]){
                     NSMutableArray *subCategories = [NSMutableArray array];
                     for (NSDictionary *category in [editedCat objectForKey:@"subCategories"]) {
                         NSMutableDictionary *editedSubCat = [NSMutableDictionary dictionaryWithDictionary:category];
                         [editedSubCat setObject:[editedSubCat objectForKey:@"subCategoryName"] forKey:@"categoryName"];
                         [editedSubCat removeObjectForKey:@"subCategoryName"];
                         [editedSubCat setObject:[editedCat objectForKey:@"categoryId"] forKey:@"categoryId"];
                         [editedSubCat setObject:@"2" forKey:@"level"];
                         [subCategories addObject:editedSubCat];
                     }
                     [editedCat setObject:subCategories forKey:@"subCategories"];
                 } else {
                     [editedCat setObject:[NSNumber numberWithBool:YES] forKey:@"shouldFetchProduct"];
                 }
                 [arrayOriginal addObject:editedCat];
//                 [self getSubCategoriesForItemWithId:[response objectForKey:@"categoryId"]];
             }
//             [arrayOriginal addObject:@{@"categoryName":@"Sample 1",@"level":@"1"}];
//             [arrayOriginal addObject:@{@"categoryName":@"Sample 2",@"level":@"1"}];
             if (!arForTable) {
                 arForTable = [NSMutableArray array];
             }
             [arForTable removeAllObjects];
             [arForTable addObjectsFromArray:arrayOriginal];
             [_exploreTableView reloadData];
             [_exploreCollectionView reloadData];
         });
     } failureHandler:^(NSError *error) {
         dispatch_async(dispatch_get_main_queue(), ^{
             self.view.userInteractionEnabled = true;
             [activityIndicator stopAnimating];
             NSLog(@"%@",error);
         });
     }];
}

//-(void)getSubCategoriesForItemWithId:(NSString *)categoryId{
//    if(!activityIndicator){
//        activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
//        activityIndicator.layer.cornerRadius = 05;
//        activityIndicator.opaque = NO;
//        activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
//        activityIndicator.center = self.view.center;
//        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
//        [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
//        [self.view addSubview: activityIndicator];
//    }
//    self.view.userInteractionEnabled = false;
//    [activityIndicator startAnimating];
//    [[RequestController sharedInstance] getExploreCategoriesWithSuccessHandler:^(NSDictionary *response) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            self.view.userInteractionEnabled = true;
//            [activityIndicator stopAnimating];
//            NSLog(@"data set: %@", response);
//            if([response objectForKey:@"token"]){
//                [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:@"token"]];
//            } else {
//                //[[HelperModule sharedInstance] showNetworkToast:YES message:@"Token not validated" animated:YES];
//            }
//            for (NSDictionary *category in [response objectForKey:Key_Response]) {
//                NSMutableDictionary *editedCat = [NSMutableDictionary dictionaryWithDictionary:category];
//                [editedCat setObject:@"1" forKey:@"level"];
//                [arrayOriginal addObject:editedCat];
//            }
//            if (!arForTable) {
//                arForTable = [NSMutableArray array];
//            }
//            [arForTable removeAllObjects];
//            [arForTable addObjectsFromArray:arrayOriginal];
//            [_exploreTableView reloadData];
//        });
//    } failureHandler:^(NSError *error) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            self.view.userInteractionEnabled = true;
//            [activityIndicator stopAnimating];
//            NSLog(@"%@",error);
//        });
//    }];
//    
//}

@end
