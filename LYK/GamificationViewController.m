//
//  GamificationViewController.m
//  LYK
//
//  Created by Subhapam on 13/12/15.
//  Copyright © 2015 Subhapam. All rights reserved.
//

#import "GamificationViewController.h"

#define NumberOfSlides      5

@interface GamificationViewController ()
{
    NSInteger count;
}

@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
//@property (weak, nonatomic) IBOutlet UIButton *prevBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

- (IBAction)nextAction:(id)sender;
//- (IBAction)prevAction:(id)sender;

@end

@implementation GamificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    count = 1;
    [_imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Screen%ld",(long)count]]];
    // Do any additional setup after loading the view from its nib.
    _pageControl.numberOfPages = NumberOfSlides;
    UISwipeGestureRecognizer *prevGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(prevAction:)];
    [prevGestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [_imageView addGestureRecognizer:prevGestureRecognizer];
    UISwipeGestureRecognizer *nextGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(nextAction:)];
    [nextGestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [_imageView addGestureRecognizer:nextGestureRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)nextAction:(id)sender {
    count++;
    _pageControl.currentPage = count-1;
    [_imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Screen%ld",(long)count]]];
    if(count == NumberOfSlides){
    } else if(count < NumberOfSlides){
    } else {
        if([_delegate respondsToSelector:@selector(shouldDismissGamificationController)]){
            [_delegate performSelector:@selector(shouldDismissGamificationController)];
        }
        [self.view removeFromSuperview];
    }
    
}

- (IBAction)prevAction:(id)sender {
    if(count == 1){
        return;
    }
    count--;
    _pageControl.currentPage = count-1;
    if(count < 1)
        count = 1;
    [_imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"Screen%ld",(long)count]]];
    
    if(count < NumberOfSlides){
        [_nextBtn setTitle:@"Next" forState:UIControlStateNormal];
    }
    
}

@end
