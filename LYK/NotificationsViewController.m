//
//  NotificationsViewController.m
//  LYK
//
//  Created by Subhapam on 12/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import "NotificationsViewController.h"
#import "NotificationActionViewController.h"

@interface NotificationsViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, NotificationActionDelegate>
{
    NSMutableArray *notificationsArray;
    UIRefreshControl *refreshControl;
    UIActivityIndicatorView *activityIndicator;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation NotificationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self getNotificationsForUser];
    self.navigationItem.title = @"Notifications";
    refreshControl = [[UIRefreshControl alloc]init];
    [_tableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshList) forControlEvents:UIControlEventValueChanged];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavHeader"]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:NotificationToActionDetailSegue]){
        ((NotificationActionViewController *)segue.destinationViewController).details = sender;
        ((NotificationActionViewController *)segue.destinationViewController).shouldFetchDetailsWithId = nil;
        ((NotificationActionViewController *)segue.destinationViewController).delegate = self;
    }
}


-(void)fetchNotificationWithId:(NSString *)notificationId{
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *path = [documentsDirectory stringByAppendingPathComponent:NotificationPlistFileName];
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    if (![fileManager fileExistsAtPath: path]) {
//        path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:NotificationPlistFileName]];
//    }
//    
//    if ([fileManager fileExistsAtPath: path]) {
//        notifications = [[NSMutableArray alloc] initWithContentsOfFile: path];
//        [_tableView reloadData];
//    }
//    [refreshControl endRefreshing];
    
    
    
    
}
- (nullable NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *eachNotification = [notificationsArray objectAtIndex:indexPath.row];
    
    NSDictionary *rejectSystemAttributes = @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:20],
                                        NSForegroundColorAttributeName:[UIColor whiteColor]};
    NSDictionary *acceptSystemAttributes = @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:20],
                                              NSForegroundColorAttributeName:[UIColor whiteColor]};
    
    UITableViewRowAction *acceptAction=[UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"          " handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        
        NSMutableDictionary *rejectDetails = [NSMutableDictionary dictionaryWithDictionary:eachNotification];
        [rejectDetails setObject:[NSNumber numberWithInteger:0] forKey:@"accept"];
        [self sendAcceptanceWithDetails:rejectDetails];
        
    }];
    
    UIImage *acceptPatternImage = [self imageForTableViewRowActionWithTitle:@"Accept" textAttributes:acceptSystemAttributes backgroundColor:[UIColor grayColor] cellHeight:50];
    acceptAction.backgroundColor=[UIColor colorWithPatternImage:acceptPatternImage];
    
    UITableViewRowAction *rejectAction=[UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"          " handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        
        NSMutableDictionary *rejectDetails = [NSMutableDictionary dictionaryWithDictionary:eachNotification];
        [rejectDetails setObject:[NSNumber numberWithInteger:1] forKey:@"accept"];
        [self sendAcceptanceWithDetails:rejectDetails];
        
    }];
    
    UIImage *rejectPatternImage = [self imageForTableViewRowActionWithTitle:@"Reject" textAttributes:rejectSystemAttributes backgroundColor:[UIColor redColor] cellHeight:50];
    rejectAction.backgroundColor=[UIColor colorWithPatternImage:rejectPatternImage];
    
    return @[rejectAction,acceptAction];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([notificationsArray count] == 0){
        UILabel *emptyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        emptyLabel.text = @"There is no notification for you now. Kindly revisit after some time.";
        emptyLabel.numberOfLines = 0;
        emptyLabel.textAlignment = NSTextAlignmentCenter;
        self.tableView.backgroundView = emptyLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 0;
    } else {
        self.tableView.backgroundView = nil;
    }
    return [notificationsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSDictionary *eachNotification = [notificationsArray objectAtIndex:indexPath.row];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [cell.textLabel sizeToFit];
    cell.textLabel.text = [eachNotification valueForKey:@"notificationTitle"]?:@"LYK Notification";
    CGSize sizeForTitle = [cell.textLabel.text getHeightHavingWidth:tableView.frame.size.width andFont:[UIFont systemFontOfSize:12.0f]];
    CGRect cellFrame = cell.textLabel.frame;
    cell.textLabel.frame = CGRectMake(cellFrame.origin.x, cellFrame.origin.y, sizeForTitle.width, sizeForTitle.height + 10);
    if(![[eachNotification objectForKey:@"isRead"] boolValue]){
        cell.textLabel.textColor = [UIColor blueColor];
    } else {
        cell.textLabel.textColor = [UIColor grayColor];
    }
    cell.imageView.image = [UIImage imageNamed:@"Bullet"];
    if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"relationRequest"]){
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"relationRemove"]){
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"shareSOS"]){
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"shareUnsafeLocation"]){
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"shareOrganizer"]){
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"shareExplore"]){
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"toDoDaily"]){
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"eventTaskDaily"]){
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"featureUsageDaily"]){
        
    } else {
        
    }
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *eachNotification = [notificationsArray objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"relationRequest"]){
        [self performSegueWithIdentifier:NotificationToActionDetailSegue sender:eachNotification];
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"relationRemove"]){
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"shareSOS"]){
//        [self performSegueWithIdentifier:NotificationToActionDetailSegue sender:eachNotification];
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"shareUnsafeLocation"]){
//        [self performSegueWithIdentifier:NotificationToActionDetailSegue sender:eachNotification];
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"shareOrganizer"]){
        [self performSegueWithIdentifier:NotificationToActionDetailSegue sender:eachNotification];
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"shareExplore"]){
//        [self performSegueWithIdentifier:NotificationToActionDetailSegue sender:eachNotification];
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"toDoDaily"]){
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"eventTaskDaily"]){
        
    } else if([[eachNotification valueForKey:@"notificationType"] isEqualToString:@"featureUsageDaily"]){
        
    } else {
        
    }
//    if ([[notificationDetails objectForKey:@"notificationType"] isEqualToString:@"shareSOS"]) {
//        if(![[notificationDetails objectForKey:@"isRead"] boolValue]){
//            NSString *alertMessage = [NSString stringWithFormat:@""];
//        }
//    } else if ([[notificationDetails objectForKey:@"notificationType"] isEqualToString:@"relationRemove"]) {
//        if(![[notificationDetails objectForKey:@"isRead"] boolValue]){
//            NSString *alertMessage = [NSString stringWithFormat:@""];
//        }
//    } else if ([[notificationDetails objectForKey:@"notificationType"] isEqualToString:@"relationRequest"]) {
//        if(![[notificationDetails objectForKey:@"isRead"] boolValue]){
//            NSString *alertMessage = [NSString stringWithFormat:@""];
//        }
//    } else {
//        
//    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(notificationsArray.count){
        NSDictionary *eachNotification = [notificationsArray objectAtIndex:indexPath.row];
        NSString *title = [eachNotification valueForKey:@"notificationTitle"]?:@"LYK Notification";
        CGSize sizeForTitle = [title getHeightHavingWidth:tableView.frame.size.width andFont:[UIFont systemFontOfSize:18.0f]];
        return  (sizeForTitle.height > 44) ? (sizeForTitle.height + 20):44;
    } else {
        return 44;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //NSInteger result = maximumOffset - currentOffset;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 10.0) {
        NSLog(@"Did reach end");
        [activityIndicator startAnimating];
        [self getNotificationsForUser];
        //[self methodThatAddsDataAndReloadsTableView];
    }
}

-(void)refreshList{
    if(notificationsArray)
       [notificationsArray removeAllObjects];
    else
        notificationsArray = [NSMutableArray array];
    [self getNotificationsForUser];
    
}

-(void)getNotificationsForUser{
    if(!activityIndicator){
        activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
        activityIndicator.layer.cornerRadius = 05;
        activityIndicator.opaque = NO;
        activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        activityIndicator.center = self.view.center;
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview: activityIndicator];
    }
    self.view.userInteractionEnabled = false;
    [activityIndicator startAnimating];
    [[RequestController sharedInstance] getNotificationsWithMaxCredentials:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                            [NSNumber numberWithInteger:notificationsArray.count], @"offset",
                                                                            [NSNumber numberWithInteger:20], @"limit", nil]
                                                            successHandler:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            NSLog(@"data set: %@", response);
            NSInteger offset = [[response objectForKey:@"offset"] integerValue];
            if(notificationsArray){
                [notificationsArray removeObjectsInRange:NSMakeRange(offset, notificationsArray.count - offset)];
            } else {
                notificationsArray = [NSMutableArray array];
            }
            notificationsArray = [[[response objectForKey:@"response"] objectForKey:@"notifications"] mutableCopy];
            [[UserData sharedInstance] setNotificationCount:[[response objectForKey:@"unreadNotificationCount"] integerValue]];
            [refreshControl endRefreshing];
            [_tableView reloadData];
            
            
            
        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            NSLog(@"%@",error);
            [refreshControl endRefreshing];
            [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
        });
    }];
}

-(void)sendAcceptanceWithDetails:(NSDictionary *)details{
    if(!activityIndicator){
        activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
        activityIndicator.layer.cornerRadius = 05;
        activityIndicator.opaque = NO;
        activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        activityIndicator.center = self.view.center;
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview: activityIndicator];
    }
    self.view.userInteractionEnabled = false;
    [activityIndicator startAnimating];
    [[RequestController sharedInstance] sendAcceptanceWithDetails:details successHandler:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            [self didPerformActionForNotificationId:[details objectForKey:@"notificationId"]];
//            NSLog(@"data set: %@", response);
//            if([_delegate respondsToSelector:@selector(didPerformActionForNotificationId:)]){
//                [_delegate performSelector:@selector(didPerformActionForNotificationId:) withObject:[_details objectForKey:@"notificationId"]];
//            }
//            [self.navigationController popViewControllerAnimated:YES];
        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            NSLog(@"%@",error);
            [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
        });
    }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex != alertView.cancelButtonIndex) {
        
    }
}

-(void)didPerformActionForNotificationId:(NSString *)notificationId{
    for (int index = 0; index < notificationsArray.count; index++) {
        NSDictionary *eachNotification = [notificationsArray objectAtIndex:index];
        if([[eachNotification objectForKey:@"notificationId"] isEqualToString:notificationId]){
            [notificationsArray removeObject:eachNotification];
        }
    }
    [_tableView reloadData];
}

- (UIImage *)imageForTableViewRowActionWithTitle:(NSString *)title textAttributes:(NSDictionary *)attributes backgroundColor:(UIColor *)color cellHeight:(CGFloat)cellHeight
{
    NSString *titleString = title;
    NSDictionary *originalAttributes = @{NSFontAttributeName: [UIFont systemFontOfSize:18]};
    CGSize originalSize = [titleString sizeWithAttributes:originalAttributes];
    //CGSize newSize = CGSizeMake(originalSize.width + kSystemTextPadding + kSystemTextPadding, originalSize.height);
    CGSize newSize = CGSizeMake(originalSize.width + 10 + 10, originalSize.height);
    CGRect drawingRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, cellHeight));
    UIGraphicsBeginImageContextWithOptions(drawingRect.size, YES, [UIScreen mainScreen].nativeScale);
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(contextRef, color.CGColor);
    CGContextFillRect(contextRef, drawingRect);
    
    UILabel *label = [[UILabel alloc] initWithFrame:drawingRect];
    label.textAlignment = NSTextAlignmentCenter;
    label.attributedText = [[NSAttributedString alloc] initWithString:title attributes:attributes];
    [label drawTextInRect:drawingRect];
    
    //This is other way how to render string
    //    CGSize size = [titleString sizeWithAttributes:attributes];
    //    CGFloat x =  (drawingRect.size.width - size.width)/2;
    //    CGFloat y =  (drawingRect.size.height - size.height)/2;
    //    drawingRect.origin = CGPointMake(x, y);
    //    [titleString drawInRect:drawingRect withAttributes:attributes];
    
    UIImage *returningImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return returningImage;
}

/*- (NSString *)whitespaceReplacementWithSystemAttributes:(NSDictionary *)systemAttributes newAttributes:(NSDictionary *)newAttributes fromString:(NSString *)str
{
    NSString *stringTitle = str;
    NSMutableString *stringTitleWS = [[NSMutableString alloc] initWithString:@""];
    
    CGFloat diff = 0;
    CGSize  stringTitleSize = [stringTitle sizeWithAttributes:newAttributes];
    CGSize stringTitleWSSize;
    NSDictionary *originalAttributes = systemAttributes;
    do {
        [stringTitleWS appendString:@" "];
        stringTitleWSSize = [stringTitleWS sizeWithAttributes:originalAttributes];
        diff = (stringTitleSize.width - stringTitleWSSize.width);
        if (diff <= 1.5) {
            break;
        }
    }
    while (diff > 0);
    
    return [stringTitleWS copy];
}*/




@end
