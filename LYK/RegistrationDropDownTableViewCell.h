//
//  RegistrationDropDownTableViewCell.h
//  LYK
//
//  Created by Subhapam on 19/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DropDownActionDelegate <NSObject>

-(void)toggleDropDownAction:(id)sender;

@end

@interface RegistrationDropDownTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *captionLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectRoleBtn;
@property (weak, nonatomic) id<DropDownActionDelegate> dropDownDelegate;

@end
