//
//  LoginViewController.m
//  LYK
//
//  Created by Subhapam on 16/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "LoginViewController.h"
#import "KeychainController.h"
#import "ResetPasswordViewController.h"
#import "RegisterViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface LoginViewController ()<UIAlertViewDelegate, GPPSignInDelegate, AVAudioPlayerDelegate>
{
    UIActivityIndicatorView *activityIndicator;
    NSMutableDictionary *socialUserDetails;
    RegisterViewController *regController;
}

@property (weak, nonatomic) IBOutlet LYKTextField *emailTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;
@property (weak, nonatomic) IBOutlet UIButton *signInBtn;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *blurView;

@property (weak, nonatomic) IBOutlet UILabel *versionLabel;


- (IBAction)doDBLogin:(id)sender;
- (IBAction)doFBLogin:(id)sender;
- (IBAction)doGoogleLogin:(id)sender;
- (IBAction)doRegistration:(id)sender;
- (IBAction)forgotPasswordAction:(id)sender;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    _signInBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _signInBtn.layer.cornerRadius = 5.0f;
    _signInBtn.layer.borderWidth = 1.0f;
    
    _signUpBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _signUpBtn.layer.cornerRadius = 5.0f;
    _signUpBtn.layer.borderWidth = 1.0f;
    
    [_versionLabel setText:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    _emailTextField.text = [[[KeychainController alloc] init] getUsernameFromKeychain];
    _passwordTextField.text = [[[KeychainController alloc] init] getPasswordFromKeychain];
    
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    // You previously set kClientId in the "Initialize the Google+ client" step
    signIn.clientID = LYKGPPClientId;
    // Uncomment one of these two statements for the scope you chose in the previous step
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    signIn.scopes = @[ @"profile" ];            // "profile" scope
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = self;
    
    _emailTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"EmailIcon"]];
    _emailTextField.leftViewMode = UITextFieldViewModeAlways;
    _passwordTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"EmailIcon"]];
    _passwordTextField.leftViewMode = UITextFieldViewModeAlways;
    
    _blurView.hidden=true;
    UITapGestureRecognizer *gesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissRegistrationController)];
    gesture.numberOfTapsRequired=1;
    [_blurView addGestureRecognizer:gesture];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self getApplicationDataFromServer];
    if(socialUserDetails.count <= 0){
        socialUserDetails = nil;
    }
//    [self playSound:0];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:ResetPasswordSegue]){
        ((ResetPasswordViewController *)segue.destinationViewController).tempPassword = _passwordTextField.text;
        _passwordTextField.text = @"";
    } else if ([segue.identifier isEqualToString:LoginToRegisterSegue]){
        ((RegisterViewController *)segue.destinationViewController).socialDetails = socialUserDetails;
    }
}


- (IBAction)doDBLogin:(id)sender {
    [self.view endEditing:YES];
    if(![self validateLoginCredentials]){
        return;
    }
    
    if([[RequestController sharedInstance] isNetworkReachable]){
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }
        
        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        
        NSString *username = _emailTextField.text;
        NSString *secureField = _passwordTextField.text;
        
        [[RequestController sharedInstance] sendLoginRequestWithUsername:username password:secureField successHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[[KeychainController alloc] init] saveCredentialsInKeychainWithUsername:username password:secureField];
                NSLog(@"Login Response :: %@",response);
                
                if ([response objectForKey:Key_Response]) {
                    if([response objectForKey:Key_Token]){
                        [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:Key_Token]];
                    } else {
                        [[HelperModule sharedInstance] showNetworkToast:YES message:TokenInvalidErrorMessage animated:YES];
                    }
                    if([response objectForKey:Key_Response] && [[[response objectForKey:Key_Response] objectForKey:@"status"] isEqualToString:@"temporary password validated"]){
                        self.view.userInteractionEnabled = true;
                        [activityIndicator stopAnimating];
                        [[UserData sharedInstance] setEmailId:_emailTextField.text];
                        [self performSegueWithIdentifier:ResetPasswordSegue sender:self];
                        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Temporary Password Validated. Please Set New Password." animated:YES];
                    } else {
                        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Login successfull" animated:YES];
                        BOOL isResponseValid = [[UserData sharedInstance] parseUserDataWithJSON:response];
                        if(isResponseValid){
                            [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:Key_Token]];
                            [self getRelations];
                        }
                        else{
                            self.view.userInteractionEnabled = true;
                            [activityIndicator stopAnimating];
                            [[HelperModule sharedInstance] showNetworkToast:YES message:[response objectForKey:@"errorInfo"] animated:YES];
                        }
                    }
                }
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                //[[[PJKeychainController alloc] init] clearKeyChain];
                [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
            });
        }];
    }
}

- (IBAction)doFBLogin:(id)sender {
    if([[RequestController sharedInstance] isNetworkReachable]){
        [self.view setUserInteractionEnabled:NO];
        [activityIndicator startAnimating];
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithReadPermissions:@[@"email", @"public_profile"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error) {
                [[HelperModule sharedInstance] showNetworkToast:YES message:@"Incomplete data from facebook" animated:YES];
                [self.view setUserInteractionEnabled:YES];
                [activityIndicator stopAnimating];
            } else if (result.isCancelled) {
                // Handle cancellations
                [[HelperModule sharedInstance] showNetworkToast:YES message:@"Facebook Login Cancelled" animated:YES];
                [self.view setUserInteractionEnabled:YES];
                [activityIndicator stopAnimating];
            } else {
                // If you ask for multiple permissions at once, you
                // should check if specific permissions missing
                if ([result.grantedPermissions containsObject:@"email"]) {
                    // Do work
                    if ([FBSDKAccessToken currentAccessToken]) {
                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email, first_name, last_name"}]
                         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                             if (!error) {
                                 NSLog(@"fetched user:%@", result);
                                 socialUserDetails = [NSMutableDictionary dictionaryWithDictionary:result];
                                 [socialUserDetails setObject:@"fb" forKey:@"socialType"];
                                 //[self doFBLoginWithCredentials:[NSDictionary dictionaryWithDictionary:result]];
                                 [[RequestController sharedInstance] sendSocialLoginRequestWithCredential:[NSDictionary dictionaryWithObjectsAndKeys:[result objectForKey:@"id"], @"identity", @"facebook", @"socialMedia",nil] successHandler:^(NSDictionary *response) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         if([response objectForKey:@"token"]){
                                             [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:@"token"]];
                                         } else {
                                             [[HelperModule sharedInstance] showNetworkToast:YES message:@"Token not validated" animated:YES];
                                         }
                                         BOOL isResponseValid = [[UserData sharedInstance] parseUserDataWithJSON:response];
                                         if(isResponseValid){
                                             [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:Key_Token]];
                                             [self getRelations];
                                         }
                                     });
                                     //        [self performSegueWithIdentifier:LoginToHomeSegue sender:self];
                                 } failureHandler:^(NSError *error) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         [activityIndicator stopAnimating];
                                         socialUserDetails = [NSMutableDictionary dictionaryWithDictionary:result];
                                         [socialUserDetails setObject:@"fb" forKey:@"socialType"];
                                         [self performSegueWithIdentifier:LoginToRegisterSegue sender:self];
                                     });
                                 }];
                             }
                         }];
                    }
                    
                }
            }
        }];
    }
    return;
}

- (IBAction)doGoogleLogin:(id)sender {
    [[HelperModule sharedInstance] showNetworkToast:YES message:@"Google plus login will be available soon." animated:YES];
    return;
    [self.view endEditing:YES];
    [activityIndicator startAnimating];
    [[GPPSignIn sharedInstance] authenticate];
    return;
}

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth error: (NSError *) error {
    
    NSLog(@"Received error %@ and auth object %@",error, auth);
    
    if (error) {
        [[HelperModule sharedInstance] showNetworkToast:YES message:[NSString stringWithFormat:@"Status: Authentication error: %@", error] animated:YES];
        return;
    }
    // getting the access token from auth
    NSString  *accessTocken = [auth valueForKey:@"accessToken"]; // access tocken pass in .pch file
    NSLog(@"%@",accessTocken);
    NSString *str=[NSString stringWithFormat:@"https://www.googleapis.com/oauth2/v1/userinfo?access_token=%@",accessTocken];
    NSString *escapedUrl = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",escapedUrl]];
    NSString *jsonData = [[NSString alloc] initWithContentsOfURL:url usedEncoding:nil error:nil];
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:[jsonData dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    // NSMutableDictionary *proDic = [[NSMutableDictionary alloc] init];
    NSString *userId=[jsonDictionary objectForKey:@"id"];
    // proDic=[jsonData JSONValue];
    // NSLog(@"user google user id  %@",signIn.userEmail); //logged in user's email id
    //    socialRegistrationSegue
    [[RequestController sharedInstance] sendSocialLoginRequestWithCredential:[NSDictionary dictionaryWithObjectsAndKeys:userId, @"identity", @"googleplus", @"socialMedia",nil] successHandler:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if([response objectForKey:@"token"]){
                [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:@"token"]];
            } else {
                [[HelperModule sharedInstance] showNetworkToast:YES message:@"Token not validated" animated:YES];
            }
            BOOL isResponseValid = [[UserData sharedInstance] parseUserDataWithJSON:response];
            if(isResponseValid){
                [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:Key_Token]];
                [self getRelations];
            }
        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            socialUserDetails = [NSMutableDictionary dictionaryWithDictionary:jsonDictionary];
            [socialUserDetails setObject:@"g" forKey:@"socialType"];
            [self performSegueWithIdentifier:LoginToRegisterSegue sender:self];
        });
    }];

    [[GPPSignIn sharedInstance] signOut];
}

- (IBAction)doRegistration:(id)sender {
   // [self.view endEditing:YES];
    //[self performSegueWithIdentifier:LoginToRegisterSegue sender:self];
    
    _blurView.alpha=0.0f;
    _blurView.hidden=false;
    
    float horizonSpace= (CGRectGetWidth(self.view.bounds) - 300.0f)/2;
    float verticalSpace= (CGRectGetHeight(self.view.bounds) - 350.0f)/2;
    if (!regController) {
        regController=[self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
        [self addChildViewController:regController];
        regController.view.alpha = 0.0f;
        regController.view.frame=CGRectMake( horizonSpace, verticalSpace, 300.0f, 350.0f);
        regController.view.layer.cornerRadius = 10.0f;
//        regController.view.clipsToBounds = true;
        regController.view.layer.masksToBounds = false;
        regController.view.layer.shadowOffset = CGSizeMake(-10, 15);
        regController.view.layer.shadowRadius = 5;
        regController.view.layer.shadowOpacity = 0.5;
        [self addChildViewController:regController];
        [_blurView addSubview:regController.view];
        [regController didMoveToParentViewController:self];
        
    }
    
    
    [UIView animateWithDuration:0.5 animations:^{
        
        _blurView.alpha=1.0f;
        regController.view.alpha = 1.0f;
       
    } completion:^(BOOL finished) {
    
//        if (!regController) {
//            regController=[self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
//        }
        
    }];
    
    
    
    
    
}

-(void)dismissRegistrationController{
    
    
    [self removeRegistrationController];
}

-(void)removeRegistrationController{
    
    [UIView animateWithDuration:0.2 animations:^{
        
        _blurView.alpha=0.0f;
        
    } completion:^(BOOL finished) {
        
        [regController willMoveToParentViewController:nil];
        [regController.view removeFromSuperview];
        [regController removeFromParentViewController];
        
        regController=nil;
        _blurView.hidden=true;
    }];
    
    
}



- (IBAction)forgotPasswordAction:(id)sender {
    [self.view endEditing:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Retrieve Password"
                                                    message:@"Enter your registered email id"
                                                   delegate:self
                                          cancelButtonTitle:@"Not Now"
                                          otherButtonTitles:@"Retrieve",nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alert.tag = 0;
    [alert show];
}


-(void)didPresentAlertView:(UIAlertView *)alertView{
    
    if(alertView.tag == 0){
        [alertView textFieldAtIndex:0].keyboardType = UIKeyboardTypeEmailAddress;
//        [alertView textFieldAtIndex:0].placeholder = @"New Password";
//        [alertView textFieldAtIndex:1].placeholder = @"Confirm Password";
//        [alertView textFieldAtIndex:0].secureTextEntry = YES;
//        [alertView textFieldAtIndex:1].secureTextEntry = YES;
    }
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 0){
        NSLog(@"%@", [alertView textFieldAtIndex:0].text);
        if(buttonIndex != alertView.cancelButtonIndex){
            if (![[alertView textFieldAtIndex:0] hasText])
                [[HelperModule sharedInstance] showNetworkToast:YES message:EnterEmailNetworkToast animated:YES];
            else if(![Validator validateEmailId:[alertView textFieldAtIndex:0].text])
                [[HelperModule sharedInstance] showNetworkToast:YES message:EnterProperEmailNetworkToast animated:YES];
            else{
                [[RequestController sharedInstance] sendForgotPasswordRequest:[alertView textFieldAtIndex:0].text successHandler:^(NSDictionary *response) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSLog(@"Success :: %@",response);
                        [[HelperModule sharedInstance] showNetworkToast:YES message:ForgotPasswordSuccessMessage animated:YES];
                    });
                } failureHandler:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSLog(@"Error :: %@",error);
                        [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo] animated:YES];
                    });
                }];
            }
        }
    } else {
        if(buttonIndex != alertView.cancelButtonIndex){
            [self getApplicationDataFromServer];
        }
    }
}


-(void)getRelations{
    [[RequestController sharedInstance] getRelationsWithSuccessHandler:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Success :: %@",response);
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            if ([response objectForKey:Key_Response]) {
                if([response objectForKey:Key_Token]){
                    [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:Key_Token]];
                } else {
                    
                }
                [[UserData sharedInstance] parseRelationDataWithJSON:[response objectForKey:Key_Response]];
                [self performSegueWithIdentifier:LoginToHomeSegue sender:self];
            }
        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Error :: %@",error);
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo] animated:YES];
        });
    }];
}

-(void)getApplicationDataFromServer{
    if([[RequestController sharedInstance] isNetworkReachable]){
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }
        [self.view setUserInteractionEnabled:NO];
        [activityIndicator startAnimating];
        [[RequestController sharedInstance] getSettingsWithSuccessHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Success :: %@",response);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                if ([response objectForKey:Key_Response]) {
//                    if([response objectForKey:Key_Token]){
//                        [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:Key_Token]];
//                    } else {
//                        [[HelperModule sharedInstance] showNetworkToast:YES message:TokenInvalidErrorMessage animated:YES];
//                    }
                    [[ApplicationData sharedInstance] parseSettingsDataFromServerWithDetails:[response objectForKey:Key_Response]];
                }
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Error :: %@",error);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo] animated:YES];
            });
        }];
    } else {
        [self.view endEditing:YES];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Error"
                                                        message:@"Unable to connect to internet!"
                                                       delegate:self
                                              cancelButtonTitle:@"Try Again"
                                              otherButtonTitles:nil];
        alert.tag = 1;
        [alert show];
    }
}

-(BOOL)validateLoginCredentials{
    
    if(![Validator validateEmailId:_emailTextField.text]){
        [[HelperModule sharedInstance] showNetworkToast:YES message:EnterProperEmailNetworkToast animated:YES];
        return false;
    } else {
        if([allTrim(_passwordTextField.text) isEqualToString:@""]){
            [[HelperModule sharedInstance] showNetworkToast:YES message:EnterPasswordNetworkToast animated:YES];
            return false;
        }
    }
    return true;
}

//- (void)playSound:(UInt32)ssID{
//    ssID = 0;
////    AudioServicesPlayAlertSound(ssID);
////    NSError *error = nil;
////    AVAudioPlayer *player = [[AVAudioPlayer alloc] initWithContentsOfURL:audioPath
////                                                                   error:&error];
////    player.numberOfLoops = -1; //Infinite
////    [player setVolume:1.0];
////    player.delegate = self;
////    [player play];
////    OSStatus error = AudioServicesCreateSystemSoundID((__bridge CFURLRef)audioPath, &ssID);
////    if (error == kAudioServicesNoError){
////        AudioServicesAddSystemSoundCompletion(ssID, NULL, NULL, (AudioServicesSystemSoundCompletionProc)MyAudioServicesSystemSoundCompletionProc, NULL);
////        AudioServicesPlaySystemSound(ssID);
////    }
//    NSURL *audioPath = [[NSBundle mainBundle] URLForResource:@"alarm1" withExtension:@"aiff"];
//    OSStatus error = AudioServicesCreateSystemSoundID((__bridge CFURLRef)audioPath, &ssID);
//    if (error == kAudioServicesNoError){
//        AudioServicesAddSystemSoundCompletion(ssID, NULL, NULL, (AudioServicesSystemSoundCompletionProc)MyAudioServicesSystemSoundCompletionProc, NULL);
//        AudioServicesPlaySystemSound(ssID);
//    }
//}
//
//void MyAudioServicesSystemSoundCompletionProc (SystemSoundID  ssID, void *clientData) {
//    NSArray *enteredSafetyRegions = (__bridge NSArray *)clientData;
//    if(true){
//        AudioServicesAddSystemSoundCompletion(ssID, NULL, NULL, (AudioServicesSystemSoundCompletionProc)MyAudioServicesSystemSoundCompletionProc, clientData);
//        AudioServicesPlaySystemSound(ssID);
//    } else {
//        AudioServicesDisposeSystemSoundID(ssID);
//    }
////    NSArray *enteredSafetyRegions = (__bridge NSArray *)clientData;
////    if(enteredSafetyRegions.count){
////        SystemSoundID ssID = 0;
////        NSURL *audioPath = [[NSBundle mainBundle] URLForResource:@"alarm" withExtension:@"wav"];
////        AudioServicesCreateSystemSoundID((__bridge CFURLRef)audioPath, &ssID);
////        AudioServicesAddSystemSoundCompletion(ssID, NULL, NULL, (AudioServicesSystemSoundCompletionProc)MyAudioServicesSystemSoundCompletionProc, clientData);
////        AudioServicesPlaySystemSound(ssID);
////    }
//}
//
//-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
//    NSLog(@"audioPlayerDidFinishPlaying");
//}
//
//-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error{
//    NSLog(@"%@",error);
//}


@end
