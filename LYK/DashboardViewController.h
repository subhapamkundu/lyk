//
//  DashboardViewController.h
//  LYK
//
//  Created by Subhapam on 19/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardViewController : UIViewController

-(void)shareCurrentItem;
-(void)pauseFlipping;
-(void)startFlipping;
-(void)externalShare;
-(void)internalShare;

@end
