//
//  ContentViewController.h
//  LYK
//
//  Created by Subhapam on 19/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *answerBtnYes;
@property (weak, nonatomic) IBOutlet UIButton *answerBtnNo;
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;


@property (strong, nonatomic) NSMutableArray *items;

@property (assign, nonatomic) NSUInteger movieIndex;

-(void)setContentImageWithURL:(NSURL *)url;
-(void)setContentDescription:(NSString *)desc;
-(void)setContentTitle:(NSString *)title;
-(void)setContentType:(NSString *)type;
-(void)setContentId:(NSString *)cardId;

@end
