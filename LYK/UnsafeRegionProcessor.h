//
//  SafetyLocationAlertController.h
//  LYK
//
//  Created by Subhapam kundu on 4/8/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnsafeRegionProcessor : NSObject

+(void)saveCircularSafetyRegionsWithRegions:(NSArray *)regions createdByUser:(BOOL)flag;

@end
