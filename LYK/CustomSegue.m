//
//  CustomSegue.m
//  LYK
//
//  Created by Subhapam on 10/12/15.
//  Copyright © 2015 Subhapam. All rights reserved.
//

#import "CustomSegue.h"

@implementation CustomSegue

-(void)perform{
    if([self.identifier isEqualToString:LoginToHomeSegue]){
        UIViewController *src = (UIViewController *) self.sourceViewController;
        UIViewController *dst = (UIViewController *) self.destinationViewController;
        [UIView transitionWithView:src.navigationController.view duration:0.8 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [src.navigationController presentViewController:dst animated:NO completion:nil];
        } completion:NULL];
    }
}

@end
