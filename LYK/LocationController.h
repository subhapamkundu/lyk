//
//  LocationController.h
//  LYK
//
//  Created by Subhapam Kundu on 13/07/15.
//  Copyright (c) 2015 Subhapam kundu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface LocationController : NSObject

+(id)sharedInstance;

-(NSDictionary *)getCurrentLocation;
-(NSDictionary *)getPreviousLocation;
-(void)setlocationInMap:(MKMapView *)mapView;

@end
