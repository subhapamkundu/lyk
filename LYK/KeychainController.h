//
//  KeychainController.h
//  LYK
//
//  Created by Subhapam Kundu on 03/09/15.
//  Copyright (c) 2015 Subhapam Kundu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeychainController : NSObject


-(void)saveCredentialsInKeychainWithUsername:(NSString *)username password:(NSString *)password;
-(NSString *)getUsernameFromKeychain;
-(NSString *)getPasswordFromKeychain;
-(void)clearKeyChain;


@end
