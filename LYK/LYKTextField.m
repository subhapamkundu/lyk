//
//  LYKTextField.m
//  LYK
//
//  Created by Subhapam Kundu on 01/09/15.
//  Copyright (c) 2015 Subhapam Kundu. All rights reserved.
//

#import "LYKTextField.h"

@interface LYKTextField () <UIPickerViewDelegate, UIPickerViewDataSource>{
//    UITextField *countryCodeTextField;
    UIPickerView *countryCodePickerView;
}

@end

@implementation LYKTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
/*[[NSAttributedString alloc] initWithString:NSLocalizedString(@"Title", @"")
 attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
 titleField.floatingLabelFont = [UIFont boldSystemFontOfSize:kJVFieldFloatingLabelFontSize];
 titleField.floatingLabelTextColor = floatingLabelColor;
 titleField.translatesAutoresizingMaskIntoConstraints = NO;
 titleField.keepBaseline = YES;*/

/*- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //[self commonInit];
        
        self.translatesAutoresizingMaskIntoConstraints = NO;
        self.keepBaseline = YES;
    }
    return self;
}

-(void)setFloatingLabelEnable:(BOOL)floatingLabelEnable{
    
    
}*/


-(CGRect)textRectForBounds:(CGRect)bounds{
    
    [super textRectForBounds:bounds];
    
    [self setFont:FONT_REGULAR(16.0f)];
    if(self.leftView){
        return CGRectMake(bounds.origin.x + (_leftPadding?_leftPadding.intValue:20), bounds.origin.y, bounds.size.width-10, bounds.size.height);
    } else {
        return CGRectMake(bounds.origin.x + (_leftPadding?_leftPadding.intValue:10), bounds.origin.y, bounds.size.width-10, bounds.size.height);
    }
    //UITextField
}

-(CGRect)editingRectForBounds:(CGRect)bounds{
    
    [super editingRectForBounds:bounds];
    
    if(self.leftView){
        return CGRectMake(bounds.origin.x +(_leftPadding?_leftPadding.intValue:20), bounds.origin.y, bounds.size.width-10, bounds.size.height);
    } else {
        return CGRectMake(bounds.origin.x +(_leftPadding?_leftPadding.intValue:10), bounds.origin.y, bounds.size.width-10, bounds.size.height);
    }
}

-(void)setFont:(UIFont *)font{
 //  [ self setFont:[UIFont systemFontOfSize:13]];
    [super setFont:font];
    
    //self.font=[UIFont systemFontOfSize:13.0f];
}

-(void)integrateCountryCodeSegment{
    _countryCodeTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 60, self.bounds.size.height)];
    _countryCodeTextField.textAlignment = NSTextAlignmentCenter;
    _countryCodeTextField.borderStyle = UITextBorderStyleLine;
    _countryCodeTextField.minimumFontSize = 10.0f;
    _countryCodeTextField.inputView = countryCodePickerView;
    self.leftViewMode = UITextFieldViewModeAlways;
    self.leftView = _countryCodeTextField;
    _countryCodeTextField.text = [[ApplicationData sharedInstance] getCurrentCountryCode];
    [self createCountryCodePickerView];
}

-(void)createCountryCodePickerView{
    countryCodePickerView = [[UIPickerView alloc] init];
    [countryCodePickerView setDataSource: self];
    [countryCodePickerView setDelegate: self];
    countryCodePickerView.showsSelectionIndicator = YES;
    _countryCodeTextField.inputView = countryCodePickerView;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [[ApplicationData sharedInstance] getCountryCodeList].count;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSDictionary *countryDetails = [[[ApplicationData sharedInstance] getCountryCodeList] objectAtIndex:row];
    _countryCodeTextField.text = [countryDetails objectForKey:@"dial_code"];
}

- (UIView*)pickerView:(UIPickerView *)thePickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    NSDictionary *countryDetails = [[[ApplicationData sharedInstance] getCountryCodeList] objectAtIndex:row];
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, thePickerView.bounds.size.width, 44)];
    UILabel* l1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, thePickerView.bounds.size.width - 80, 44)];
    //    l1.font = [UIFont sytemFontOfSize:22]; // choose desired size
    //    l1.tag = VIEWTAG_LINE1;
    l1.text = [NSString stringWithFormat:@"%@ (%@)",[countryDetails objectForKey:@"name"],[countryDetails objectForKey:@"code"]];
    l1.textAlignment = NSTextAlignmentLeft;
    [v addSubview: l1];
    
    UILabel* l2 = [[UILabel alloc] initWithFrame:CGRectMake(thePickerView.bounds.size.width - 100, 0, 80, 44)];
    //    l2.font = [UIFont sytemFontOfSize:14]; // choose desired size
    //    l2.tag = VIEWTAG_LINE2;
    l2.text = [NSString stringWithFormat:@"%@",[countryDetails objectForKey:@"dial_code"]];
    l2.textAlignment = NSTextAlignmentRight;
    [v addSubview: l2];
    
    return v;
}

@end
