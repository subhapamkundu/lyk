//
//  SafetyViewController.h
//  LYK
//
//  Created by Subhapam on 19/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SafetyViewController : UIViewController

-(IBAction)sosAction;
-(IBAction)safetyRegionAction;

@end
