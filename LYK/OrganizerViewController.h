//
//  OrganizerViewController.h
//  LYK
//
//  Created by Subhapam on 19/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIImageView+AFNetworking.h>

@interface OrganizerViewController : UIViewController

-(void)addSchedule;
-(void)openViewOption;

@end
