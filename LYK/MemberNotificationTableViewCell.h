//
//  MemberNotificationTableViewCell.h
//  LYK
//
//  Created by Subhapam on 13/11/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MemberNotificationTableViewCellDelegates <NSObject>

-(void)shouldAcceptRelationshipWithDetails:(Relationship *)relationDetails;
-(void)shouldRejectRelationshipWithDetails:(Relationship *)relationDetails;

@end

@interface MemberNotificationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
@property (weak, nonatomic) IBOutlet UIButton *rejectBtn;
@property (weak, nonatomic) IBOutlet UILabel *textLbl;
@property (weak, nonatomic) Relationship *relationDetails;
@property (weak, nonatomic) id<MemberNotificationTableViewCellDelegates> memberNotificationTableViewCellDelegate;


- (IBAction)acceptRelationship:(id)sender;
- (IBAction)rejectRelationship:(id)sender;

@end
