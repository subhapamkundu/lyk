//
//  HelperModule.m
//  LYK
//
//  Created by Subhapam on 18/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "HelperModule.h"

@implementation HelperModule

+(HelperModule *)sharedInstance
{
    static HelperModule *sharedInstance_ = nil;
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedInstance_ = [[HelperModule alloc] init];
    });
    
    return sharedInstance_;
}

-(void)setOrientation:(UIInterfaceOrientation)orientation
{
    _orientation = orientation;
    if (orientation == UIInterfaceOrientationPortrait) {
        [[HelperModule sharedInstance] setIsLandscape:NO];
        if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger: UIInterfaceOrientationPortrait] forKey:@"orientation"];
        }
    }
    else {
        [[HelperModule sharedInstance] setIsLandscape:YES];
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger: UIInterfaceOrientationLandscapeLeft] forKey:@"orientation"];
    }
}

- (void)raiseAlertWithTitle:(NSString *)title message:(NSString *)message
{
    [self raiseAlertWithTitle:title message:message Tag:0 delegate:nil];
}


- (void)raiseAlertWithTitle:(NSString *)title message:(NSString *)message Tag:(int)tag delegate:(id<UIAlertViewDelegate>)delegate
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:@"OK" otherButtonTitles:nil];
    alert.tag=tag;
    [alert show];
}

-(NSString*)getStoryBoardName:(NSString*)OriginalnibName{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        return [NSString stringWithFormat:@"%@_iPad",OriginalnibName];
    }
    else{
        return [NSString stringWithFormat:@"%@_iPhone",OriginalnibName];
    }
}

-(BOOL)isiPad{
    return (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad);
}

-(NSString *)getDeviceToken {
    return [UIDevice currentDevice].identifierForVendor.UUIDString;
}


#pragma mark- Show/Hide Toast View

//Show toast message and desolve automatically
-(void)showNetworkToast:(BOOL)show message:(NSString *)message animated:(BOOL)animated
{
    if (show) {
        if (!_toastView) {
            UIView *networktostView = [[UIView alloc] init];
            networktostView.frame = CGRectMake(20 - [UIScreen mainScreen].bounds.size.width-50, [UIScreen mainScreen].bounds.size.height/2 - 20, [UIScreen mainScreen].bounds.size.width-50, 40);
            
            networktostView.backgroundColor = kDefaulColor;
            
            //   networktostView.layer.borderColor = [UIColor whiteColor].CGColor;
            //   networktostView.layer.borderWidth = 1.0f;
            
            UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
            [window addSubview:networktostView];
            networktostView.alpha = 0.0f;
            _toastView=networktostView;
            _toastView.layer.zPosition = MAXFLOAT;
            
            UILabel *mesgLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, _toastView.frame.size.width-10, _toastView.frame.size.height-10)];
            mesgLabel.font = [UIFont systemFontOfSize:12];
            mesgLabel.textColor = [UIColor whiteColor];
            mesgLabel.lineBreakMode = NSLineBreakByWordWrapping;
            mesgLabel.numberOfLines = 20;
            mesgLabel.backgroundColor = [UIColor clearColor];
            mesgLabel.textAlignment = NSTextAlignmentCenter;
            [_toastView addSubview:mesgLabel];
            _toastLabel=mesgLabel;
        }
        _toastView.frame = CGRectMake(20, [UIScreen mainScreen].bounds.size.height-80, [UIScreen mainScreen].bounds.size.width-40, 40);
        _toastLabel.frame = CGRectMake(5, 5, _toastView.frame.size.width-10, _toastView.frame.size.height-10);
        
        CGSize size = [self getLabelSizeFortext:message forWidth:_toastLabel.frame.size.width WithFont:_toastLabel.font];
        _toastView.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width-(size.width+40))/2, [UIScreen mainScreen].bounds.size.height/2-(size.height+20+20), size.width+40, size.height+25);
        _toastLabel.frame = CGRectMake(20, 10, _toastView.frame.size.width-40, _toastView.frame.size.height-25);
        _toastLabel.text = message;
        _toastView.layer.cornerRadius = _toastView.frame.size.height/2;
        if (animated) {
            [UIView animateWithDuration:0.4 animations:^{
                _toastView.alpha = 1.0f;
            } completion:^(BOOL finished) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self showNetworkToast:NO message:@"" animated:animated];
                });
            }];
        }
        else {
            _toastView.alpha = 1.0f;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self showNetworkToast:NO message:@"" animated:animated];
            });
        }
    }
    else {
        if (animated) {
            [UIView animateWithDuration:0.4 animations:^{
                _toastView.alpha = 0.0f;
            } completion:^(BOOL finished) {
                
            }];
        }
        else {
            _toastView.alpha = 0.0f;
        }
    }
}
-(CGSize)getLabelSizeFortext:(NSString *)text forWidth:(float)width WithFont:(UIFont *)font
{
    CGSize constraint = CGSizeMake(width, MAXFLOAT);
    // Get the size of the text given the CGSize we just made as a constraint
    
    CGRect titleRect = [text boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingTruncatesLastVisibleLine) attributes:@{NSFontAttributeName:font} context:nil];
    return titleRect.size;
    
}

//Returns the message size
-(CGSize)getLabelSizeFortext:(NSString *)text forHeight:(float)height WithFont:(UIFont *)font
{
    CGSize constraint = CGSizeMake(MAXFLOAT, height);
    // Get the size of the text given the CGSize we just made as a constraint
    
    CGRect titleRect = [text boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingTruncatesLastVisibleLine) attributes:@{NSFontAttributeName:font} context:nil];
    return titleRect.size;
    
}



@end
