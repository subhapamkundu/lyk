//
//  LocationController.m
//  LYK
//
//  Created by Subhapam Kundu on 13/07/15.
//  Copyright (c) 2015 Subhapam kundu. All rights reserved.
//

#import "LocationController.h"
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>


@interface LocationController ()<CLLocationManagerDelegate>



@end

@implementation LocationController
{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLLocation *previousLocation;
}

+(id)sharedInstance{
    static LocationController *sharedInstance_ = nil;
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedInstance_ = [[LocationController alloc] init];
        sharedInstance_->locationManager = [[CLLocationManager alloc] init];
        sharedInstance_->locationManager.delegate = sharedInstance_;
        sharedInstance_->locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        if(IS_OS_8_OR_LATER) {
            NSUInteger code = [CLLocationManager authorizationStatus];
            if (code == kCLAuthorizationStatusNotDetermined && ([sharedInstance_->locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [sharedInstance_->locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
                // choose one request according to your business.
                if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                    [sharedInstance_->locationManager requestAlwaysAuthorization];
                } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                    [sharedInstance_->locationManager  requestWhenInUseAuthorization];
                } else {
                    NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
                }
            }
        }
        [sharedInstance_->locationManager startUpdatingLocation];
    });
    return sharedInstance_;
}

-(NSDictionary *)getCurrentLocation {
    return [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude], @"longitude", [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude], @"latitude", nil];
}

-(NSDictionary *)getPreviousLocation {
    return [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%f",previousLocation.coordinate.longitude], @"longitude", [NSString stringWithFormat:@"%f",previousLocation.coordinate.latitude], @"latitude", nil];
}

-(void)setlocationInMap:(MKMapView *)mapView{
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = currentLocation.coordinate.latitude;
    newRegion.center.longitude = currentLocation.coordinate.longitude;
    newRegion.span.latitudeDelta = 0.008388;
    newRegion.span.longitudeDelta = 0.016243;
    [mapView setRegion:newRegion animated:YES];
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%s -> %@", __PRETTY_FUNCTION__, error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    currentLocation = newLocation;
    previousLocation = oldLocation;
    double distance = [currentLocation distanceFromLocation:previousLocation]/1000;
    if(distance >= 100){
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDictionary *locationDetails = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:currentLocation.coordinate.latitude],@"lat",[NSNumber numberWithDouble:currentLocation.coordinate.longitude],@"longi", [dateFormatter stringFromDate:[NSDate date]],@"dateTime", nil];
        [[RequestController sharedInstance] addMemberLocationWithDetails:locationDetails successHandler:^(NSDictionary *response) {
    //        NSLog(@"%@",response);
        } failureHandler:^(NSError *error) {
            //
        }];
    }
}

@end
