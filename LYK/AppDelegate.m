//
//  AppDelegate.m
//  LYK
//
//  Created by Subhapam on 16/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <GooglePlus/GooglePlus.h>

@import GoogleMaps;

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
//    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
//     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:122.0f/255.0f green:176.0f/255.0f blue:89.0f/255.0f alpha:1.0f]];
    [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"SelectedBg"]];
//    //text tint color
//    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor greenColor], UITextAttributeTextColor, nil]
//                                             forState:UIControlStateNormal];

    [GMSServices provideAPIKey:@"AIzaSyBzjm9IFkEICM0UMjf10ujXSmz88ocvJ4I"];
    return YES;
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    NSLog(@"open url:%@",url);
    if([url.scheme isEqualToString:@"tel"]){
        [[UIApplication sharedApplication] openURL:url];
        return true;
    } else {
        BOOL flag = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                   openURL:url
                                                         sourceApplication:sourceApplication
                                                                annotation:annotation];
        flag = [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation];
        return flag;
    }
    
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler{
    NSLog(@"Push Notification: %@",userInfo);
    [self saveNotificationWithInfo:[userInfo objectForKey:@"custom"]];
    completionHandler(UIBackgroundFetchResultNewData);
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSLog(@"Token: %@",deviceToken);
    NSString *token = [deviceToken description];
    token = [token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    [[ApplicationData sharedInstance] setDeviceToken:token];
    [[ApplicationData sharedInstance] setDeviceId:[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
    NSLog(@"Token: %@",token);
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    NSLog(@"%@",[error localizedDescription]);
}


-(void)saveNotificationWithInfo:(NSMutableDictionary *)notification{
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *path = [documentsDirectory stringByAppendingPathComponent:NotificationPlistFileName];
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    if (![fileManager fileExistsAtPath: path]) {
//        path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:NotificationPlistFileName]];
//    }
//    NSMutableArray *notifications;
//    
//    if ([fileManager fileExistsAtPath: path]) {
//        notifications = [[NSMutableArray alloc] initWithContentsOfFile: path];
//    }
//    else {
//        // If the file doesn’t exist, create an empty array
//        notifications = [NSMutableArray array];
//    }
//    
//    //To insert the data into the plist
//    [notification setObject:[[notification objectForKey:@"aps"] objectForKey:@"alert"] forKey:@"title"];
//    notification = [NSMutableDictionary dictionaryWithDictionary:[notification objectForKey:@"custom"]];
//    [notification setObject:[[notification objectForKey:@"aps"] objectForKey:@"alert"] forKey:@"title"];
//    [notifications addObject:notification];
//    [notifications writeToFile:path atomically:YES];

    // Update
    NSString *notificationCount = [[notification objectForKey:@"message"] objectForKey:@"unreadNotificationCount"];
    NSString *notificationUserId = [NSString stringWithFormat:@"%@",[[notification objectForKey:@"message"] objectForKey:@"userId"]];
    if([notificationUserId isEqualToString:[[UserData sharedInstance] getUserId]])
        [[UserData sharedInstance] setNotificationCount:notificationCount.integerValue];

}

@end
