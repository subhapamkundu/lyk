//
//  ExploreProductDetailViewController.m
//  LYK
//
//  Created by Subhapam on 22/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import "ExploreProductDetailViewController.h"

@interface ExploreProductDetailViewController ()
{
    NSDate *startTime;
}

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet LYKTextField *specializationLabel;
@property (weak, nonatomic) IBOutlet LYKTextField *nameLabel;
@property (weak, nonatomic) IBOutlet LYKTextField *organizationNameLabel;
@property (weak, nonatomic) IBOutlet LYKTextField *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextView *addressTextView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;


@end

@implementation ExploreProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _addressTextView.layer.cornerRadius = 3.0f;
    _addressTextView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _addressTextView.layer.borderWidth = 1.0f;
    NSArray *actionButtons = @[
                               [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"ExternalShare"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(externalShare)],
                               [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"InternalShare"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(internalShare)]
                               ];
    
    self.parentViewController.navigationItem.rightBarButtonItems = actionButtons;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)viewDidAppear:(BOOL)animated{
    _specializationLabel.text = [[_info objectForKey:@"productDetails"] objectForKey:@"specialization"]?:@"";
    _nameLabel.text = [[_info objectForKey:@"productDetails"] objectForKey:@"productName"]?:@"";
    _organizationNameLabel.text = [[_info objectForKey:@"productDetails"] objectForKey:@"organizationName"]?:@"";
    _phoneLabel.text = [[_info objectForKey:@"productDetails"] objectForKey:@"productContactNo"]?:@"";
    _addressTextView.text = [[_info objectForKey:@"productDetails"] objectForKey:@"address"]?:@"";
    [self populateMap];
}

-(void)populateMap{
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    CLLocationCoordinate2D productCoordinate = CLLocationCoordinate2DMake([[[_info objectForKey:@"productDetails"] objectForKey:@"latitude"] doubleValue],[[[_info objectForKey:@"productDetails"] objectForKey:@"longitude"] doubleValue]);
//    [annotation setCoordinate:productCoordinate];
//    [annotation setTitle:@"Title"]; //You can set the subtitle too
//    [self.mapView addAnnotation:annotation];
    
    MKCoordinateSpan span = MKCoordinateSpanMake(0.1, 0.1);
    MKCoordinateRegion region = {productCoordinate, span};
    
    [annotation setCoordinate:productCoordinate];
    
    [_mapView setRegion:region];
    [_mapView addAnnotation:annotation];
}

-(void)externalShare{
    NSString *shareText = @"This text I am sharing";
    NSArray *itemsToShare = @[shareText,[NSURL URLWithString:@"http://www.google.com"]];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[];
    activityVC.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Activity Status: %@ : %@", activityType, @"OK");
        });
        if (completed){
            NSLog(@"The Activity: %@ was completed", activityType);
        } else {
            NSLog(@"The Activity: %@ was NOT completed", activityType);
        }
        
    };
    [self presentViewController:activityVC animated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    }];
    
}

-(void)internalShare{
    
}

@end
