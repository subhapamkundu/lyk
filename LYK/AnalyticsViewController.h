//
//  AnalyticsViewController.h
//  LYK
//
//  Created by Subhapam on 16/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnalyticsViewController : UIViewController

-(void)pauseFlipping;

@end
