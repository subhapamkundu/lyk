//
//  RegisterViewController.m
//  LYK
//
//  Created by Subhapam on 17/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

//168,207,67

#import "RegisterViewController.h"
#import "RegistrationTextTableViewCell.h"
#import "RegistrationDropDownTableViewCell.h"
#import "TermsViewController.h"

#define TextTableViewCellIdentifier             @"registrationTextTableViewCell"
#define DropDownTableViewCellIdentifier         @"registrationDropDownTableViewCell"
#define CheckboxCellIdentifier                  @"registrationcheckboxcell"
#define TermsConditionSegue                     @"termssegue"

@interface RegisterViewController ()<UITableViewDataSource, UITableViewDelegate, DropDownActionDelegate, DropDownDelegate, EmailSelectionActionDelegate, UITextFieldDelegate>
{
    UIDatePicker *datePicker;
    NSMutableDictionary *tableData;
    DropDown *dropDown;
    BOOL isMale;
    BOOL isGenderSelected;
    BOOL isOwnEmailId;
    BOOL shouldIncreaseCellHeight;
    BOOL isTermsAccepted;
    NSArray *gender;
    UIActivityIndicatorView *activityIndicator;
    TermsViewController *terms;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;
@property (weak, nonatomic) IBOutlet UILabel *acceptTermsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *acceptTermsCheckImageView;

- (IBAction)doRegistration:(id)sender;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [self.navigationItem setTitle:RegistrationScreenTitle];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"Back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(goBack)];
    
    gender = @[@"Select Gender",@"Male",@"Female"];
    isMale = true;
    isGenderSelected = false;
    isOwnEmailId = true;
    isTermsAccepted=false;
    [self createDatePicker];
    tableData = [NSMutableDictionary dictionary];
    
    _tableView.delegate=self;
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Accept terms & Conditions"];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    _acceptTermsLabel.attributedText=attributeString;
    
    UITapGestureRecognizer *gesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoTermsScreen:)];
    gesture.numberOfTapsRequired=1;
    [_acceptTermsLabel addGestureRecognizer:gesture];
    UITapGestureRecognizer *checkgesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkboxAction:)];
    gesture.numberOfTapsRequired=1;
    [_acceptTermsCheckImageView addGestureRecognizer:checkgesture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [_socialDetails removeAllObjects];
}

-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark - UITableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row != 6){
        RegistrationTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TextTableViewCellIdentifier];
        if (cell == nil) {
            cell = [[RegistrationTextTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:TextTableViewCellIdentifier];
        }
        //        cell.dataTextField.delegate = self;
        //        cell.dataTextField.inputView = nil;
        //        NSString *fieldValue = [tableData objectForKey:[NSNumber numberWithInteger:indexPath.row]];
        //        if(fieldValue){
        //            cell.dataTextField.text = fieldValue;
        //            if(cell.dataTextField.tag == 4){
        //                cell.dataTextField.text = [[fieldValue componentsSeparatedByString:@"-"] lastObject];
        //            }
        //        }
        //        cell.dataTextField.tag = indexPath.row;
        //        switch (indexPath.row) {
        //            case 0:{
        //                NSMutableAttributedString *stringText = [[NSMutableAttributedString alloc] initWithString:@"*First Name"];
        //                [stringText addAttributes: @{NSForegroundColorAttributeName:[UIColor redColor],NSBaselineOffsetAttributeName : @1} range: NSMakeRange(0, 1)];
        //                cell.captionLabel.attributedText = stringText;
        //                cell.isTextFieldEditable = YES;
        //                if(_socialDetails){
        //                    cell.dataTextField.text = [[_socialDetails objectForKey:@"socialType"] isEqualToString:@"g"]?[_socialDetails objectForKey:@"given_name"]:@"";
        //                    [tableData setObject:cell.dataTextField.text forKey:[NSNumber numberWithInteger:indexPath.row]];
        //                }
        //                cell.dataTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        //            }
        //                break;
        //            case 1:{
        //                cell.captionLabel.text = @"Middle Name";
        //                cell.isTextFieldEditable = YES;
        //                cell.dataTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        //            }
        //                break;
        //            case 2:{
        //                NSMutableAttributedString *stringText = [[NSMutableAttributedString alloc] initWithString:@"*Last Name"];
        //                [stringText addAttributes: @{NSForegroundColorAttributeName:[UIColor redColor],NSBaselineOffsetAttributeName : @1} range: NSMakeRange(0, 1)];
        //                cell.captionLabel.attributedText = stringText;
        //                cell.isTextFieldEditable = YES;
        //                if(_socialDetails){
        //                    cell.dataTextField.text = [[_socialDetails objectForKey:@"socialType"] isEqualToString:@"g"]?[_socialDetails objectForKey:@"family_name"]:@"";
        //                    [tableData setObject:cell.dataTextField.text forKey:[NSNumber numberWithInteger:indexPath.row]];
        //                }
        //                cell.dataTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        //            }
        //                break;
        //            case 3:{
        ////                cell.captionLabel.text = @"Email Id";
        //                if(_socialDetails){
        //                    cell.dataTextField.text = [[_socialDetails objectForKey:@"socialType"] isEqualToString:@"g"]?[_socialDetails objectForKey:@"email"]:@"";
        //                    [tableData setObject:cell.dataTextField.text forKey:[NSNumber numberWithInteger:indexPath.row]];
        //                }
        //
        ////                if(isOwnEmailId)
        ////                    cell.captionLabel.text = @"Email Id";
        ////                else
        ////                    cell.captionLabel.text = @"Parent's Email Id";
        //                NSMutableAttributedString *stringText = [[NSMutableAttributedString alloc] initWithString:@"*Email Id"];
        //                [stringText addAttributes: @{NSForegroundColorAttributeName:[UIColor redColor],NSBaselineOffsetAttributeName : @1} range: NSMakeRange(0, 1)];
        //                cell.captionLabel.attributedText = stringText;
        //                cell.isTextFieldEditable = YES;
        //                cell.dataTextField.keyboardType = UIKeyboardTypeEmailAddress;
        //                cell.emailSelectionDelegate = self;
        //                if(cell.emailIdSegment.selectedSegmentIndex == UISegmentedControlNoSegment){
        //                    cell.emailIdSegment.selectedSegmentIndex = 0;
        //                    [self emailIdSelectionSegmentValueChanged:cell.emailIdSegment];
        //                }
        ////                cell.emailIdSegment.hidden = isParent;
        ////                if(!isParent){
        ////                    if(cell.emailIdSegment.selectedSegmentIndex == UISegmentedControlNoSegment){
        ////                        cell.emailIdSegment.selectedSegmentIndex = 0;
        ////                        [cell.emailIdSegment sendActionsForControlEvents:UIControlEventValueChanged];
        ////                    }
        ////                }
        //            }
        //                break;
        //            case 4:{
        //                NSMutableAttributedString *stringText = [[NSMutableAttributedString alloc] initWithString:@"*Mobile Number"];
        //                [stringText addAttributes: @{NSForegroundColorAttributeName:[UIColor redColor],NSBaselineOffsetAttributeName : @1} range: NSMakeRange(0, 1)];
        //                cell.captionLabel.attributedText = stringText;
        //                cell.isTextFieldEditable = YES;
        //                cell.dataTextField.keyboardType = UIKeyboardTypePhonePad;
        //                [cell.dataTextField integrateCountryCodeSegment];
        //                cell.dataTextField.leftPadding = [NSNumber numberWithInteger:70];
        //            }
        //                break;
        //            case 5:{
        ////                cell.captionLabel.text = @"Date Of Birth";
        //                NSMutableAttributedString *stringText = [[NSMutableAttributedString alloc] initWithString:@"*Date Of Birth"];
        //                [stringText addAttributes: @{NSForegroundColorAttributeName:[UIColor redColor],NSBaselineOffsetAttributeName : @3} range: NSMakeRange(0, 1)];
        //                cell.captionLabel.attributedText = stringText;
        //                cell.isTextFieldEditable = NO;
        //                cell.dataTextField.inputView = datePicker;
        //            }
        //                break;
        //            default:
        //                break;
        //        }
        //        return cell;
        NSLog(@"table data : %@ \n for index path : %ld",tableData, (long)indexPath.row);
        return [self configureCellForTableViewCell:cell cellForRowAtIndexPath:indexPath];
    } else {
        RegistrationDropDownTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DropDownTableViewCellIdentifier];
        if (cell == nil) {
            cell = [[RegistrationDropDownTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:DropDownTableViewCellIdentifier];
        }
        //        cell.selectRoleBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        //        cell.selectRoleBtn.layer.cornerRadius = 5.0f;
        //        cell.selectRoleBtn.layer.borderWidth = 1.0f;
        //        cell.dropDownDelegate = self;
        //        NSString *fieldValue = [tableData objectForKey:[NSNumber numberWithInteger:indexPath.row]];
        //        if(fieldValue)
        //            [cell.selectRoleBtn setTitle:fieldValue forState:UIControlStateNormal];
        //        else
        //            [cell.selectRoleBtn setTitle:[gender objectAtIndex:0] forState:UIControlStateNormal];
        //        NSMutableAttributedString *stringText = [[NSMutableAttributedString alloc] initWithString:@"*Sign Up As"];
        //        [stringText addAttributes: @{NSForegroundColorAttributeName:[UIColor redColor],NSBaselineOffsetAttributeName : @3} range: NSMakeRange(0, 1)];
        //        cell.captionLabel.attributedText = stringText;
        //        return cell;
        NSLog(@"table data : %@ \n for index path : %ld",tableData, (long)indexPath.row);
        return [self configureCellForTableViewCell:cell cellForRowAtIndexPath:indexPath];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(shouldIncreaseCellHeight && indexPath.row == 6){
        return 170;
    } else {
        if(indexPath.row == 3){
            return 59;
        }
        return 59;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    //    if(indexPath.row != 6){
    //        return [self configureCellForTableViewCell:cell cellForRowAtIndexPath:indexPath];
    //    } else {
    //        RegistrationDropDownTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DropDownTableViewCellIdentifier];
    //        if (cell == nil) {
    //            cell = [[RegistrationDropDownTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:DropDownTableViewCellIdentifier];
    //        }
    NSLog(@"table data : %@ \n for index path : %ld",tableData, (long)indexPath.row);
    [self configureCellForTableViewCell:cell cellForRowAtIndexPath:indexPath];
    //    }
}

-(UITableViewCell *)configureCellForTableViewCell:(UITableViewCell *)tableViewCell cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row != 6){
        RegistrationTextTableViewCell *cell = (RegistrationTextTableViewCell *)tableViewCell;
        if (cell == nil) {
            cell = [[RegistrationTextTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:TextTableViewCellIdentifier];
        }
        cell.dataTextField.delegate = self;
        cell.dataTextField.inputView = nil;
        NSString *fieldValue = [tableData objectForKey:[NSNumber numberWithLong:indexPath.row]];
        if(fieldValue){
            cell.dataTextField.text = fieldValue;
            if(cell.dataTextField.tag == 4){
                cell.dataTextField.text = [[fieldValue componentsSeparatedByString:@"-"] lastObject];
            }
        }
        cell.dataTextField.tag = indexPath.row;
        cell.dataTextField.leftView = nil;
        cell.dataTextField.leftPadding = [NSNumber numberWithInteger:10];
        switch (indexPath.row) {
            case 0:{
                NSMutableAttributedString *stringText = [[NSMutableAttributedString alloc] initWithString:@"*First Name"];
                [stringText addAttributes: @{NSForegroundColorAttributeName:[UIColor redColor],NSBaselineOffsetAttributeName : @1} range: NSMakeRange(0, 1)];
                cell.captionLabel.attributedText = stringText;
                cell.isTextFieldEditable = YES;
                if(_socialDetails){
                    if([[_socialDetails objectForKey:@"socialType"] isEqualToString:@"g"]){
                        cell.dataTextField.text = [_socialDetails objectForKey:@"given_name"];
                    } else if ([[_socialDetails objectForKey:@"socialType"] isEqualToString:@"fb"]){
                        cell.dataTextField.text = [_socialDetails objectForKey:@"first_name"];
                    } else {
                        cell.dataTextField.text = @"";
                    }
                    [tableData setObject:cell.dataTextField.text forKey:[NSNumber numberWithInteger:indexPath.row]];
                }
                cell.dataTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                cell.dataTextField.leftView = nil;
                cell.dataTextField.leftPadding = [NSNumber numberWithInteger:10];
                cell.dataTextField.text = [tableData objectForKey:[NSNumber numberWithLong:indexPath.row]];
            }
                break;
            case 1:{
                cell.captionLabel.text = @"Middle Name";
                cell.isTextFieldEditable = YES;
                cell.dataTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                cell.dataTextField.leftView = nil;
                cell.dataTextField.leftPadding = [NSNumber numberWithInteger:10];
                cell.dataTextField.text = [tableData objectForKey:[NSNumber numberWithLong:indexPath.row]];
            }
                break;
            case 2:{
                NSMutableAttributedString *stringText = [[NSMutableAttributedString alloc] initWithString:@"*Last Name"];
                [stringText addAttributes: @{NSForegroundColorAttributeName:[UIColor redColor],NSBaselineOffsetAttributeName : @1} range: NSMakeRange(0, 1)];
                cell.captionLabel.attributedText = stringText;
                cell.isTextFieldEditable = YES;
                if(_socialDetails){
                    if([[_socialDetails objectForKey:@"socialType"] isEqualToString:@"g"]){
                        cell.dataTextField.text = [_socialDetails objectForKey:@"family_name"];
                    } else if ([[_socialDetails objectForKey:@"socialType"] isEqualToString:@"fb"]){
                        cell.dataTextField.text = [_socialDetails objectForKey:@"last_name"];
                    } else {
                        cell.dataTextField.text = @"";
                    }
//                    cell.dataTextField.text = [[_socialDetails objectForKey:@"socialType"] isEqualToString:@"g"]?[_socialDetails objectForKey:@"family_name"]:@"";
                    [tableData setObject:cell.dataTextField.text forKey:[NSNumber numberWithLong:indexPath.row]];
                }
                cell.dataTextField.leftView = nil;
                cell.dataTextField.leftPadding = [NSNumber numberWithInteger:10];
                cell.dataTextField.text = [tableData objectForKey:[NSNumber numberWithLong:indexPath.row]];
                cell.dataTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
            }
                break;
            case 3:{
                //                cell.captionLabel.text = @"Email Id";
                if(_socialDetails){
                    if([[_socialDetails objectForKey:@"socialType"] isEqualToString:@"g"]){
                        cell.dataTextField.text = [_socialDetails objectForKey:@"email"];
                    } else if ([[_socialDetails objectForKey:@"socialType"] isEqualToString:@"fb"]){
                        cell.dataTextField.text = [_socialDetails objectForKey:@"email"];
                    } else {
                        cell.dataTextField.text = @"";
                    }
//                    cell.dataTextField.text = [[_socialDetails objectForKey:@"socialType"] isEqualToString:@"g"]?[_socialDetails objectForKey:@"email"]:@"";
                    [tableData setObject:cell.dataTextField.text forKey:[NSNumber numberWithInteger:indexPath.row]];
                }
                
                //                if(isOwnEmailId)
                //                    cell.captionLabel.text = @"Email Id";
                //                else
                //                    cell.captionLabel.text = @"Parent's Email Id";
                NSMutableAttributedString *stringText = [[NSMutableAttributedString alloc] initWithString:@"*Email Id"];
                [stringText addAttributes: @{NSForegroundColorAttributeName:[UIColor redColor],NSBaselineOffsetAttributeName : @1} range: NSMakeRange(0, 1)];
                cell.captionLabel.attributedText = stringText;
                cell.isTextFieldEditable = YES;
                cell.dataTextField.keyboardType = UIKeyboardTypeEmailAddress;
                cell.emailSelectionDelegate = self;
                cell.dataTextField.leftView = nil;
                cell.dataTextField.leftPadding = [NSNumber numberWithInteger:10];
                cell.dataTextField.text = [tableData objectForKey:[NSNumber numberWithLong:indexPath.row]];
                if(cell.emailIdSegment.selectedSegmentIndex == UISegmentedControlNoSegment){
                    cell.emailIdSegment.selectedSegmentIndex = 0;
                    [self emailIdSelectionSegmentValueChanged:cell.emailIdSegment];
                }
                //                cell.emailIdSegment.hidden = isParent;
                //                if(!isParent){
                //                    if(cell.emailIdSegment.selectedSegmentIndex == UISegmentedControlNoSegment){
                //                        cell.emailIdSegment.selectedSegmentIndex = 0;
                //                        [cell.emailIdSegment sendActionsForControlEvents:UIControlEventValueChanged];
                //                    }
                //                }
            }
                break;
            case 4:{
                NSMutableAttributedString *stringText = [[NSMutableAttributedString alloc] initWithString:@"*Mobile Number"];
                [stringText addAttributes: @{NSForegroundColorAttributeName:[UIColor redColor],NSBaselineOffsetAttributeName : @1} range: NSMakeRange(0, 1)];
                cell.captionLabel.attributedText = stringText;
                cell.isTextFieldEditable = YES;
                cell.dataTextField.keyboardType = UIKeyboardTypePhonePad;
                [cell.dataTextField integrateCountryCodeSegment];
                cell.dataTextField.leftPadding = [NSNumber numberWithInteger:70];
                cell.dataTextField.text = [tableData objectForKey:[NSNumber numberWithLong:indexPath.row]];
                cell.dataTextField.countryCodeTextField.text = [tableData objectForKey:@"countryCode"]?:[[ApplicationData sharedInstance] getCurrentCountryCode];
            }
                break;
            case 5:{
                //                cell.captionLabel.text = @"Date Of Birth";
                NSMutableAttributedString *stringText = [[NSMutableAttributedString alloc] initWithString:@"*Date Of Birth"];
                [stringText addAttributes: @{NSForegroundColorAttributeName:[UIColor redColor],NSBaselineOffsetAttributeName : @3} range: NSMakeRange(0, 1)];
                cell.captionLabel.attributedText = stringText;
                cell.isTextFieldEditable = NO;
                cell.dataTextField.leftView = nil;
                cell.dataTextField.inputView = datePicker;
                cell.dataTextField.leftPadding = [NSNumber numberWithInteger:10];
                cell.dataTextField.text = [tableData objectForKey:[NSNumber numberWithLong:indexPath.row]];
            }
                break;
            default:
                break;
        }
        return cell;
    } else {
        RegistrationDropDownTableViewCell *cell = (RegistrationDropDownTableViewCell *)tableViewCell;;
        if (cell == nil) {
            cell = [[RegistrationDropDownTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:DropDownTableViewCellIdentifier];
        }
        cell.selectRoleBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        cell.selectRoleBtn.layer.cornerRadius = 5.0f;
        cell.selectRoleBtn.layer.borderWidth = 1.0f;
        cell.dropDownDelegate = self;
        NSString *fieldValue = [tableData objectForKey:[NSNumber numberWithInteger:indexPath.row]];
        if(fieldValue)
            [cell.selectRoleBtn setTitle:fieldValue forState:UIControlStateNormal];
        else
            [cell.selectRoleBtn setTitle:[gender objectAtIndex:0] forState:UIControlStateNormal];
        NSMutableAttributedString *stringText = [[NSMutableAttributedString alloc] initWithString:@"*Sign Up As"];
        [stringText addAttributes: @{NSForegroundColorAttributeName:[UIColor redColor],NSBaselineOffsetAttributeName : @3} range: NSMakeRange(0, 1)];
        cell.captionLabel.attributedText = stringText;
        return cell;
    }
}


-(void)checkboxAction:(UITapGestureRecognizer *)gesture{
    isTermsAccepted=!isTermsAccepted;
    
    if (isTermsAccepted) {
        [_acceptTermsCheckImageView setImage:[UIImage imageNamed:@"SelectedCheckbox"]];
    }else{
        [_acceptTermsCheckImageView setImage:[UIImage imageNamed:@"DeselectedCheckbox"]];
    }
    
   // [self performSegueWithIdentifier:TermsConditionSegue sender:nil];
    
}
-(void)gotoTermsScreen:(UITapGestureRecognizer *)gesture{
   [self performSegueWithIdentifier:TermsConditionSegue sender:nil];
    
//    if (!terms) {
//        terms=[self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
//        terms.view.frame=CGRectMake( horizonSpace, verticalSpace, 280.0f, 408.0f);
//        
//        [self addChildViewController:terms];
//        [sel addSubview:regController.view];
//        [regController didMoveToParentViewController:self];
//    }
    
}

#pragma mark -
#pragma mark - UITextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    textField.backgroundColor = [UIColor clearColor];
    
    if (!textField.background)
         textField.background=[UIImage imageNamed:@"TextField"];
    
   
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField.keyboardType == UIKeyboardTypeEmailAddress){
        if([Validator validateEmailId:textField.text]){
            
            textField.background=nil;
        } else {
            textField.backgroundColor = [UIColor colorWithRed:180.0f/255.0f green:0.0f blue:0.0f alpha:0.3f];
        }
    }
    if(textField.keyboardType == UIKeyboardTypeNamePhonePad){
        if([Validator validatePhoneNumber:textField.text withCountryCode:@"in"]){
            textField.background=nil;
        } else {
            textField.backgroundColor = [UIColor colorWithRed:180.0f/255.0f green:0.0f blue:0.0f alpha:0.3f];
        }
    }
    if(textField.tag != 5){
        [tableData setObject:textField.text forKey:[NSNumber numberWithInteger:textField.tag]];
        textField.background=nil;
    }else
        textField.text = [tableData objectForKey:[NSNumber numberWithInteger:textField.tag]];
    
    if(textField.tag == 4){
        LYKTextField *lykTextField = (LYKTextField *)textField;
        [tableData setObject:[NSString stringWithFormat:@"%@-%@",lykTextField.countryCodeTextField.text, lykTextField.text] forKey:[NSNumber numberWithInteger:textField.tag]];
        textField.background=nil;
    }
    [textField setBackground:[UIImage imageNamed:@"TextField"]];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.tag >= 0 && textField.tag <= 2){
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    } else if(textField.tag == 3){
        NSCharacterSet *invalidCharSet = [NSCharacterSet characterSetWithCharactersInString:@" "];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    } else if (textField.tag == 4){
        NSUInteger newLength = textField.text.length + string.length - range.length;
        return (newLength > 10) ? false : true;
    } else {
        return true;
    }
    // Only characters in the NSCharacterSet you choose will insertable.
}

#pragma mark -
#pragma mark - Action

- (IBAction)doRegistration:(id)sender {
//    UIApplication *myapp = [UIApplication sharedApplication];
//    [myapp performSelector:@selector(suspend)];
//    return;
    if(!isTermsAccepted){
        [[HelperModule sharedInstance] showNetworkToast:YES message:AcceptTermsErrorMessage animated:YES];
        return;
    }
    NSMutableDictionary *registerDetails = [self populateDataInDictionary];
    if(!registerDetails)
        return;
    [self.view endEditing:YES];
    if([[RequestController sharedInstance] isNetworkReachable]){
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }

        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        self.view.userInteractionEnabled = false;
        [[RequestController sharedInstance] checkIfUserExistWithEmail:[registerDetails objectForKey:@"email"] successHandler:^(NSDictionary *response){
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"If User Exist Response :: %@",response);
                if([response objectForKey:@"error"]){
                    [[HelperModule sharedInstance] showNetworkToast:YES message:@"User Already Registered" animated:YES];
                } else {
                    [[RequestController sharedInstance] sendRegisterRequestWithCredentials:registerDetails successHandler:^(NSDictionary *response){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.view.userInteractionEnabled = true;
                            [activityIndicator stopAnimating];
                            NSLog(@"User Registration Response :: %@",response);
                            if([response objectForKey:@"error"]){
                                [[HelperModule sharedInstance] showNetworkToast:YES message:[response objectForKey:@"error"] animated:YES];
                            } else {
                                [[HelperModule sharedInstance] showNetworkToast:YES message:@"Registration Successful. Please check your email for temporary password" animated:YES];
                                [[UserData sharedInstance] setEmailId:[registerDetails objectForKey:@"email"]];
                                [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:Key_Token]];
                                [self goBack];
                            }
                        });
                    } failureHandler:^(NSError *error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.view.userInteractionEnabled = true;
                            [activityIndicator stopAnimating];
                            //[[[PJKeychainController alloc] init] clearKeyChain];
                            [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
                        });
                    }];
                }
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                //[[[PJKeychainController alloc] init] clearKeyChain];
                [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:NoResponseErrorMessage animated:YES];
            });
        }];
    }
}

-(NSMutableDictionary *)populateDataInDictionary{
    NSMutableDictionary *registerDetails = [NSMutableDictionary dictionary];
    if (![tableData objectForKey:[NSNumber numberWithInt:0]] || [[tableData objectForKey:[NSNumber numberWithInt:0]] isEqualToString:BlankText]) {
        [[HelperModule sharedInstance] showNetworkToast:YES message:EnterFirstNameErrorMessage animated:YES];
        return nil;
    } else {
        [registerDetails setObject:[tableData objectForKey:[NSNumber numberWithInt:0]]?:BlankText forKey:Key_FirstName];
    }
    [registerDetails setObject:[tableData objectForKey:[NSNumber numberWithInt:1]]?:BlankText forKey:Key_MiddleName];
    [registerDetails setObject:[tableData objectForKey:[NSNumber numberWithInt:2]]?:BlankText forKey:Key_LastName];
    if (![[tableData objectForKey:[NSNumber numberWithInt:3]] isEqualToString:BlankText]) {
        if(![Validator validateEmailId:[tableData objectForKey:[NSNumber numberWithInt:3]]]){
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"Invalid email id" animated:YES];
            return nil;
        }
        [registerDetails setObject:[tableData objectForKey:[NSNumber numberWithInt:3]]?:BlankText forKey:@"email"];
    } else {
        [registerDetails setObject:BlankText forKey:@"email"];
    }
    [registerDetails setObject:[NSNumber numberWithUnsignedInteger:isOwnEmailId?1:2] forKey:@"emailType"];
    [registerDetails setObject:[tableData objectForKey:[NSNumber numberWithInt:4]]?:BlankText forKey:@"contactNo"];
//    if (![tableData objectForKey:[NSNumber numberWithInt:5]] || [[tableData objectForKey:[NSNumber numberWithInt:5]] isEqualToString:BlankText]) {
//        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Please enter date of birth" animated:YES];
//        return nil;
//    } else {
//        [registerDetails setObject:[tableData objectForKey:[NSNumber numberWithInt:5]]?:BlankText forKey:@"dateOfBirth"];
//    }
//    if (!isGenderSelected) {
//        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Please select your gender" animated:YES];
//        return nil;
//    } else {
//        [registerDetails setObject:[NSString stringWithFormat:@"%@",isMale?@"1":@"2"] forKey:@"gender"];
//    }
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    NSDate *startD = [dateFormatter dateFromString:[tableData objectForKey:[NSNumber numberWithInt:5]]];
//    NSDate *endD = [NSDate date];
//    
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSUInteger unitFlags = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
//    NSDateComponents *components = [calendar components:unitFlags fromDate:startD toDate:endD options:0];
//    NSInteger year  = [components year];
//    [registerDetails setObject:[NSNumber numberWithLong:(long)year] forKey:@"age"];
    if(_socialDetails){
        if([[_socialDetails objectForKey:@"socialType"] isEqualToString:@"fb"]) {
            [registerDetails setObject:@"facebook" forKey:@"socialMedia"];
            [registerDetails setObject:[_socialDetails objectForKey:@"id"] forKey:@"facebookId"];
        }
        else {
            [registerDetails setObject:@"googleplus" forKey:@"socialMedia"];
            [registerDetails setObject:[_socialDetails objectForKey:@"id"] forKey:@"googlePlusUserId"];
        }
    }
    
    if (!isTermsAccepted){
        
        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Please accept terms & conditions" animated:YES];
        return nil;
        
    }else
       return registerDetails;
    
        
    
    
    
}



- (void)createDatePicker {
    
    UIView *pickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 216)];
    pickerView.backgroundColor = [UIColor colorWithRed:109.0/255.0 green:110.0/255.0 blue:120.0/255.0 alpha:1];
    
    datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    
    NSString *maxDateString = @"01-Jan-1956";
    NSString *minDateString = @"31-Dec-2020";
    // the date formatter used to convert string to date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // the specific format to use
    dateFormatter.dateFormat = @"dd-MMM-yyyy";
    // converting string to date
    NSDate *theMaximumDate = [dateFormatter dateFromString: maxDateString];
    NSDate *theMinimumDate = [dateFormatter dateFromString: minDateString];
    // repeat the same logic for theMinimumDate if needed
    
    // here you can assign the max and min dates to your datePicker
    [datePicker setMaximumDate:[NSDate date]]; //the min age restriction
    [datePicker setMinimumDate:theMaximumDate]; //the max age restriction (if needed, or else dont use this line)
    
    // set the mode
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker setDate:[dateFormatter dateFromString: @"01-Jan-2000"]];
    
    // update the textfield with the date everytime it changes with selector defined below
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
}

-(void)goBack{
    //[self disableTextFieldValidation];
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)updateTextField:(id)sender {
    UIDatePicker *picker = (UIDatePicker*)sender;
    [tableData setObject:[self formatDate:picker.date] forKey:[NSNumber numberWithInt:5]];
}

- (NSString *)formatDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

-(void)toggleDropDownAction:(id)sender{
    NSLog(@"toggle drop down");
    
    if(dropDown == nil) {
        CGFloat f = 120;
        dropDown = [[DropDown alloc] showDropDown:sender height:&f arr:gender direction:@"down"];
        dropDown.selectorDelegate = self;
        shouldIncreaseCellHeight = true;
    }
    else {
        [dropDown hideDropDown:sender];
        shouldIncreaseCellHeight = false;
        dropDown = nil;
        
        if([((UIButton *)sender).currentTitle isEqualToString:[gender objectAtIndex:0]]){
            isMale = true;
        } else {
            isMale = false;
        }
    }
    [_tableView beginUpdates];
    [_tableView endUpdates];
    
}

-(void)didSelectItem:(NSString *)selectedItem atIndex:(NSNumber *)index{
    NSLog(@"%@",selectedItem);
    shouldIncreaseCellHeight = false;
    dropDown = nil;
    [tableData setObject:selectedItem forKey:[NSNumber numberWithInt:6]];
    if(([selectedItem isEqualToString:[gender objectAtIndex:0]])){
        isGenderSelected = false;
    } else if([selectedItem isEqualToString:[gender objectAtIndex:1]]){
        isMale = true;
        isGenderSelected = true;
        //        isOwnEmailId = true;
        [_tableView reloadData];
        //        return;
    } else if(([selectedItem isEqualToString:[gender objectAtIndex:2]])){
        isGenderSelected = true;
        isMale = false;
        [_tableView reloadData];
        //        return;
    }
    //    [_tableView beginUpdates];
    //    [_tableView endUpdates];
}

-(void)emailIdSelectionSegmentValueChanged:(id)sender{
    NSLog(@"Segment value changed");
    isOwnEmailId = ((UISegmentedControl *)sender).selectedSegmentIndex == 0;
}


@end
