//
//  NotificationActionViewController.m
//  LYK
//
//  Created by Subhapam on 30/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import "NotificationActionViewController.h"
#import <UIImageView+AFNetworking.h>

@interface NotificationActionViewController ()<MKMapViewDelegate>
{
    NSMutableArray *selectedCoordinates;
    MKPolyline *routeLine;
    UIActivityIndicatorView *activityIndicator;
}

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet LYKTextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

// Relationship Detail Elements

@property (weak, nonatomic) IBOutlet UIView *relationElementsView;
@property (weak, nonatomic) IBOutlet LYKTextField *relationEmailTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *relationPhoneNumberTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *relationNameTextField;

// Organizer Detail Elements
@property (weak, nonatomic) IBOutlet UIView *organizerElementsView;
@property (weak, nonatomic) IBOutlet LYKTextField *organizerTitleTextField;
@property (weak, nonatomic) IBOutlet UITextView *organizerDescriptionTextView;
@property (weak, nonatomic) IBOutlet LYKTextField *organizerStartDateTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *organizerEndDateTextField;

- (IBAction)rejectAction:(id)sender;
- (IBAction)acceptAction:(id)sender;

@end

@implementation NotificationActionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"Notification Details: %@",_details);
    NSDictionary *notificationDetails = [_details objectForKey:@"notificationDetails"];
    _profileImageView.layer.cornerRadius = _profileImageView.bounds.size.width/2;
    [_profileImageView setImageWithURL:[NSURL URLWithString:[[notificationDetails objectForKey:@"userDetails"] objectForKey:@"imageUrl"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    _firstNameTextField.text = [[notificationDetails objectForKey:@"userDetails"] objectForKey:@"firstName"];
    _lastNameTextField.text = [[notificationDetails objectForKey:@"userDetails"] objectForKey:@"lastName"];
    NSString *genderStr = nil;
    if([[[notificationDetails objectForKey:@"userDetails"] objectForKey:@"gender"] isEqualToString:@"Male"]){
        genderStr = @"He";
    } else {
        genderStr = @"She";
    }
    if([[_details valueForKey:@"notificationType"] isEqualToString:@"relationRequest"]){
        [_headerLabel setText:[NSString stringWithFormat:@"Added you as:"]];
        _relationEmailTextField.text = [[notificationDetails objectForKey:@"userDetails"] objectForKey:@"email"];
        _relationPhoneNumberTextField.text = [[notificationDetails objectForKey:@"userDetails"] objectForKey:@"contactNo"];
        _relationNameTextField.text = [notificationDetails objectForKey:@"roleDesc"];
        _relationElementsView.hidden = false;
    } else if([[_details valueForKey:@"notificationType"] isEqualToString:@"shareSOS"]){
        //sosname,sosnumber
        [_headerLabel setText:[NSString stringWithFormat:@"Shared the following SOS with you:"]];
    } else if([[_details valueForKey:@"notificationType"] isEqualToString:@"shareUnsafeLocation"]){
        //regionName,regionLatlong set
        [_headerLabel setText:[NSString stringWithFormat:@"Shared this location as unsafe for you:"]];
    } else if([[_details valueForKey:@"notificationType"] isEqualToString:@"shareOrganizer"]){
        //eventName,eventdetails
        [_headerLabel setText:[NSString stringWithFormat:@"Shared with you a task:"]];
        _organizerTitleTextField.text = [[notificationDetails objectForKey:@"eventDetails"] objectForKey:@"eventSubject"]?:@"";
        _organizerDescriptionTextView.text = [[notificationDetails objectForKey:@"eventDetails"] objectForKey:@"eventContent"]?:@"";
        _organizerStartDateTextField.text = [NSString stringWithFormat:@"%@ %@",[[notificationDetails objectForKey:@"eventDetails"] objectForKey:@"eventStartDate"]?:@"", [[notificationDetails objectForKey:@"eventDetails"] objectForKey:@"startTime"]?:@""];
        _organizerEndDateTextField.text = [NSString stringWithFormat:@"%@ %@",[[notificationDetails objectForKey:@"eventDetails"] objectForKey:@"eventEndDate"]?:@"", [[notificationDetails objectForKey:@"eventDetails"] objectForKey:@"endTime"]?:@""];
        _organizerElementsView.hidden = false;
    } else if([[_details valueForKey:@"notificationType"] isEqualToString:@"shareExplore"]){
    } else {
        
    }
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavHeader"]];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if([[_details objectForKey:@"notificationType"] isEqualToString:@"safetyLocation"]){
//        _mapView.hidden = true;
//        _relationTextField.hidden = false;
    } else {
//        _mapView.hidden = false;
//        _relationTextField.hidden = true;
        [self plotMapWithCoordinates];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)plotMapWithCoordinates{
    for (NSDictionary *point in [_details objectForKey:@""]) {
        double lat = [[point objectForKey:@"lat"] doubleValue];
        double longi = [[point objectForKey:@"longi"] doubleValue];
        CLLocationCoordinate2D touchMapCoordinate = CLLocationCoordinate2DMake(lat, longi);
        MKPointAnnotation *annot = [[MKPointAnnotation alloc] init];
        annot.coordinate = touchMapCoordinate;
        [_mapView addAnnotation:annot];
        [selectedCoordinates addObject:annot];
    }
    [self drawBoundary];
}

-(void)drawBoundary{
    if([selectedCoordinates count]>2){
        MKMapPoint* pointArr = malloc(sizeof(CLLocationCoordinate2D) * [selectedCoordinates count]);
        for (int index = 0; index < [selectedCoordinates count]; index++) {
            MKPointAnnotation *annot = [selectedCoordinates objectAtIndex:index];
            MKMapPoint point = MKMapPointForCoordinate(annot.coordinate);
            pointArr[index] = point;
        }
        routeLine = [MKPolyline polylineWithPoints:pointArr count:[selectedCoordinates count]];
        [_mapView setVisibleMapRect:[routeLine boundingMapRect]]; //If you want the route to be visible
        NSArray *arr = _mapView.overlays;
        [_mapView removeOverlays:arr];
        [_mapView addOverlay:routeLine];
    }
}

-(MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
    MKOverlayView* overlayView = nil;
    if(overlay == routeLine)
    {
        MKPolylineView *routeLineView = [[MKPolylineView alloc] initWithPolyline:routeLine];
        routeLineView.fillColor = [UIColor colorWithRed:0.945 green:0.027 blue:0.957  alpha:1];
        routeLineView.strokeColor = [UIColor colorWithRed:0.945 green:0.027 blue:0.957 alpha:1];
        routeLineView.lineWidth = 4;
        overlayView = routeLineView;
    }
    return overlayView;
}

- (IBAction)rejectAction:(id)sender {
    NSMutableDictionary *rejectDetails = [NSMutableDictionary dictionaryWithDictionary:_details];
    [rejectDetails setObject:[NSNumber numberWithInteger:0] forKey:@"accept"];
    [self sendAcceptanceWithDetails:rejectDetails];
}

- (IBAction)acceptAction:(id)sender {
    NSMutableDictionary *acceptDetails = [NSMutableDictionary dictionaryWithDictionary:_details];
    [acceptDetails setObject:[NSNumber numberWithInteger:1] forKey:@"accept"];
    [self sendAcceptanceWithDetails:acceptDetails];
}

-(void)sendAcceptanceWithDetails:(NSDictionary *)details{
    if(!activityIndicator){
        activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
        activityIndicator.layer.cornerRadius = 05;
        activityIndicator.opaque = NO;
        activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        activityIndicator.center = self.view.center;
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview: activityIndicator];
    }
    self.view.userInteractionEnabled = false;
    [activityIndicator startAnimating];
    [[RequestController sharedInstance] sendAcceptanceWithDetails:details successHandler:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            NSLog(@"data set: %@", response);
            if([_delegate respondsToSelector:@selector(didPerformActionForNotificationId:)]){
                [_delegate performSelector:@selector(didPerformActionForNotificationId:) withObject:[_details objectForKey:@"notificationId"]];
            }
            [self.navigationController popViewControllerAnimated:YES];
        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            NSLog(@"%@",error);
            [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
        });
    }];
}


@end
