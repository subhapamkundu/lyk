//
//  SafetyViewController.m
//  LYK
//
//  Created by Subhapam on 19/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "SafetyViewController.h"
#import "MVPlaceSearchTextField.h"
#import "SafetyActionViewController.h"
#import "UnsafeRegionProcessor.h"
#import <objc/runtime.h>
#import <GoogleMaps/GoogleMaps.h>
#import <QuartzCore/QuartzCore.h>
#import "HomeTabBarController.h"
#import "SafetyRegionTableViewCell.h"

typedef NS_ENUM(NSInteger, SafetyDisplayState)
{
    SafetyDisplayHome = 0,
    SafetyDisplaySOS,
    SafetyDisplayRegion
};

#define OFFSET 268435456
#define RADIUS 85445659.44705395

@interface SafetyViewController ()<UITableViewDataSource, UITableViewDelegate, MenuTransitionDelegate,UIGestureRecognizerDelegate>
{
    NSMutableArray *selectedCoordinates;
    NSMutableArray *restrictedPlaces;
    NSMutableArray *sosNumbers;
    NSMutableArray *routeLines; //your line
    MKPolyline *routeLine;
    MKPolylineView *routeLineView; //overlay view
    SafetyDisplayState displayState;
    BOOL fetchedSOSListByUser, fetchedSOSListForUser, fetchedRegionListByUser, fetchedRegionListForUser, shouldAddNew;
    NSMutableArray *sosListForUser;
    NSMutableArray *sosListByUser;
    NSMutableArray *regionListForUser;
    NSMutableArray *regionListByUser;
    UIActivityIndicatorView *activityIndicator;
    UIRefreshControl *refreshControl;
    NSDictionary *detailsToEdit;
    
    
    __unsafe_unretained SafetyViewController * welf;
    NSArray *arrColor;
    CATextLayer *textLayer1;
    CATextLayer *textLayer2;
    
    NSUInteger selectedIndex;
    
    NSTimer *timer;
    
}

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *gMapView;
@property (weak, nonatomic) IBOutlet UIView *actionView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *theNewActionBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewBG;
@property (weak, nonatomic) IBOutlet UIButton *btnSafety;
@property (weak, nonatomic) IBOutlet UIButton *btnSosNumber;


- (IBAction)dismissDialogue:(id)sender;
- (IBAction)save:(id)sender;
- (IBAction)segmentedControlValueChanged:(id)sender;


@end

@implementation SafetyViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
//                                          initWithTarget:self action:@selector(handleLongPress:)];
//    lpgr.minimumPressDuration = 1.0; //user needs to press for 1 seconds
//    [_mapView addGestureRecognizer:lpgr];
//    selectedCoordinates = [NSMutableArray array];
    displayState = SafetyDisplayHome;
    refreshControl = [[UIRefreshControl alloc]init];
    [_tableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshList:) forControlEvents:UIControlEventValueChanged];
    _theNewActionBtn.hidden = true;
    _theNewActionBtn.layer.cornerRadius = 5.0f;
    
    //_tableView.rowHeight=UITableViewAutomaticDimension;
    
   welf=self;
    arrColor=@[[UIColor redColor],[UIColor greenColor],[UIColor blueColor],[UIColor purpleColor],[UIColor yellowColor],[UIColor blackColor]];
    
    selectedIndex=NSNotFound;
    
   // [self initializeTextlayer];
    
    //[self animate];
    
    [self initializeGesture];
    
   // timer=[NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(animateTextColor) userInfo:nil repeats:YES];
    
}
//-(void)animateTextColor{
//    
//    [UIView animateWithDuration:1.50 delay:<#(NSTimeInterval)#> options:<#(UIViewAnimationOptions)#> animations:<#^(void)animations#> completion:<#^(BOOL finished)completion#>]
//}


-(void)initializeGesture{
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]
                                               initWithTarget:self action:@selector(longPressGestureRecognized:)];
    [self.tableView addGestureRecognizer:longPress];
}

- (void)longPressGestureRecognized:(UILongPressGestureRecognizer *)sender {
    
    UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;
    UIGestureRecognizerState state = longPress.state;
    
    CGPoint location = [longPress locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    if (indexPath==nil) {
        selectedIndex=NSNotFound;
    }else if (state==UIGestureRecognizerStateBegan){
         NSLog(@"long press on table view at row %ld", indexPath.row);
        if (selectedIndex==indexPath.row)
            selectedIndex=NSNotFound;
        else
            selectedIndex=indexPath.row;
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }else{
        //selectedIndex=NSNotFound;
         NSLog(@"gestureRecognizer.state = %ld", sender.state);
    }
   
}

-(void)initializeTextlayer{
    
    textLayer1 = [[CATextLayer alloc] init];
    [textLayer1 setFrame:self.btnSafety.frame];
    textLayer1.contentsScale = [[UIScreen mainScreen] scale];
    textLayer1.rasterizationScale = [[UIScreen mainScreen] scale];
    textLayer1.alignmentMode = kCAAlignmentCenter;

    [self.btnSafety.titleLabel.layer addSublayer:textLayer1];
    //[self.btnSosNumber.titleLabel.layer addSublayer:textLayer];
    
    textLayer1.string=self.btnSafety.titleLabel.text;
    textLayer1.foregroundColor = self.btnSafety.titleLabel.textColor.CGColor;
    [self.btnSafety setNeedsDisplay];
    
}
-(void)animate{
    
    
    UIColor *col=(UIColor *)[arrColor objectAtIndex:(arc4random() % [arrColor count])] ;
    
    [UIView animateWithDuration:2.0 animations:^{
        
      
        textLayer1.foregroundColor = col.CGColor;
        [welf.btnSafety setNeedsDisplay];
         //[welf.btnSafety setTitleColor:col forState:UIControlStateNormal];
        
        
    } completion:^(BOOL finished) {
        
        if (finished) {
            
            
            NSLog(@"sjbfhjksd");
            sleep(2);
            [welf animate];
        }
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    ((HomeTabBarController *)self.parentViewController).menuTransitionDelegate = self;
}
#pragma mark - Drawer Menu Transition delegate

-(void)menuWillShow{
    self.view.userInteractionEnabled = false;
}

-(void)menuDidShow{
    
}

-(void)menuDidHide{
    self.view.userInteractionEnabled = true;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    SafetyActionViewController *safetyActionViewController = (SafetyActionViewController *)segue.destinationViewController;
    [safetyActionViewController setDetailsToEdit:detailsToEdit];
    if(displayState == SafetyDisplaySOS){
        if(shouldAddNew){
            [safetyActionViewController setActionViewState:ActionDisplayNewSOS];
        } else {
            if(_segmentControl.selectedSegmentIndex == 0){
                [safetyActionViewController setActionViewState:ActionDisplayAddedSOSByOther];
            } else if (_segmentControl.selectedSegmentIndex == 1){
                [safetyActionViewController setActionViewState:ActionDisplayAddedSOSByMe];
            }
        }
    } else if(displayState == SafetyDisplayRegion){
        if(shouldAddNew){
            [safetyActionViewController setActionViewState:ActionDisplayNewRegion];
        } else {
            if(_segmentControl.selectedSegmentIndex == 0){
                [safetyActionViewController setActionViewState:ActionDisplayAddedRegionByOther];
            } else if (_segmentControl.selectedSegmentIndex == 1){
                [safetyActionViewController setActionViewState:ActionDisplayAddedRegionByMe];
            }
        }
    }
    detailsToEdit = nil;
}


//- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
//{
//    NSUInteger zoomLevel = [self getZoomLevel];
//    if(zoomLevel > 13){
//        if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
//            return;
//        
//        CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
//        CLLocationCoordinate2D touchMapCoordinate =
//        [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
//        
//        MKPointAnnotation *annot = [[MKPointAnnotation alloc] init];
//        annot.coordinate = touchMapCoordinate;
//        [self.mapView addAnnotation:annot];
//        
//        [selectedCoordinates addObject:annot];
//    }
//    
//    [self drawBoundary];
//}

//- (NSUInteger)getZoomLevel {
//    MKCoordinateRegion region = _mapView.region;
//    double centerPixelX = [self longitudeToPixelSpaceX:region.center.longitude];
//    double topLeftPixelX = [self longitudeToPixelSpaceX:region.center.longitude - region.span.longitudeDelta / 2];
//    double scaledMapWidth = (centerPixelX - topLeftPixelX) * 2;
//    CGSize mapSizeInPixels = _mapView.bounds.size;
//    double zoomScale = scaledMapWidth / mapSizeInPixels.width;
//    double zoomExponent = log(zoomScale) / log(2);
//    double zoomLevel = 20 - zoomExponent;
//    return zoomLevel;
//}
//
//- (double)longitudeToPixelSpaceX:(double)longitude
//{
//    return round(OFFSET + RADIUS * longitude * M_PI / 180.0);
//}

//-(void)showDialogue{
//    _dialogueView.hidden = false;
//    _mapView.userInteractionEnabled = false;
//    _dialogueView.transform = CGAffineTransformMakeScale(0.01, 0.01);
//    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
//        _dialogueView.transform = CGAffineTransformIdentity;
//    } completion:^(BOOL finished){
//        // do something once the animation finishes, put it here
//    }];
//}

//- (IBAction)dismissDialogue:(id)sender {
//    _dialogueView.transform = CGAffineTransformIdentity;
//    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
//        _dialogueView.transform = CGAffineTransformMakeScale(0.01, 0.01);
//    } completion:^(BOOL finished){
//        _dialogueView.hidden = YES;
//        _mapView.userInteractionEnabled = true;
//    }];
//}

- (IBAction)save:(id)sender {
    
}

- (IBAction)segmentedControlValueChanged:(id)sender {
    if(displayState == SafetyDisplaySOS){
        if(_segmentControl.selectedSegmentIndex == 0){
            _theNewActionBtn.hidden = true;
            if(!fetchedSOSListForUser){
                [self fetchSOSListCreatedForUser];
            } else {
                [_tableView reloadData];
            }
        } else if (_segmentControl.selectedSegmentIndex == 1){
            _theNewActionBtn.hidden = false;
            if(!fetchedSOSListByUser){
                [self fetchSOSListCreatedByUser];
            } else {
                [_tableView reloadData];
            }
        }
    } else if(displayState == SafetyDisplayRegion){
        if(_segmentControl.selectedSegmentIndex == 0){
            _theNewActionBtn.hidden = true;
            if(!fetchedRegionListForUser){
                [self fetchRegionListCreatedForUser];
            } else {
                [_tableView reloadData];
            }
        } else if (_segmentControl.selectedSegmentIndex == 1){
            _theNewActionBtn.hidden = false;
            if(!fetchedRegionListByUser){
                [self fetchRegionListCreatedByUser];
            } else {
                [_tableView reloadData];
            }
        }
    }
}

//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if(buttonIndex != alertView.cancelButtonIndex){
//        if(buttonIndex == 2){
//            object_setClass(_dialogueTextField, [MVPlaceSearchTextField class]);
//            ((MVPlaceSearchTextField *)_dialogueTextField).placeSearchDelegate = self;
//            ((MVPlaceSearchTextField *)_dialogueTextField).strApiKey = @"AIzaSyCDi2dklT-95tEHqYoE7Tklwzn3eJP-MtM";
//            ((MVPlaceSearchTextField *)_dialogueTextField).superViewOfList = _dialogueView;  // View, on which Autocompletion list should be appeared.
//            ((MVPlaceSearchTextField *)_dialogueTextField).autoCompleteShouldHideOnSelection = YES;
//            ((MVPlaceSearchTextField *)_dialogueTextField).maximumNumberOfAutoCompleteRows = 3;
//            ((MVPlaceSearchTextField *)_dialogueTextField).autoCompleteRegularFontName = @"HelveticaNeue-Bold";
//            ((MVPlaceSearchTextField *)_dialogueTextField).autoCompleteBoldFontName = @"HelveticaNeue";
//            ((MVPlaceSearchTextField *)_dialogueTextField).autoCompleteTableCornerRadius = 0.0;
//            ((MVPlaceSearchTextField *)_dialogueTextField).autoCompleteRowHeight = 35;
//            ((MVPlaceSearchTextField *)_dialogueTextField).autoCompleteTableCellTextColor = [UIColor colorWithWhite:0.131 alpha:1.000];
//            ((MVPlaceSearchTextField *)_dialogueTextField).autoCompleteFontSize = 14;
//            ((MVPlaceSearchTextField *)_dialogueTextField).autoCompleteTableBorderWidth = 1.0;
//            ((MVPlaceSearchTextField *)_dialogueTextField).showTextFieldDropShadowWhenAutoCompleteTableIsOpen = YES;
//            ((MVPlaceSearchTextField *)_dialogueTextField).autoCompleteShouldHideOnSelection = YES;
//            ((MVPlaceSearchTextField *)_dialogueTextField).autoCompleteShouldHideClosingKeyboard = YES;
//            ((MVPlaceSearchTextField *)_dialogueTextField).autoCompleteShouldSelectOnExactMatchAutomatically = YES;
//            ((MVPlaceSearchTextField *)_dialogueTextField).autoCompleteTableFrame = CGRectMake(((MVPlaceSearchTextField *)_dialogueTextField).frame.origin.x, ((MVPlaceSearchTextField *)_dialogueTextField).frame.size.height+((MVPlaceSearchTextField *)_dialogueTextField).frame.origin.y + 10.0, ((MVPlaceSearchTextField *)_dialogueTextField).frame.size.width, 110.0);
//        } else if (buttonIndex == 1){
//            object_setClass(_dialogueTextField, [UITextField class]);
//        }
//        [self showDialogue];
//    } else {
//        [self dismissDialogue:nil];
//    }
//}

#pragma mark -
#pragma mark Place search Textfield Delegates
#pragma mark -

//-(void)placeSearchResponseForSelectedPlace:(NSMutableDictionary*)responseDict{
//    [self.view endEditing:YES];
//    NSLog(@"%@",responseDict);
//
//    NSDictionary *aDictLocation=[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"];
//    NSLog(@"SELECTED ADDRESS :%@",aDictLocation);
//}
//-(void)placeSearchWillShowResult{
//    
//}
//-(void)placeSearchWillHideResult{
//    
//}
//-(void)placeSearchResultCell:(UITableViewCell *)cell withPlaceObject:(PlaceObject *)placeObject atIndex:(NSInteger)index{
//    if(index%2==0){
//        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
//    }else{
//        cell.contentView.backgroundColor = [UIColor whiteColor];
//    }
//}

//-(void)drawBoundary{
//    if([selectedCoordinates count]>2){
//        MKMapPoint* pointArr = malloc(sizeof(CLLocationCoordinate2D) * [selectedCoordinates count]);
//        for (int index = 0; index < [selectedCoordinates count]; index++) {
//            MKPointAnnotation *annot = [selectedCoordinates objectAtIndex:index];
//            MKMapPoint point = MKMapPointForCoordinate(annot.coordinate);
//            pointArr[index] = point;
//        }
//        routeLine = [MKPolyline polylineWithPoints:pointArr count:[selectedCoordinates count]];
//        [self.mapView setVisibleMapRect:[routeLine boundingMapRect]]; //If you want the route to be visible
//        NSArray *arr = _mapView.overlays;
//        [_mapView removeOverlays:arr];
//        [self.mapView addOverlay:routeLine];
//    }
//}
//
//-(MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
//{
//    MKOverlayView* overlayView = nil;
//    if(overlay == routeLine)
//    {
//        routeLineView = [[MKPolylineView alloc] initWithPolyline:routeLine];
//        routeLineView.fillColor = [UIColor colorWithRed:0.945 green:0.027 blue:0.957  alpha:1];
//        routeLineView.strokeColor = [UIColor colorWithRed:0.945 green:0.027 blue:0.957 alpha:1];
//        routeLineView.lineWidth = 4;
//        overlayView = routeLineView;
//    }
//    return overlayView;
//}

-(IBAction)sosAction{
    
   
    _imageviewBG.image=[UIImage imageNamed:@"sosListBG"];
    
    NSArray *actionButtons = @[
                               [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"CallActive"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(sosAction)],
                               [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"LocationInactive"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(safetyRegionAction)]
                               ];
    
    self.parentViewController.navigationItem.rightBarButtonItems = actionButtons;
    NSLog(@"sosAction");
    [_segmentControl setSelectedSegmentIndex:0];
    if(!fetchedSOSListForUser)
        [self fetchSOSListCreatedForUser];
    if(displayState == SafetyDisplaySOS){
        [self hideActionView];
        return;
    }
    displayState = SafetyDisplaySOS;
    [_segmentControl setTitle:@"SOS added for you" forSegmentAtIndex:0];
    [_segmentControl setTitle:@"SOS added by you" forSegmentAtIndex:1];
    [self showActionView];
    _theNewActionBtn.hidden = true;
}

-(IBAction)safetyRegionAction{
    NSLog(@"safetyRegionAction");
    
//    _btnSafety.hidden=YES;
//    _btnSosNumber.hidden=YES;
    _imageviewBG.image=[UIImage imageNamed:@"safetyRegionListBG"];
    
    
    NSArray *actionButtons = @[
                               [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"CallInactive"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(sosAction)],
                               [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"LocationActive"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(safetyRegionAction)]
                               ];
    
    self.parentViewController.navigationItem.rightBarButtonItems = actionButtons;
    [_segmentControl setSelectedSegmentIndex:0];
    if(!fetchedRegionListForUser)
        [self fetchRegionListCreatedForUser];
    if(displayState == SafetyDisplayRegion){
        [self hideActionView];
        return;
    }
    displayState = SafetyDisplayRegion;
    [_segmentControl setTitle:@"Safety region for you" forSegmentAtIndex:0];
    [_segmentControl setTitle:@"Safety region by you" forSegmentAtIndex:1];
    [self showActionView];
    _theNewActionBtn.hidden = true;
}

-(void)showActionView{
    
    _btnSafety.hidden=YES;
    _btnSosNumber.hidden=YES;
    
    _actionView.hidden = false;
    [_tableView reloadData];
}

-(void)hideActionView{
    
    _btnSafety.hidden=NO;
    _btnSosNumber.hidden=NO;
    
    displayState = SafetyDisplayHome;
    _actionView.hidden = true;
}

-(void)fetchSOSListCreatedByUser{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    if([[RequestController sharedInstance] isNetworkReachable]){
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }
        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        fetchedSOSListByUser = true;
        [[RequestController sharedInstance] getSOSListByUserWithSuccessHandler:^(NSDictionary *response) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 self.view.userInteractionEnabled = true;
                 [activityIndicator stopAnimating];
                 [refreshControl endRefreshing];
                 NSLog(@"data set: %@", response);
                 sosListByUser = [[response objectForKey:@"response"] mutableCopy];
                 [_tableView reloadData];
             });
         } failureHandler:^(NSError *error) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 self.view.userInteractionEnabled = true;
                 [activityIndicator stopAnimating];
                 [refreshControl endRefreshing];
                 [_tableView reloadData];
                 NSLog(@"%@",error);
             });
         }];
    }
}

-(void)fetchSOSListCreatedForUser{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    if([[RequestController sharedInstance] isNetworkReachable]){
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }
        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        fetchedSOSListForUser = true;
        [[RequestController sharedInstance] getSOSListForUserWithSuccessHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [refreshControl endRefreshing];
                NSLog(@"data set: %@", response);
                sosListForUser = [[response objectForKey:@"response"] mutableCopy];
                [_tableView reloadData];
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [refreshControl endRefreshing];
                [_tableView reloadData];
                NSLog(@"%@",error);
            });
        }];
    }
}

-(void)fetchRegionListCreatedByUser{
    if([[RequestController sharedInstance] isNetworkReachable]){
        NSLog(@"%s",__PRETTY_FUNCTION__);
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }
        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        fetchedRegionListByUser = true;
        [[RequestController sharedInstance] getRegionListByUserWithSuccessHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [refreshControl endRefreshing];
                NSLog(@"data set: %@", response);
                regionListByUser = [[[response objectForKey:@"response"] objectForKey:@"locations"] mutableCopy];
                [UnsafeRegionProcessor saveCircularSafetyRegionsWithRegions:regionListByUser createdByUser:true];
                [_tableView reloadData];
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [refreshControl endRefreshing];
                [_tableView reloadData];
                NSLog(@"%@",error);
            });
        }];
    }
}

-(void)fetchRegionListCreatedForUser{
    if([[RequestController sharedInstance] isNetworkReachable]){
        NSLog(@"%s",__PRETTY_FUNCTION__);
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }
        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        fetchedRegionListForUser = true;
        [[RequestController sharedInstance] getRegionListForUserWithSuccessHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [refreshControl endRefreshing];
                NSLog(@"data set: %@", response);
                regionListForUser = [[[[response objectForKey:@"response"] objectAtIndex:0] objectForKey:@"locations"] mutableCopy];
                [UnsafeRegionProcessor saveCircularSafetyRegionsWithRegions:regionListForUser createdByUser:false];
                [_tableView reloadData];
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [refreshControl endRefreshing];
                [_tableView reloadData];
                NSLog(@"%@",error);
            });
        }];
    }
}

-(void)refreshList:(UIRefreshControl *)sender{
    if(displayState == SafetyDisplaySOS){
        if(_segmentControl.selectedSegmentIndex == 0){
            [self fetchSOSListCreatedForUser];
        } else if (_segmentControl.selectedSegmentIndex == 1){
            [self fetchSOSListCreatedByUser];
        }
    } else if(displayState == SafetyDisplayRegion){
        if(_segmentControl.selectedSegmentIndex == 0){
            [self fetchRegionListCreatedForUser];
        } else if (_segmentControl.selectedSegmentIndex == 1){
            [self fetchRegionListCreatedByUser];
        }
    }
}

/*- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}*/

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(displayState == SafetyDisplaySOS){
        if(_segmentControl.selectedSegmentIndex == 0){
            return [sosListForUser count];
        } else if (_segmentControl.selectedSegmentIndex == 1){
            return [sosListByUser count];
        }
    } else if(displayState == SafetyDisplayRegion){
        if(_segmentControl.selectedSegmentIndex == 0){
            return [regionListForUser count];
        } else if (_segmentControl.selectedSegmentIndex == 1){
            return [regionListByUser count];
        }
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(displayState == SafetyDisplayRegion && selectedIndex==indexPath.row ){
        
        return 204.0f;
    }else
        return 58.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellIdentifier"];
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cellIdentifier"];
    }
    if(displayState == SafetyDisplaySOS){
        UIButton *callButton = [[UIButton alloc] init];
        [callButton setTitle:nil forState:UIControlStateNormal];
        [callButton setBackgroundImage:[UIImage imageNamed:@"CallActive"] forState:UIControlStateNormal];
        callButton.tag = indexPath.row;
        [callButton addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside];
        [callButton setFrame:CGRectMake(0, 0, 40,40)];
        cell.accessoryView = callButton;
        if(_segmentControl.selectedSegmentIndex == 0){
            NSDictionary *sosDetails = [sosListForUser objectAtIndex:indexPath.row];
//            cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[[sosDetails objectForKey:@"createdBy"]objectForKey:@"imageUrl"]?:@""]];
            cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width/2;
            cell.imageView.clipsToBounds = true;
            cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)",[sosDetails objectForKey:@"sosName"]?:@"", [sosDetails objectForKey:@"mobileNo"]?:@""];
            NSMutableArray *nameSegments = [NSMutableArray array];
            [nameSegments addObject:[[sosDetails objectForKey:@"createdBy"]objectForKey:@"firstName"]];
            [nameSegments addObject:[[sosDetails objectForKey:@"createdBy"]objectForKey:@"middleName"]];
            [nameSegments addObject:[[sosDetails objectForKey:@"createdBy"]objectForKey:@"lastName"]];
            cell.detailTextLabel.text = [nameSegments componentsJoinedByString:@" "];
        } else if (_segmentControl.selectedSegmentIndex == 1){
            NSDictionary *sosDetails = [sosListByUser objectAtIndex:indexPath.row];
//            cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[[sosDetails objectForKey:@"createdBy"]objectForKey:@"imageUrl"]?:@""]];
            cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width/2;
            cell.imageView.clipsToBounds = true;
            cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)",[sosDetails objectForKey:@"sosName"]?:@"", [sosDetails objectForKey:@"mobileNo"]?:@""];
            cell.detailTextLabel.text = [sosDetails objectForKey:@"sosMail"]?:@"";
        }
        
        return cell;
    } else if(displayState == SafetyDisplayRegion){
        
        //
        
        SafetyRegionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SAFETYREGION"];
        if(!cell){
            cell = [[SafetyRegionTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"SAFETYREGION"];
        }
        
        
        
        
//        UIButton *globeIcon = [[UIButton alloc] init];
//        [globeIcon setTitle:nil forState:UIControlStateNormal];
//        [globeIcon setBackgroundImage:[UIImage imageNamed:@"globeIcon"] forState:UIControlStateNormal];
//        globeIcon.tag = indexPath.row;
//        [globeIcon addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside];
//        [globeIcon setFrame:CGRectMake(0, 0, 40,40)];
//        globeIcon.userInteractionEnabled=NO;
//        cell.accessoryView = globeIcon;
        if(_segmentControl.selectedSegmentIndex == 0){
            NSDictionary *regionDetails = [regionListForUser objectAtIndex:indexPath.row];
//            cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[[regionDetails objectForKey:@"addedBy"]objectForKey:@"imageUrl"]?:@""]];
            cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width/2;
            cell.imageView.clipsToBounds = true;
            cell.lblAddress.text= [NSString stringWithFormat:@"%@",[regionDetails objectForKey:@"placeName"]?:@""];
            //cell.textLabel.text = [NSString stringWithFormat:@"%@",[regionDetails objectForKey:@"placeName"]?:@""];
            NSMutableArray *nameSegments = [NSMutableArray array];
            [nameSegments addObject:[[regionDetails objectForKey:@"addedBy"]objectForKey:@"firstName"]];
            [nameSegments addObject:[[regionDetails objectForKey:@"addedBy"]objectForKey:@"middleName"]];
            [nameSegments addObject:[[regionDetails objectForKey:@"addedBy"]objectForKey:@"lastName"]];
            //cell.detailTextLabel.text = [nameSegments componentsJoinedByString:@" "];
            cell.lblName.text=[nameSegments componentsJoinedByString:@" "];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
//                cell.mapview.myLocationEnabled = YES;
                //cell.mapview.camera=[GMSCameraPosition cameraWithTarget:cell.mapview.myLocation.coordinate zoom:12];
                
                NSDictionary *dict=[[regionDetails objectForKey:@"latLong"]objectAtIndex:0];
                cell.mapview.camera=[GMSCameraPosition cameraWithLatitude:[[dict objectForKey:@"lat"]doubleValue] longitude:[[dict objectForKey:@"lat"]doubleValue] zoom:6.0];

                [self addPolylineIn:cell.mapview usingCoordinates:[regionDetails objectForKey:@"latLong"]];
            });
//
        } else if (_segmentControl.selectedSegmentIndex == 1){
            NSDictionary *regionDetails = [regionListByUser objectAtIndex:indexPath.row];
//            cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[[sosDetails objectForKey:@"createdBy"]objectForKey:@"imageUrl"]?:@""]];
            cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width/2;
            cell.imageView.clipsToBounds = true;
            //cell.textLabel.text = [NSString stringWithFormat:@"%@",[regionDetails objectForKey:@"placeName"]?:@""];
             cell.lblAddress.text= [NSString stringWithFormat:@"%@",[regionDetails objectForKey:@"placeName"]?:@""];
//            NSMutableArray *nameSegments = [NSMutableArray array];
//            [nameSegments addObject:[[sosDetails objectForKey:@"createdBy"]objectForKey:@"firstName"]];
//            [nameSegments addObject:[[sosDetails objectForKey:@"createdBy"]objectForKey:@"middleName"]];
//            [nameSegments addObject:[[sosDetails objectForKey:@"createdBy"]objectForKey:@"lastName"]];
//            cell.detailTextLabel.text = [nameSegments componentsJoinedByString:@" "];
        }
        cell.backgroundColor=[UIColor clearColor];
        //cell.accessoryView = nil;
        
        return cell;
    }else{
        return [UIView new];
    }
    
    
}

-(void)addPolylineIn:(GMSMapView *)view usingCoordinates:(NSArray *)arrCordinate{
    
     GMSMutablePath *rect = [GMSMutablePath path];
        for (NSDictionary *dict in arrCordinate) {
     
        [rect addLatitude:[[dict objectForKey:@"lat"]doubleValue] longitude:[[dict objectForKey:@"longi"]doubleValue]];
      
    }
    
    GMSPolygon *polygon = [GMSPolygon polygonWithPath:rect];
    polygon.fillColor = [UIColor colorWithRed:0.25 green:0 blue:0 alpha:0.05];
    polygon.strokeColor = [UIColor blackColor];
    polygon.strokeWidth = 2;
    polygon.map = view;


}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    detailsToEdit = nil;
    if(displayState == SafetyDisplaySOS){
        if(_segmentControl.selectedSegmentIndex == 1){
            detailsToEdit = [sosListByUser objectAtIndex:indexPath.row];
            [self goToActionViewController:NO];
        }
    } else if(displayState == SafetyDisplayRegion){
        if(_segmentControl.selectedSegmentIndex == 0)
            detailsToEdit = [regionListForUser objectAtIndex:indexPath.row];
        else
            detailsToEdit = [regionListByUser objectAtIndex:indexPath.row];
        [self goToActionViewController:NO];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!activityIndicator){
        activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
        activityIndicator.layer.cornerRadius = 05;
        activityIndicator.opaque = NO;
        activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        activityIndicator.center = self.view.center;
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview: activityIndicator];
    }
    self.view.userInteractionEnabled = false;
    [activityIndicator startAnimating];
    if(displayState == SafetyDisplaySOS){
        if(_segmentControl.selectedSegmentIndex == 0){
            NSString *sosId = [[sosListForUser objectAtIndex:indexPath.row] objectForKey:@"sosId"];
            [[RequestController sharedInstance] removeSOSRequestWithId:sosId successHandler:^(NSDictionary *response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    [refreshControl endRefreshing];
                    [sosListForUser removeObjectAtIndex:indexPath.row];
                    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                });
            } failureHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    NSLog(@"%@",error);
                });
            }];
        } else if (_segmentControl.selectedSegmentIndex == 1){
            NSString *sosId = [[sosListByUser objectAtIndex:indexPath.row] objectForKey:@"sosId"];
            [[RequestController sharedInstance] removeSOSRequestWithId:sosId successHandler:^(NSDictionary *response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    [refreshControl endRefreshing];
                    [sosListByUser removeObjectAtIndex:indexPath.row];
                    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                });
            } failureHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    NSLog(@"%@",error);
                });
            }];
        }
    } else if(displayState == SafetyDisplayRegion){
        if(_segmentControl.selectedSegmentIndex == 0){
            NSString *sosId = [[regionListForUser objectAtIndex:indexPath.row] objectForKey:@"placeId"];
            [[RequestController sharedInstance] removeRegionRequestWithId:sosId successHandler:^(NSDictionary *response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    [refreshControl endRefreshing];
                    [regionListForUser removeObjectAtIndex:indexPath.row];
                    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                });
            } failureHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    NSLog(@"%@",error);
                });
            }];
        } else if (_segmentControl.selectedSegmentIndex == 1){
            NSString *sosId = [[regionListByUser objectAtIndex:indexPath.row] objectForKey:@"placeId"];
            [[RequestController sharedInstance] removeRegionRequestWithId:sosId successHandler:^(NSDictionary *response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    [refreshControl endRefreshing];
                    [regionListByUser removeObjectAtIndex:indexPath.row];
                    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                });
            } failureHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    NSLog(@"%@",error);
                });
            }];
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1.0f)];
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    NSString *phoneNumber = [@"tel://" stringByAppendingString:[[sosListForUser objectAtIndex:indexPath.row] objectForKey:@"mobileNo"]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(IBAction)performNewAction{
    [self goToActionViewController:YES];
}

-(void)goToActionViewController:(BOOL)flag{
    shouldAddNew = flag;
    [self performSegueWithIdentifier:SafetyToActionSegue sender:nil];
}

-(void)callAction:(id)sender{
    UIButton *callBtn = sender;
    NSString *pureNumbers = nil;
    if(displayState == SafetyDisplaySOS){
        if(_segmentControl.selectedSegmentIndex == 0){
            pureNumbers = [[[[sosListForUser objectAtIndex:callBtn.tag] objectForKey:@"mobileNo"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
        } else {
            pureNumbers = [[[[sosListByUser objectAtIndex:callBtn.tag] objectForKey:@"mobileNo"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
        }
    }
    
    NSString *phoneNumber = [@"tel://" stringByAppendingString:pureNumbers];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    
}

@end
