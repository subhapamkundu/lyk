//
//  ApplicationData.h
//  LYK
//
//  Created by Subhapam on 18/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApplicationData : NSObject

@property (strong, nonatomic) NSString *authToken;
@property (strong, nonatomic) NSString *deviceToken;
@property (strong, nonatomic) NSString *deviceId;

+(id)sharedInstance;

-(void)parseSettingsDataFromServerWithDetails:(NSDictionary *)details;

-(NSArray *)getInterests;
-(NSArray *)getRoles;

-(NSArray *)getCountryCodeList;
-(NSString *)getCurrentCountryCode;


@end
