//
//  DetailContentViewController.m
//  LYK
//
//  Created by Subhapam on 13/12/15.
//  Copyright © 2015 Subhapam. All rights reserved.
//

#import "DetailContentViewController.h"
#import "ShareViewController.h"

@interface DetailContentViewController ()<UIWebViewDelegate>
{
    NSString *_url;
    UIImage *_thumbnailImage;
    NSDate *startTime;
}
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UILabel *urlLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@end

@implementation DetailContentViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"DetailContentViewController");
    startTime = [NSDate date];
    _webView.delegate = self;
    NSArray *actionButtons = @[
                               [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"ExternalShare"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(externalShare)],
                               [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"InternalShare"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(internalShare)]
                               ];
    
    self.navigationItem.rightBarButtonItems = actionButtons;
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavHeader"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    if(_url){
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:[_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        [_webView loadRequest:requestObj];
        [_urlLabel setText:_url];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:WebDetailToShareSegue]){
        ((ShareViewController *)segue.destinationViewController).itemDetails = _itemDetails;
    }
}


-(void)setURL:(NSString *)url{
    _url = [url stringByRemovingPercentEncoding];
}

-(void)setImage:(UIImage *)thumbnailImage{
    _thumbnailImage = thumbnailImage;
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if([[RequestController sharedInstance] isNetworkReachable]){
        [_activityIndicator startAnimating];
        return YES;
    } else {
        return NO;
    }
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [_activityIndicator stopAnimating];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [[HelperModule sharedInstance] showNetworkToast:YES message:[error localizedDescription] animated:YES];
    [_activityIndicator stopAnimating];
}

-(void)sendUserViewDuration{
    
}

-(void)shareCard{
    [self performSegueWithIdentifier:WebDetailToShareSegue sender:nil];
}

-(void)externalShare{
    NSArray *itemsToShare = @[@"Check this out",[NSURL URLWithString:[_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]],_thumbnailImage?:[UIImage imageNamed:@"Logo"]];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[];
    activityVC.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Activity Status: %@ : %@", activityType, @"OK");
        });
        if (completed){
            NSLog(@"The Activity: %@ was completed", activityType);
        } else {
            NSLog(@"The Activity: %@ was NOT completed", activityType);
        }
        
    };
    [self presentViewController:activityVC animated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    }];
    
}

-(void)internalShare{
    [self shareCard];
}

@end
