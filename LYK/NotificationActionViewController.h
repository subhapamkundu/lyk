//
//  NotificationActionViewController.h
//  LYK
//
//  Created by Subhapam on 30/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NotificationActionDelegate <NSObject>

-(void)didPerformActionForNotificationId:(NSString *)notificationId;

@end

@interface NotificationActionViewController : UIViewController

@property (nonatomic, weak) NSDictionary *details;
@property (nonatomic, weak) NSString *shouldFetchDetailsWithId;
@property (nonatomic, weak) id<NotificationActionDelegate> delegate;

@end
