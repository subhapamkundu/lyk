//
//  RequestController.m
//  LYK
//
//  Created by Subhapam on 16/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "RequestController.h"
#import <AFNetworking/AFNetworking.h>


//#define LYKBaseURL                  @"http://wbphlox.com/lykjwt/"
//#define LYKBaseURL                  @"http://52.21.205.216:8080/lykjwt/"
#define LYKBaseURL                  @"http://www.signofme.com:8080/lykjwt/"



#define LYKLoginAPI                         @"index.php?/user/login"
#define LYKRegisterAPI                      @"index.php?/user/"
#define LYKSocialLoginAPI                   @"index.php?/user/socialLogin"
#define LYKSocialRegisterAPI                @"index.php?/user/socialRegister"
#define LYKEmailExistAPI                    @"index.php?/user/isUserExist"
#define LYKChangeTempAPI                    @"index.php?/user/changeTempPassword"
#define LYKForgotPasswordAPI                @"index.php?/user/userForgetPassword"
#define LYKUpdateUserAPI                    @"index.php?/user/update"
#define LYKRelatedDataSetAPI                @"index.php?/RelatedData/getRelatedDataCards"
#define LYKGetRelationsAPI                  @"index.php?/user/getRelations"
#define LYKUpdateProfileAPI                 @"index.php?/user/edit"
#define LYKGetSettingsAPI                   @"index.php?/user/settings"
#define LYKAcceptRejectRelationshipAPI      @"index.php?/user/acceptRelation"
#define LYKSaveUpdateInterestsAPI           @"index.php?/user/saveUserInterests"
#define LYKUploadImageAPI                   @"index.php?/user/uploadUserImage"
#define LYKGetInterestsAPI                  @"index.php?/user/getUserInterests"
#define LYKChangePasswordAPI                @"index.php?/user/changePassword"
#define LYKRemoveRelationshipAPI            @"index.php?/user/removeRelation"
#define LYKSOSListByUserAPI                 @"index.php?/sos/sosByUser"
#define LYKSOSListForUserAPI                @"index.php?/sos/sosForUser"
#define LYKRegionByUserAPI                  @"index.php?/restrictedplace/getRestrictedPlacesByUser"
#define LYKRegionForUserAPI                 @"index.php?/restrictedplace/getRestrictedPlacesForUser"
#define LYKRemoveSOSAPI                     @"index.php?/sos/delete/"
#define LYKRemoveRegionAPI                  @"index.php?/restrictedplace/delete"
#define LYKNewSOSAPI                        @"index.php?/sos/"
#define LYKNewRegionAPI                     @"index.php?/restrictedplace/"
//#define LYKNewTodoAPI                       @"index.php?/toDo"
//#define LYKNewEventAPI                      @"index.php?/events/"
#define LYKNewTaskAPI                       @"index.php?/organizer/createShareOrganizerActivity"
#define LYKUpcomingEventAPI                 @"index.php?/organizer/getOrganizerActivities"
#define LYKRemoveOrganizerActivityAPI       @"index.php?/organizer/deleteOrganizerActivity"
#define LYKExploreDataSetAPI                @"index.php?/explore/productsRandomDataset"
#define LYKExploreCategoriesAPI             @"index.php?/explore/getAllCategorySubcategory/"
#define LYKExploreSubCategoriesAPI          @"index.php?/explore/category/"
#define LYKAnalyticsDataSetAPI              @"index.php?/analytics/getAnalyticsData"
#define LYKNotificationListAPI              @"index.php?/Notify/fetchNotificationsList"
#define LYKNotificationDetailsAPI           @"index.php?/Notify/fetchNotification"
#define LYKFetchProductsAPI                 @"index.php?/explore/productsByCategorySubCategory"
#define LYKFetchProductDetailsAPI           @"index.php?/explore/getProductDetails"
#define LYKTrackTimeSpentAPI                @"index.php?/RelatedData/insertTimeSpent"
#define LYKAddMemberLocationAPI             @"index.php?/restrictedplace/addMemberLocation"
#define LYKNotificationActionAPI            @"index.php?/Notify/acceptNotification"
#define LYKShareCardActionAPI               @"index.php?/analytics/share"
#define LYKSubmitAnswerActionAPI            @"index.php?/relatedData/giveAnswer"
#define LYKSetUserStatusAPI                 @"index.php?/user/setStatus"



#ifndef NetworkRequestJSONKey
#define NetworkRequestJSONKey

#define JSONKey_APIKey                                          @"apiKey"
#define JSONKey_UserType                                        @"userType"
#define JSONKey_DeviceType                                      @"deviceType"
#define JSONKey_DeviceId                                        @"deviceId"
#define JSONKey_DeviceToken                                     @"pushKeyString"
#define JSONKey_Identity                                        @"identity"
#define JSONKey_Password                                        @"password"
#define JSONKey_UserId                                          @"userId"
#define JSONKey_CheckType                                       @"checkType"
#define JSONKey_PushDetails                                     @"push"



#endif


#ifndef NetworkRequestConfigString
#define NetworkRequestConfigString

#define NetworkRequestHTTPMethodPOST                            @"POST"
#define NetworkRequestHTTPMethodGET                             @"GET"
#define NetworkRequestHeaderValueJSON                           @"application/json"
#define NetworkRequestHeaderContentType                         @"Content-Type"
#define NetworkRequestHeaderAccept                              @"Accept"
#define NetworkRequestHeaderValueMultipart                      [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]


#endif

#ifndef NetworkRequestErrorString
#define NetworkRequestErrorString

#define NetworkRequestErrorDomain                               @"NetworkError"
#define NetworkRequest_NetworkUnavailableKey                    @"Network Unavailable"

#endif



@interface RequestController()
{
    AFNetworkReachabilityStatus afNetworkReachabilityStatus;
    UILabel *networkStatusLabel;
    NSTimer *networkValidatorTimer;
}

@end


@implementation RequestController

+(id)sharedInstance{
    static RequestController *sharedInstance_ = nil;
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedInstance_ = [[RequestController alloc] init];
    });
    return sharedInstance_;
}

-(NSInteger)isNetworkReachable {
    if(!afNetworkReachabilityStatus){
        if(!networkStatusLabel){
            networkStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 60)];
            [networkStatusLabel setText:@"Network Unavailable"];
            [networkStatusLabel setTextAlignment:NSTextAlignmentCenter];
            [networkStatusLabel setFont:[UIFont fontWithName:@"Futura" size:16]];
            [networkStatusLabel setBackgroundColor:[UIColor redColor]];
            [[[[UIApplication sharedApplication] delegate] window] addSubview:networkStatusLabel];
            [[[[UIApplication sharedApplication] delegate] window] bringSubviewToFront:networkStatusLabel];
            networkValidatorTimer = [NSTimer timerWithTimeInterval:10 target:self selector:@selector(isNetworkReachable) userInfo:nil repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:networkValidatorTimer forMode:NSRunLoopCommonModes];
        }
        
    } else {
        [networkStatusLabel removeFromSuperview];
        [networkValidatorTimer invalidate];
        networkValidatorTimer = nil;
        networkStatusLabel = nil;
    }
    return afNetworkReachabilityStatus;
}

-(void)startMonitoringNetwork
{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
        afNetworkReachabilityStatus = status;
    }];
}

-(void)checkIfUserExistWithEmail:(NSString *)email successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *checkData = [NSMutableDictionary dictionary];
        [checkData setObject:email forKey:JSONKey_Identity];
        [checkData setObject:@"email" forKey:JSONKey_CheckType];
        [checkData setObject:DeviceID forKey:JSONKey_DeviceId];
        NSError *jsonError;
        NSMutableURLRequest *checkRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKEmailExistAPI)]];
        checkRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:checkData options:NSJSONWritingPrettyPrinted error:&jsonError];
        checkRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [checkRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [checkRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:checkRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}


-(void)sendRegisterRequestWithCredentials:(NSDictionary *)credentials successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock {
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *registerData = [NSMutableDictionary dictionary];
        [registerData setObject:credentials forKey:@"rootAc"];
        [registerData setObject:DeviceID forKey:JSONKey_DeviceId];
        NSError *jsonError;
        NSMutableURLRequest *registerRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKRegisterAPI)]];
        registerRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:registerData options:NSJSONWritingPrettyPrinted error:&jsonError];
        registerRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [registerRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [registerRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:registerRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)sendSocialRegisterRequestWithCredentials:(NSDictionary *)credentials successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock {
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *registerData = [NSMutableDictionary dictionaryWithDictionary:credentials];
        [registerData setObject:DeviceID forKey:JSONKey_DeviceId];
        NSError *jsonError;
        NSMutableURLRequest *registerRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKSocialRegisterAPI)]];
        registerRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:registerData options:NSJSONWritingPrettyPrinted error:&jsonError];
        registerRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [registerRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [registerRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:registerRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}


-(void)sendLoginRequestWithUsername:(NSString *)username password:(NSString *)password successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock {
    if(afNetworkReachabilityStatus){
        NSDictionary *pushDetails = nil;
        if([[ApplicationData sharedInstance] deviceToken])
            pushDetails = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"ios", JSONKey_DeviceType,
                           [[ApplicationData sharedInstance] deviceId], JSONKey_DeviceId,
                           [[ApplicationData sharedInstance] deviceToken], JSONKey_DeviceToken,nil];
        NSMutableDictionary *loginData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   username, JSONKey_Identity,
                                   password, JSONKey_Password, nil];
        if(pushDetails)
            [loginData setObject:pushDetails forKey:JSONKey_PushDetails];
        NSError *jsonError;
        NSMutableURLRequest *loginRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKLoginAPI)]];
        loginRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:loginData options:NSJSONWritingPrettyPrinted error:&jsonError];
        loginRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [loginRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [loginRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:loginRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)sendSocialLoginRequestWithCredential:(NSDictionary *)credential successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *socialLoginData = [[NSDictionary dictionaryWithDictionary:credential] mutableCopy];
        NSError *jsonError;
        NSMutableURLRequest *loginRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKSocialLoginAPI)]];
        loginRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:socialLoginData options:NSJSONWritingPrettyPrinted error:&jsonError];
        loginRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [loginRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [loginRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:loginRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)sendChangeTempPasswordRequest:(NSDictionary *)credentials successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *changeTempData = [[NSDictionary dictionaryWithDictionary:credentials] mutableCopy];
        [changeTempData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        NSError *jsonError;
        NSMutableURLRequest *loginRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKChangeTempAPI)]];
        loginRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:changeTempData options:NSJSONWritingPrettyPrinted error:&jsonError];
        loginRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [loginRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [loginRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:loginRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)sendForgotPasswordRequest:(NSString *)username successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSDictionary *forgotPasswordData = [NSDictionary dictionaryWithObjectsAndKeys:
                                            username, JSONKey_Identity, nil];
        NSError *jsonError;
        NSMutableURLRequest *loginRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKForgotPasswordAPI)]];
        loginRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:forgotPasswordData options:NSJSONWritingPrettyPrinted error:&jsonError];
        loginRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [loginRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [loginRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:loginRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)addMemberWithCredentials:(NSArray *)members successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *addMemberData = [NSMutableDictionary dictionary];
//        NSMutableDictionary *childSegment = [NSMutableDictionary dictionary];
//        for (int index = 0; index < [members count]; index++) {
//            [childSegment setObject:[members objectAtIndex:index] forKey:[NSString stringWithFormat:@"%d",index+1]];
//        }
        [addMemberData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [addMemberData setObject:members forKey:@"childs"];
        [addMemberData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        NSError *jsonError;
        NSMutableURLRequest *loginRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKUpdateUserAPI)]];
        loginRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:addMemberData options:NSJSONWritingPrettyPrinted error:&jsonError];
        loginRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [loginRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [loginRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:loginRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getHomeListWithMaxCredentials:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:requestJSON];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *dataSetRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKRelatedDataSetAPI)]];
        dataSetRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        dataSetRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:dataSetRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getRelationsWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionary];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *getRelationsRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKGetRelationsAPI)]];
        getRelationsRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        getRelationsRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [getRelationsRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [getRelationsRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:getRelationsRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)updateProfileWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:details];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *getRelationsRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKUpdateProfileAPI)]];
        getRelationsRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        getRelationsRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [getRelationsRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [getRelationsRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:getRelationsRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)acceptRejectRelationshipWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:details];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *getRelationsRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKAcceptRejectRelationshipAPI)]];
        getRelationsRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        getRelationsRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [getRelationsRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [getRelationsRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:getRelationsRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)saveUpdateInterestsWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:details];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *getRelationsRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKSaveUpdateInterestsAPI)]];
        getRelationsRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        getRelationsRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [getRelationsRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [getRelationsRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:getRelationsRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getSettingsWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableURLRequest *getRelationsRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKGetSettingsAPI)]];
        getRelationsRequest.HTTPMethod = NetworkRequestHTTPMethodGET;
        [getRelationsRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [getRelationsRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:getRelationsRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)uploadImageWithData:(NSData *)imageData successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableData *body = [NSMutableData data];
        NSString *boundary = @"ImageDataBounday";
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userImage\"; filename=\"test.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSMutableURLRequest *uploadImageRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKUploadImageAPI)]];
        uploadImageRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [uploadImageRequest addValue:NetworkRequestHeaderValueMultipart forHTTPHeaderField:NetworkRequestHeaderContentType];
        [uploadImageRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [uploadImageRequest setHTTPBody:body];
        [self sendRequestWithRequest:uploadImageRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getUserInterestsWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionary];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *getRelationsRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKGetInterestsAPI)]];
        getRelationsRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        getRelationsRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [getRelationsRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [getRelationsRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:getRelationsRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)changePasswordRequest:(NSDictionary *)credentials successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *changeTempData = [[NSDictionary dictionaryWithDictionary:credentials] mutableCopy];
        [changeTempData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [changeTempData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        NSError *jsonError;
        NSMutableURLRequest *loginRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKChangePasswordAPI)]];
        loginRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:changeTempData options:NSJSONWritingPrettyPrinted error:&jsonError];
        loginRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [loginRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [loginRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:loginRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)removeRelationshipRequest:(NSString *)relativeId successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *rejectRelationshipData = [NSMutableDictionary dictionary];
        [rejectRelationshipData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [rejectRelationshipData setObject:relativeId forKey:@"relativeId"];
        [rejectRelationshipData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        NSError *jsonError;
        NSMutableURLRequest *rejectRelationshipRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKRemoveRelationshipAPI)]];
        rejectRelationshipRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:rejectRelationshipData options:NSJSONWritingPrettyPrinted error:&jsonError];
        rejectRelationshipRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [rejectRelationshipRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [rejectRelationshipRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:rejectRelationshipRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getSOSListByUserWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionary];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *getSOSListByUserRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKSOSListByUserAPI)]];
        getSOSListByUserRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        getSOSListByUserRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [getSOSListByUserRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [getSOSListByUserRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:getSOSListByUserRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getSOSListForUserWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionary];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *getSOSListForUserRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKSOSListForUserAPI)]];
        getSOSListForUserRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        getSOSListForUserRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [getSOSListForUserRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [getSOSListForUserRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:getSOSListForUserRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getRegionListByUserWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionary];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *getRegionListByUserRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKRegionByUserAPI)]];
        getRegionListByUserRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        getRegionListByUserRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [getRegionListByUserRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [getRegionListByUserRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:getRegionListByUserRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getRegionListForUserWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionary];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *getRegionListForUserRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKRegionForUserAPI)]];
        getRegionListForUserRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        getRegionListForUserRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [getRegionListForUserRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [getRegionListForUserRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:getRegionListForUserRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)removeSOSRequestWithId:(NSString *)sosId successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *removeSOSData = [NSMutableDictionary dictionary];
        [removeSOSData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [removeSOSData setObject:sosId forKey:@"sosId"];
        [removeSOSData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        NSError *jsonError;
        NSMutableURLRequest *removeSOSRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKRemoveSOSAPI)]];
        removeSOSRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:removeSOSData options:NSJSONWritingPrettyPrinted error:&jsonError];
        removeSOSRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [removeSOSRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [removeSOSRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:removeSOSRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)removeRegionRequestWithId:(NSString *)regionId successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *removeRegionData = [NSMutableDictionary dictionary];
        [removeRegionData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [removeRegionData setObject:regionId forKey:@"placeId"];
        [removeRegionData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        NSError *jsonError;
        NSMutableURLRequest *removeRegionRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKRemoveRegionAPI)]];
        removeRegionRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:removeRegionData options:NSJSONWritingPrettyPrinted error:&jsonError];
        removeRegionRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [removeRegionRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [removeRegionRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:removeRegionRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)addSOSNumberWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *newSOSData = [NSMutableDictionary dictionaryWithDictionary:details];
        [newSOSData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [newSOSData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        NSError *jsonError;
        NSMutableURLRequest *newSOSRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKNewSOSAPI)]];
        newSOSRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:newSOSData options:NSJSONWritingPrettyPrinted error:&jsonError];
        newSOSRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [newSOSRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [newSOSRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:newSOSRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)addSafetyRegionWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *newRegionData = [NSMutableDictionary dictionaryWithDictionary:details];
        [newRegionData setObject:[[UserData sharedInstance] getUserId] forKey:@"addedBy"];
        [newRegionData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        NSError *jsonError;
        NSMutableURLRequest *newRegionRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKNewRegionAPI)]];
        newRegionRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:newRegionData options:NSJSONWritingPrettyPrinted error:&jsonError];
        newRegionRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [newRegionRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [newRegionRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:newRegionRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

//-(void)addTodoWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
//    if(afNetworkReachabilityStatus){
//        NSMutableDictionary *newTodoData = [NSMutableDictionary dictionaryWithDictionary:details];
//        [newTodoData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
//        [newTodoData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
//        NSError *jsonError;
//        NSMutableURLRequest *newTodoRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKNewTodoAPI)]];
//        newTodoRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:newTodoData options:NSJSONWritingPrettyPrinted error:&jsonError];
//        newTodoRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
//        [newTodoRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
//        [newTodoRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
//        [self sendRequestWithRequest:newTodoRequest successHandler:successBlock failureHandler:failureBlock];
//    } else {
//        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
//    }
//}
//
//-(void)addEventWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
//    if(afNetworkReachabilityStatus){
//        NSMutableDictionary *newEventData = [NSMutableDictionary dictionaryWithDictionary:details];
//        [newEventData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
//        [newEventData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
//        NSError *jsonError;
//        NSMutableURLRequest *newEventRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKNewEventAPI)]];
//        newEventRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:newEventData options:NSJSONWritingPrettyPrinted error:&jsonError];
//        newEventRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
//        [newEventRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
//        [newEventRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
//        [self sendRequestWithRequest:newEventRequest successHandler:successBlock failureHandler:failureBlock];
//    } else {
//        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
//    }
//}

-(void)addTaskWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *newTaskData = [NSMutableDictionary dictionaryWithDictionary:details];
        [newTaskData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [newTaskData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        NSError *jsonError;
        NSMutableURLRequest *newTaskRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKNewTaskAPI)]];
        newTaskRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:newTaskData options:NSJSONWritingPrettyPrinted error:&jsonError];
        newTaskRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [newTaskRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [newTaskRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:newTaskRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getEventListWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *upcomingEventData = [NSMutableDictionary dictionary];
        [upcomingEventData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [upcomingEventData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        NSError *jsonError;
        NSMutableURLRequest *upcomingEventRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKUpcomingEventAPI)]];
        upcomingEventRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:upcomingEventData options:NSJSONWritingPrettyPrinted error:&jsonError];
        upcomingEventRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [upcomingEventRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [upcomingEventRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:upcomingEventRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)removeOrganizerActivityWithId:(NSString *)organizerActivityId successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *removeOrganizerActivityData = [NSMutableDictionary dictionary];
        [removeOrganizerActivityData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [removeOrganizerActivityData setObject:organizerActivityId forKey:@"eventId"];
        [removeOrganizerActivityData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        NSError *jsonError;
        NSMutableURLRequest *removeOrganizerActivityRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKRemoveOrganizerActivityAPI)]];
        removeOrganizerActivityRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:removeOrganizerActivityData options:NSJSONWritingPrettyPrinted error:&jsonError];
        removeOrganizerActivityRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [removeOrganizerActivityRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [removeOrganizerActivityRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:removeOrganizerActivityRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getExploreListWithMaxCredentials:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:requestJSON];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        NSError *jsonError;
        NSMutableURLRequest *dataSetRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKExploreDataSetAPI)]];
        dataSetRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        dataSetRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:dataSetRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getExploreCategoriesWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
//        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:requestJSON];
//        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
//        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
//        NSError *jsonError;
        NSMutableURLRequest *dataSetRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKConcatStrings(LYKBaseURL,LYKExploreCategoriesAPI),[[ApplicationData sharedInstance] authToken])]];
        
//        dataSetRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        dataSetRequest.HTTPMethod = NetworkRequestHTTPMethodGET;
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:dataSetRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getExploreSubcategoriesWithId:(NSString *)categoryId successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionary];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [requestData setObject:categoryId forKey:@"categoryId"];
        NSError *jsonError;
        NSMutableURLRequest *dataSetRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKConcatStrings(LYKBaseURL,LYKExploreSubCategoriesAPI),[[ApplicationData sharedInstance] authToken])]];
        
        dataSetRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        dataSetRequest.HTTPMethod = NetworkRequestHTTPMethodGET;
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:dataSetRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getAnalyiticsListWithMaxCredentials:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:requestJSON];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *dataSetRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKAnalyticsDataSetAPI)]];
        dataSetRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        dataSetRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:dataSetRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getNotificationsWithMaxCredentials:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:requestJSON];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *dataSetRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKNotificationListAPI)]];
        dataSetRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        dataSetRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:dataSetRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getExploreProductsWithCredentials:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:requestJSON];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *dataSetRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKFetchProductsAPI)]];
        dataSetRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        dataSetRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:dataSetRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)getExploreProductDetailsWithCredentials:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:requestJSON];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *dataSetRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKFetchProductDetailsAPI)]];
        dataSetRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        dataSetRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:dataSetRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)trackUserTimeSpentWithCredentials:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:requestJSON];
        [requestData setObject:[[UserData sharedInstance] getUserId] forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken] forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *dataSetRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKTrackTimeSpentAPI)]];
        dataSetRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        dataSetRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:dataSetRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)addMemberLocationWithDetails:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:requestJSON];
        [requestData setObject:[[UserData sharedInstance] getUserId]?:@"" forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken]?:@"" forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *dataSetRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKAddMemberLocationAPI)]];
        dataSetRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        dataSetRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:dataSetRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)sendAcceptanceWithDetails:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:requestJSON];
        [requestData setObject:[[UserData sharedInstance] getUserId]?:@"" forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken]?:@"" forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *dataSetRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKNotificationActionAPI)]];
        dataSetRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        dataSetRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:dataSetRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)submitAnswerForQuestionWithDetails:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *))successBlock failureHandler:(void (^)(NSError *))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:requestJSON];
        [requestData setObject:[[UserData sharedInstance] getUserId]?:@"" forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken]?:@"" forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *dataSetRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKSubmitAnswerActionAPI)]];
        dataSetRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        dataSetRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:dataSetRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}


-(void)shareCardWithDetails:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *))successBlock failureHandler:(void (^)(NSError *))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:requestJSON];
        [requestData setObject:[[UserData sharedInstance] getUserId]?:@"" forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken]?:@"" forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *dataSetRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKShareCardActionAPI)]];
        dataSetRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        dataSetRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:dataSetRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}

-(void)setUserStatusWithDetails:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *))successBlock failureHandler:(void (^)(NSError *))failureBlock{
    if(afNetworkReachabilityStatus){
        NSMutableDictionary *requestData = [NSMutableDictionary dictionaryWithDictionary:requestJSON];
        [requestData setObject:[[UserData sharedInstance] getUserId]?:@"" forKey:Key_UserId];
        [requestData setObject:[[ApplicationData sharedInstance] authToken]?:@"" forKey:Key_Token];
        
        NSError *jsonError;
        NSMutableURLRequest *dataSetRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LYKConcatStrings(LYKBaseURL,LYKSetUserStatusAPI)]];
        dataSetRequest.HTTPBody = [NSJSONSerialization dataWithJSONObject:requestData options:NSJSONWritingPrettyPrinted error:&jsonError];
        dataSetRequest.HTTPMethod = NetworkRequestHTTPMethodPOST;
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderContentType];
        [dataSetRequest addValue:NetworkRequestHeaderValueJSON forHTTPHeaderField:NetworkRequestHeaderAccept];
        [self sendRequestWithRequest:dataSetRequest successHandler:successBlock failureHandler:failureBlock];
    } else {
        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[self networkUnavailableDictionary]]);
    }
}











-(void)sendRequestWithRequest:(NSMutableURLRequest *)request successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock {
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"Response :: %@",httpResponse);
        if(error){
            if(failureBlock){
                failureBlock(error);
            }
        } else if([httpResponse statusCode] != 200){
            if(failureBlock){
                failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:[httpResponse statusCode] userInfo:[NSDictionary dictionaryWithObject:[NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]] forKey:NetworkRequest_ErrorInfo]]);
            }
        } else {
            NSError *jsonError;
            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&jsonError];
            if(jsonError){
                NSLog(@"response JSON error :: %@",jsonError);
                if(failureBlock){
                    failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1002 userInfo:[NSDictionary dictionaryWithObject:@"Please try again later. Server is experiencing some error" forKey:NetworkRequest_ErrorInfo]]);
                }
            }else {
                NSLog(@"response JSON :: %@",jsonResponse);
                if([jsonResponse objectForKey:@"error"]){
                    if(failureBlock){
                        failureBlock([NSError errorWithDomain:NetworkRequestErrorDomain code:-1001 userInfo:[NSDictionary dictionaryWithObject:[jsonResponse objectForKey:@"error"] forKey:NetworkRequest_ErrorInfo]]);
                    }
                } else {
                    if(successBlock){
                        successBlock(jsonResponse);
                    }
                }
            }
        }
    }] resume];
}

-(NSDictionary *)networkUnavailableDictionary{
    return [NSDictionary dictionaryWithObject:NetworkRequest_NetworkUnavailableKey forKey:NetworkRequest_ErrorInfo];
}

@end
