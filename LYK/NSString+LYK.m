//
//  NSString+LYK.m
//  LYK
//
//  Created by Subhapam on 01/02/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import "NSString+LYK.h"

@implementation NSString (LYK)

- (CGSize)getHeightHavingWidth:(CGFloat)widthValue andFont:(UIFont *)font {
    CGSize size = CGSizeZero;
        //iOS 7
    size = [self sizeWithFont:font constrainedToSize:CGSizeMake(widthValue, FLT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    size = [self sizeWithAttributes:@{NSFontAttributeName: font}];
    CGRect frame = [self boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:font } context:nil];
    size = CGSizeMake(frame.size.width, frame.size.height + 1);
    size = CGSizeMake(size.width, size.height + 1);
    return size;
}

@end
