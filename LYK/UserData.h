//
//  UserData.h
//  LYK
//
//  Created by Subhapam on 18/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <Foundation/Foundation.h>

//@interface FlipDataSet : NSObject
//
//@property (nonatomic) NSInteger maxNewsId;
//@property (nonatomic) NSInteger maxInsightId;
//@property (nonatomic) NSInteger maxQuestionId;
//@property (nonatomic) NSInteger maxOfferId;
//@property (nonatomic) NSInteger maxProductId;
//@property (nonatomic) NSMutableArray *contents;
//
//-(BOOL)parseDataSetWithDetails:(NSDictionary *)json;
//
//@end


@interface Relationship : NSObject

/*
 NSString *relationshipId;
 NSString *relationshipPhone;
 NSString *relationshipEmail;
 NSString *relationshipFirstName;
 NSString *relationshipMiddleName;
 NSString *relationshipLastName;
 NSString *relationshipRoleId;
 NSString *relationshipChildId;
 NSString *relationshipParentId;
 NSString *relationshipState;
 BOOL relationshipAccepted;
 */

-(BOOL)parseRelationshipDataWithDetails:(NSDictionary *)json;
-(BOOL)parsePendingRelationsDataWithJSON:(NSDictionary *)json;


-(NSString *)getRelationshipId;
-(NSString *)getUserName;
-(NSString *)getPhone;
-(NSString *)getEmail;
-(NSString *)getRelationDescription;
-(NSString *)getRelationRoleId;
-(NSString *)getChildId;
-(NSString *)getParentId;
-(NSString *)getRelationImageURL;

-(void)setFirstName:(NSString *)fname;
-(void)setMiddleName:(NSString *)mname;
-(void)setLastName:(NSString *)lname;
-(void)setPhone:(NSString *)contactNo;
-(void)setEmail:(NSString *)email;
-(void)setRelationDescription:(NSString *)desc;
-(void)setRoleId:(NSString *)roleId;



@end


@interface SOSData : NSObject

-(BOOL)parseSOSDataWithDetails:(NSDictionary *)json;

@end


@interface UserData : NSObject

//@property (nonatomic) FlipDataSet *dataSet;

+(id)sharedInstance;

-(BOOL)parseUserDataWithJSON:(NSDictionary *)json;
-(BOOL)parseRelationDataWithJSON:(NSDictionary *)json;
-(BOOL)parsePendingRelationsDataWithJSON:(NSDictionary *)json;
//-(BOOL)parseFlipDataSetWithJSON:(NSDictionary *)json;
-(BOOL)parseSOSDataSetWithJSON:(NSDictionary *)json;


-(NSString *)getUserName;
-(NSString *)getEmailId;
-(NSString *)getProfilePicURL;
-(NSMutableArray *)getMembersArray;
-(NSMutableArray *)getMembersArrayWithSelf;
-(NSMutableArray *)getPendingMembersArray;
-(NSUInteger)getRoleId;
-(NSString *)getUserId;
-(NSString *)getDOB;
-(NSString *)getUserFirstName;
-(NSString *)getUserMiddleName;
-(NSString *)getUserLastName;
-(NSString *)getUserPhoneNumber;
-(NSString *)getUserPhoneNumberCountryCode;
-(NSInteger)getNotificationCount;
//-(NSMutableArray *)getSOSListByUser;
//-(NSMutableArray *)getSOSListForUser;
-(NSString *)getUserStatus;


-(void)setUserId:(NSString *)uid;
-(void)setEmailId:(NSString *)email;
-(void)setFirstName:(NSString *)fname;
-(void)setMiddleName:(NSString *)mname;
-(void)setLastName:(NSString *)lname;
-(void)setPhoneNumber:(NSString *)number;
-(void)setPhoneNumberCountryCode:(NSString *)number;
-(void)setNotificationCount:(NSInteger)count;
-(void)setStatus:(NSString *)status;

-(NSDictionary *)getUserDetails;

-(void)clearUserDetails;


@end
