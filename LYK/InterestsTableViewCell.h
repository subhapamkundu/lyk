//
//  InterestsTableViewCell.h
//  LYK
//
//  Created by Subhapam on 16/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InterestsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkImageView;
@end
