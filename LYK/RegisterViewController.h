//
//  RegisterViewController.h
//  LYK
//
//  Created by Subhapam on 17/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController

@property (weak, nonatomic) NSMutableDictionary *socialDetails;

@end
