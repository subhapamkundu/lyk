//
//  RequestController.h
//  LYK
//
//  Created by Subhapam on 16/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestController : NSObject

+(id)sharedInstance;

-(void)startMonitoringNetwork;
-(NSInteger)isNetworkReachable;

-(void)checkIfUserExistWithEmail:(NSString *)email successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)sendRegisterRequestWithCredentials:(NSDictionary *)credentials successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)sendSocialRegisterRequestWithCredentials:(NSDictionary *)credentials successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)sendLoginRequestWithUsername:(NSString *)username password:(NSString *)password successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)sendSocialLoginRequestWithCredential:(NSDictionary *)credential successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)sendChangeTempPasswordRequest:(NSDictionary *)credentials successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)sendForgotPasswordRequest:(NSString *)username successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)addMemberWithCredentials:(NSArray *)members successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getHomeListWithMaxCredentials:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getRelationsWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)updateProfileWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)acceptRejectRelationshipWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)saveUpdateInterestsWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getSettingsWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)uploadImageWithData:(NSData *)imageData successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getUserInterestsWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)changePasswordRequest:(NSDictionary *)credentials successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)removeRelationshipRequest:(NSString *)relativeId successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getSOSListByUserWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getSOSListForUserWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getRegionListByUserWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getRegionListForUserWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)removeSOSRequestWithId:(NSString *)sosId successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)removeRegionRequestWithId:(NSString *)regionId successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)addSOSNumberWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)addSafetyRegionWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

//-(void)addTodoWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;
//
//-(void)addEventWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)addTaskWithDetails:(NSDictionary *)details successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getEventListWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)removeOrganizerActivityWithId:(NSString *)organizerActivityId successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getExploreListWithMaxCredentials:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getExploreCategoriesWithSuccessHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getExploreSubcategoriesWithId:(NSString *)categoryId successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getAnalyiticsListWithMaxCredentials:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getNotificationsWithMaxCredentials:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getExploreProductsWithCredentials:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)getExploreProductDetailsWithCredentials:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)trackUserTimeSpentWithCredentials:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)addMemberLocationWithDetails:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)sendAcceptanceWithDetails:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)shareCardWithDetails:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *response))successBlock failureHandler:(void (^)(NSError *error))failureBlock;

-(void)submitAnswerForQuestionWithDetails:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *))successBlock failureHandler:(void (^)(NSError *))failureBlock;

-(void)setUserStatusWithDetails:(NSDictionary *)requestJSON successHandler:(void (^)(NSDictionary *))successBlock failureHandler:(void (^)(NSError *))failureBlock;

@end
