//
//  SafetyActionViewController.h
//  LYK
//
//  Created by Subhapam on 01/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ActionDisplayState)
{
    ActionDisplayNone = 0,
    ActionDisplayNewSOS,
    ActionDisplayAddedSOSByMe,
    ActionDisplayAddedSOSByOther,
    ActionDisplayNewRegion,
    ActionDisplayAddedRegionByMe,
    ActionDisplayAddedRegionByOther
};

@interface SafetyActionViewController : UIViewController

@property (nonatomic, weak)NSDictionary *detailsToEdit;

-(void)setActionViewState:(ActionDisplayState)state;

@end
