//
//  OrganizerActionViewController.m
//  LYK
//
//  Created by Subhapam on 02/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import "OrganizerActionViewController.h"
#import "SelectRelationTableViewCell.h"
#import <UIImageView+AFNetworking.h>

@interface OrganizerActionViewController ()<UITextFieldDelegate>
{
    UIDatePicker *datePicker;
    UIDatePicker *timePicker;
    NSMutableDictionary *selectedUsers;
    NSArray *relations;
    UIActivityIndicatorView *activityIndicator;
    UITextField *selectedTextField;
    UIRefreshControl *refreshControl;
}

@property (weak, nonatomic) IBOutlet LYKTextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet LYKTextField *startDateTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *startTimeTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *endDateTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *endTimeTextField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

@implementation OrganizerActionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createDatePicker];
    [self createTimePicker];
    _startDateTextField.inputView = datePicker;
    _startTimeTextField.inputView = timePicker;
    _endDateTextField.inputView = datePicker;
    _endTimeTextField.inputView = timePicker;
    refreshControl = [[UIRefreshControl alloc]init];
    [_tableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(getRelations) forControlEvents:UIControlEventValueChanged];
    relations = [[UserData sharedInstance] getMembersArrayWithSelf];
    if(_selectedActivity){
        for (Relationship *relation in [[UserData sharedInstance] getMembersArrayWithSelf]) {
            NSString *relId = [relation getRelationshipId];
            for (NSDictionary *dict in [_selectedActivity objectForKey:@"sharedWithUser"]) {
                NSString *sharedWithId = [dict objectForKey:@"userId"];
                if([sharedWithId isEqualToString:relId]){
                    if(!selectedUsers)
                        selectedUsers = [NSMutableDictionary dictionary];
                    [selectedUsers setObject:relation forKey:relId];
                }
            }
        }
        [_tableView reloadData];
        _tableView.allowsSelection = false;
        _startDateTextField.clearButtonMode = UITextFieldViewModeNever;
        _endDateTextField.clearButtonMode = UITextFieldViewModeNever;
        _startTimeTextField.clearButtonMode = UITextFieldViewModeNever;
        _endTimeTextField.clearButtonMode = UITextFieldViewModeNever;
        [_titleTextField setText:[_selectedActivity objectForKey:@"eventSubject"]];
        [_descriptionTextView setText:[_selectedActivity objectForKey:@"eventContent"]];
        [_startDateTextField setText:[_selectedActivity objectForKey:@"eventStartDate"]];
        [_endDateTextField setText:[_selectedActivity objectForKey:@"eventEndDate"]];
        [_startTimeTextField setText:[_selectedActivity objectForKey:@"startTime"]];
        [_endTimeTextField setText:[_selectedActivity objectForKey:@"endTime"]];
        NSArray *actionButtons = @[
                                   [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"ExternalShare"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(externalShare)],
                                   [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"InternalShare"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(internalShare)]
                                   ];
        
        self.parentViewController.navigationItem.rightBarButtonItems = actionButtons;
        _startDateTextField.userInteractionEnabled = false;
        _startTimeTextField.userInteractionEnabled = false;
        _endDateTextField.userInteractionEnabled = false;
        _endTimeTextField.userInteractionEnabled = false;
//        _descriptionTextView.userInteractionEnabled = false;
        _titleTextField.userInteractionEnabled = false;
    } else {
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveTask)]];
        _tableView.allowsSelection = true;
    }
    
    _descriptionTextView.layer.cornerRadius = 3.0f;
    _descriptionTextView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _descriptionTextView.layer.borderWidth = 1.0f;
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavHeader"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)externalShare{
    NSString *shareText = @"This text I am sharing";
    NSArray *itemsToShare = @[shareText,[NSURL URLWithString:@"http://www.google.com"]];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[];
    activityVC.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Activity Status: %@ : %@", activityType, @"OK");
        });
        if (completed){
            NSLog(@"The Activity: %@ was completed", activityType);
        } else {
            NSLog(@"The Activity: %@ was NOT completed", activityType);
        }
        
    };
    [self presentViewController:activityVC animated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    }];
    
}

-(void)internalShare{
    
}

-(void)saveTask{
    if([allTrim(_titleTextField.text) isEqualToString:@""]){
        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Please enter a title" animated:YES];
        return;
    } else {
        
        NSString *title = [_titleTextField text];
        NSString *description = [_descriptionTextView text];
        NSString *startDate = [_startDateTextField text];
        NSString *startTime = [_startTimeTextField text];
        NSString *endDate = [_endDateTextField text];
        NSString *endTime = [_endTimeTextField text];
        NSArray *users = [selectedUsers allKeys];
        
        BOOL isDateTimeCombinationValidated = [self validateStartEndDateTimeCombination];
        if(!isDateTimeCombinationValidated){
            return;
        }
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterShortStyle];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *eventEntryDate = [dateFormatter stringFromDate:[NSDate date]];
        NSMutableDictionary *details = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        title, @"eventSubject",
                                        description, @"eventContent",
                                        startDate, @"eventStartDate",
                                        endDate, @"eventEndDate",
                                        users?:@[[[UserData sharedInstance] getUserId]], @"shareWithUser",
                                        startTime?:@"",@"startTime",
                                        endTime?:@"",@"endTime",
                                        @"1", @"eventImportanceFlag",
                                        eventEntryDate,@"eventEntryDate", nil];
        if([[RequestController sharedInstance] isNetworkReachable]){
            if(!activityIndicator){
                activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
                activityIndicator.layer.cornerRadius = 05;
                activityIndicator.opaque = NO;
                activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
                activityIndicator.center = self.view.center;
                activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
                [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
                [self.view addSubview: activityIndicator];
            }
            self.view.userInteractionEnabled = false;
            [activityIndicator startAnimating];
            if (![allTrim(_endDateTextField.text) isEqualToString:@""] && ![allTrim(_startDateTextField.text) isEqualToString:@""]){
                [details setObject:@"Task" forKey:@"eventType"];
    //            [[RequestController sharedInstance] addTaskWithDetails:details successHandler:^(NSDictionary *response) {
    //                dispatch_async(dispatch_get_main_queue(), ^{
    //                    self.view.userInteractionEnabled = true;
    //                    [activityIndicator stopAnimating];
    //                    NSLog(@"data set: %@", response);
    //                    if([_delegate respondsToSelector:@selector(didSaveEvent)]){
    //                        [_delegate performSelector:@selector(didSaveEvent)];
    //                    }
    //                    [[HelperModule sharedInstance] showNetworkToast:YES message:@"Task added successfully" animated:YES];
    //                    [self clearData];
    //                    [self.navigationController popViewControllerAnimated:YES];
    //                });
    //            } failureHandler:^(NSError *error) {
    //                dispatch_async(dispatch_get_main_queue(), ^{
    //                    self.view.userInteractionEnabled = true;
    //                    [activityIndicator stopAnimating];
    //                    NSLog(@"%@",error);
    //                    [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
    //                });
    //            }];
            } else if([allTrim(_endDateTextField.text) isEqualToString:@""] && ![allTrim(_startDateTextField.text) isEqualToString:@""]){
                [details setObject:@"Event" forKey:@"eventType"];
    //            [[RequestController sharedInstance] addEventWithDetails:details successHandler:^(NSDictionary *response) {
    //                dispatch_async(dispatch_get_main_queue(), ^{
    //                    self.view.userInteractionEnabled = true;
    //                    [activityIndicator stopAnimating];
    //                    NSLog(@"data set: %@", response);
    //                    if([_delegate respondsToSelector:@selector(didSaveEvent)]){
    //                        [_delegate performSelector:@selector(didSaveEvent)];
    //                    }
    //                    [[HelperModule sharedInstance] showNetworkToast:YES message:@"Event added successfully" animated:YES];
    //                    [self clearData];
    //                    [self.navigationController popViewControllerAnimated:YES];
    //                });
    //            } failureHandler:^(NSError *error) {
    //                dispatch_async(dispatch_get_main_queue(), ^{
    //                    self.view.userInteractionEnabled = true;
    //                    [activityIndicator stopAnimating];
    //                    NSLog(@"%@",error);
    //                    [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
    //                });
    //            }];
            } else {
                [details setObject:@"Todo" forKey:@"eventType"];
    //            [[RequestController sharedInstance] addTodoWithDetails:details successHandler:^(NSDictionary *response) {
    //                dispatch_async(dispatch_get_main_queue(), ^{
    //                    self.view.userInteractionEnabled = true;
    //                    [activityIndicator stopAnimating];
    //                    NSLog(@"data set: %@", response);
    //                    if([_delegate respondsToSelector:@selector(didSaveEvent)]){
    //                        [_delegate performSelector:@selector(didSaveEvent)];
    //                    }
    //                    [[HelperModule sharedInstance] showNetworkToast:YES message:@"Todo added successfully" animated:YES];
    //                    [self clearData];
    //                    
    //                    [self.navigationController popViewControllerAnimated:YES];
    //                });
    //            } failureHandler:^(NSError *error) {
    //                dispatch_async(dispatch_get_main_queue(), ^{
    //                    self.view.userInteractionEnabled = true;
    //                    [activityIndicator stopAnimating];
    //                    NSLog(@"%@",error);
    //                    [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
    //                });
    //            }];
            }
            [[RequestController sharedInstance] addTaskWithDetails:details successHandler:^(NSDictionary *response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    NSLog(@"data set: %@", response);
                    if([_delegate respondsToSelector:@selector(didSaveEvent)]){
                        [_delegate performSelector:@selector(didSaveEvent)];
                    }
                    [[HelperModule sharedInstance] showNetworkToast:YES message:@"Todo added successfully" animated:YES];
                    [self clearData];
                    
                    [self.navigationController popViewControllerAnimated:YES];
                });
            } failureHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    NSLog(@"%@",error);
                    [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
                });
            }];
        }
    }
}

- (void)createDatePicker {
    
    UIView *pickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 216)];
    pickerView.backgroundColor = [UIColor colorWithRed:109.0/255.0 green:110.0/255.0 blue:120.0/255.0 alpha:1];
    
    datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    
    NSString *minDateString = @"01-Jan-1956";
    NSString *maxDateString = @"31-Dec-2020";
    // the date formatter used to convert string to date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // the specific format to use
    dateFormatter.dateFormat = @"dd-MMM-yyyy";
    // converting string to date
    NSDate *theMinimumDate = [dateFormatter dateFromString: minDateString];
    NSDate *theMaximumDate = [dateFormatter dateFromString: maxDateString];
    // repeat the same logic for theMinimumDate if needed
    
    // here you can assign the max and min dates to your datePicker
    [datePicker setMaximumDate:theMaximumDate]; //the min age restriction
    [datePicker setMinimumDate:[NSDate date]]; //the max age restriction (if needed, or else dont use this line)
    
    // set the mode
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    
    // update the textfield with the date everytime it changes with selector defined below
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
}

- (void)createTimePicker {
    
    UIView *pickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 216)];
    pickerView.backgroundColor = [UIColor colorWithRed:109.0/255.0 green:110.0/255.0 blue:120.0/255.0 alpha:1];
    
    timePicker = [[UIDatePicker alloc]init];
    [timePicker setDate:[NSDate date]];
    
    NSString *minDateString = @"01-Jan-1956";
    NSString *maxDateString = @"31-Dec-2020";
    // the date formatter used to convert string to date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // the specific format to use
    dateFormatter.dateFormat = @"dd-MMM-yyyy";
    // converting string to date
    NSDate *theMinimumDate = [dateFormatter dateFromString: minDateString];
    NSDate *theMaximumDate = [dateFormatter dateFromString: maxDateString];
    // repeat the same logic for theMinimumDate if needed
    
    // here you can assign the max and min dates to your datePicker
//    [timePicker setMaximumDate:theMaximumDate]; //the min age restriction
//    [timePicker setMinimumDate:[NSDate date]]; //the max age restriction (if needed, or else dont use this line)
    
    // set the mode
    [timePicker setDatePickerMode:UIDatePickerModeTime];
    
    // update the textfield with the date everytime it changes with selector defined below
    [timePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    selectedTextField = textField;
    if([selectedTextField.text isEqualToString:@""]){
        if(selectedTextField.inputView == datePicker){
            selectedTextField.text = [self formatDate:datePicker.date];
        } else if (selectedTextField.inputView == timePicker){
            selectedTextField.text = [self formatTime:timePicker.date];
        }
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
}

-(void)updateTextField:(id)sender {
    UIDatePicker *picker = (UIDatePicker*)sender;
    if(picker.datePickerMode == UIDatePickerModeTime){
        [selectedTextField setText:[self formatTime:picker.date]];
    } else {
        [selectedTextField setText:[self formatDate:picker.date]];
    }
}

- (NSString *)formatDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

-(NSString *)formatTime:(NSDate *)time{
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm:ss"]; //24hr time format
    NSString *timeString = [outputFormatter stringFromDate:time];
    return timeString;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[UserData sharedInstance] getMembersArrayWithSelf] count];
}

-(SelectRelationTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectRelationTableViewCell *cell = (SelectRelationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"selectRelationCellIdentifier"];
    if(!cell){
        cell = [[SelectRelationTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"selectRelationCellIdentifier"];
    }
    Relationship *relation = [[[UserData sharedInstance] getMembersArrayWithSelf] objectAtIndex:indexPath.row];
    if([selectedUsers objectForKey:[relation getRelationshipId]]){
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleSelected"]];
    } else {
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleDeselected"]];
    }
    [cell.nameLabel setText:[relation getUserName]];
    [cell.detailsLabel setText:[relation getEmail]];
    cell.profileImageView.layer.cornerRadius = cell.profileImageView.bounds.size.width/2;
    cell.profileImageView.layer.borderColor = [UIColor clearColor].CGColor;
    cell.profileImageView.layer.borderWidth = 1.0f;
    [cell.profileImageView setImageWithURL:[NSURL URLWithString:[relation getRelationImageURL]] placeholderImage:[UIImage imageNamed:@"Avatar.png"]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectRelationTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Relationship *relation = [[[UserData sharedInstance] getMembersArrayWithSelf] objectAtIndex:indexPath.row];
    if(!selectedUsers)
        selectedUsers = [NSMutableDictionary dictionary];
    if(![selectedUsers objectForKey:[relation getRelationshipId]]){
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleSelected"]];
        [selectedUsers setObject:relation forKey:[relation getRelationshipId]];
    } else {
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleDeselected"]];
        [selectedUsers removeObjectForKey:[relation getRelationshipId]];
    }
    
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"Select Family Members";
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(void)clearData{
    _titleTextField.text = @"";
    _descriptionTextView.text = @"";
    _startDateTextField.text = @"";
    _startTimeTextField.text = @"";
    _endDateTextField.text = @"";
    _endTimeTextField.text = @"";
    [selectedUsers removeAllObjects];
    [_tableView reloadData];
}

-(BOOL)validateStartEndDateTimeCombination{
    NSString *startDate = [_startDateTextField text];
    NSString *startTime = [_startTimeTextField text];
    NSString *endDate = [_endDateTextField text];
    NSString *endTime = [_endTimeTextField text];
    
    NSDateFormatter *validateDateFormatterWithTime = [[NSDateFormatter alloc] init];
    [validateDateFormatterWithTime setDateStyle:NSDateFormatterShortStyle];
    [validateDateFormatterWithTime setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    NSDateFormatter *validateDateFormatterWithoutTime = [[NSDateFormatter alloc] init];
    [validateDateFormatterWithoutTime setDateStyle:NSDateFormatterShortStyle];
    [validateDateFormatterWithoutTime setDateFormat:@"yyyy-MM-dd"];
    NSDate *eventStartDate = nil;
    NSDate *eventEndDate = nil;
    
    if(startDate && ![startDate isEqualToString:@""]){
        if(startTime && ![startTime isEqualToString:@""]){
            eventStartDate = [validateDateFormatterWithTime dateFromString:[NSString stringWithFormat:@"%@T%@",startDate,startTime]];
        } else {
            eventStartDate = [validateDateFormatterWithoutTime dateFromString:[NSString stringWithFormat:@"%@",startDate]];
        }
    }
    if(endDate && ![endDate isEqualToString:@""]){
        if(endTime && ![endTime isEqualToString:@""]){
            eventEndDate = [validateDateFormatterWithTime dateFromString:[NSString stringWithFormat:@"%@T%@",endDate,endTime]];
        } else {
            eventEndDate = [validateDateFormatterWithoutTime dateFromString:[NSString stringWithFormat:@"%@",endDate]];
        }
    }
    
    if(eventStartDate){
        if([eventStartDate compare:[NSDate date]] == NSOrderedAscending){
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"Start date & time can't be prior than now" animated:YES];
            return false;
        }
    }
    if(eventEndDate){
        if([eventEndDate compare:[NSDate date]] == NSOrderedAscending){
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"End date & time can't be prior than now" animated:YES];
            return false;
        }
    }
    
    if(eventStartDate && eventEndDate){
        if ([eventEndDate compare:eventStartDate] != NSOrderedDescending){
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"End date & time can't be prior than start date and time" animated:YES];
            return false;
        }
    }
    return true;
}

-(void)getRelations{
    if([[RequestController sharedInstance] isNetworkReachable]){
        [[RequestController sharedInstance] getRelationsWithSuccessHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Success :: %@",response);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [refreshControl endRefreshing];
                if ([response objectForKey:Key_Response]) {
                    if([response objectForKey:Key_Token]){
                        [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:Key_Token]];
                    } else {
                        
                    }
                    [[UserData sharedInstance] parseRelationDataWithJSON:[response objectForKey:Key_Response]];
                    [_tableView reloadData];
                }
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Error :: %@",error);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo] animated:YES];
            });
        }];
    }
}



@end
