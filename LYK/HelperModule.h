//
//  HelperModule.h
//  LYK
//
//  Created by Subhapam on 18/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HelperModule : NSObject

@property (nonatomic, readwrite) UIInterfaceOrientation orientation;
@property (nonatomic, readwrite) BOOL isLandscape;
@property (nonatomic, weak) UIView *toastView;
@property (nonatomic, weak) UILabel *toastLabel;

+(HelperModule *)sharedInstance;

-(void)showNetworkToast:(BOOL)show message:(NSString *)message animated:(BOOL)animated;


@end
