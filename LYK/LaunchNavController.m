//
//  LaunchNavController.m
//  LYK
//
//  Created by Subhapam on 16/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "LaunchNavController.h"
#import "UserDefaultsController.h"
#import <CoreLocation/CoreLocation.h>
#import <AudioToolbox/AudioServices.h>

@interface LaunchNavController ()<CLLocationManagerDelegate>
{
    NSMutableArray *safetyRegionList;
    CLLocationManager *locationManager;
    CLLocation *lastUpdatedLocation;
}
@property (nonatomic) BOOL isPlayingAlarm;
@property (strong, nonatomic) NSMutableArray *enteredSafetyRegions;
@end

@implementation LaunchNavController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.navigationItem.hidesBackButton = YES;
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"Back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(goBack)];
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    locationManager.delegate = self;
    // iOS 8 and higher
    if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        // TODO: Remember to add the NSLocationWhenInUseUsageDescription plist key
        [locationManager requestAlwaysAuthorization];
    }
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9) {
        locationManager.allowsBackgroundLocationUpdates = YES;
    }
    [locationManager startUpdatingLocation];
    [self refreshSafetyListFromUserDefaults];
    
//    [locationManager performSelector:@selector(shouldCacheLocationChange)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)goBack{
    [self popViewControllerAnimated:YES];
}

-(void)refreshSafetyListFromUserDefaults{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    NSDictionary *safetyList = [UserDefaultsController getSafetyListByUserFromUserDefaults];
    for (NSString *sosId in [safetyList allKeys]) {
        if(!safetyRegionList)
            safetyRegionList = [NSMutableArray array];
        [safetyRegionList addObject:[safetyList objectForKey:sosId]];
    }
    
    safetyList = [UserDefaultsController getSafetyListForUserFromUserDefaults];
    for (NSString *sosId in [safetyList allKeys]) {
        if(!safetyRegionList)
            safetyRegionList = [NSMutableArray array];
        [safetyRegionList addObject:[safetyList objectForKey:sosId]];
    }
    NSLog(@"safetyRegionList: %@",safetyRegionList);
    //    [self playSound:0];
    //    [locationManager startMonitoringForRegion:[[CLCircularRegion alloc] initWithCenter: radius: identifier:""]];
}

-(void)updateUnsafeRegionForMonitoringWithCurrentLocation:(CLLocation *)currentLocation{
    NSMutableArray *tempArr = [NSMutableArray array];
    for (NSDictionary *region in safetyRegionList) {
        NSMutableDictionary *tempRegion = [NSMutableDictionary dictionaryWithDictionary:region];
        CLLocationCoordinate2D centre = CLLocationCoordinate2DMake([[region objectForKey:@"centreX"] doubleValue], [[region objectForKey:@"centreY"] doubleValue]);
        CLLocationDistance dist = [currentLocation distanceFromLocation:[[CLLocation alloc] initWithLatitude:centre.latitude longitude:centre.longitude]];
        [tempRegion setObject:[NSNumber numberWithDouble:dist] forKey:@"dist"];
        if(tempArr.count){
            for (int index = 0; index < tempArr.count; index++) {
                if(dist < [[[tempArr objectAtIndex:index] objectForKey:@"dist"] doubleValue]){
                    [tempArr insertObject:tempRegion atIndex:index];
                    break;
                }
                if(index == tempArr.count){
                    [tempArr addObject:tempRegion];
                }
            }
            
        } else {
            [tempArr addObject:tempRegion];
        }
    }
    safetyRegionList = tempArr;
    int maxUnsafeRegionMonitored = (safetyRegionList.count > 20) ? 20 : (int)safetyRegionList.count;
    NSArray *currentMonitoredRegions = [locationManager.monitoredRegions allObjects];
    for (CLRegion *reg in currentMonitoredRegions) {
        [locationManager stopMonitoringForRegion:reg];
    }
    
    for (int index = 0; index < maxUnsafeRegionMonitored; index++) {
        CLLocationCoordinate2D centre = CLLocationCoordinate2DMake([[[safetyRegionList objectAtIndex:index] objectForKey:@"centreX"] doubleValue], [[[safetyRegionList objectAtIndex:index] objectForKey:@"centreY"] doubleValue]);
        CLLocationDistance radius = [[[safetyRegionList objectAtIndex:index] objectForKey:@"radius"] doubleValue];
        CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:centre radius:radius identifier:[[safetyRegionList objectAtIndex:index] objectForKey:@"regionId"]];
        region.notifyOnEntry = true;
        region.notifyOnExit = true;
        [self createNotificationForUnsafeRegionNamed:[[safetyRegionList objectAtIndex:index] objectForKey:@"regionName"]].region = region;
        [locationManager startMonitoringForRegion:region];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *curLocation = [locations lastObject];
    if(lastUpdatedLocation && [curLocation distanceFromLocation:lastUpdatedLocation] > 300){
        [self updateUnsafeRegionForMonitoringWithCurrentLocation:curLocation];
    } else {
    }
    lastUpdatedLocation = curLocation;
    // Output the time the location update was received
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
}

-(void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region{
}

-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region{
    if(!_enteredSafetyRegions){
        _enteredSafetyRegions = [NSMutableArray array];
    }
    [_enteredSafetyRegions addObject:region.identifier];
    //[self playSound:[region.identifier intValue]];
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region{
    [_enteredSafetyRegions removeObject:region.identifier];
}

- (void)playSound:(UInt32)ssID{
    //    SystemSoundID ssID = 0;
    if(!_isPlayingAlarm){
        NSURL *audioPath = [[NSBundle mainBundle] URLForResource:@"alarm1" withExtension:@"aiff"];
        OSStatus error = AudioServicesCreateSystemSoundID((__bridge CFURLRef)audioPath, &ssID);
        if (error == kAudioServicesNoError){
            _isPlayingAlarm = true;
            AudioServicesAddSystemSoundCompletion(ssID, NULL, NULL, (AudioServicesSystemSoundCompletionProc)MyAudioServicesSystemSoundCompletionProc, (__bridge void * _Nullable)(self));
            AudioServicesPlaySystemSound(ssID);
        }
    }
}

void MyAudioServicesSystemSoundCompletionProc (SystemSoundID  ssID, void *clientData) {
    LaunchNavController *obj = (__bridge LaunchNavController *)clientData;
    if(obj.enteredSafetyRegions.count){
        AudioServicesAddSystemSoundCompletion(ssID, NULL, NULL, (AudioServicesSystemSoundCompletionProc)MyAudioServicesSystemSoundCompletionProc, clientData);
        AudioServicesPlaySystemSound(ssID);
        //        SystemSoundID ssID = 0;
        //        NSURL *audioPath = [[NSBundle mainBundle] URLForResource:@"alarm" withExtension:@"wav"];
        //        AudioServicesCreateSystemSoundID((__bridge CFURLRef)audioPath, &ssID);
        //        AudioServicesAddSystemSoundCompletion(ssID, NULL, NULL, (AudioServicesSystemSoundCompletionProc)MyAudioServicesSystemSoundCompletionProc, clientData);
        //        AudioServicesPlaySystemSound(ssID);
    } else {
        obj.isPlayingAlarm = false;
        AudioServicesDisposeSystemSoundID(ssID);
    }
    
}

-(UILocalNotification *)createNotificationForUnsafeRegionNamed:(NSString *)name{
//    var notification = UILocalNotification()
//    notification.alertBody = "Todo Item \"\(item.title)\" Is Overdue" // text that will be displayed in the notification
//    notification.alertAction = "open" // text that is displayed after "slide to..." on the lock screen - defaults to "slide to view"
//    notification.fireDate = item.deadline // todo item due date (when notification will be fired)
//    notification.soundName = UILocalNotificationDefaultSoundName // play default sound
//    notification.userInfo = ["UUID": item.UUID, ] // assign a unique identifier to the notification so that we can retrieve it later
//    notification.category = "TODO_CATEGORY"
//    UIApplication.sharedApplication().scheduleLocalNotification(notification)
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertTitle = @"You Entered An Unsafe Region";
    notification.alertBody = [NSString stringWithFormat:@"ALERT !: You have entered %@. It's an unsafe region for you",name];
    notification.category = @"UnsafeRegionAlert";
    notification.soundName = UILocalNotificationDefaultSoundName;
    notification.alertAction = @"open";
    notification.hasAction = false;
    return notification;
}


@end
