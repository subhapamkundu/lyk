//
//  SOSRightListTableViewCell.h
//  LYK
//
//  Created by Subhapam on 21/11/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SOSRightListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sosNumberLabel;


-(void)setUserProfilePicURL:(NSString *)urlStr;

@end
