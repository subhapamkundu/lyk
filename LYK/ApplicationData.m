//
//  ApplicationData.m
//  LYK
//
//  Created by Subhapam on 18/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "ApplicationData.h"

@interface ApplicationData ()
{
    NSMutableArray *roles;
    NSMutableArray *interests;
    NSArray *countryCodeList;
}

@end

@implementation ApplicationData

+(id)sharedInstance{
    static ApplicationData *sharedInstance_ = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        sharedInstance_ = [[ApplicationData alloc] init];
        [sharedInstance_ getCountryCodeList];
    });
    return sharedInstance_;
}

-(void)parseSettingsDataFromServerWithDetails:(NSDictionary *)details{
    NSLog(@"Settings Data: %@",details);
    if([details objectForKey:@"interests"]){
        interests = [[details objectForKey:@"interests"] mutableCopy];
    }
    if([details objectForKey:@"roles"]){
        roles = [[details objectForKey:@"roles"] mutableCopy];
    }
}

-(NSArray *)getInterests{
    return interests;
}

-(NSArray *)getRoles{
    return roles;
}

-(NSArray *)getCountryCodeList{
    if(!countryCodeList){
        NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"]];
        NSError *localError = nil;
        countryCodeList = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    }
    return countryCodeList;
}

-(NSString *)getCurrentCountryCode{
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    for (NSDictionary *dict in countryCodeList) {
        if([[dict objectForKey:@"code"] isEqualToString:countryCode]){
            return [dict objectForKey:@"dial_code"];
        }
    }
    return @"";
}


@end
