//
//  HomeNavController.h
//  LYK
//
//  Created by Subhapam on 17/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol MenuTransitionDelegate <NSObject>
//@required
//-(void)menuWillShow;
//-(void)menuDidShow;
//-(void)menuDidHide;
//
//@end

@interface HomeNavController : UINavigationController

//@property (nonatomic, weak)id<MenuTransitionDelegate> menuTransitionDelegate;

//-(void)toggleMenuWithDelegate;
//-(void)closeMenuIfOpen;
//-(UIView *)syncMenuView;
//-(void)shouldShowMenu:(BOOL)flag;


@end
