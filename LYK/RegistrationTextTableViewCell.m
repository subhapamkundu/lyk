//
//  RegistrationTableViewCell.m
//  LYK
//
//  Created by Subhapam on 18/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "RegistrationTextTableViewCell.h"

@interface RegistrationTextTableViewCell ()

- (IBAction)emailIdSelectionSegmentValueChanged:(id)sender;

@end

@implementation RegistrationTextTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    
}

- (IBAction)emailIdSelectionSegmentValueChanged:(id)sender{
    
    if(_emailIdSegment.selectedSegmentIndex == 0){
        _captionLabel.text = @"Email Id";
    } else {
        _captionLabel.text = @"Parent's Email Id";
    }
    
    if([_emailSelectionDelegate respondsToSelector:@selector(emailIdSelectionSegmentValueChanged:)]){
        [_emailSelectionDelegate performSelector:@selector(emailIdSelectionSegmentValueChanged:) withObject:sender];
    }
}



@end
