//
//  DropDown.m
//  LYK
//
//  Created by Subhapam on 19/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "DropDown.h"

@interface DropDown ()<UITableViewDataSource, UITableViewDelegate>
{
    UITableView *tableView;
    UIButton *btnSender;
    NSArray *list;
    NSString *animationDirection;
}

@end

@implementation DropDown

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (id)showDropDown:(UIButton *)btn height:(CGFloat *)height arr:(NSArray *)arr direction:(NSString *)direction {
    btnSender = btn;
    animationDirection = @"down";
    tableView = (UITableView *)[super init];
    if (self) {
        // Initialization code
        CGRect btnFrame = btn.frame;
        list = [NSArray arrayWithArray:arr];
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btnFrame.origin.x, btnFrame.origin.y, btnFrame.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, -5);
        }else if ([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(btnFrame.origin.x, btnFrame.origin.y+btnFrame.size.height, btnFrame.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, 5);
        }
        
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius = 8;
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        
        tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, btnFrame.size.width, 0)];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.layer.cornerRadius = 5;
        tableView.backgroundColor = [UIColor colorWithRed:0.239 green:0.239 blue:0.239 alpha:1];
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.separatorColor = [UIColor grayColor];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2];
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btnFrame.origin.x, btnFrame.origin.y-*height, btnFrame.size.width, *height);
        } else if([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(btnFrame.origin.x, btnFrame.origin.y+btnFrame.size.height, btnFrame.size.width, *height);
        }
        tableView.frame = CGRectMake(0, 0, btnFrame.size.width, *height);
        [UIView commitAnimations];
        [btn.superview addSubview:self];
        [self addSubview:tableView];
    }
    [tableView reloadData];
    return self;
}

-(void)hideDropDown:(UIButton *)b {
    CGRect btn = b.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    if ([animationDirection isEqualToString:@"up"]) {
        self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
    }else if ([animationDirection isEqualToString:@"down"]) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 0);
    }
    tableView.frame = CGRectMake(0, 0, btn.size.width, 0);
    [UIView commitAnimations];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [list count];
}


- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    cell.textLabel.text =[list objectAtIndex:indexPath.row];
    
    cell.textLabel.textColor = [UIColor blackColor];
    
    UIView * v = [[UIView alloc] init];
    v.backgroundColor = [UIColor grayColor];
    cell.selectedBackgroundView = v;
    
    return cell;
}

-(UIView *)tableView:(UITableView *)_tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.frame.size.width, 1)];
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self hideDropDown:btnSender];
    
    UITableViewCell *c = [_tableView cellForRowAtIndexPath:indexPath];
    [btnSender setTitle:c.textLabel.text forState:UIControlStateNormal];
    
    [_selectorDelegate didSelectItem:c.textLabel.text atIndex:[NSNumber numberWithInteger:indexPath.row]];
//    
//    if([_selectorDelegate respondsToSelector:@selector(didSelectItem:atIndex:)]){
//        [_selectorDelegate performSelector:@selector(didSelectItem:atIndex:) withObject:c.textLabel.text withObject:[NSNumber numberWithInteger:indexPath.row]];
//    }
}


@end
