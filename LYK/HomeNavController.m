//
//  HomeNavController.m
//  LYK
//
//  Created by Subhapam on 17/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "HomeNavController.h"
#import "MenuViewController.h"
#import "LaunchNavController.h"
#import "DashboardViewController.h"
#import "AddMemberModalViewController.h"
#import "GamificationViewController.h"

@interface HomeNavController ()<GamificationDelegate>
{
    MenuViewController *menuViewController;
    GamificationViewController *gmvc;
    BOOL isMenuOpen;
    BOOL shouldShowMenu;
    CGRect initialFrame;
    BOOL isPromptedForAddMember;
}
@end

@implementation HomeNavController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavigationBarHidden:NO animated:YES];
//    self.navigationItem.hidesBackButton = YES;
//    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(showMenu)];
//    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
//    [self.view addGestureRecognizer:swipeRight];
//    
//    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideMenu)];
//    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
//    [self.view addGestureRecognizer:swipeLeft];
//    shouldShowMenu = YES;
//    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"MenuIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(toggleMenuWithDelegate)];
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:((HomeNavController *)self.navigationController) action:@selector(toggleMenuWithDelegate)];
}

-(void)viewDidAppear:(BOOL)animated{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    //For retrieving
    BOOL shouldShowGamification = [[defaults objectForKey:@"isGamificationShown"] boolValue];
    if(!shouldShowGamification){
        //For saving
        [defaults setObject:[NSNumber numberWithBool:YES] forKey:@"isGamificationShown"];
        [defaults synchronize];
        gmvc = [[GamificationViewController alloc] initWithNibName:@"GamificationViewController" bundle:nil];
        [gmvc.view setFrame:[[UIApplication sharedApplication] keyWindow].bounds];
        gmvc.delegate = self;
        [[[UIApplication sharedApplication] keyWindow] addSubview:gmvc.view];
    } else {
        if(!isPromptedForAddMember){
            isPromptedForAddMember = YES;
            if([[[UserData sharedInstance] getMembersArray] count] <= 0){
                [((DashboardViewController *)((UITabBarController *)self.topViewController).viewControllers[0]) pauseFlipping];
                [self performSegueWithIdentifier:AddMemberSegue sender:self];
            }
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:UserProfileSegue]){
    }
    if([segue.identifier isEqualToString:AddMemberSegue]){
        
    }
}



//-(void)toggleMenuWithDelegate {
//    if(!menuViewController)
//        [self setupMenuView];
//    //pickJiMenuViewController.menuDelegate = delegate;
//    if(isMenuOpen){
//        [self hideMenu];
//    } else {
//        [self showMenu];
//    }
//}
//
//-(void)closeMenuIfOpen {
//    if(isMenuOpen){
//        [self hideMenu];
//    }
//}
//
//-(void)shouldShowMenu:(BOOL)flag {
//    shouldShowMenu = flag;
//}
//
//-(void)showMenu {
//    if(shouldShowMenu) {
//        if(!isMenuOpen) {
//            if([_menuTransitionDelegate respondsToSelector:@selector(menuWillShow)]){
//                [_menuTransitionDelegate performSelector:@selector(menuWillShow)];
//            }
//            if(!menuViewController)
//                [self setupMenuView];
//            [self.topViewController.view setUserInteractionEnabled:false];
//            [UIView animateWithDuration:SlideMenuAnimationTime animations:^{
//                // note CGAffineTransforms doesn't use coordinates, they use offset
//                // so an offset of (320,0) from coordinate (-320,0) would slide the
//                // menuView out into view
//                menuViewController.view.frame = CGRectMake(0, SlideMenuStartOffsetY, menuViewController.view.bounds.size.width, self.view.bounds.size.height-SlideMenuEndOffsetHeight);
//                //self.view.frame = CGRectMake(pickJiMenuViewController.view.bounds.size.width, 0, self.view.bounds.size.width, self.view.bounds.size.height);
//            } completion:^(BOOL finished) {
//                if(finished)
//                {
//                    isMenuOpen = YES;
//                    if([_menuTransitionDelegate respondsToSelector:@selector(menuDidShow)]){
//                        [_menuTransitionDelegate performSelector:@selector(menuDidShow)];
//                    }
//                }
//            }];
//        }
//    }
//}
//
//-(void)hideMenu {
//    if(isMenuOpen){
//        //    [self.view bringSubviewToFront:pickJiMenuViewController.view];
//        [self.topViewController.view setUserInteractionEnabled:true];
//        [UIView animateWithDuration:SlideMenuAnimationTime animations:^{
//            menuViewController.view.frame = CGRectMake(-menuViewController.view.bounds.size.width, SlideMenuStartOffsetY, menuViewController.view.bounds.size.width, self.view.bounds.size.height-SlideMenuEndOffsetHeight);
//            //self.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
//        } completion:^(BOOL finished) {
//            if(finished){
//                isMenuOpen = NO;
//                if([_menuTransitionDelegate respondsToSelector:@selector(menuDidHide)]){
//                    [_menuTransitionDelegate performSelector:@selector(menuDidHide)];
//                }
//            }
//        }];
//    }
//}
//
//
//#pragma mark - Menu view modules
//
//-(void)setupMenuView{
//    menuViewController = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
//    [menuViewController.view setFrame:CGRectMake(-menuViewController.view.bounds.size.width, SlideMenuStartOffsetY, menuViewController.view.bounds.size.width, self.view.bounds.size.height-SlideMenuEndOffsetHeight)];
//    //    [self addChildViewController:pickJiMenuViewController];
//    menuViewController.menuDelegate = self;
//    [[[[UIApplication sharedApplication] delegate] window] addSubview:menuViewController.view];
//    [self.view sendSubviewToBack:menuViewController.view];
//    isMenuOpen = NO;
//}
//
//#pragma mark -
//#pragma mark Menu Action Delegates
//#pragma mark -
//
//-(void)didSelectProductsAction{    
//    [self closeMenuIfOpen];
//}
//
//-(void)didSelectUpgradeAction{
//    [self closeMenuIfOpen];
//}
//
//-(void)didSelectAddFamily{
//    [self closeMenuIfOpen];
//    [self performSegueWithIdentifier:AddMemberSegue sender:self];
//}
//
//-(void)didSelectNotificationAction{
//    [self closeMenuIfOpen];
//    [self performSegueWithIdentifier:NotificationsSegue sender:self];
//}
//
//-(void)didSelectAccountAction{
//    [self closeMenuIfOpen];
//    [self performSegueWithIdentifier:UserProfileSegue sender:self];
//}
//
//-(void)didSelectPlayDemo{
//    [self closeMenuIfOpen];
//    gmvc = [[GamificationViewController alloc] initWithNibName:@"GamificationViewController" bundle:nil];
//    [gmvc.view setFrame:[[UIApplication sharedApplication] keyWindow].bounds];
//    gmvc.delegate = self;
//    [[[UIApplication sharedApplication] keyWindow] addSubview:gmvc.view];
//}
//
//-(void)didSelectActivityLogAction{
//    [self closeMenuIfOpen];
//}
//
//-(void)didSelectFeedbackAction{
//    [self closeMenuIfOpen];
//}
//
//-(void)didSelectLogoutAction {
//    [self closeMenuIfOpen];
//    LaunchNavController *launchNavController = (LaunchNavController *)[self navigationController];
//    [launchNavController popToViewController:[launchNavController.viewControllers objectAtIndex:1] animated:YES];
//    [[UserData sharedInstance] clearUserDetails];
//    
//    [self releaseAllViewControllers];
//}
//
//-(void)releaseAllViewControllers{
//    
//}

-(void)shouldDismissGamificationController{
    [gmvc.view removeFromSuperview];
    gmvc = nil;
    if(!isPromptedForAddMember){
        isPromptedForAddMember = YES;
        if([[[UserData sharedInstance] getMembersArray] count] <= 0){
            [((DashboardViewController *)((UITabBarController *)self.topViewController).viewControllers[0]) pauseFlipping];
            [self performSegueWithIdentifier:AddMemberSegue sender:self];
        }
    }
}



@end
