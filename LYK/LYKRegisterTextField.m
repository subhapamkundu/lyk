//
//  LYKRegisterTextField.m
//  LYK
//
//  Created by S. Kundu on 22/06/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import "LYKRegisterTextField.h"

@implementation LYKRegisterTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(CGRect)textRectForBounds:(CGRect)bounds{
    [self setFont:FONT_REGULAR(16.0f)];
    if(self.leftView){
        return CGRectMake(bounds.origin.x +10, bounds.origin.y, bounds.size.width-10, bounds.size.height);
    } else {
        return CGRectMake(bounds.origin.x +10, bounds.origin.y, bounds.size.width-10, bounds.size.height);
    }
}

-(CGRect)editingRectForBounds:(CGRect)bounds{
    if(self.leftView){
        return CGRectMake(bounds.origin.x +10, bounds.origin.y, bounds.size.width-10, bounds.size.height);
    } else {
        return CGRectMake(bounds.origin.x +10, bounds.origin.y, bounds.size.width-10, bounds.size.height);
    }
}


@end
