//
//  CategoryCollectionViewCell.h
//  LYK
//
//  Created by Subhapam on 06/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;

@end
