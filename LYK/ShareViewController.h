//
//  ShareViewController.h
//  LYK
//
//  Created by Subhapam on 05/02/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareViewController : UIViewController

@property (nonatomic, weak) NSDictionary *itemDetails;

@end
