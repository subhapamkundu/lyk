//
//  EditRelationshipViewController.m
//  LYK
//
//  Created by Subhapam on 05/12/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "EditRelationshipViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "FamilyListTableViewCell.h"


#define familyListCellIdentifier                        @"FAMILYLIST"

@interface EditRelationshipViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, DropDownDelegate, ABPeoplePickerNavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
    UIActivityIndicatorView *activityIndicator;
    DropDown *dropDown;
    NSMutableArray *roles;
    NSMutableArray *relationsArray;
    NSMutableArray *pendingRelationsArray;
    NSArray *arrCommon;
    Relationship *selectedRelation;
    ABPeoplePickerNavigationController *addressBookController;
    UITextField *countryCodeTextField;
    UIPickerView *countryCodePickerView;
}

@property (weak, nonatomic) IBOutlet UIView *editMemberDialogueView;
@property (weak, nonatomic) IBOutlet LYKTextField *emailIdTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *nameTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *editAddBtn;
@property (weak, nonatomic) IBOutlet UIButton *showAddMemberBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *relationTypeBtn;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentButton;
- (IBAction)actionSegmentButton:(id)sender;

- (IBAction)cancelAction:(id)sender;
- (IBAction)editAddMemberAction:(id)sender;
- (IBAction)toggleDropDownAction:(id)sender;


@end

@implementation EditRelationshipViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    roles = [NSMutableArray array];
    for (NSDictionary *relation in [[ApplicationData sharedInstance] getRoles]) {
        [roles addObject:[relation objectForKey:@"roleDesc"]];
    }
    relationsArray = [[UserData sharedInstance] getMembersArray];
    pendingRelationsArray = [[UserData sharedInstance]getPendingMembersArray];
    
//    _cancelBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    _cancelBtn.layer.cornerRadius = 5.0f;
//    _cancelBtn.layer.borderWidth = 1.0f;
//    
//    _editAddBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    _editAddBtn.layer.cornerRadius = 5.0f;
//    _editAddBtn.layer.borderWidth = 1.0f;
    
    _relationTypeBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _relationTypeBtn.layer.cornerRadius = 5.0f;
    _relationTypeBtn.layer.borderWidth = 1.0f;
    [_relationTypeBtn setTitle:[roles objectAtIndex:0] forState:UIControlStateNormal];
    
    [self getRelations];
    [self getPendingRelations];
    
    UIButton *magnifyingGlass = [[UIButton alloc] init];
    [magnifyingGlass setTitle:[[NSString alloc] initWithUTF8String:"\xF0\x9F\x94\x8D"] forState:UIControlStateNormal];
    [magnifyingGlass sizeToFit];
    [magnifyingGlass addTarget:self action:@selector(openAddressBook:) forControlEvents:UIControlEventTouchUpInside];
    
    [_emailIdTextField setRightView:magnifyingGlass];
    [_emailIdTextField setRightViewMode:UITextFieldViewModeAlways];
    
    countryCodeTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 60, _phoneTextField.bounds.size.height)];
    countryCodeTextField.textAlignment = NSTextAlignmentCenter;
    countryCodeTextField.borderStyle = UITextBorderStyleLine;
    countryCodeTextField.minimumFontSize = 10.0f;
    countryCodeTextField.inputView = countryCodePickerView;
    _phoneTextField.leftViewMode = UITextFieldViewModeAlways;
    _phoneTextField.leftView = countryCodeTextField;
    countryCodeTextField.text = [[ApplicationData sharedInstance] getCurrentCountryCode];
    [self createCountryCodePickerView];
    
    _phoneTextField.leftPadding = [NSNumber numberWithInteger:70];
    
    _tableView.estimatedRowHeight=121.0f;
    _tableView.rowHeight=UITableViewAutomaticDimension;
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavHeader"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(dropDown){
        [dropDown hideDropDown:_relationTypeBtn];
    }
    return YES;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrCommon count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 121.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    cell.textLbl.text = [NSString stringWithFormat:@"Section %ld Row number %ld", (long)indexPath.section, (long)indexPath.row];
    /*UITableViewCell *cell = nil;
    static NSString *CellIdentifier = @"memberCellIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.contentView.frame = CGRectMake(0, 0, tableView.frame.size.width, 53.0f);
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", [((Relationship *)[relationsArray objectAtIndex:indexPath.row]) getUserName]];
<<<<<<< HEAD
    //cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ (%@)", [((Relationship *)[relationsArray objectAtIndex:indexPath.row]) getUserName], [((Relationship *)[relationsArray objectAtIndex:indexPath.row]) getChildId]];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
=======
    //cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ (%@)", [((Relationship *)[relationsArray objectAtIndex:indexPath.row]) getUserName], [((Relationship *)[relationsArray objectAtIndex:indexPath.row]) getChildId]];*/
    
    /* Edited on 02.08.2016
     *by Gouranga Sasmal
     */
    
    FamilyListTableViewCell *cell = nil;
    
    cell = [tableView dequeueReusableCellWithIdentifier:familyListCellIdentifier];
    if (cell == nil) {
        cell = [[FamilyListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:familyListCellIdentifier];
    }
    cell.contentView.frame = CGRectMake(0, 0, tableView.frame.size.width, 53.0f);
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    //cell.textLabel.text = [NSString stringWithFormat:@"%@", [((Relationship *)[relationsArray objectAtIndex:indexPath.row]) getUserName]];
    
    cell.lblName.text=[NSString stringWithFormat:@"%@", [((Relationship *)[arrCommon objectAtIndex:indexPath.row]) getUserName]];
    cell.lblEmail.text=[NSString stringWithFormat:@"%@", [((Relationship *)[arrCommon objectAtIndex:indexPath.row]) getEmail]];
    cell.lblPhone.text=[NSString stringWithFormat:@"%@", [((Relationship *)[arrCommon objectAtIndex:indexPath.row]) getPhone]];
    cell.lblRelation.text=[NSString stringWithFormat:@"%@", [((Relationship *)[arrCommon objectAtIndex:indexPath.row]) getRelationDescription]];
    
    [cell.progImageView setImageWithURL:[NSURL URLWithString:[((Relationship *)[arrCommon objectAtIndex:indexPath.row]) getRelationImageURL]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.progImageView.layer.masksToBounds=YES;
        cell.progImageView.layer.cornerRadius=cell.progImageView.bounds.size.width/2;
        
    });
    
    if (_segmentButton.selectedSegmentIndex==1) {
        cell.btnEdit.hidden=YES;
    }else{
        cell.btnEdit.hidden=NO;
        cell.btnEdit.tag=indexPath.row;
        [cell.btnEdit addTarget:self action:@selector(updateContact:) forControlEvents:UIControlEventTouchUpInside];
    }
   
    
    
//>>>>>>> fc593bc01c97fac0ada0f0360e8cd2719ebfac25
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"didSelectRowAtIndexPath");
//    selectedRelation = [relationsArray objectAtIndex:indexPath.row];
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    
//    [self updateViewToEditRelationship];
    
}

/* Edited on 02.08.2016
 *by Gouranga Sasmal
 *Update contact
 */
-(void)updateContact:(UIButton *)sender{
    
    selectedRelation = [relationsArray objectAtIndex:sender.tag];
    
    [self updateViewToEditRelationship];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete ) {
        Relationship *rel = nil;
        if (_segmentButton.selectedSegmentIndex==0) {
            rel=[relationsArray objectAtIndex:indexPath.row];
            [relationsArray removeObjectAtIndex:indexPath.row];
        }else{
            rel=[pendingRelationsArray objectAtIndex:indexPath.row];
            [pendingRelationsArray removeObjectAtIndex:indexPath.row];
        }
//        [relationsArray objectAtIndex:indexPath.row];
//        [relationsArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        if([[RequestController sharedInstance] isNetworkReachable]){
            if(!activityIndicator){
                activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
                activityIndicator.layer.cornerRadius = 05;
                activityIndicator.opaque = NO;
                activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
                activityIndicator.center = self.view.center;
                activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
                [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
                [self.view addSubview: activityIndicator];
            }
            
            self.view.userInteractionEnabled = false;
            [activityIndicator startAnimating];
            [[RequestController sharedInstance] removeRelationshipRequest:[rel getRelationshipId] successHandler:^(NSDictionary *response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"Success :: %@",response);
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    if ([response objectForKey:Key_Response]) {
                        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Relationship removed successfully" animated:YES];
                        if([response objectForKey:Key_Token] && _segmentButton.selectedSegmentIndex==0){
                            [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:Key_Token]];
                        } else {
                            
                        }
                    }
                });
            } failureHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"Error :: %@",error);
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo] animated:YES];
                });
            }];
        }
    }
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 53.0f;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 30.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1.0f)];
//    UIButton *addMemberBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [addMemberBtn setFrame:CGRectMake(0, 0, tableView.frame.size.width, 30.0f)];
//    [addMemberBtn setTitle:@"Add Member" forState:UIControlStateNormal];
//    [addMemberBtn setBackgroundColor:[UIColor redColor]];
//    [addMemberBtn addTarget:self action:@selector(showAddMemberView) forControlEvents:UIControlEventTouchUpInside];
//    return addMemberBtn;
}

-(void)didSelectItem:(NSString *)selectedItem atIndex:(NSNumber *)index{
    NSLog(@"%@",selectedItem);
    dropDown = nil;
}


-(void)getRelations{
    if([[RequestController sharedInstance] isNetworkReachable]){
        [[RequestController sharedInstance] getRelationsWithSuccessHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Success :: %@",response);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                if ([response objectForKey:Key_Response]) {
                    if([response objectForKey:Key_Token]){
                        [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:Key_Token]];
                    } else {
                        
                    }
                    [[UserData sharedInstance] parseRelationDataWithJSON:[response objectForKey:Key_Response]];
                    arrCommon=relationsArray;
                    [_tableView reloadData];
                }
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Error :: %@",error);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo] animated:YES];
            });
        }];
    }
}

-(void)getPendingRelations{
    
    if([[RequestController sharedInstance] isNetworkReachable]){
        [[RequestController sharedInstance] getRelationsWithSuccessHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Success :: %@",response);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                if ([response objectForKey:Key_Response]) {
//                    if([response objectForKey:Key_Token]){
//                        [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:Key_Token]];
//                    } else {
//                        
//                    }
                    [[UserData sharedInstance] parsePendingRelationsDataWithJSON:[response objectForKey:Key_Response]];
                    pendingRelationsArray = [[UserData sharedInstance]getPendingMembersArray];
                   // arrCommon=pendingRelationsArray;
                   // [_tableView reloadData];
                }
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Error :: %@",error);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo] animated:YES];
            });
        }];
    }
}

-(IBAction)toggleDropDownAction:(id)sender{
    NSLog(@"toggle drop down");
    
    if(dropDown == nil) {
        CGFloat f = 160;
        dropDown = [[DropDown alloc] showDropDown:sender height:&f arr:roles direction:@"down"];
        dropDown.selectorDelegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }    
}

- (IBAction)actionSegmentButton:(id)sender {
    
    NSLog(@"selected %d",_segmentButton.selectedSegmentIndex);
    
    if (_segmentButton.selectedSegmentIndex==0)
        arrCommon=relationsArray;
    else
        arrCommon=pendingRelationsArray;
    
    
    [_tableView reloadData];
    
}

- (IBAction)cancelAction:(id)sender {
    selectedRelation = nil;
    _editMemberDialogueView.hidden = true;
    _showAddMemberBtn.hidden = false;
    _emailIdTextField.text = @"";
    _nameTextField.text = @"";
    _phoneTextField.text = @"";
    [_relationTypeBtn setTitle:[roles objectAtIndex:0] forState:UIControlStateNormal];
    [_editAddBtn setTitle:@"Add Member" forState:UIControlStateNormal];
}

- (IBAction)editAddMemberAction:(id)sender {
    if([[RequestController sharedInstance] isNetworkReachable]){
        [self.view endEditing:YES];
        if (![_emailIdTextField.text isEqualToString:BlankText]) {
            if(![Validator validateEmailId:_emailIdTextField.text]){
                [[HelperModule sharedInstance] showNetworkToast:YES message:@"Invalid email id" animated:YES];
                return;
            } else if ([allTrim(_emailIdTextField.text) isEqualToString:[[UserData sharedInstance] getEmailId]]){
                    [[HelperModule sharedInstance] showNetworkToast:YES message:@"You are not allowed to add yourself as a relation" animated:YES];
                return;
            }
        } else{
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"Enter email id" animated:YES];
            return;
        }
        if ([_nameTextField.text isEqualToString:BlankText]) {
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"Please enter name" animated:YES];
            return;
        }
        if(![Validator validatePhoneNumber:_phoneTextField.text withCountryCode:@"in"]){
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"Invalid Phone number" animated:YES];
            return;
        }
        NSString *roleId = 0;
        for(NSDictionary *role in [[ApplicationData sharedInstance] getRoles]){
            if([[role objectForKey:@"roleDesc"] isEqualToString:_relationTypeBtn.currentTitle]){
                roleId = [role objectForKey:@"id"];
            }
        }
        NSArray *components = [_nameTextField.text componentsSeparatedByString:@" "];
        NSString *lastName = nil, *middleName = nil, *firstName = nil;
        if([components count] >= 3){
            lastName = [components lastObject];
            middleName = [components objectAtIndex:[components count]-2];
            firstName = [[components subarrayWithRange:NSMakeRange(0, components.count - 2)] componentsJoinedByString:@" "];
        } else if ([components count]==1){
            firstName = [components componentsJoinedByString:@" "];
        } else {
            lastName = [components lastObject];
            firstName = [components firstObject];
        }
        if([_editAddBtn.currentTitle isEqualToString:@"Send confirmation"]){
            NSDictionary *member = [NSDictionary dictionaryWithObjectsAndKeys:
                                    roleId, @"roleId",
                                    _relationTypeBtn.currentTitle, @"roleDesc",
                                    firstName?:@"", @"firstName",
                                    middleName?:@"", @"middleName",
                                    lastName?:@"", @"lastName",
                                    _emailIdTextField.text, @"email",
                                    [NSString stringWithFormat:@"%@-%@",countryCodeTextField.text,_phoneTextField.text], @"contactNo", nil];
            [self saveUpdatedRelationsWithDetails:@[member]];
        } else {
            [selectedRelation setFirstName:firstName];
            [selectedRelation setMiddleName:middleName];
            [selectedRelation setLastName:lastName];
            [selectedRelation setEmail:_emailIdTextField.text];
            [selectedRelation setRelationDescription:_relationTypeBtn.currentTitle];
            [selectedRelation setRoleId:[NSString stringWithFormat:@"%@",roleId]];
            relationsArray = [[UserData sharedInstance] getMembersArray];
            NSMutableArray *arr = [NSMutableArray array];
            for(Relationship *rel in relationsArray){
                NSArray *components = [[rel getUserName] componentsSeparatedByString:@" "];
                NSString *lName = nil, *mName = nil, *fName = nil;
                if([components count] >= 3){
                    lName = [components lastObject];
                    mName = [components objectAtIndex:[components count]-2];
                    fName = [[components subarrayWithRange:NSMakeRange(0, components.count - 2)] componentsJoinedByString:@" "];
                } else if ([components count]==1){
                    fName = [components componentsJoinedByString:@" "];
                } else {
                    lName = [components lastObject];
                    fName = [components firstObject];
                }
                NSDictionary *member = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [rel getRelationRoleId], @"roleId",
                                        [rel getRelationDescription], @"roleDesc",
                                        fName, @"firstName",
                                        mName, @"middleName",
                                        lName, @"lastName",
                                        [rel getEmail], @"email",
                                        [rel getPhone], @"contactNo", nil];
                [arr addObject:member];
            }
            [self saveUpdatedRelationsWithDetails:arr];
        }
    }

}

-(NSString *)originalphoneNumber:(NSString *)phone{
    
    /*
     {
     code = AQ;
     "dial_code" = "<null>";
     name = Antarctica;
     }*/
    
    if (phone) {
        
        
        for (NSDictionary *dict in [[ApplicationData sharedInstance] getCountryCodeList]) {
            
            if ([[dict objectForKey:@"dial_code"] isKindOfClass:[NSNull class]]) {
            
                continue;
            }
            
            if ([phone containsString:[dict objectForKey:@"dial_code"]]) {
                
                phone=[phone stringByReplacingOccurrencesOfString:[dict objectForKey:@"dial_code"] withString:@""];
                break;
            }
        }
    }
    phone=[phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
    return phone;
   
}

-(IBAction)showAddMemberView{
    selectedRelation = nil;
    _editMemberDialogueView.hidden = false;
    _showAddMemberBtn.hidden = true;
}

-(void)updateViewToEditRelationship{
    _editMemberDialogueView.hidden = false;
    _showAddMemberBtn.hidden = true;
    _emailIdTextField.text = [selectedRelation getEmail]?:@"";
    _nameTextField.text = [selectedRelation getUserName]?:@"";
   //_phoneTextField.text = [selectedRelation getPhone]?:@"";
    _phoneTextField.text = [selectedRelation getPhone]?[self originalphoneNumber:[selectedRelation getPhone]]:@"";
    [_relationTypeBtn setTitle:[selectedRelation getRelationDescription] forState:UIControlStateNormal];
    [_editAddBtn setTitle:@"Edit Member" forState:UIControlStateNormal];
}

-(void)saveUpdatedRelationsWithDetails:(NSArray *)relations{
    if([[RequestController sharedInstance] isNetworkReachable]){
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }
        
        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        
        [[RequestController sharedInstance] addMemberWithCredentials:relations successHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                NSLog(@"add member Response :: %@",response);
                if ([response objectForKey:Key_Response]) {
                    [self getRelations];
                    [[HelperModule sharedInstance] showNetworkToast:YES message:[NSString stringWithFormat:@"We are forwarding your request to %@",_nameTextField.text] animated:YES];
                    _nameTextField.text = @"";
                    _emailIdTextField.text = @"";
                    _phoneTextField.text = @"";
                }
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                //[[[PJKeychainController alloc] init] clearKeyChain];
                [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
            });
        }];
    }
}

-(void)openAddressBook:(id)sender{
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
        //1
        NSLog(@"Denied");
        UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Cannot Load Contact" message: @"You must give the app permission to load the contact first." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [cantAddContactAlert show];
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        //2
        NSLog(@"Authorized");
        addressBookController = [[ABPeoplePickerNavigationController alloc] init];
        [addressBookController setPeoplePickerDelegate:self];
        [self presentViewController:addressBookController animated:NO completion:nil];
    } else{ //ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined
        //3
        NSLog(@"Not determined");
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            if (!granted){
                //4
                NSLog(@"Just denied");
                return;
            } else {
                addressBookController = [[ABPeoplePickerNavigationController alloc] init];
                [addressBookController setPeoplePickerDelegate:self];
                [self presentViewController:addressBookController animated:YES completion:nil];
            }
            //5
            NSLog(@"Just authorized");
        });
    }
}

-(void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person{
    [_nameTextField setText:@""];
    [_phoneTextField setText:@""];
    [_emailIdTextField setText:@""];
    NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString *middleName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonMiddleNameProperty);
    NSString *lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    NSArray *phones = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(ABRecordCopyValue(person, kABPersonPhoneProperty));
    NSArray *emails = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(ABRecordCopyValue(person, kABPersonEmailProperty));
    NSMutableArray *nameSegments = [NSMutableArray array];
    if(firstName)
        [nameSegments addObject:firstName];
    if(middleName)
        [nameSegments addObject:middleName];
    if(lastName)
        [nameSegments addObject:lastName];
    [_nameTextField setText:[nameSegments componentsJoinedByString:@" "]];
    if(phones)
        [_phoneTextField setText:[phones objectAtIndex:0]?:@""];
    if(emails)
        [_emailIdTextField setText:[emails objectAtIndex:0]?:@""];
}

-(void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
    
}

-(void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker{
    
}


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [[ApplicationData sharedInstance] getCountryCodeList].count;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSDictionary *countryDetails = [[[ApplicationData sharedInstance] getCountryCodeList] objectAtIndex:row];
    countryCodeTextField.text = [countryDetails objectForKey:@"dial_code"];
}

- (UIView*)pickerView:(UIPickerView *)thePickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    NSDictionary *countryDetails = [[[ApplicationData sharedInstance] getCountryCodeList] objectAtIndex:row];
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, thePickerView.bounds.size.width, 44)];
    UILabel* l1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, thePickerView.bounds.size.width - 80, 44)];
    //    l1.font = [UIFont sytemFontOfSize:22]; // choose desired size
    //    l1.tag = VIEWTAG_LINE1;
    l1.text = [NSString stringWithFormat:@"%@ (%@)",[countryDetails objectForKey:@"name"],[countryDetails objectForKey:@"code"]];
    l1.textAlignment = NSTextAlignmentLeft;
    [v addSubview: l1];
    
    UILabel* l2 = [[UILabel alloc] initWithFrame:CGRectMake(thePickerView.bounds.size.width - 100, 0, 80, 44)];
    //    l2.font = [UIFont sytemFontOfSize:14]; // choose desired size
    //    l2.tag = VIEWTAG_LINE2;
    l2.text = [NSString stringWithFormat:@"%@",[countryDetails objectForKey:@"dial_code"]];
    l2.textAlignment = NSTextAlignmentRight;
    [v addSubview: l2];
    
    return v;
}
-(void)createCountryCodePickerView{
    countryCodePickerView = [[UIPickerView alloc] init];
    [countryCodePickerView setDataSource: self];
    [countryCodePickerView setDelegate: self];
    countryCodePickerView.showsSelectionIndicator = YES;
    countryCodeTextField.inputView = countryCodePickerView;
}


@end
