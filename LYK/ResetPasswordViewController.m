//
//  ResetPasswordViewController.m
//  LYK
//
//  Created by Subhapam on 13/11/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "ResetPasswordViewController.h"

@interface ResetPasswordViewController ()
{
    UIActivityIndicatorView *activityIndicator;
}
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet LYKTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *confirmTextField;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *resetBtn;


- (IBAction)cancelAction:(id)sender;
- (IBAction)resetAction:(id)sender;



@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _containerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _containerView.layer.cornerRadius = 5.0f;
    _containerView.layer.borderWidth = 1.0f;
    
    _cancelBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _cancelBtn.layer.cornerRadius = 5.0f;
    _cancelBtn.layer.borderWidth = 1.0f;
    
    _resetBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _resetBtn.layer.cornerRadius = 5.0f;
    _resetBtn.layer.borderWidth = 1.0f;
    
    _containerView.layer.masksToBounds = NO;
    _containerView.layer.cornerRadius = 8; // if you like rounded corners
    _containerView.layer.shadowOffset = CGSizeMake(-7, 20);
    _containerView.layer.shadowRadius = 5;
    _containerView.layer.shadowOpacity = 0.5;
    //_containerView.layer.shadowPath = [UIBezierPath bezierPathWithRect:_containerView.bounds].CGPath;
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavHeader"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)resetAction:(id)sender {
    if([[RequestController sharedInstance] isNetworkReachable]){
        if(![_passwordTextField.text isEqualToString:@""] && [_confirmTextField.text isEqualToString:_passwordTextField.text]){
            NSDictionary *credential = [NSDictionary dictionaryWithObjectsAndKeys:[[UserData sharedInstance] getEmailId],@"identity",_tempPassword,@"tempPassword", _confirmTextField.text,@"newPass", nil];
            if([[RequestController sharedInstance] isNetworkReachable]){
                if(!activityIndicator){
                    activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
                    activityIndicator.layer.cornerRadius = 05;
                    activityIndicator.opaque = NO;
                    activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
                    activityIndicator.center = self.view.center;
                    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
                    [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
                    [self.view addSubview: activityIndicator];
                }
                self.view.userInteractionEnabled = false;
                [activityIndicator startAnimating];
                [[RequestController sharedInstance] sendChangeTempPasswordRequest:credential successHandler:^(NSDictionary *response) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.view.userInteractionEnabled = true;
                        if([response objectForKey:@"error"]){
                            [activityIndicator stopAnimating];
                        } else {
                            [[HelperModule sharedInstance] showNetworkToast:YES message:[response objectForKey:Key_Response] animated:YES];
                            [activityIndicator stopAnimating];
                        }
                        [self dismissViewControllerAnimated:YES completion:nil];
                    });
                } failureHandler:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.view.userInteractionEnabled = true;
                        [activityIndicator stopAnimating];
                        [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:NoResponseErrorMessage animated:YES];
                    });
                }];
            }
        } else {
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"Password does not match" animated:YES];
        }
    }
}
@end
