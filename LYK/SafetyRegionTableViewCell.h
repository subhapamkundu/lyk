//
//  SafetyRegionTableViewCell.h
//  LYK
//
//  Created by Gouranga Sasmal on 04/08/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMaps;

//#import <MapKit/MapKit.h>

@interface SafetyRegionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet GMSMapView *mapview;

@end
