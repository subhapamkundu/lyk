//
//  HomeTabBarController.h
//  LYK
//
//  Created by Subhapam on 13/12/15.
//  Copyright © 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuTransitionDelegate <NSObject>
@required
-(void)menuWillShow;
-(void)menuDidShow;
-(void)menuDidHide;

@end

@interface HomeTabBarController : UITabBarController

@property (nonatomic, weak)id<MenuTransitionDelegate> menuTransitionDelegate;

-(void)closeMenuIfOpen;

-(void)showAddFamilyActionView;

@end
