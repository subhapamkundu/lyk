//
//  RegistrationTableViewCell.h
//  LYK
//
//  Created by Subhapam on 18/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EmailSelectionActionDelegate <NSObject>

-(void)emailIdSelectionSegmentValueChanged:(id)sender;

@end


@interface RegistrationTextTableViewCell : UITableViewCell <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *captionLabel;
@property (weak, nonatomic) IBOutlet LYKTextField *dataTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *emailIdSegment;
@property (nonatomic) BOOL isTextFieldEditable;
@property (weak, nonatomic) id<EmailSelectionActionDelegate> emailSelectionDelegate;

@end
