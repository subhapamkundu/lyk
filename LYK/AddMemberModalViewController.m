//
//  AddMemberModalViewController.m
//  LYK
//
//  Created by Subhapam on 13/11/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "AddMemberModalViewController.h"
#import "MemberNotificationTableViewCell.h"
#import "InterestsTableViewCell.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface AddMemberModalViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, DropDownDelegate, MemberNotificationTableViewCellDelegates, ABPeoplePickerNavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
    UIActivityIndicatorView *activityIndicator;
    DropDown *dropDown;
    NSMutableArray *roles;
    NSMutableArray *addedRelationsArray;
    NSMutableArray *relationsArray;
    NSMutableArray *pendingRelationsArray;
    NSMutableArray *savedInterestsArray;
    BOOL isMembersArrayUpdated;
    ABPeoplePickerNavigationController *addressBookController;
    UITextField *countryCodeTextField;
    UIPickerView *countryCodePickerView;
}


@property (weak, nonatomic) IBOutlet UIView *memberContainerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *relationButton;
@property (weak, nonatomic) IBOutlet LYKTextField *relationNameTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *relationEmailTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *relationPhoneTextField;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
//@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIButton *needApprovalSegmentBtn;
@property (weak, nonatomic) IBOutlet UIButton *familySegmentBtn;
@property (weak, nonatomic) IBOutlet UIButton *interestsSegmentBtn;


- (IBAction)cancelAddingMember:(id)sender;
- (IBAction)saveMemberDetails:(id)sender;
- (IBAction)segmentControlValueChangedAction:(id)sender;
- (IBAction)moveNextAction:(id)sender;


- (IBAction)saveRelationshipDetailsAndGoToHobbies:(id)sender;

@end

@implementation AddMemberModalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    roles = [NSMutableArray array];
    for (NSDictionary *relation in [[ApplicationData sharedInstance] getRoles]) {
        [roles addObject:[relation objectForKey:@"roleDesc"]];
    }
    
    pendingRelationsArray = [[UserData sharedInstance] getPendingMembersArray];
    
//    _tableView.layer.cornerRadius = 5.0f;
//    _tableView.layer.borderWidth = 1.0f;
//    _tableView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    _nextButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _nextButton.layer.cornerRadius = 5.0f;
    _nextButton.layer.borderWidth = 1.0f;
    
    _relationButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _relationButton.layer.cornerRadius = 5.0f;
    _relationButton.layer.borderWidth = 1.0f;
    [_relationButton setTitle:[roles objectAtIndex:0] forState:UIControlStateNormal];
    
    _addButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _addButton.layer.cornerRadius = 5.0f;
    _addButton.layer.borderWidth = 1.0f;
    
    _cancelButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _cancelButton.layer.cornerRadius = 5.0f;
    _cancelButton.layer.borderWidth = 1.0f;
    
    [_needApprovalSegmentBtn setSelected:true];
    [self updateHeaderLabel];
    
    if(!savedInterestsArray)
        savedInterestsArray = [NSMutableArray array];
    for (NSDictionary *dict in [[ApplicationData sharedInstance] getInterests]) {
        NSMutableDictionary *interest = [NSMutableDictionary dictionaryWithDictionary:dict];
        [interest setObject:[NSNumber numberWithBool:NO] forKey:@"selected"];
        [savedInterestsArray addObject:interest];
    }
    
    UIButton *magnifyingGlass = [[UIButton alloc] init];
    [magnifyingGlass setTitle:[[NSString alloc] initWithUTF8String:"\xF0\x9F\x94\x8D"] forState:UIControlStateNormal];
    [magnifyingGlass sizeToFit];
    [magnifyingGlass addTarget:self action:@selector(openAddressBook:) forControlEvents:UIControlEventTouchUpInside];
    
    [_relationEmailTextField setRightView:magnifyingGlass];
    [_relationEmailTextField setRightViewMode:UITextFieldViewModeAlways];
    
    countryCodeTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 60, _relationPhoneTextField.bounds.size.height)];
    countryCodeTextField.textAlignment = NSTextAlignmentCenter;
    countryCodeTextField.borderStyle = UITextBorderStyleLine;
    countryCodeTextField.minimumFontSize = 10.0f;
    countryCodeTextField.inputView = countryCodePickerView;
    _relationPhoneTextField.leftViewMode = UITextFieldViewModeAlways;
    _relationPhoneTextField.leftView = countryCodeTextField;
    countryCodeTextField.text = [[ApplicationData sharedInstance] getCurrentCountryCode];
    [self createCountryCodePickerView];
    
    _relationPhoneTextField.leftPadding = [NSNumber numberWithInt:70];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavHeader"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(dropDown){
        [dropDown hideDropDown:_relationButton];
    }
    
    
    return YES;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([self getSelectedSegment] == 0){ // pending notifications
        if ([pendingRelationsArray count] == 0){
            UILabel *emptyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
            emptyLabel.text = @"Be the first one in your family to use lyk. Invite members to lyk and get them closer digitally";
            emptyLabel.numberOfLines = 0;
            emptyLabel.textAlignment = NSTextAlignmentCenter;
            self.tableView.backgroundView = emptyLabel;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 0;
        } else {
            self.tableView.backgroundView = nil;
        }
        return [pendingRelationsArray count];
    }
    else if([self getSelectedSegment] == 1){ // accepted members
        if ([relationsArray count] == 0){
            UILabel *emptyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
            emptyLabel.text = @"Be the first one in your family to use lyk. Invite members to lyk and get them closer digitally";
            emptyLabel.numberOfLines = 0;
            emptyLabel.textAlignment = NSTextAlignmentCenter;
            self.tableView.backgroundView = emptyLabel;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 0;
        } else {
            self.tableView.backgroundView = nil;
        }
        return [relationsArray count];
    } else { // interests
        self.tableView.backgroundView = nil;
        return [savedInterestsArray count];
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    cell.textLbl.text = [NSString stringWithFormat:@"Section %ld Row number %ld", (long)indexPath.section, (long)indexPath.row];
    UITableViewCell *cell = nil;
    if ([self getSelectedSegment] == 0) {
        static NSString *CellIdentifier = @"memberNotificationCellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[MemberNotificationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.contentView.frame = CGRectMake(0, 0, tableView.frame.size.width, 53.0f);
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
        ((MemberNotificationTableViewCell *)cell).textLbl.text = [NSString stringWithFormat:@"%@", [((Relationship *)[pendingRelationsArray objectAtIndex:indexPath.row]) getUserName]];
        ((MemberNotificationTableViewCell *)cell).relationDetails = [pendingRelationsArray objectAtIndex:indexPath.row];
//        ((MemberNotificationTableViewCell *)cell).textLbl.text = [NSString stringWithFormat:@"Notifications "];
        ((MemberNotificationTableViewCell *)cell).memberNotificationTableViewCellDelegate = self;
    }
    else if([self getSelectedSegment] == 1){
        static NSString *CellIdentifier = @"memberCellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        cell.contentView.frame = CGRectMake(0, 0, tableView.frame.size.width, 53.0f);
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor darkGrayColor];
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [[relationsArray objectAtIndex:indexPath.row] objectForKey:@"firstName"]];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ (%@)", [[relationsArray objectAtIndex:indexPath.row] objectForKey:@"email"], [[relationsArray objectAtIndex:indexPath.row] objectForKey:@"relation"]];
    }
    else {
        InterestsTableViewCell *cell = (InterestsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"interestsCellIdentifier"];
        if(!cell){
            cell = [[InterestsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"interestsCellIdentifier"];
        }
        cell.detailsLabel.text = [[savedInterestsArray objectAtIndex:indexPath.row] objectForKey:@"interestName"];
        //    cell.accessoryType = UITableViewCellAccessoryNone;
        if([[[savedInterestsArray objectAtIndex:indexPath.row] objectForKey:@"selected"] boolValue]){
            [cell.checkImageView setImage:[UIImage imageNamed:@"CircleSelected"]];
        } else {
            [cell.checkImageView setImage:[UIImage imageNamed:@"CircleDeselected"]];
        }
        return cell;
        
//        static NSString *CellIdentifier = @"interestCellIdentifier";
//        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//        if (cell == nil) {
//            cell = [[InterestsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//        }
//        ((InterestsTableViewCell *)cell).contentView.frame = CGRectMake(0, 0, tableView.frame.size.width, 53.0f);
//        cell.contentView.backgroundColor = [UIColor clearColor];
//        //cell.backgroundColor = [UIColor clearColor];
////        cell.textLabel.textColor = [UIColor darkGrayColor];
////        cell.detailTextLabel.textColor = [UIColor darkGrayColor];
////        
//        ((InterestsTableViewCell *)cell).detailsLabel.text = [[[[ApplicationData sharedInstance] getInterests] objectAtIndex:indexPath.row] objectForKey:@"interestName"];
////        cell.accessoryType = UITableViewCellAccessoryNone;
//        for(NSDictionary *dict in savedInterestsArray){
//            if([[dict objectForKey:@"interestName"] isEqualToString:cell.textLabel.text]){
//                cell.backgroundColor = [UIColor lightGrayColor];
////                cell.accessoryType = UITableViewCellAccessoryCheckmark;
//                [((InterestsTableViewCell *)cell).checkImageView setImage:[UIImage imageNamed:@"CircleSelected"]];
//            } else {
//                cell.backgroundColor = [UIColor whiteColor];
////                cell.accessoryType = UITableViewCellAccessoryNone;
//                [((InterestsTableViewCell *)cell).checkImageView setImage:[UIImage imageNamed:@"CircleDeselected"]];
//            }
//        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"didSelectRowAtIndexPath");
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if([self getSelectedSegment] == 2){
        NSLog(@"didSelectRowAtIndexPath");
        InterestsTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        NSMutableDictionary *dict = [savedInterestsArray objectAtIndex:indexPath.row];
        [dict setObject:[NSNumber numberWithBool:![[dict objectForKey:@"selected"] boolValue]] forKey:@"selected"];
        if ([[dict objectForKey:@"selected"] boolValue]) {
            [cell.checkImageView setImage:[UIImage imageNamed:@"CircleSelected"]];
        } else {
            [cell.checkImageView setImage:[UIImage imageNamed:@"CircleDeselected"]];
        }
//        for(NSDictionary *dict in savedInterestsArray){
//            if([[dict objectForKey:@"interestName"] isEqualToString:cell.textLabel.text]){
//                [savedInterestsArray addObject:[[[ApplicationData sharedInstance] getInterests] objectAtIndex:indexPath.row]];
//                cell.backgroundColor = [UIColor lightGrayColor];
//                cell.accessoryType = UITableViewCellAccessoryNone;
//            } else {
//                [savedInterestsArray removeObject:[[[ApplicationData sharedInstance] getInterests] objectAtIndex:indexPath.row]];
//                cell.backgroundColor = [UIColor whiteColor];
//                cell.accessoryType = UITableViewCellAccessoryNone;
//            }
//        }
//        if(cell.accessoryType == UITableViewCellAccessoryNone){
//            if(!savedInterestsArray)
//                savedInterestsArray = [NSMutableArray array];
//            [savedInterestsArray addObject:[[[ApplicationData sharedInstance] getInterests] objectAtIndex:indexPath.row]];
//            cell.backgroundColor = [UIColor lightGrayColor];
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        } else {
//            [savedInterestsArray removeObject:[[[ApplicationData sharedInstance] getInterests] objectAtIndex:indexPath.row]];
//            cell.backgroundColor = [UIColor whiteColor];
//            cell.accessoryType = UITableViewCellAccessoryNone;
//        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(InterestsTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self getSelectedSegment] == 2){
//        for(NSDictionary *dict in savedInterestsArray){
//            if([[dict objectForKey:@"interestName"] isEqualToString:cell.textLabel.text]){
//                cell.accessoryType = UITableViewCellAccessoryCheckmark;
//                cell.backgroundColor = [UIColor lightGrayColor];
//            } else {
//                cell.backgroundColor = [UIColor whiteColor];
//                cell.accessoryType = UITableViewCellAccessoryNone;
//            }
//        }
        cell.detailsLabel.text = [[savedInterestsArray objectAtIndex:indexPath.row] objectForKey:@"interestName"];
        if([[[savedInterestsArray objectAtIndex:indexPath.row] objectForKey:@"selected"] boolValue]){
            [cell.checkImageView setImage:[UIImage imageNamed:@"CircleSelected"]];
        } else {
            [cell.checkImageView setImage:[UIImage imageNamed:@"CircleDeselected"]];
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    if([self getSelectedSegment] == 0)
        return NO;
    else if([self getSelectedSegment] == 1)
        return YES;
    else
        return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [relationsArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 53.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if([self getSelectedSegment] == 0)
        return 1.0f;
    else if([self getSelectedSegment] == 1)
        return 30.0f;
    else
        return 30.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1.0f)];
//    if(_segmentControl.selectedSegmentIndex == 0){
//        
//    } else if(_segmentControl.selectedSegmentIndex == 1){
//        UIButton *addMemberBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [addMemberBtn setFrame:CGRectMake(0, 0, tableView.frame.size.width, 30.0f)];
//        [addMemberBtn setTitle:@"Add Member" forState:UIControlStateNormal];
//        [addMemberBtn setBackgroundColor:[UIColor redColor]];
//        [addMemberBtn addTarget:self action:@selector(showAddMemberView) forControlEvents:UIControlEventTouchUpInside];
//        return addMemberBtn;
//    } else {
//        UIButton *saveInterestBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [saveInterestBtn setFrame:CGRectMake(0, 0, tableView.frame.size.width, 30.0f)];
//        [saveInterestBtn setTitle:@"Save & Continue" forState:UIControlStateNormal];
//        [saveInterestBtn setBackgroundColor:[UIColor redColor]];
//        [saveInterestBtn addTarget:self action:@selector(saveInterestAndDismiss) forControlEvents:UIControlEventTouchUpInside];
//        return saveInterestBtn;
//    }
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [[ApplicationData sharedInstance] getCountryCodeList].count;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSDictionary *countryDetails = [[[ApplicationData sharedInstance] getCountryCodeList] objectAtIndex:row];
    countryCodeTextField.text = [countryDetails objectForKey:@"dial_code"];
}

- (UIView*)pickerView:(UIPickerView *)thePickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    NSDictionary *countryDetails = [[[ApplicationData sharedInstance] getCountryCodeList] objectAtIndex:row];
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, thePickerView.bounds.size.width, 44)];
    UILabel* l1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, thePickerView.bounds.size.width - 80, 44)];
    //    l1.font = [UIFont sytemFontOfSize:22]; // choose desired size
    //    l1.tag = VIEWTAG_LINE1;
    l1.text = [NSString stringWithFormat:@"%@ (%@)",[countryDetails objectForKey:@"name"],[countryDetails objectForKey:@"code"]];
    l1.textAlignment = NSTextAlignmentLeft;
    [v addSubview: l1];
    
    UILabel* l2 = [[UILabel alloc] initWithFrame:CGRectMake(thePickerView.bounds.size.width - 100, 0, 80, 44)];
    //    l2.font = [UIFont sytemFontOfSize:14]; // choose desired size
    //    l2.tag = VIEWTAG_LINE2;
    l2.text = [NSString stringWithFormat:@"%@",[countryDetails objectForKey:@"dial_code"]];
    l2.textAlignment = NSTextAlignmentRight;
    [v addSubview: l2];
    
    return v;
}
-(void)createCountryCodePickerView{
    countryCodePickerView = [[UIPickerView alloc] init];
    [countryCodePickerView setDataSource: self];
    [countryCodePickerView setDelegate: self];
    countryCodePickerView.showsSelectionIndicator = YES;
    countryCodeTextField.inputView = countryCodePickerView;
}


-(void)shouldAcceptRelationshipWithDetails:(Relationship *)relationDetails{
    if([[RequestController sharedInstance] isNetworkReachable]){
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }
        
        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        NSString *relativeId = nil;
        NSString *status = @"1";
        if([[[UserData sharedInstance] getUserId] isEqualToString:[relationDetails getChildId]]){
            relativeId = [relationDetails getParentId];
        } else {
            relativeId = [relationDetails getChildId];
        }
        if(!relativeId){
            relativeId = [relationDetails getRelationshipId];
        }
        NSDictionary *details = [NSDictionary dictionaryWithObjectsAndKeys:relativeId,@"relativeId",status, @"accept", nil];
        [self acceptRejectRelationshipWithDetails:details];
        [[[UserData sharedInstance] getPendingMembersArray] removeObject:relationDetails];
        pendingRelationsArray = [[UserData sharedInstance] getPendingMembersArray];
        [_tableView reloadData];
    }
}

-(void)shouldRejectRelationshipWithDetails:(Relationship *)relationDetails{
    if([[RequestController sharedInstance] isNetworkReachable]){
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }
        
        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        NSString *relativeId = nil;
        NSString *status = @"0";
        if([[[UserData sharedInstance] getUserId] isEqualToString:[relationDetails getChildId]]){
            relativeId = [relationDetails getParentId];
        } else {
            relativeId = [relationDetails getChildId];
        }
        NSDictionary *details = [NSDictionary dictionaryWithObjectsAndKeys:[relationDetails getRelationshipId],@"relativeId",status, @"accept", nil];
        [self acceptRejectRelationshipWithDetails:details];
        [[[UserData sharedInstance] getPendingMembersArray] removeObject:relationDetails];
        pendingRelationsArray = [[UserData sharedInstance] getPendingMembersArray];
        [_tableView reloadData];
    }
}

-(void)back{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)showAddMemberView{
    NSLog(@"showAddMemberView");
    _memberContainerView.hidden = false;
    _relationPhoneTextField.leftPadding = [NSNumber numberWithInteger:70];
}

-(void)hideAddMemberView{
    NSLog(@"hideAddMemberView");
    _memberContainerView.hidden = true;
}

- (IBAction)cancelAddingMember:(id)sender{
    [self hideAddMemberView];
}

- (IBAction)saveMemberDetails:(id)sender{
    if ([_relationNameTextField.text isEqualToString:BlankText]) {
        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Please enter name" animated:YES];
        return;
    }
    if (![_relationEmailTextField.text isEqualToString:BlankText]) {
        if(![Validator validateEmailId:_relationEmailTextField.text]){
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"Invalid email id" animated:YES];
            return;
        }
    } else{
        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Enter email id" animated:YES];
        return;
    }
    if(![Validator validatePhoneNumber:_relationPhoneTextField.text withCountryCode:@"in"]){
        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Invalid Phone number" animated:YES];
        return;
    }
    
    [self.view endEditing:YES];
    
    if(!relationsArray){
        relationsArray = [NSMutableArray array];
    }
    for(NSDictionary *member in relationsArray){
        if([[member objectForKey:@"email"] isEqualToString:_relationEmailTextField.text]){
            [relationsArray removeObject:member];
        }
    }
    NSString *roleId = 0;
    for(NSDictionary *role in [[ApplicationData sharedInstance] getRoles]){
        if([[role objectForKey:@"roleDesc"] isEqualToString:_relationButton.currentTitle]){
            roleId = [role objectForKey:@"id"];
        }
    }
    NSArray *components = [_relationNameTextField.text componentsSeparatedByString:@" "];
    NSString *lastName = nil, *middleName = nil, *firstName = nil;
    if([components count] >= 3){
        lastName = [components lastObject];
        middleName = [components objectAtIndex:[components count]-2];
        firstName = [[components subarrayWithRange:NSMakeRange(0, components.count - 2)] componentsJoinedByString:@" "];
    } else if ([components count]==1){
        firstName = [components componentsJoinedByString:@" "];
    } else {
        lastName = [components lastObject];
        firstName = [components firstObject];
    }
    NSDictionary *member = [NSDictionary dictionaryWithObjectsAndKeys:
                            roleId, @"roleId",
                            _relationButton.currentTitle, @"roleDesc",
                            firstName?:@"", @"firstName",
                            middleName?:@"", @"middleName",
                            lastName?:@"", @"lastName",
                            _relationEmailTextField.text, @"email",
                            [NSString stringWithFormat:@"%@-%@",countryCodeTextField.text,_relationPhoneTextField.text], @"contactNo", nil];
    [relationsArray addObject:member];
    isMembersArrayUpdated = YES;
    [_tableView reloadData];
    [self hideAddMemberView];
}

- (IBAction)segmentControlValueChangedAction:(id)sender {
    [_tableView reloadData];
    [self updateHeaderLabel];
}

- (IBAction)moveNextAction:(id)sender {
    if([self getSelectedSegment] != 2){
        _needApprovalSegmentBtn.selected = false;
        _familySegmentBtn.selected = false;
        _interestsSegmentBtn.selected = false;
        if([self getSelectedSegment] == 0){
            _needApprovalSegmentBtn.selected = true;
        } else if([self getSelectedSegment] == 1){
            _familySegmentBtn.selected = true;
        } else {
            _interestsSegmentBtn.selected = true;
        }
        
        //[_segmentControl setSelectedSegmentIndex:_segmentControl.selectedSegmentIndex+1];
        [_tableView reloadData];
        [self updateHeaderLabel];
    } else {
        [self saveInterestAndDismiss];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

-(void)updateHeaderLabel{
    _memberContainerView.hidden = true;
    switch ([self getSelectedSegment]) {
        case 0:{
            [_headerLabel setText:@"Relationship Requests"];
        }
            break;
        case 1:{
            [_headerLabel setText:@"Relationships added by you"];
        }
            break;
        case 2:{
            [_headerLabel setText:@"Interests"];
            [self getUserInterests];
        }
            break;
            
        default:
            break;
    }
    if([self getSelectedSegment] == 2){
        [_nextButton setTitle:@"Done" forState:UIControlStateNormal];
    } else {
        [_nextButton setTitle:@"Next" forState:UIControlStateNormal];
    }
}

-(int)getSelectedSegment{
    if(_needApprovalSegmentBtn.selected){
        return 0;
    } else if (_familySegmentBtn.selected){
        return 1;
    } else {
        return 2;
    }
    
}

- (IBAction)saveRelationshipDetailsAndGoToHobbies:(id)sender{
    if(!isMembersArrayUpdated || [relationsArray count] <= 0){
        
    } else {
        if([[RequestController sharedInstance] isNetworkReachable]){
            if(!activityIndicator){
                activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
                activityIndicator.layer.cornerRadius = 05;
                activityIndicator.opaque = NO;
                activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
                activityIndicator.center = self.view.center;
                activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
                [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
                [self.view addSubview: activityIndicator];
            }
            
            self.view.userInteractionEnabled = false;
            [activityIndicator startAnimating];
                    
            [[RequestController sharedInstance] addMemberWithCredentials:relationsArray successHandler:^(NSDictionary *response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    NSLog(@"add member Response :: %@",response);
                    if ([response objectForKey:Key_Response]) {
                        
                    }
                });
            } failureHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    //[[[PJKeychainController alloc] init] clearKeyChain];
                    [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
                });
            }];
        }
    }
}


-(IBAction)toggleDropDownAction:(id)sender{
    NSLog(@"toggle drop down");
    
    if(dropDown == nil) {
        CGFloat f = 160;
        dropDown = [[DropDown alloc] showDropDown:sender height:&f arr:roles direction:@"down"];
        dropDown.selectorDelegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }
    [_tableView beginUpdates];
    [_tableView endUpdates];
    
}

-(void)didSelectItem:(NSString *)selectedItem atIndex:(NSNumber *)index{
    NSLog(@"%@",selectedItem);
    dropDown = nil;
}



-(void)saveInterestAndDismiss{
    if([savedInterestsArray count] > 0){
//        NSMutableArray *savedInterestsIdArr = [NSMutableArray array];
//        self.view.userInteractionEnabled = false;
//        [activityIndicator startAnimating];
//        for (NSDictionary *dict in savedInterestsArray) {
//            [savedInterestsIdArr addObject:[dict objectForKey:@"interestId"]];
//        }
        
        NSMutableArray *savedInterestsIdArr = [NSMutableArray array];
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }
        
        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        for (NSDictionary *dict in savedInterestsArray) {
            if([[dict objectForKey:@"selected"] boolValue])
                [savedInterestsIdArr addObject:[NSString stringWithFormat:@"%@",[dict objectForKey:@"interestId"]]];
        }
        [self saveInterestsWithDetails:[NSDictionary dictionaryWithObject:savedInterestsIdArr forKey:@"interests"]];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)acceptRejectRelationshipWithDetails:(NSDictionary *)details{
    [[RequestController sharedInstance] acceptRejectRelationshipWithDetails:details successHandler:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Success :: %@",response);
//            self.view.userInteractionEnabled = true;
//            [activityIndicator stopAnimating];
            if ([response objectForKey:Key_Response]) {
                [[HelperModule sharedInstance] showNetworkToast:YES message:[response objectForKey:Key_Response] animated:YES];
                [self getRelations];
            }
        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Error :: %@",error);
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo] animated:YES];
        });
    }];
}

-(void)getRelations{
    if([[RequestController sharedInstance] isNetworkReachable]){
        [[RequestController sharedInstance] getRelationsWithSuccessHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Success :: %@",response);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                if ([response objectForKey:Key_Response]) {
                    if([response objectForKey:Key_Token]){
                        [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:Key_Token]];
                    } else {
                        
                    }
                    [[UserData sharedInstance] parseRelationDataWithJSON:[response objectForKey:Key_Response]];
                    addedRelationsArray = [[UserData sharedInstance] getMembersArray];
                    [_tableView reloadData];
                }
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Error :: %@",error);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo] animated:YES];
            });
        }];
    }
}

-(void)getUserInterests{
    if([[RequestController sharedInstance] isNetworkReachable]){
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }
        
        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        [[RequestController sharedInstance] getUserInterestsWithSuccessHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Success :: %@",response);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                if([response objectForKey:Key_Response]) {
                    NSArray *userInterestsArray = [[response objectForKey:Key_Response] objectForKey:@"interests"];
                    if(!savedInterestsArray)
                        savedInterestsArray = [NSMutableArray array];
                    else
                        [savedInterestsArray removeAllObjects];
                    for (NSDictionary *dict in [[ApplicationData sharedInstance] getInterests]) {
                        NSMutableDictionary *interest = [NSMutableDictionary dictionaryWithDictionary:dict];
                        BOOL shouldSelect = NO;
                        for(int index = 0; index < [userInterestsArray count]; index++){
                            if([[[userInterestsArray objectAtIndex:index] objectForKey:@"interestId"] integerValue] == [[interest objectForKey:@"interestId"] integerValue]){
                                shouldSelect = YES;
                                break;
                            }
                        }
                        [interest setObject:[NSNumber numberWithBool:shouldSelect] forKey:@"selected"];
                        [savedInterestsArray addObject:interest];
                    }
                    [_tableView reloadData];
                }
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Error :: %@",error);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo] animated:YES];
            });
        }];
    }
}


-(void)saveInterestsWithDetails:(NSDictionary *)details{
    if([[RequestController sharedInstance] isNetworkReachable]){
        [[RequestController sharedInstance] saveUpdateInterestsWithDetails:details successHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Success :: %@",response);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                if ([response objectForKey:Key_Response]) {
                    [[HelperModule sharedInstance] showNetworkToast:YES message:@"Thanks for sharing your preferences, this will help LYK to serve your family better" animated:YES];
                }
                [self dismissViewControllerAnimated:YES completion:nil];
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Error :: %@",error);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo] animated:YES];
            });
        }];
    }
}

-(void)openAddressBook:(id)sender{
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
        //1
        NSLog(@"Denied");
        UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Cannot Load Contact" message: @"You must give the app permission to load the contact first." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [cantAddContactAlert show];
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        //2
        NSLog(@"Authorized");
        addressBookController = [[ABPeoplePickerNavigationController alloc] init];
        [addressBookController setPeoplePickerDelegate:self];
        [self presentViewController:addressBookController animated:YES completion:nil];
    } else{ //ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined
        //3
        NSLog(@"Not determined");
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            if (!granted){
                //4
                NSLog(@"Just denied");
                return;
            } else {
                addressBookController = [[ABPeoplePickerNavigationController alloc] init];
                [addressBookController setPeoplePickerDelegate:self];
                [self presentViewController:addressBookController animated:YES completion:nil];
            }
            //5
            NSLog(@"Just authorized");
        });
    }
}

-(void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person{
    [_relationNameTextField setText:@""];
    [_relationPhoneTextField setText:@""];
    [_relationEmailTextField setText:@""];
    NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString *middleName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonMiddleNameProperty);
    NSString *lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    NSArray *phones = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(ABRecordCopyValue(person, kABPersonPhoneProperty));
    NSArray *emails = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(ABRecordCopyValue(person, kABPersonEmailProperty));
    NSMutableArray *nameSegments = [NSMutableArray array];
    if(firstName)
        [nameSegments addObject:firstName];
    if(middleName)
        [nameSegments addObject:middleName];
    if(lastName)
        [nameSegments addObject:lastName];
    [_relationNameTextField setText:[nameSegments componentsJoinedByString:@" "]];
    if(phones)
        [_relationPhoneTextField setText:[phones objectAtIndex:0]?:@""];
    if(emails)
        [_relationEmailTextField setText:[emails objectAtIndex:0]?:@""];
}

-(void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
    
}

-(void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker{
    
}


@end
