//
//  InterestsViewController.m
//  LYK
//
//  Created by Subhapam on 05/12/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "InterestsViewController.h"
#import "InterestsTableViewCell.h"

@interface InterestsViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    UIActivityIndicatorView *activityIndicator;
    NSMutableArray *savedInterestsArray;
}
@property (weak, nonatomic) IBOutlet UIButton *updatePrefBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

- (IBAction)updatePrefAction:(id)sender;

@end

@implementation InterestsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _updatePrefBtn.layer.cornerRadius = 5.0f;
    _updatePrefBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _updatePrefBtn.layer.borderWidth = 1.0f;
    if(!savedInterestsArray)
        savedInterestsArray = [NSMutableArray array];
    for (NSDictionary *dict in [[ApplicationData sharedInstance] getInterests]) {
        NSMutableDictionary *interest = [NSMutableDictionary dictionaryWithDictionary:dict];
        [interest setObject:[NSNumber numberWithBool:NO] forKey:@"selected"];
        [savedInterestsArray addObject:interest];
    }
//    [self getUserInterests];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavHeader"]];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self getUserInterests];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[ApplicationData sharedInstance] getInterests] count];
}

-(InterestsTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    InterestsTableViewCell *cell = (InterestsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"interestsCellIdentifier"];
    if(!cell){
        cell = [[InterestsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"interestsCellIdentifier"];
    }
    cell.detailsLabel.text = [[savedInterestsArray objectAtIndex:indexPath.row] objectForKey:@"interestName"];
//    cell.accessoryType = UITableViewCellAccessoryNone;
    if([[[savedInterestsArray objectAtIndex:indexPath.row] objectForKey:@"selected"] boolValue]){
        [cell.checkImageView setImage:[UIImage imageNamed:@"SelectedCheckbox"]];
    } else {
        [cell.checkImageView setImage:[UIImage imageNamed:@"DeselectedCheckbox"]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(InterestsTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    InterestsTableViewCell *cell = (InterestsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"interestsCellIdentifier"];
//    if(!cell){
//        cell = [[InterestsTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"selectRelationCellIdentifier"];
//    }
    cell.detailsLabel.text = [[savedInterestsArray objectAtIndex:indexPath.row] objectForKey:@"interestName"];
    if([[[savedInterestsArray objectAtIndex:indexPath.row] objectForKey:@"selected"] boolValue]){
        [cell.checkImageView setImage:[UIImage imageNamed:@"SelectedCheckbox"]];
    } else {
        [cell.checkImageView setImage:[UIImage imageNamed:@"DeselectedCheckbox"]];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"didSelectRowAtIndexPath");
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    InterestsTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSMutableDictionary *dict = [savedInterestsArray objectAtIndex:indexPath.row];
    [dict setObject:[NSNumber numberWithBool:![[dict objectForKey:@"selected"] boolValue]] forKey:@"selected"];
    if ([[dict objectForKey:@"selected"] boolValue]) {
        [cell.checkImageView setImage:[UIImage imageNamed:@"SelectedCheckbox"]];
    } else {
        [cell.checkImageView setImage:[UIImage imageNamed:@"DeselectedCheckbox"]];
    }
    
//    NSDictionary *selectedDictionary = [[[ApplicationData sharedInstance] getInterests] objectAtIndex:indexPath.row];
//    
//    for(int index = 0; index < [savedInterestsArray count]; index++){
//        NSDictionary *dict = [savedInterestsArray objectAtIndex:index];
//        if([[dict objectForKey:@"interestName"] isEqualToString:[selectedDictionary objectForKey:@"interestName"]]){
//            [savedInterestsArray removeObjectAtIndex:index];
//            [cell.checkImageView setImage:[UIImage imageNamed:@"CircleDeselected"]];
//            return;
//        }
//    }
//    if(!savedInterestsArray)
//        savedInterestsArray = [NSMutableArray array];
//    [savedInterestsArray addObject:selectedDictionary];
//    [cell.checkImageView setImage:[UIImage imageNamed:@"CircleSelected"]];
}

-(void)getUserInterests{
    if([[RequestController sharedInstance] isNetworkReachable]){
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }

        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        [[RequestController sharedInstance] getUserInterestsWithSuccessHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Success :: %@",response);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                if([response objectForKey:Key_Response]) {
                    NSArray *userInterestsArray = [[response objectForKey:Key_Response] objectForKey:@"interests"];
                    if(!savedInterestsArray)
                        savedInterestsArray = [NSMutableArray array];
                    else
                        [savedInterestsArray removeAllObjects];
                    for (NSDictionary *dict in [[ApplicationData sharedInstance] getInterests]) {
                        NSMutableDictionary *interest = [NSMutableDictionary dictionaryWithDictionary:dict];
                        BOOL shouldSelect = NO;
                        for(int index = 0; index < [userInterestsArray count]; index++){
                            if([[[userInterestsArray objectAtIndex:index] objectForKey:@"interestId"] integerValue] == [[interest objectForKey:@"interestId"] integerValue]){
                                shouldSelect = YES;
                                break;
                            }
                        }
                        [interest setObject:[NSNumber numberWithBool:shouldSelect] forKey:@"selected"];
                        [savedInterestsArray addObject:interest];
                    }
                    
                    
                    
                    
                    [_tableview reloadData];
                }
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Error :: %@",error);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo] animated:YES];
            });
        }];
    }
}

- (IBAction)updatePrefAction:(id)sender{
    if([savedInterestsArray count] > 0){
        NSMutableArray *savedInterestsIdArr = [NSMutableArray array];
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }

        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        for (NSDictionary *dict in savedInterestsArray) {
            if([[dict objectForKey:@"selected"] boolValue])
                [savedInterestsIdArr addObject:[NSString stringWithFormat:@"%@",[dict objectForKey:@"interestId"]]];
        }
        [self saveInterestsWithDetails:[NSDictionary dictionaryWithObject:savedInterestsIdArr forKey:@"interests"]];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)saveInterestsWithDetails:(NSDictionary *)details{
    if([[RequestController sharedInstance] isNetworkReachable]){
        [[RequestController sharedInstance] saveUpdateInterestsWithDetails:details successHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Success :: %@",response);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                if ([response objectForKey:Key_Response]) {
                    [[HelperModule sharedInstance] showNetworkToast:YES message:@"Thanks for sharing your preferences, this will help LYK to serve your family better" animated:YES];
                }
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Error :: %@",error);
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo] animated:YES];
            });
        }];
    }
}


@end
