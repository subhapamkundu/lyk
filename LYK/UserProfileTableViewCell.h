//
//  UserProfileTableViewCell.h
//  LYK
//
//  Created by S. Kundu on 23/06/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfileTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *leftIconImageView;
@property (weak, nonatomic) IBOutlet LYKTextField *dataTextField;

@end
