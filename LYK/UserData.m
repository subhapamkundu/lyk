//
//  UserData.m
//  LYK
//
//  Created by Subhapam on 18/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "UserData.h"
#import "UserDefaultsController.h"
#import "UnsafeRegionProcessor.h"

//@interface FlipDataSet ()
//{
//    
//}
//@end
//
//@implementation FlipDataSet
//
//-(BOOL)parseDataSetWithDetails:(NSDictionary *)json{
//    _maxNewsId = [[json objectForKey:@"maxNewsId"] integerValue];
//    _maxInsightId = [[json objectForKey:@"maxInsightId"] integerValue];
//    _maxQuestionId = [[json objectForKey:@"maxQuestionId"] integerValue];
//    _maxOfferId = [[json objectForKey:@"maxOfferId"] integerValue];
//    _maxProductId = [[json objectForKey:@"maxProductId"] integerValue];
//    for (NSDictionary *details in [json objectForKey:@"contents"]) {
//        NSDictionary *parsedDetails = nil;
//        if([[details objectForKey:@"type"] isEqualToString:@"news"]){
//            parsedDetails = [NSDictionary dictionaryWithObjectsAndKeys:
//                             [[details objectForKey:@"content"] objectForKey:@"newsId"]?:@"", @"id",
//                             [[details objectForKey:@"content"] objectForKey:@"userId"]?:@"", @"userId",
//                             [[details objectForKey:@"content"] objectForKey:@"newsTitle"]?:@"", @"statement",
//                             [[details objectForKey:@"content"] objectForKey:@"newsImageUrl"]?:@"", @"imageURL",
//                             [[details objectForKey:@"content"] objectForKey:@"newsDescription"]?:@"", @"desc",
//                             [[details objectForKey:@"content"] objectForKey:@"newsLink"]?:@"", @"linkURL",
//                             @"news", @"type",nil];
//        } else if ([[details objectForKey:@"type"] isEqualToString:@"insight"]){
//            parsedDetails = [NSDictionary dictionaryWithObjectsAndKeys:
//                             [[details objectForKey:@"content"] objectForKey:@"insightId"]?:@"", @"id",
//                             [[details objectForKey:@"content"] objectForKey:@"userId"]?:@"", @"userId",
//                             [[details objectForKey:@"content"] objectForKey:@"statement"]?:@"", @"statement",
//                             [[details objectForKey:@"content"] objectForKey:@"insightImageUrl"]?:@"", @"imageURL",
//                             [[details objectForKey:@"content"] objectForKey:@"recoTxt"]?:@"", @"desc",
//                             [[details objectForKey:@"content"] objectForKey:@"recoLink"]?:@"", @"linkURL",
//                             @"insight", @"type", nil];
//        } else if ([[details objectForKey:@"type"] isEqualToString:@"question"]){
//            parsedDetails = [NSDictionary dictionaryWithObjectsAndKeys:
//                             [[details objectForKey:@"content"] objectForKey:@"id"]?:@"", @"id",
//                             [[details objectForKey:@"content"] objectForKey:@"userId"]?:@"", @"userId",
//                             [[details objectForKey:@"content"] objectForKey:@"subject"]?:@"", @"statement",
//                             [[details objectForKey:@"content"] objectForKey:@"questionImageUrl"]?:@"", @"imageURL",
//                             [[details objectForKey:@"content"] objectForKey:@"contentBody"]?:@"", @"desc",
//                             [[details objectForKey:@"content"] objectForKey:@"url"]?:@"", @"linkURL",
//                             @"question", @"type", nil];
//        } else if ([[details objectForKey:@"type"] isEqualToString:@"offer"]){
//            NSDictionary *itemDetail = [[[details objectForKey:@"content"] objectForKey:@"itemDetail"] objectAtIndex:0];
//            parsedDetails = [NSDictionary dictionaryWithObjectsAndKeys:
//                             [[details objectForKey:@"content"] objectForKey:@"id"]?:@"", @"id",
//                             [[details objectForKey:@"content"] objectForKey:@"userId"]?:@"", @"userId",
//                             [[details objectForKey:@"content"] objectForKey:@"statement"]?:@"", @"statement",
//                             [[details objectForKey:@"content"] objectForKey:@"offerImageUrl"]?:@"", @"imageURL",
//                             [itemDetail objectForKey:@"ItemDesc"]?:@"", @"desc",
//                             [[details objectForKey:@"content"] objectForKey:@"offerUrl"]?:@"", @"linkURL",
//                             @"offer", @"type", nil];
//        } else if ([[details objectForKey:@"type"] isEqualToString:@"product"]){
//            parsedDetails = [NSDictionary dictionaryWithObjectsAndKeys:
//                             [[details objectForKey:@"content"] objectForKey:@"productId"]?:@"", @"id",
//                             [[details objectForKey:@"content"] objectForKey:@"userId"]?:@"", @"userId",
//                             [[details objectForKey:@"content"] objectForKey:@"itemDesc"]?:@"", @"statement",
//                             [[details objectForKey:@"content"] objectForKey:@"productImageUrl"]?:@"", @"imageURL",
//                             [[details objectForKey:@"content"] objectForKey:@"offer"]?:@"", @"desc",
//                             [[details objectForKey:@"content"] objectForKey:@"productItemUrl"]?:@"", @"linkURL",
//                             @"product", @"type", nil];
//        }
//        if(!_contents)
//            _contents = [NSMutableArray array];
//        [_contents insertObject:parsedDetails atIndex:[_contents count]];
//    }
//    return YES;
//}
//
//@end



@interface Relationship ()
{
    NSString *relationshipId;
    NSString *relationshipPhone;
    NSString *relationshipEmail;
    NSString *relationshipFirstName;
    NSString *relationshipMiddleName;
    NSString *relationshipLastName;
    NSString *relationshipRoleId;
    NSString *relationshipChildId;
    NSString *relationshipParentId;
    NSString *relationshipState;
    NSString *relationshipImageURL;
    BOOL relationshipAccepted;
}

-(void)setMyselfAsRelationWithUserData:(UserData *)me;

@end

@implementation Relationship

-(void)setMyselfAsRelationWithUserData:(UserData *)me{
    relationshipId = [me getUserId];
    relationshipPhone = [me getUserPhoneNumber];
    relationshipEmail = [me getEmailId];
    relationshipFirstName = @"For";
    relationshipMiddleName = @"";
    relationshipLastName = @"Myself";
//    relationshipRoleId = [me get];
//    relationshipChildId = [me getUserId];
//    relationshipParentId = [me getUserId];
//    relationshipState = ;
    relationshipAccepted = YES;
}

-(BOOL)parseRelationshipDataWithDetails:(NSDictionary *)json{
    if([json objectForKey:@"userId"])
        relationshipId = [json objectForKey:@"userId"];
    
    if([json objectForKey:@"firstName"])
        relationshipFirstName = [json objectForKey:@"firstName"];
    
    if([json objectForKey:@"middleName"])
        relationshipMiddleName = [json objectForKey:@"middleName"];
    
    if([json objectForKey:@"lastName"])
        relationshipLastName = [json objectForKey:@"lastName"];
    
    if([json objectForKey:@"contactNo"])
        relationshipPhone = [json objectForKey:@"contactNo"];
    
    if([json objectForKey:@"email"])
        relationshipEmail = [json objectForKey:@"email"];
    
    if([json objectForKey:@"roleId"])
        relationshipRoleId = [json objectForKey:@"roleId"];
    
    if([json objectForKey:@"childId"])
        relationshipChildId = [json objectForKey:@"childId"];
    
    if([json objectForKey:@"parentId"])
        relationshipParentId = [json objectForKey:@"parentId"];
    
    if([json objectForKey:@"relation"])
        relationshipState = [json objectForKey:@"relation"];
    
    if([json objectForKey:@"imageUrl"])
        relationshipImageURL = [json objectForKey:@"imageUrl"];
    
    if([json objectForKey:@"accepted"])
        relationshipAccepted = YES;
    else
        relationshipAccepted = NO;
    relationshipAccepted = YES;
    return YES;
}

-(NSString *)getUserName{
    return [NSString stringWithFormat:@"%@ %@ %@",relationshipFirstName?:@"", relationshipMiddleName?:@"", relationshipLastName?:@""];
}

-(NSString *)getChildId{
    return relationshipChildId;
}

-(NSString *)getParentId{
    return relationshipParentId;
}

-(NSString *)getRelationshipId{
    return relationshipId;
}

-(NSString *)getPhone{
    return relationshipPhone;
}
-(NSString *)getEmail{
    return relationshipEmail;
}
-(NSString *)getRelationDescription{
    return relationshipState;
}
-(NSString *)getRelationImageURL{
    return relationshipImageURL;
}



-(NSString *)getRelationRoleId{
    return relationshipRoleId;
}

-(void)setFirstName:(NSString *)fname{
    relationshipFirstName = fname;
}
-(void)setMiddleName:(NSString *)mname{
    relationshipMiddleName = mname;
}

-(void)setLastName:(NSString *)lname{
    relationshipLastName = lname;
}

-(void)setPhone:(NSString *)contactNo{
    relationshipPhone = contactNo;
}

-(void)setEmail:(NSString *)email{
    relationshipEmail = email;
}

-(void)setRelationDescription:(NSString *)desc{
    relationshipState = desc;
}

-(void)setRoleId:(NSString *)roleId{
    relationshipRoleId = roleId;
}


@end

#pragma mark - SOS Data Class

@interface SOSData ()
{
    NSString *sosId;
    NSString *sosName;
    NSString *sosNumber;
    NSString *sosEmail;
    BOOL isSOSSetByMe;
    BOOL sosAccepted;
}

@end

@implementation SOSData

-(BOOL)parseSOSDataWithDetails:(NSDictionary *)json{
    if(![[json objectForKey:@"id"] isKindOfClass:[NSNull class]])
        sosId = [json objectForKey:@"id"];
    
    if(![[json objectForKey:@"name"] isKindOfClass:[NSNull class]])
        sosName = [json objectForKey:@"name"];
    
    if(![[json objectForKey:@"email"] isKindOfClass:[NSNull class]])
        sosEmail = [json objectForKey:@"email"];
    
    if(![[json objectForKey:@"accepted"] isKindOfClass:[NSNull class]])
        sosAccepted = [[json objectForKey:@"accepted"] boolValue];
    
    if(![[json objectForKey:@"isSOSSetByMe"] isKindOfClass:[NSNull class]])
        isSOSSetByMe = [[json objectForKey:@"isSOSSetByMe"] boolValue];
    
    return YES;
}

@end

#pragma mark - User Data Class


@interface UserData ()
{
    NSString *userId;
    NSString *userFirstName;
    NSString *userMiddleName;
    NSString *userLastName;
    NSString *userProfilePicURL;
    NSString *emailId;
    NSString *userPhone;
    NSString *userPhoneCountryCode;
    NSString *userDOB;
    NSUInteger roleId;
    NSUInteger genderId;
    NSDictionary *userDetails;
    NSMutableArray *relationships;
    NSMutableArray *pendingRelationships;
    NSMutableArray *sosListByUser;
    NSMutableArray *sosListForUser;
    NSString *userStatus;
    NSInteger unreadNotificationCount;
}
@end



@implementation UserData


+(id)sharedInstance{
    static UserData *sharedInstance_ = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        sharedInstance_ = [[UserData alloc] init];
    });
    return sharedInstance_;
}

-(BOOL)parseUserDataWithJSON:(NSDictionary *)json{
    userDetails = [json objectForKey:Key_Response];
    if([[json objectForKey:Key_Response] objectForKey:@"email"])
        emailId = [[json objectForKey:Key_Response] objectForKey:@"email"];
    else
        return NO;
    
    if([[json objectForKey:Key_Response] objectForKey:@"userId"])
        userId = [[json objectForKey:Key_Response] objectForKey:@"userId"];
    else
        return NO;
    
    if([[json objectForKey:Key_Response] objectForKey:@"roleId"])
        roleId = [[[json objectForKey:Key_Response] objectForKey:@"roleId"] integerValue];
    if([[json objectForKey:Key_Response] objectForKey:@"gender"])
        genderId = [[[json objectForKey:Key_Response] objectForKey:@"gender"] integerValue];
    
    userFirstName = [[json objectForKey:Key_Response] objectForKey:@"firstName"]?:BlankText;
    userMiddleName = [[json objectForKey:Key_Response] objectForKey:@"middleName"]?:BlankText;
    userLastName = [[json objectForKey:Key_Response] objectForKey:@"lastName"]?:BlankText;
    userProfilePicURL = [[json objectForKey:Key_Response] objectForKey:@"imageUrl"]?:BlankText;
    userStatus = [[json objectForKey:Key_Response] objectForKey:@"status"]?:@"Available";
//    NSString *phone = [[json objectForKey:Key_Response] objectForKey:@"contactNo"]?:BlankText;
    userPhone = [[json objectForKey:Key_Response] objectForKey:@"contactNo"]?:BlankText;
    userPhoneCountryCode = [[json objectForKey:Key_Response] objectForKey:@"countryCode"]?:BlankText;
//    NSArray *phoneNumberSegments = [phone componentsSeparatedByString:@"-"];
//    userPhone = [phoneNumberSegments lastObject];
//    if(phoneNumberSegments.count > 1)
//        userPhoneCountryCode = [phoneNumberSegments objectAtIndex:0];
    userDOB = [[json objectForKey:Key_Response] objectForKey:@"dateOfBirth"]?:BlankText;
    
    [self fetchSOSListCreatedByUser];
    [self fetchSOSListCreatedForUser];
    [self fetchRegionListCreatedByUser];
    [self fetchRegionListCreatedForUser];
    
    return YES;
}

-(BOOL)parseRelationDataWithJSON:(NSDictionary *)json{
    
    BOOL flag = NO;
    if(!relationships){
        relationships = [NSMutableArray array];
    } else {
        [relationships removeAllObjects];
    }
    for(NSDictionary *dict in [json objectForKey:@"relations"]){
        Relationship *rel = [[Relationship alloc] init];
        flag = [rel parseRelationshipDataWithDetails:dict];
        if(flag){
            [relationships addObject:rel];
        } else {
            break;
        }
    }
    /*if(!pendingRelationships){
        pendingRelationships = [NSMutableArray array];
    } else {
        [pendingRelationships removeAllObjects];
    }
    for(NSDictionary *dict in [json objectForKey:@"pending"]){
        Relationship *rel = [[Relationship alloc] init];
        flag = [rel parseRelationshipDataWithDetails:dict];
        if(flag){
            [pendingRelationships addObject:rel];
        } else {
            break;
        }
    }*/
    return flag;
}

-(BOOL)parsePendingRelationsDataWithJSON:(NSDictionary *)json{
    
    BOOL flag = NO;
    if(!pendingRelationships){
        pendingRelationships = [NSMutableArray array];
    } else {
        [pendingRelationships removeAllObjects];
    }
    
    //change the key name
    for(NSDictionary *dict in [json objectForKey:@"relations"]){
        Relationship *rel = [[Relationship alloc] init];
        flag = [rel parseRelationshipDataWithDetails:dict];
        if(flag){
            [pendingRelationships addObject:rel];
        } else {
            break;
        }
    }
    
    
    return flag;
}

//-(BOOL)parseFlipDataSetWithJSON:(NSDictionary *)json{
//    BOOL flag = NO;
//    if(!_dataSet)
//        _dataSet= [[FlipDataSet alloc] init];
//    flag = [_dataSet parseDataSetWithDetails:[json objectForKey:@"response"]];
//    return flag;
//}

-(BOOL)parseSOSDataSetWithJSON:(NSDictionary *)json isCreatedBySelf:(BOOL)isCreatedBySelf{
    BOOL isSuccessful = NO;
    if(isCreatedBySelf){
        if(!sosListByUser){
            sosListByUser = [NSMutableArray array];
        }
    } else {
        if(!sosListForUser){
            sosListForUser = [NSMutableArray array];
        }
    }
    for(NSDictionary *dict in [json objectForKey:@"response"]){
        SOSData *sosData = [[SOSData alloc] init];
        isSuccessful = [sosData parseSOSDataWithDetails:dict];
        if(isCreatedBySelf){
            if(!isSuccessful){
                sosListByUser = nil;
                break;
            } else {
                [sosListByUser addObject:sosData];
            }
        } else {
            if(!isSuccessful){
                sosListForUser = nil;
                break;
            } else {
                [sosListForUser addObject:sosData];
            }
        }
    }
    return isSuccessful;
}

-(void)setUserId:(NSString *)uid{
    userId = uid;
}

-(void)setEmailId:(NSString *)email{
    emailId = email;
}

-(void)setFirstName:(NSString *)fname{
    userFirstName = fname;
}

-(void)setMiddleName:(NSString *)mname{
    userMiddleName = mname;
}

-(void)setLastName:(NSString *)lname{
    userLastName = lname;
}

-(void)setPhoneNumber:(NSString *)number{
    userPhone = number;
}

-(void)setPhoneNumberCountryCode:(NSString *)number{
    userPhoneCountryCode = number;
}

-(void)setNotificationCount:(NSInteger)count{
    unreadNotificationCount = count;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UnreadNotificationCountUpdated" object:nil];
    [UIApplication sharedApplication].applicationIconBadgeNumber = unreadNotificationCount;
}

-(void)setStatus:(NSString *)status{
    userStatus = status;
}

-(NSString *)getUserName{
    return LYKConcatStrings(LYKConcatStrings(LYKConcatStrings(userFirstName?:@"", userFirstName?@" ":@""), LYKConcatStrings(userMiddleName?:@"",userMiddleName?@" ":@"")),userLastName?:@"");
}

-(NSString *)getUserFirstName{
    return userFirstName?:@"";
}

-(NSString *)getUserMiddleName{
    return userMiddleName?:@"";
}

-(NSString *)getUserLastName{
    return userLastName?:@"";
}


-(NSString *)getProfilePicURL{
    return userProfilePicURL;
}

-(NSString *)getEmailId{
    return emailId?:@"";
}

-(NSMutableArray *)getMembersArray{
    return relationships;
}

-(NSMutableArray *)getMembersArrayWithSelf{
    NSMutableArray *arr = [NSMutableArray array];
//    Relationship *me = [[Relationship alloc] init];
//    [me setMyselfAsRelationWithUserData:self];
//    [arr addObject:me];
    [arr addObjectsFromArray:relationships];
    return arr;
}

-(NSMutableArray *)getPendingMembersArray{
    
    return pendingRelationships;
}


-(NSUInteger)getRoleId{
    return roleId;
}

-(NSString *)getUserId{
    return userId;
}

-(NSString *)getDOB{
    return userDOB?:@"";
}

-(NSString *)getUserPhoneNumber{
    return userPhone?:@"";
}

-(NSString *)getUserPhoneNumberCountryCode{
    return userPhoneCountryCode?:@"";
}

-(NSDictionary *)getUserDetails{
    return userDetails;
}

-(NSInteger)getNotificationCount{
    return unreadNotificationCount;
}

-(NSString *)getUserStatus{
    return userStatus;
}
//-(NSMutableArray *)getSOSListByUser{
//    return sosListByUser;
//}
//
//-(NSMutableArray *)getSOSListForUser{
//    return sosListForUser;
//}

-(void)fetchSOSListCreatedByUser{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    [[RequestController sharedInstance] getSOSListByUserWithSuccessHandler:^(NSDictionary *response) {
//        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"data set: %@", response);
//            sosListByUser = [[response objectForKey:@"response"] mutableCopy];
            NSMutableDictionary *sosToSave = [NSMutableDictionary dictionary];
            for (NSDictionary *sos in [response objectForKey:@"response"]) {
                [sosToSave setObject:@{@"mobileNo":[sos objectForKey:@"mobileNo"],@"sosName":[sos objectForKey:@"sosName"]} forKey:[sos objectForKey:@"sosId"]];
            }
            [UserDefaultsController saveSOSListByUserToUserDefaults:sosToSave deleteExisting:YES];
//        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%@",error);
            NSMutableDictionary *sosToSave = [NSMutableDictionary dictionary];
            [UserDefaultsController saveSOSListByUserToUserDefaults:sosToSave deleteExisting:YES];
        });
    }];
}

-(void)fetchSOSListCreatedForUser{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    [[RequestController sharedInstance] getSOSListForUserWithSuccessHandler:^(NSDictionary *response) {
//        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"data set: %@", response);
//            sosListForUser = [[response objectForKey:@"response"] mutableCopy];
            NSMutableDictionary *sosToSave = [NSMutableDictionary dictionary];
            for (NSDictionary *sos in [response objectForKey:@"response"]) {
                [sosToSave setObject:@{@"mobileNo":[sos objectForKey:@"mobileNo"],@"sosName":[sos objectForKey:@"sosName"]} forKey:[sos objectForKey:@"sosId"]];
            }
            [UserDefaultsController saveSOSListForUserToUserDefaults:sosToSave deleteExisting:YES];
//        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSMutableDictionary *sosToSave = [NSMutableDictionary dictionary];
            [UserDefaultsController saveSOSListForUserToUserDefaults:sosToSave deleteExisting:YES];
            NSLog(@"%@",error);
        });
    }];
}

-(void)fetchRegionListCreatedByUser{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    [[RequestController sharedInstance] getRegionListByUserWithSuccessHandler:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"data set: %@", response);
            [UnsafeRegionProcessor saveCircularSafetyRegionsWithRegions:[[response objectForKey:@"response"] objectForKey:@"locations"] createdByUser:true];
        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%@",error);
        });
    }];
}

-(void)fetchRegionListCreatedForUser{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    [[RequestController sharedInstance] getRegionListForUserWithSuccessHandler:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"data set: %@", response);
            [UnsafeRegionProcessor saveCircularSafetyRegionsWithRegions:[[[response objectForKey:@"response"] objectAtIndex:0] objectForKey:@"locations"] createdByUser:false];
        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%@",error);
        });
    }];
}


-(void)clearUserDetails{
    userDetails = nil;
    emailId = nil;
    userId = nil;
    roleId = 0;
    genderId = 0;
    userFirstName = nil;
    userMiddleName = nil;
    userLastName = nil;
    userProfilePicURL = nil;
    userPhone = nil;
    userDOB = nil;
    relationships = nil;
    pendingRelationships = nil;
//    sosList = nil;
}


@end
