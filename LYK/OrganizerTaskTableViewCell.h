//
//  OrganizerTaskTableViewCell.h
//  LYK
//
//  Created by Subhapam kundu on 4/6/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrganizerTaskTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *taskTitle;
@property (weak, nonatomic) IBOutlet UILabel *taskDesc;
@property (weak, nonatomic) IBOutlet UIImageView *firstSharedDPImageView;
@property (weak, nonatomic) IBOutlet UIImageView *secondSharedDPImageView;
@property (weak, nonatomic) IBOutlet UIImageView *thirdSharedDPImageView;
@property (weak, nonatomic) IBOutlet UIImageView *leftBarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *barImageView;
@property (weak, nonatomic) IBOutlet UILabel *dpCountLabel;

@end
