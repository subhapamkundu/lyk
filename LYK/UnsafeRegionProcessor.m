//
//  SafetyLocationAlertController.m
//  LYK
//
//  Created by Subhapam kundu on 4/8/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import "UnsafeRegionProcessor.h"
#import "UserDefaultsController.h"

@implementation UnsafeRegionProcessor

+(void)saveCircularSafetyRegionsWithRegions:(NSArray *)regions createdByUser:(BOOL)flag{
    NSMutableDictionary *circularSafetyRegions = nil;
    for (NSDictionary *region in regions) {
        NSString *regionId = [region objectForKey:@"placeId"];
        NSString *regionName = [region objectForKey:@"placeName"];
        NSArray *latLongSet = [region objectForKey:@"latLong"];
        CLLocationCoordinate2D maxCoordinate, minCoordinate;
        double maxX = 0, maxY = 0, minX = 360, minY = 360;
        for (NSDictionary *latLong in latLongSet) {
            double lat = [[latLong objectForKey:@"lat"] doubleValue];
            double longi = [[latLong objectForKey:@"longi"] doubleValue];
            if(maxX < lat){
                maxX = lat;
            }
            if(maxY < longi){
                maxY = longi;
            }
            if(minX > lat){
                minX = lat;
            }
            if(minY > longi){
                minY = longi;
            }
        }
        maxCoordinate = CLLocationCoordinate2DMake(maxX, maxY);
        minCoordinate = CLLocationCoordinate2DMake(minX, minY);
        
        NSNumber *centreX = [NSNumber numberWithDouble:(maxX +minX)/2];
        NSNumber *centreY = [NSNumber numberWithDouble:(maxY +minY)/2];
        NSNumber *radius = [NSNumber numberWithDouble:[[[CLLocation alloc] initWithLatitude:maxX longitude:maxY] distanceFromLocation:[[CLLocation alloc] initWithLatitude:minX longitude:minY]]/2];
        if(!circularSafetyRegions)
            circularSafetyRegions = [NSMutableDictionary dictionary];
        [circularSafetyRegions setObject:@{@"centreX":centreX, @"centreY":centreY, @"radius":radius, @"regionId":regionId, @"regionName":regionName} forKey:regionId];
    }
    if (flag) {
        [UserDefaultsController saveSafetyLocationsByUserToUserDefaults:circularSafetyRegions deleteExisting:true];
    } else {
        [UserDefaultsController saveSafetyLocationsForUserToUserDefaults:circularSafetyRegions deleteExisting:true];
    }
    
    //circularSafetyRegions;
}


@end
