//
//  ExploreProductListViewController.m
//  LYK
//
//  Created by Subhapam on 22/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import "ExploreProductListViewController.h"
#import "ExploreProductDetailViewController.h"
#import "DetailContentViewController.h"

@interface ExploreProductListViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    UIActivityIndicatorView *activityIndicator;
    NSMutableArray *productList;
    NSDictionary *productDetails;
}

@property (weak, nonatomic) IBOutlet UITableView *productListTableView;



@end

@implementation ExploreProductListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"%@",_info);
    [self.navigationItem setTitle:[_info objectForKey:@"categoryName"]];
    [self getProductListWithCategoryId:[_info objectForKey:@"categoryId"] subCategoryId:[_info objectForKey:@"subCategoryId"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:ProductListToDetailSegue]){
        ExploreProductDetailViewController *vc = (ExploreProductDetailViewController *)segue.destinationViewController;
        [vc setInfo:sender];
    } else if([segue.identifier isEqualToString:ProductListToWebDetailSegue]){
        DetailContentViewController *vc = (DetailContentViewController *)segue.destinationViewController;
        [vc setURL:sender];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return productList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"productListCellIdentifier"];
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"productListCellIdentifier"];
    }
    cell.textLabel.text = [[[productList objectAtIndex:indexPath.row] objectForKey:@"productDetails"] objectForKey:@"productName"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self fetchProductDetailsAtIndex:indexPath.row];
}




-(void)getProductListWithCategoryId:(NSString *)categoryId subCategoryId:(NSString *)subCategoryId{
    if(!activityIndicator){
        activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
        activityIndicator.layer.cornerRadius = 05;
        activityIndicator.opaque = NO;
        activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        activityIndicator.center = self.view.center;
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview: activityIndicator];
    }
    self.view.userInteractionEnabled = false;
    [activityIndicator startAnimating];
    NSMutableDictionary *requestIds = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       categoryId, @"categoryId",
                                       subCategoryId, @"subCategoryId", nil];
    [[RequestController sharedInstance] getExploreProductsWithCredentials:requestIds successHandler:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            NSLog(@"data set: %@", response);
            if([response objectForKey:@"token"]){
                [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:@"token"]];
            } else {
                //[[HelperModule sharedInstance] showNetworkToast:YES message:@"Token not validated" animated:YES];
            }
            if([response objectForKey:@"response"]){
                productList = [NSMutableArray arrayWithArray:[[response objectForKey:@"response"] objectForKey:@"products"]];
                [_productListTableView reloadData];
            }
        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            NSLog(@"%@",error);
        });
    }];
}

-(void)fetchProductDetailsAtIndex:(NSInteger)index{
    if(!activityIndicator){
        activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
        activityIndicator.layer.cornerRadius = 05;
        activityIndicator.opaque = NO;
        activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        activityIndicator.center = self.view.center;
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview: activityIndicator];
    }
    self.view.userInteractionEnabled = false;
    [activityIndicator startAnimating];
    NSMutableDictionary *requestIds = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       [[productList objectAtIndex:index] objectForKey:@"productId"], @"productId", nil];
    [[RequestController sharedInstance] getExploreProductDetailsWithCredentials:requestIds successHandler:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            NSLog(@"data set: %@", response);
            if([response objectForKey:@"token"]){
                [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:@"token"]];
            } else {
                //[[HelperModule sharedInstance] showNetworkToast:YES message:@"Token not validated" animated:YES];
            }
            if([response objectForKey:@"response"]){
                productDetails = [response objectForKey:@"response"];
                if([[productDetails objectForKey:@"productType"] isEqualToString:@"doctor"]){
                    [self performSegueWithIdentifier:ProductListToDetailSegue sender:productDetails];
                } else if([[productDetails objectForKey:@"productType"] isEqualToString:@"tutor"]){
                    [self performSegueWithIdentifier:ProductListToDetailSegue sender:productDetails];
                } else {
                    [self performSegueWithIdentifier:ProductListToWebDetailSegue sender:[[productDetails objectForKey:@"productDetails"] objectForKey:@"productItemUrl"]];
                }
            }
        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            NSLog(@"%@",error);
        });
    }];
}



@end
