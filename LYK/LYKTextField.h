//
//  LYKTextField.h
//  LYK
//
//  Created by Subhapam Kundu on 01/09/15.
//  Copyright (c) 2015 Subhapam Kundu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"

@interface LYKTextField : JVFloatLabeledTextField

@property (nonatomic, retain) UITextField *countryCodeTextField;
@property (nonatomic, retain) NSNumber *leftPadding;

-(void)integrateCountryCodeSegment;

/*/**
 * Controls whether this marker should be flat against the Earth's surface (YES)
 * or a billboard facing the camera (NO, default).
 *
@property(nonatomic, assign, getter=isFlat) BOOL flat;*/
/*
 *By setting 'YES', textfield will look like android style (YES, default)
 */

@property (nonatomic,assign,getter=isFloatingLabeEnable) BOOL floatingLabelEnable;

@end
