//
//  ContentViewController.m
//  LYK
//
//  Created by Subhapam on 19/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "ContentViewController.h"
#import <UIImageView+AFNetworking.h>

#define FRAME_MARGIN 10

@interface ContentViewController (){
    NSURL *imageURL;
    NSString *description;
    NSString *contentTitle;
    NSString *contentId;
    NSString *contentType;
    UIActivityIndicatorView *activityIndicator;
}

- (IBAction)answerBtnYesAction:(id)sender;
- (IBAction)answerBtnNoAction:(id)sender;

@end

@implementation ContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_imageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"newsBG"]];
    [_label setText:description];
    [_titleLabel setText:contentTitle];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _answerBtnNo.hidden = ![contentType isEqualToString:@"question"];
    _answerBtnYes.hidden = ![contentType isEqualToString:@"question"];
    
//    _answerBtnNo.layer.borderColor = [UIColor darkGrayColor].CGColor;
//    _answerBtnNo.layer.borderWidth = 0.0f;
    _answerBtnNo.layer.cornerRadius = 5.0f;
//    _answerBtnYes.layer.borderColor = [UIColor darkGrayColor].CGColor;
//    _answerBtnYes.layer.borderWidth = 1.0f;
    _answerBtnYes.layer.cornerRadius = 5.0f;
//    CAGradientLayer *gradientLayerYes = [CAGradientLayer layer];
//    gradientLayerYes.frame = _answerBtnYes.layer.bounds;
//    
//    gradientLayerYes.colors = [NSArray arrayWithObjects:
//                            (id)[UIColor colorWithWhite:1.0f alpha:0.1f].CGColor,
//                            (id)[UIColor colorWithWhite:0.4f alpha:0.5f].CGColor,
//                            nil];
//    
//    gradientLayerYes.locations = [NSArray arrayWithObjects:
//                               [NSNumber numberWithFloat:0.0f],
//                               [NSNumber numberWithFloat:1.0f],
//                               nil];
//    gradientLayerYes.cornerRadius = _answerBtnYes.layer.cornerRadius;
//    [_answerBtnYes.layer addSublayer:gradientLayerYes];
//    CAGradientLayer *gradientLayerNo = [CAGradientLayer layer];
//    gradientLayerNo.frame = _answerBtnNo.layer.bounds;
//    
//    gradientLayerNo.colors = [NSArray arrayWithObjects:
//                               (id)[UIColor colorWithWhite:1.0f alpha:0.1f].CGColor,
//                               (id)[UIColor colorWithWhite:0.4f alpha:0.5f].CGColor,
//                               nil];
//    
//    gradientLayerNo.locations = [NSArray arrayWithObjects:
//                                  [NSNumber numberWithFloat:0.0f],
//                                  [NSNumber numberWithFloat:1.0f],
//                                  nil];
//    gradientLayerNo.cornerRadius = _answerBtnNo.layer.cornerRadius;
//    [_answerBtnNo.layer addSublayer:gradientLayerNo];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    _answerBtnNo.hidden = ![contentType isEqualToString:@"question"];
    _answerBtnYes.hidden = ![contentType isEqualToString:@"question"];

    
//    CGRect frame = self.imageView.frame;
//    CGFloat maxPictureWidth = frame.size.width - 2 * FRAME_MARGIN;
//    CGFloat	maxPictureHeight = frame.size.height - 2 * FRAME_MARGIN;
//    CGFloat fitToWidthHeight = maxPictureWidth * (3./4);
//    CGFloat fitToHeightWidth = maxPictureHeight * (4./3);
//    
//    BOOL fitToWidth = fitToHeightWidth > maxPictureWidth;
//    CGFloat contentGap = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone? 10 : 20;
//    
//    if (fitToWidth)
//    {
//        if (maxPictureWidth > 480)
//        {
//            maxPictureWidth = 480;
//            fitToWidthHeight = 360;
//        }
//        
//        //        CGFloat pictureHeightWithFrame = fitToWidthHeight + 2 * FRAME_MARGIN;
//        //        CGFloat pictureWidthWithFrame = maxPictureWidth + 2 * FRAME_MARGIN;
//        
//        //        self.imageFrame.frame = CGRectMake((frame.size.width - pictureWidthWithFrame) / 2, 0, pictureWidthWithFrame, pictureHeightWithFrame);
//        //        self.descriptionField.frame = CGRectMake((frame.size.width - pictureWidthWithFrame) / 2, pictureHeightWithFrame + contentGap, pictureWidthWithFrame, frame.size.height - (pictureHeightWithFrame + contentGap));
//    }
//    else
//    {
//        if (maxPictureHeight > 360)
//        {
//            maxPictureHeight = 360;
//            fitToHeightWidth = 480;
//        }
//        
//        //        CGFloat pictureWidthWithFrame = fitToHeightWidth + 2 * FRAME_MARGIN;
//        //        CGFloat pictureHeightWithFrame = maxPictureHeight + 2 * FRAME_MARGIN;
//        
//        //        self.imageFrame.frame = CGRectMake(0, (frame.size.height - pictureHeightWithFrame) / 2, pictureWidthWithFrame, pictureHeightWithFrame);
//        //        self.descriptionField.frame = CGRectMake(pictureWidthWithFrame + contentGap, (frame.size.height - pictureHeightWithFrame) / 2, frame.size.width - (pictureWidthWithFrame + contentGap), pictureHeightWithFrame);
//    }
    
    // during rotation we'll get a separate callback and animate the change in shadowPath
    //    if (![self isRotating])
    //        [self setShadowPathsWithAnimationDuration:0];
    //
    //    NSLog(@"viewWillLayoutSubviews");
}

- (void)setShadowPathsWithAnimationDuration:(NSTimeInterval)duration
{
    UIBezierPath *newPath = [UIBezierPath bezierPathWithRect:self.imageView.bounds];
    CGPathRef oldPath = CGPathRetain([self.imageView.layer shadowPath]);
    [self.imageView.layer setShadowPath:[newPath CGPath]];
    if (duration > 0){
        CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"shadowPath"];
        [pathAnimation setFromValue:(__bridge id)oldPath];
        [pathAnimation setToValue:(id)[self.imageView.layer shadowPath]];
        [pathAnimation setDuration:duration];
        [pathAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [pathAnimation setRemovedOnCompletion:YES];
        [self.imageView.layer addAnimation:pathAnimation forKey:@"shadowPath"];
    }
    CGPathRelease(oldPath);
}

-(void)setContentImageWithURL:(NSURL *)url{
    imageURL = url;
    

}

-(void)setContentDescription:(NSString *)desc{
    description = desc;
}

-(void)setContentTitle:(NSString *)title{
    contentTitle = title;
}

-(void)setContentType:(NSString *)type{
    contentType = type;
}

-(void)setContentId:(NSString *)cardId{
    contentId = cardId;
}


- (IBAction)answerBtnYesAction:(id)sender {
    [self submitAnswer:@"Yes"];
}

- (IBAction)answerBtnNoAction:(id)sender {
    [self submitAnswer:@"No"];
}

-(void)submitAnswer:(NSString *)answer{
    _answerLabel.hidden = false;
    _answerBtnYes.hidden = true;
    _answerBtnNo.hidden = true;
    NSDictionary *answerDict = [NSDictionary dictionaryWithObjectsAndKeys:contentId,@"questionId", answer, @"answer", nil];
    [[RequestController sharedInstance] submitAnswerForQuestionWithDetails:answerDict successHandler:nil failureHandler:nil];
    
}

@end
