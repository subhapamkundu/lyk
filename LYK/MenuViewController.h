//
//  MenuViewController.H
//  LYK
//
//  Created by Subhapam kundu on 15/07/15.
//  Copyright (c) 2015 Subhapam kundu. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ScreenState)
{
    ScreenStateNone = -1,
//    ScreenStateProducts,
//    ScreenStateUpgrade,
    ScreenStateAddFamily,
    ScreenStateAccount,
    ScreenStateNotifications,
    ScreenStateDemo,
//    ScreenStateActivityLog,
//    ScreenStateFeedback,
    ScreenStateAboutUs,
    ScreenStatePrivacyPolicy,
    ScreenStateFAQ,
    ScreenStateLogout
};

@protocol MenuViewDelegate <NSObject>

@required

-(void)shouldCloseMenu;
-(void)didSelectProductsAction;
-(void)didSelectUpgradeAction;
-(void)didSelectAddFamily;
-(void)didSelectAccountAction;
-(void)didSelectNotificationAction;
-(void)didSelectPlayDemo;
-(void)didSelectActivityLogAction;
-(void)didSelectFeedbackAction;
-(void)didSelectAboutUs;
-(void)didSelectPrivacyPolicy;
-(void)didSelectFAQ;
-(void)didSelectLogoutAction;

@end

@interface MenuViewController : UIViewController

@property(nonatomic, assign)id<MenuViewDelegate> menuDelegate;

- (void)updateMenuDetails;

@end
