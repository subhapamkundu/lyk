//
//  LYKRegisterTextField.h
//  LYK
//
//  Created by S. Kundu on 22/06/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LYKRegisterTextField : UITextField

@property (nonatomic, retain) UITextField *countryCodeTextField;

-(void)integrateCountryCodeSegment;

@end
