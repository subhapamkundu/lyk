//
//  UtilityHeader.h
//  LYK
//
//  Created by Subhapam on 16/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#ifndef LYK_UtilityHeader_h
#define LYK_UtilityHeader_h

/*======================================================================*/
/*  Message Strings Declaration Starts  */

#define BlankText                                   @""

#define NoResponseErrorMessage                      @"No Response"
#define EnterEmailNetworkToast                      @"Please enter email Id"
#define EnterProperEmailNetworkToast                @"Please enter proper email id"
#define EnterPasswordNetworkToast                   @"Please enter password"
#define ForgotPasswordSuccessMessage                @"Your temporary password is sent to your registered email. Please check and activate."
#define EnterFirstNameErrorMessage                  @"Please enter first name"
#define TokenInvalidErrorMessage                    @"Token not validated"
#define EnterLastNameErrorMessage                   @"Please enter last name"
#define EnterMobileNumberErrorMessage               @"Please enter mobile number"
#define AcceptTermsErrorMessage                     @"Please read and accept terms and condition"



/*  Message Strings Declaration Ends  */
/*======================================================================*/
/*  Interface Strings Declaration Starts  */

#define RegistrationScreenTitle                 @"Create Account"


/*  Interface Strings Declaration Ends  */
/*======================================================================*/
/*  Asset Name Declaration Starts  */


/*  Asset Name Declaration Ends  */
/*======================================================================*/
/*  Segue Declaration Starts  */

#define SplashToLoginSegue                  @"SplashToLoginSegueIdentifier"
#define LoginToRegisterSegue                @"LoginToRegistrationSegueIdentifier"
#define LoginToHomeSegue                    @"LoginToHomeSegueIdentifier"
#define ResetPasswordSegue                  @"ResetPasswordModalSegueIdentifier"
#define RelationshipDetailsSegue            @"RelationshipDetailsSegueIdentifier"
#define AddMemberSegue                      @"AddMemberSegueIdentifier"
#define UserProfileSegue                    @"UserProfileSegueIdentifier"
#define NotificationsSegue                  @"NotificationsSegueIdentifier"
#define HomeToDetailSegue                   @"HomeToDetailSegueIdentifier"
#define AnalyticsToDetailSegue              @"AnalyticsToDetailSegueIdentifier"
#define SafetyToActionSegue                 @"SafetyToActionSegueIdentifier"
#define OrganizerToActionSegue              @"OrganizerToActionSegueIdentifier"
#define ExploreToProductListSegue           @"ExploreToProductListSegueIdentifier"
#define ProductListToWebDetailSegue         @"ProductListToWebDetailSegueIdentifier"
#define ProductListToDetailSegue            @"ProductListToDetailSegueIdentifier"
#define NotificationToActionDetailSegue     @"NotificationActionSegueIdentifier"
#define WebDetailToShareSegue               @"WebDetailToShareSegueIdentifier"
#define ExploreDetailToShareSegue           @"ExploreDetailToShareSegueIdentifier"
#define AboutUsSegue                        @"aboutusSegueIdentifier"
#define PrivacyPolicySegue                  @"privacypolicySegueIdentifier"
#define FAQSegue                            @"FAQSegueIdentifier"



/*  Segue Declaration Ends  */
/*======================================================================*/
/*  Font Declaration Starts  */


/*  Font Declaration Ends  */
/*======================================================================*/
/*  Color Declaration Starts  */

#define kDefaulColor [UIColor colorWithRed:39.0/255.0 green:103.0/255.0 blue:163.0/255.0 alpha:1.0]


/*  Color Declaration Ends  */
/*======================================================================*/
/*  Macro Declaration Starts  */

#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]

#define LYKConcatStrings(firstStr,secondStr) [NSString stringWithFormat:@"%@%@",firstStr, secondStr]

#define LYKSegueWithIdentifier(str) [NSString stringWithFormat:@"%@SegueIdentifier",str]

#define IS_IPHONE_4         ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && MAX([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width) == 480)

#define IS_IPHONE_5         ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && MAX([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width) == 568)

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define DeviceID            [UIDevice currentDevice].identifierForVendor.UUIDString

#ifdef DEBUG
#define DebugLog(s, ...) NSLog(s, ##__VA_ARGS__)
#else
#define DebugLog(s, ...)
#endif

#define ReleaseLog(s, ...) NSLog(s, ##__VA_ARGS__)

/*  Macro Declaration Ends  */
/*======================================================================*/
/*  Constant Declaration Starts  */


#define LYKGPPClientId                  @"147987262837-jp6qk97q5d81o2jrl1194avec87cjl64.apps.googleusercontent.com";
#define GoogleAPIKey                    @"AIzaSyCDi2dklT-95tEHqYoE7Tklwzn3eJP-MtM"


#define SlideMenuAnimationTime          0.4
#define SlideMenuStartOffsetY           0 // to start from top or 65 to start just below navigation bar
#define SlideMenuEndOffsetHeight        49 // to end at the bottom of window use 0 or 114 or 49 to end just above tab bar depending on navigation bar height

#define NetworkRequest_ErrorInfo                                @"errorInfo"

/*  Constant Declaration Ends  */
/*======================================================================*/
/*  Dictionary Key Declaration Starts  */

#define Key_Response                                @"response"
#define Key_Token                                   @"token"
#define Key_FirstName                               @"firstName"
#define Key_MiddleName                              @"middleName"
#define Key_LastName                                @"lastName"
#define Key_UserId                                  @"userId"



/*  Dictionary Key Declaration Ends  */
/*======================================================================*/

#define NotificationPlistFileName       @"notifications.plist"


//----- Font --------//

#define FONT_LIGHT(fontSize)                [UIFont fontWithName:@"HelveticaNeue-Thin" size:fontSize]
#define FONT_REGULAR(fontSize)              [UIFont fontWithName:@"HelveticaNeue" size:fontSize]
#define FONT_BOLD(fontSize)                 [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize]


#endif
