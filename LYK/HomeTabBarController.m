//
//  HomeTabBarController.m
//  LYK
//
//  Created by Subhapam on 13/12/15.
//  Copyright © 2015 Subhapam. All rights reserved.
//

#import "HomeTabBarController.h"
#import "MenuViewController.h"
#import "GamificationViewController.h"
#import "DashboardViewController.h"
#import "SafetyViewController.h"
#import "OrganizerViewController.h"
#import "ExploreViewController.h"
#import "AnalyticsViewController.h"
#import "EditRelationshipViewController.h"


@interface HomeTabBarController ()<MenuViewDelegate, GamificationDelegate, UITabBarControllerDelegate>
{
    MenuViewController *menuViewController;
    GamificationViewController *gmvc;
    BOOL isMenuOpen;
    BOOL shouldShowMenu;
    CGRect initialFrame;
    BOOL isPromptedForAddMember;
}

@end

@implementation HomeTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    shouldShowMenu = YES;
    self.delegate = self;
    [self refreshDrawerButton];
    
    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:((DashboardViewController *)[self.viewControllers objectAtIndex:0]) action:@selector(shareCurrentItem)]];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(showMenu)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideMenu)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDrawerButton) name:@"UnreadNotificationCountUpdated" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDrawerButton) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDrawerButton) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    //For retrieving
    BOOL shouldShowGamification = [[defaults objectForKey:@"isGamificationShown"] boolValue];
    if(!shouldShowGamification){
        //For saving
        [defaults setObject:[NSNumber numberWithBool:YES] forKey:@"isGamificationShown"];
        [defaults synchronize];
        gmvc = [[GamificationViewController alloc] initWithNibName:@"GamificationViewController" bundle:nil];
        [gmvc.view setFrame:[[UIApplication sharedApplication] keyWindow].bounds];
        gmvc.delegate = self;
        [[[UIApplication sharedApplication] keyWindow] addSubview:gmvc.view];
    } else {
        if(!isPromptedForAddMember){
            isPromptedForAddMember = YES;
            if([[[UserData sharedInstance] getMembersArray] count] <= 0){
//                [((DashboardViewController *)((UITabBarController *)self.navigationController.topViewController).viewControllers[0]) pauseFlipping];
                [self performSegueWithIdentifier:RelationshipDetailsSegue sender:self];
            }
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self hideMenu];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:AddMemberSegue]){
        EditRelationshipViewController *ervc = segue.destinationViewController;
        [ervc showAddMemberView];
    }
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    switch (tabBarController.selectedIndex) {
        case 0:{ //Home
            [((AnalyticsViewController *)[self viewControllers][0]) pauseFlipping];
//            self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:((DashboardViewController *)viewController) action:@selector(shareCurrentItem)]];
        NSArray *actionButtons = @[
                                   [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"ExternalShare"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:((DashboardViewController *)((UITabBarController *)self.navigationController.topViewController).viewControllers[0]) action:@selector(externalShare)],
                                   [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"InternalShare"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:((DashboardViewController *)((UITabBarController *)self.navigationController.topViewController).viewControllers[0]) action:@selector(internalShare)]
                                   ];
            self.navigationItem.rightBarButtonItems = nil;
            UIBarButtonItem *addMemberBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"AddFamily"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(didSelectAddFamily)];
            self.navigationItem.rightBarButtonItem = addMemberBarButtonItem;
            [((DashboardViewController *)((UITabBarController *)self.navigationController.topViewController).viewControllers[0]) pauseFlipping];

        }
            break;
        case 1:{ // Safety
            [((DashboardViewController *)[self viewControllers][0]) pauseFlipping];
            [((AnalyticsViewController *)[self viewControllers][0]) pauseFlipping];
            NSArray *actionButtons = @[
                                       [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"CallInactive"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:((SafetyViewController *)viewController) action:@selector(sosAction)],
                                       [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"LocationInactive"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:((SafetyViewController *)viewController) action:@selector(safetyRegionAction)]
                                       ];
            
            self.navigationItem.rightBarButtonItems = actionButtons;
        }
            break;
        case 2:{
            [((DashboardViewController *)[self viewControllers][0]) pauseFlipping];
            [((AnalyticsViewController *)[self viewControllers][0]) pauseFlipping];
            NSArray *actionButtons = @[
                                       [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:((OrganizerViewController *)viewController) action:@selector(addSchedule)],
                                       [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:((OrganizerViewController *)viewController) action:@selector(openViewOption)]
                                       ];
            
            self.navigationItem.rightBarButtonItems = actionButtons;
        }
            break;
        case 3:{
            [((DashboardViewController *)[self viewControllers][0]) pauseFlipping];
            [((AnalyticsViewController *)[self viewControllers][0]) pauseFlipping];
            NSArray *actionButtons = @[
                                       [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"CallInactive"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:((SafetyViewController *)viewController) action:@selector(sosAction)],
                                       [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"LocationActive"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:((SafetyViewController *)viewController) action:@selector(safetyRegionAction)]
                                       ];
            
            self.navigationItem.rightBarButtonItems = nil;
        }
            break;
        case 4:{
            [((DashboardViewController *)[self viewControllers][0]) pauseFlipping];
            NSArray *actionButtons = @[
                                       [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"Call"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:((SafetyViewController *)viewController) action:@selector(sosAction)],
                                       [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"Location"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:((SafetyViewController *)viewController) action:@selector(safetyRegionAction)]
                                       ];
            
            self.navigationItem.rightBarButtonItems = nil;
        }
            break;
            
        default:
            break;
    }
}


-(void)toggleMenuWithDelegate {
    if(!menuViewController)
        [self setupMenuView];
    //pickJiMenuViewController.menuDelegate = delegate;
    if(isMenuOpen){
        [self hideMenu];
    } else {
        [self showMenu];
    }
}

-(void)closeMenuIfOpen {
    if(isMenuOpen){
        [self hideMenu];
    }
}

-(void)shouldShowMenu:(BOOL)flag {
    shouldShowMenu = flag;
}

-(void)showMenu {
    if(shouldShowMenu) {
        if(!isMenuOpen) {
            if([_menuTransitionDelegate respondsToSelector:@selector(menuWillShow)]){
                [_menuTransitionDelegate performSelector:@selector(menuWillShow)];
            }
            if(!menuViewController)
                [self setupMenuView];
            else
                [menuViewController updateMenuDetails];
            [((DashboardViewController *)[self viewControllers][0]) pauseFlipping];
            //[self.navigationController.topViewController.view setUserInteractionEnabled:false];
            [UIView animateWithDuration:SlideMenuAnimationTime animations:^{
                // note CGAffineTransforms doesn't use coordinates, they use offset
                // so an offset of (320,0) from coordinate (-320,0) would slide the
                // menuView out into view
                menuViewController.view.frame = CGRectMake(0, SlideMenuStartOffsetY, menuViewController.view.bounds.size.width, self.view.bounds.size.height-SlideMenuEndOffsetHeight);
                //self.view.frame = CGRectMake(pickJiMenuViewController.view.bounds.size.width, 0, self.view.bounds.size.width, self.view.bounds.size.height);
            } completion:^(BOOL finished) {
                if(finished)
                {
                    isMenuOpen = YES;
                    if([_menuTransitionDelegate respondsToSelector:@selector(menuDidShow)]){
                        [_menuTransitionDelegate performSelector:@selector(menuDidShow)];
                    }
                }
            }];
        }
    }
}

-(void)hideMenu {
    if(isMenuOpen){
        //    [self.view bringSubviewToFront:pickJiMenuViewController.view];
        [self.navigationController.topViewController.view setUserInteractionEnabled:true];
        [((DashboardViewController *)[self viewControllers][0]) startFlipping];
        [UIView animateWithDuration:SlideMenuAnimationTime animations:^{
            menuViewController.view.frame = CGRectMake(-menuViewController.view.bounds.size.width, SlideMenuStartOffsetY, menuViewController.view.bounds.size.width, self.view.bounds.size.height-SlideMenuEndOffsetHeight);
            //self.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
        } completion:^(BOOL finished) {
            if(finished){
                isMenuOpen = NO;
                if([_menuTransitionDelegate respondsToSelector:@selector(menuDidHide)]){
                    [_menuTransitionDelegate performSelector:@selector(menuDidHide)];
                }
            }
        }];
    }
}


#pragma mark - Menu view modules

-(void)setupMenuView{
    menuViewController = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
    [menuViewController.view setFrame:CGRectMake(-menuViewController.view.bounds.size.width, SlideMenuStartOffsetY, menuViewController.view.bounds.size.width, self.view.bounds.size.height-SlideMenuEndOffsetHeight)];
    //    [self addChildViewController:pickJiMenuViewController];
    menuViewController.menuDelegate = self;
    [[[[UIApplication sharedApplication] delegate] window] addSubview:menuViewController.view];
    [self.view sendSubviewToBack:menuViewController.view];
    isMenuOpen = NO;
}

#pragma mark -
#pragma mark Menu Action Delegates
#pragma mark -

-(void)shouldCloseMenu{
    [self closeMenuIfOpen];
}

-(void)didSelectProductsAction{
    [self closeMenuIfOpen];
}

-(void)didSelectUpgradeAction{
    [self closeMenuIfOpen];
}

-(void)didSelectAddFamily{
    [self closeMenuIfOpen];
    [self performSegueWithIdentifier:AddMemberSegue sender:self];
}

-(void)didSelectAccountAction{
    [self closeMenuIfOpen];
    [self performSegueWithIdentifier:UserProfileSegue sender:self];
}

-(void)didSelectPlayDemo{
    [self closeMenuIfOpen];
    gmvc = [[GamificationViewController alloc] initWithNibName:@"GamificationViewController" bundle:nil];
    [gmvc.view setFrame:[[UIApplication sharedApplication] keyWindow].bounds];
    gmvc.delegate = self;
    [[[UIApplication sharedApplication] keyWindow] addSubview:gmvc.view];
}

-(void)didSelectNotificationAction{
    [self closeMenuIfOpen];
    [self performSegueWithIdentifier:NotificationsSegue sender:self];
}

-(void)didSelectActivityLogAction{
    [self closeMenuIfOpen];
}

-(void)didSelectFeedbackAction{
    [self closeMenuIfOpen];
}

-(void)didSelectAboutUs{
    [self closeMenuIfOpen];
    [self performSegueWithIdentifier:AboutUsSegue sender:self];
}
-(void)didSelectPrivacyPolicy{
    [self closeMenuIfOpen];
    [self performSegueWithIdentifier:PrivacyPolicySegue sender:self];
}
-(void)didSelectFAQ{
    [self closeMenuIfOpen];
    [self performSegueWithIdentifier:FAQSegue sender:self];
}

-(void)didSelectLogoutAction {
    [self closeMenuIfOpen];
    [((DashboardViewController *)((UITabBarController *)self.navigationController.topViewController).viewControllers[0]) pauseFlipping];
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    [[UserData sharedInstance] clearUserDetails];
    [self releaseAllViewControllers];
}

-(void)releaseAllViewControllers{
    
}

-(void)shouldDismissGamificationController{
    [gmvc.view removeFromSuperview];
    gmvc = nil;
    if(!isPromptedForAddMember){
        isPromptedForAddMember = YES;
        if([[[UserData sharedInstance] getMembersArray] count] <= 0){
            [((DashboardViewController *)((UITabBarController *)self.navigationController.topViewController).viewControllers[0]) pauseFlipping];
            [self performSegueWithIdentifier:RelationshipDetailsSegue sender:self];
        }
    }
}

-(void)refreshDrawerButton{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.navigationItem.leftBarButtonItem = nil;
        UIView* leftButtonView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 20)];
        
        UIImageView* drawerImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        drawerImage.backgroundColor = [UIColor clearColor];
        [drawerImage setImage:[UIImage imageNamed:@"MenuIcon"]];
        //        leftButton.autoresizesSubviews = YES;
        //        leftButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
        //[leftButton addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
        [leftButtonView addSubview:drawerImage];
        
        UIButton* countButton = [UIButton buttonWithType:UIButtonTypeSystem];
        countButton.userInteractionEnabled = false;
        countButton.backgroundColor = [UIColor clearColor];
        [countButton setBackgroundImage:[UIImage imageNamed:@"NotificationTag"] forState:UIControlStateNormal];
        countButton.tintColor = [UIColor whiteColor]; //Your desired color.
        [countButton setTitle:[NSString stringWithFormat:@"%ld",[[UserData sharedInstance] getNotificationCount]] forState:UIControlStateNormal];
        //[countButton setTitle:@"9" forState:UIControlStateNormal];
        [countButton.titleLabel setFont:FONT_REGULAR(8.0f)];
        //        CGSize maximumSize = CGSizeMake(20, 15);
        CGRect stringRect = [countButton.currentTitle boundingRectWithSize:CGSizeMake(20, 18) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:FONT_REGULAR(9.0f) } context:nil];
        //        CGSize stringSize = [countButton.currentTitle sizeWithFont:FONT_LIGHT(8.0f)
        //                                   constrainedToSize:maximumSize
        //                                       lineBreakMode:countButton.titleLabel.lineBreakMode];
        stringRect.size.width = stringRect.size.width + 10;
        if(stringRect.size.width < 15)
            stringRect.size.width = 15;
        countButton.frame = CGRectMake(25 - stringRect.size.width, -10, stringRect.size.width, 18);
        //        countButton.autoresizesSubviews = YES;
        //        countButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
        //[leftButton addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
        if(![countButton.currentTitle isEqualToString:@"0"])
            [leftButtonView addSubview:countButton];
        
        UIButton* leftButton = [UIButton buttonWithType:UIButtonTypeSystem];
        leftButton.backgroundColor = [UIColor clearColor];
        leftButton.frame = CGRectMake(0, 0, 25, 20);
        leftButton.tintColor = [UIColor clearColor]; //Your desired color.
        [leftButton addTarget:self action:@selector(toggleMenuWithDelegate) forControlEvents:UIControlEventTouchUpInside];
        //        leftButton.autoresizesSubviews = YES;
        //        leftButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
        //[leftButton addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
        [leftButtonView addSubview:leftButton];
        
        UIBarButtonItem* leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:leftButtonView];
        self.navigationItem.leftBarButtonItem = leftBarButton;
        
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavHeader"]];
        
    });
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSLog(@"From KVO");
    
//    if([keyPath isEqualToString:@"number"])
//    {
//        id oldC = [change objectForKey:NSKeyValueChangeOldKey];
//        id newC = [change objectForKey:NSKeyValueChangeNewKey];
//        
//        NSLog(@"%@ %@", oldC, newC);
//    }
}

-(void)showAddFamilyActionView{
    [self didSelectAddFamily];
}


@end
