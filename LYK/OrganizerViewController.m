//
//  OrganizerViewController.m
//  LYK
//
//  Created by Subhapam on 19/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "OrganizerViewController.h"
#import "OrganizerActionViewController.h"
#import "JTCalendar.h"
#import "MADayView.h"
#import "MAEvent.h"
#import "MAEventKitDataSource.h"
#import "OrganizerTaskTableViewCell.h"
#import "HomeTabBarController.h"
#import "DayCollectionViewCell.h"

#define DATE_COMPONENTS (NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay | NSWeekCalendarUnit |  NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal)
#define CURRENT_CALENDAR [NSCalendar currentCalendar]
#define dayCellIdentifier           @"DAY"
#define dayCellHeight               60

static NSDate *date = nil;

typedef NS_ENUM(NSInteger, EventsType)
{
    EventType_Task = 0,
    EventType_Event,
    EventType_Today
};

@interface OrganizerViewController ()<JTCalendarDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate,MADayViewDataSource,MADayViewDelegate, OrganizerActionDelegate,UICollectionViewDelegateFlowLayout>
{
    JTCalendarManager *calendarManager;
    NSMutableArray *_datesSelected;
    NSMutableArray *_eventsByDate;
    JTCalendarDayView *selectedDate;
    UIActivityIndicatorView *activityIndicator;
    UIRefreshControl *refreshControl;
    NSDictionary *selectedActivity;
//    MAEvent *event;
    MAEventKitDataSource *eventKitDataSource;
    
    NSArray *arrCollectionCellInformation;
    NSArray *arrStaticTime;
    float dayCellWidth;
    float screenWindth;
    NSDate *dayViewDate;
}

@property (weak, nonatomic) IBOutlet JTCalendarMenuView *menuView;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet MADayView *dayView;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionview;
@property (weak, nonatomic) IBOutlet UIView *dateView;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
- (IBAction)actionFowardDate:(id)sender;
- (IBAction)actionBackwardDate:(id)sender;


- (IBAction)reloadList:(id)sender;

@end

@implementation OrganizerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    calendarManager = [JTCalendarManager new];
    calendarManager.delegate = self;
    
    
    // Generate random events sort by date using a dateformatter for the demonstration
    [self createRandomEvents];
    
    //    _menuView.contentRatio = .75;
    //    calendarManager.settings.weekDayFormat = JTCalendarWeekDayFormatSingle;
    //    calendarManager.dateHelper.calendar.locale = [NSLocale localeWithLocaleIdentifier:@"fr_FR"];
    
    [calendarManager setMenuView:_menuView];
    [calendarManager setContentView:_calendarView];
    [calendarManager setDate:[NSDate date]];
    
    _datesSelected = [NSMutableArray new];
    [self getEvents];
    refreshControl = [[UIRefreshControl alloc]init];
    [_tableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(getEvents) forControlEvents:UIControlEventValueChanged];
    _dayView.autoScrollToFirstEvent = YES;
    
    
//    _calendarView.hidden=YES;
//    _menuView.hidden=YES;
    
    dayCellWidth=0.0f;
    
    NSMutableArray *temp=[NSMutableArray array];
    // [self printAddres:temp];
    for (int i=0; i<=24; i=i+2) {
        
        [temp addObject:[NSString stringWithFormat:@"%d",i]];
       /* if (i<10)
            [temp addObject:[NSString stringWithFormat:@"0%d:00 AM",i]];
        else if (i<12)
            [temp addObject:[NSString stringWithFormat:@"%d:00 AM",i]];
        else if (i==12)
            [temp addObject:[NSString stringWithFormat:@"%d:00 PM",i]];
        else if (i!=24)
            [temp addObject:i%12<10?[NSString stringWithFormat:@"0%d:00 PM",i%12]:[NSString stringWithFormat:@"%d:00 PM",i%12]];*/
    }
  
    
    arrStaticTime = temp;
    dayViewDate=[NSDate date];
    
     //dayViewDate=[dayViewDate dateByAddingTimeInterval:60*60*24*1];
    _lblDate.text=[self dayViewDateString:dayViewDate];
    

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    ((HomeTabBarController *)self.parentViewController).menuTransitionDelegate = self;
    screenWindth=self.view.bounds.size.width;
    [_collectionview reloadData];
    _dateView.hidden=false;//
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if(selectedActivity){
//        NSString *s = @"yyyy/MM/dd HH:mm:ss";
//        NSDateFormatter *inDateFormatter = [[NSDateFormatter alloc] init];
//        inDateFormatter.dateFormat = s;
//        inDateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
//        NSDate *inDate = selectedDate.date;
//        
//        // about output date(IST)
//        NSDateFormatter *outDateFormatter = [[NSDateFormatter alloc] init];
//        outDateFormatter.timeZone = [NSTimeZone systemTimeZone];
//        outDateFormatter.dateFormat = s;
//        NSString *outDateStr = [outDateFormatter stringFromDate:inDate];
//        
//        // final output
//        NSLog(@"[in]%@ -> [out]%@", inDate, outDateStr);
        ((OrganizerActionViewController *)segue.destinationViewController).selectedActivity = selectedActivity;
        selectedActivity = nil;
    } else {
        ((OrganizerActionViewController *)segue.destinationViewController).delegate = self;
    }
}

#pragma mark - Drawer Menu Transition delegate

-(void)menuWillShow{
    self.view.userInteractionEnabled = false;
}

-(void)menuDidShow{
    
}

-(void)menuDidHide{
    self.view.userInteractionEnabled = true;
}

#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    // Today
    if([calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor blueColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if([self isInDatesSelected:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor redColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Other month
    else if(![calendarManager.dateHelper date:_calendarView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    
    if([self haveEventForDay:dayView.date]){
        dayView.dotView.hidden = NO;
    }
    else{
        dayView.dotView.hidden = YES;
    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    
    if([self isInDatesSelected:dayView.date]){
        [_datesSelected removeObject:dayView.date];
        if(selectedDate.date == dayView.date)
            selectedDate = nil;
        [UIView transitionWithView:dayView
                          duration:.3
                           options:0
                        animations:^{
                            [calendarManager reload];
                            dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
                        } completion:nil];
    }
    else{
        if(selectedDate){
            [_datesSelected removeObject:selectedDate.date];
            [UIView transitionWithView:selectedDate
                              duration:.3
                               options:0
                            animations:^{
                                [calendarManager reload];
                                selectedDate.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
                            } completion:^(BOOL finished) {
                                if(finished){
                                    [_datesSelected addObject:dayView.date];
                                    selectedDate = dayView;
                                    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
                                    [UIView transitionWithView:dayView
                                                      duration:.3
                                                       options:0
                                                    animations:^{
                                                        [calendarManager reload];
                                                        dayView.circleView.transform = CGAffineTransformIdentity;
                                                    } completion:nil];
                                }
                            }];
        } else {
            [_datesSelected addObject:dayView.date];
            selectedDate = dayView;
            dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
            [UIView transitionWithView:dayView
                              duration:.3
                               options:0
                            animations:^{
                                [calendarManager reload];
                                dayView.circleView.transform = CGAffineTransformIdentity;
                            } completion:nil];
        }
    }

    // Load the previous or next page if touch a day from another month
    
    if(![calendarManager.dateHelper date:_calendarView.date isTheSameMonthThan:dayView.date]){
        if([_calendarView.date compare:dayView.date] == NSOrderedAscending){
            [_calendarView loadNextPageWithAnimation];
        }
        else{
            [_calendarView loadPreviousPageWithAnimation];
        }
    }
}

#pragma mark - Date selection

- (BOOL)isInDatesSelected:(NSDate *)date
{
    for(NSDate *dateSelected in _datesSelected){
        if([calendarManager.dateHelper date:dateSelected isTheSameDayThan:date]){
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - Fake data

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    for(NSDictionary *event in _eventsByDate){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSDate *startDate = [dateFormatter dateFromString:[event objectForKey:@"eventStartDate"]];
        NSDate *endDate = [dateFormatter dateFromString:[event objectForKey:@"eventEndDate"]];
        if(startDate && [date compare:startDate] == NSOrderedSame)
            return YES;
        else if(endDate)
            return !([date compare:startDate] == NSOrderedAscending) && !([date compare:endDate] == NSOrderedDescending);
    }
    return NO;
    
}

- (NSArray *)getEventArray:(NSDate *)date
{
    NSMutableArray *temp=[NSMutableArray array];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    for(NSDictionary *event in _eventsByDate){
        
        NSMutableDictionary *tempDict=[NSMutableDictionary dictionary];
        [tempDict setDictionary:event];
        
        NSDate *startDate = [dateFormatter dateFromString:[event objectForKey:@"eventStartDate"]];
        NSDate *endDate = [dateFormatter dateFromString:[event objectForKey:@"eventEndDate"]];
        
        
        
        if([self isCurrentDate:date BetweenStartDate:startDate andEndDate:endDate]){
            
            if ([event objectForKey:@"eventEndDate"] && ![[event objectForKey:@"eventEndDate"]isEqual:@""])
                [tempDict setObject:[NSNumber numberWithBool:YES] forKey:@"endDateAvailable"];
            else
                [tempDict setObject:[NSNumber numberWithBool:NO] forKey:@"endDateAvailable"];
            
            [temp addObject:tempDict];
        }
        
        
    }
    
    //dayCellWidth
   
    return (NSArray *)temp;
    
}

-(BOOL)isCurrentDate:(NSDate *)cDate BetweenStartDate:(NSDate *)sDate andEndDate:(NSDate *)ldate{
    
    if (sDate && ldate) {
        /*if((sDate && [cDate compare:sDate] == (NSOrderedSame || NSOrderedAscending)) || (ldate && [ldate compare:cDate] == (NSOrderedSame || NSOrderedDescending)))
            return YES;*/
        
        cDate=[self getDate:cDate] ;
        sDate=[self getDate:sDate] ;
        ldate=[self getDate:ldate] ;
        
        
        
        NSComparisonResult result=[sDate compare:cDate];
        NSComparisonResult result2=[cDate compare:ldate];
        
        if (result==NSOrderedSame)
            return YES;

        if (result2==NSOrderedSame)
           return YES;
        
        if (result2==NSOrderedAscending && result==NSOrderedAscending)
           return YES;
        

    }

    return NO;
    
}



- (void)createRandomEvents
{
    //_eventsByDate = [NSMutableDictionary dictionary];
    
//    for(int i = 0; i < 30; ++i){
//        // Generate 30 random dates between now and 60 days later
//        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
//        
//        // Use the date as key for eventsByDate
//        NSString *key = [[self dateFormatter] stringFromDate:randomDate];
//        
//        if(!_eventsByDate[key]){
//            _eventsByDate[key] = [NSMutableArray new];
//        }
//        
//        [_eventsByDate[key] addObject:randomDate];
//    }
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([_eventsByDate count] == 0){
        UILabel *emptyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        emptyLabel.text = @"No task is created or shared with you. Schedule and add task.";
        emptyLabel.numberOfLines = 0;
        emptyLabel.textAlignment = NSTextAlignmentCenter;
        self.tableView.backgroundView = emptyLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 0;
    } else {
        self.tableView.backgroundView = nil;
    }
    return [_eventsByDate count];
}

-(OrganizerTaskTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"organizerTaskTableViewCell";
    
    OrganizerTaskTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = (OrganizerTaskTableViewCell *)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    
    if ([[[_eventsByDate objectAtIndex:indexPath.row] objectForKey:@"eventType"]isEqualToString:@"Event"] || [[[_eventsByDate objectAtIndex:indexPath.row] objectForKey:@"eventType"]isEqualToString:@"Task"]) {
        
        cell.barImageView.backgroundColor=(UIColor *)[[_eventsByDate objectAtIndex:indexPath.row]objectForKey:@"color"];
    }else{
        cell.barImageView.backgroundColor=[UIColor whiteColor];
    }
    
    cell.taskTitle.text = [[_eventsByDate objectAtIndex:indexPath.row] objectForKey:@"eventSubject"];
    NSMutableArray *dates = [NSMutableArray arrayWithObjects:
                             [[_eventsByDate objectAtIndex:indexPath.row] objectForKey:@"eventStartDate"],
                             [[_eventsByDate objectAtIndex:indexPath.row] objectForKey:@"eventEndDate"], nil];
//    cell.taskDesc.numberOfLines = 1;
    cell.taskDesc.text = [NSString stringWithFormat:@"%@ (%@)",[[_eventsByDate objectAtIndex:indexPath.row] objectForKey:@"eventContent"]?:@"", [dates count]?[dates componentsJoinedByString:@" to "]:@"Todo"];
    //cell.leftBarImageView.backgroundColor=(UIColor *)[[_eventsByDate objectAtIndex:indexPath.row] objectForKey:@"color"]?:[UIColor whiteColor];
    NSArray *sharedWithUserArr = [[_eventsByDate objectAtIndex:indexPath.row] objectForKey:@"sharedWithUser"];
    if (sharedWithUserArr.count >= 3) {
        [cell.firstSharedDPImageView setImageWithURL:[NSURL URLWithString:[[sharedWithUserArr objectAtIndex:0] objectForKey:@"imageUrl"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        [cell.secondSharedDPImageView setImageWithURL:[NSURL URLWithString:[[sharedWithUserArr objectAtIndex:1] objectForKey:@"imageUrl"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        if (sharedWithUserArr.count > 3){
            [cell.thirdSharedDPImageView setImage:[UIImage imageNamed:@"Avatar"]];
            cell.dpCountLabel.text = [NSString stringWithFormat:@"+%lu",(unsigned long)((NSArray *)[[_eventsByDate objectAtIndex:indexPath.row] objectForKey:@"sharedWithUser"]).count - 2];
        } else {
            [cell.thirdSharedDPImageView setImageWithURL:[NSURL URLWithString:[[sharedWithUserArr objectAtIndex:2] objectForKey:@"imageUrl"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            cell.dpCountLabel.text = @"";
        }
    } else if (sharedWithUserArr.count == 2){
        cell.firstSharedDPImageView.image = nil;
        [cell.secondSharedDPImageView setImageWithURL:[NSURL URLWithString:[[sharedWithUserArr objectAtIndex:0] objectForKey:@"imageUrl"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        [cell.thirdSharedDPImageView setImageWithURL:[NSURL URLWithString:[[sharedWithUserArr objectAtIndex:1] objectForKey:@"imageUrl"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        cell.dpCountLabel.text = @"";
    } else if (sharedWithUserArr.count == 1){
        cell.firstSharedDPImageView.image = nil;
        cell.secondSharedDPImageView.image = nil;
        [cell.thirdSharedDPImageView setImageWithURL:[NSURL URLWithString:[[sharedWithUserArr objectAtIndex:0] objectForKey:@"imageUrl"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        cell.dpCountLabel.text = @"";
    } else {
        cell.firstSharedDPImageView.image = nil;
        cell.secondSharedDPImageView.image = nil;
        cell.thirdSharedDPImageView.image = nil;
        cell.dpCountLabel.text = @"";
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedActivity = [_eventsByDate objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:OrganizerToActionSegue sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"Added Tasks, Events or Todos";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if([[RequestController sharedInstance] isNetworkReachable]){
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }
        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        NSString *organizerActivityId = [[_eventsByDate objectAtIndex:indexPath.row] objectForKey:@"eventId"];
        [[RequestController sharedInstance] removeOrganizerActivityWithId:organizerActivityId successHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [refreshControl endRefreshing];
                [_eventsByDate removeObjectAtIndex:indexPath.row];
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                NSLog(@"%@",error);
            });
        }];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

-(void)addSchedule{
    [self performSegueWithIdentifier:OrganizerToActionSegue sender:nil];
}

-(void)openViewOption{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Switch View"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet]; // 1
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Month View"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              NSLog(@"You pressed button one");
                                                              _dayView.hidden = true;
                                                              _menuView.hidden = false;
                                                              _calendarView.hidden = false;
                                                              
                                                              _collectionview.hidden=true;
                                                              _dateView.hidden=true;
                                                          }]; // 2
//    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Week View"
//                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                                                               NSLog(@"You pressed button two");
//                                                           }]; // 3
    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"Day View"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               NSLog(@"You pressed button three");
                                                               _dayView.hidden = true;
                                                               _menuView.hidden = true;
                                                               _calendarView.hidden = true;
                                                               
                                                               _collectionview.hidden=false;
                                                               _dateView.hidden=false;
                                                           }]; // 4
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                          style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                              NSLog(@"You pressed cancel button");
                                                          }]; // 5
    [alert addAction:firstAction]; // 6
//    [alert addAction:secondAction]; // 7
    [alert addAction:thirdAction]; // 8
    [alert addAction:cancelAction]; // 9
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)getEvents{
    if([[RequestController sharedInstance] isNetworkReachable]){
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }
        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        [[RequestController sharedInstance] getEventListWithSuccessHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [refreshControl endRefreshing];
                NSLog(@"data set: %@", response);
                _eventsByDate = [response objectForKey:Key_Response];
                _eventsByDate = [self addRandomColor:[response objectForKey:Key_Response]];
                
                //arrCollectionCellInformation = [self getEventArray:dayViewDate];
                
                [calendarManager reload];
                [_tableView reloadData];
                
                [_collectionview reloadData];
                dayCellWidth= self.view.bounds.size.width;
    //            for (NSDictionary *event in [response objectForKey:Key_Response]) {
    //                [_eventsByDate setObject:event forKey:[event objectForKey:@"eventId"]];
    //            }
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [refreshControl endRefreshing];
                NSLog(@"%@",error);
                [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
            });
        }];
    }
}



#pragma mark UICollectionViewDataSource & UICollectionViewDelegate


- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [_collectionview.collectionViewLayout invalidateLayout];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    arrCollectionCellInformation = [self getEventArray:dayViewDate];
    int eventCount= (int)arrCollectionCellInformation.count;
   // NSLog(@"%d",60 + (eventCount - 4 )*12);
    /*
     * Fixed row 12
     * variable size column
     * Here eventCount is the number of column.
     */
    if (eventCount <= 4)
        return 60.0f; //12 * (4+1)
    else
        return 60.0f + (eventCount - 4 )*12;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    int eventCount= (int)arrCollectionCellInformation.count;
    
    if (eventCount>=5) {
       // NSLog(@"%@",NSStringFromCGSize(CGSizeMake(screenWindth/eventCount+1,64.0)));
        NSLog(@"sdg %f",screenWindth/(eventCount+1));
        
       return CGSizeMake((int)screenWindth/(eventCount+1),64.0);
    }
    
    return CGSizeMake(screenWindth/5.0f,screenWindth/5.0f);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DayCollectionViewCell *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:dayCellIdentifier forIndexPath:indexPath];
    
    int eventCount= (int)arrCollectionCellInformation.count;
    
    NSLog(@"%d",indexPath.item);
    
    if (eventCount<=4){
        myCell.eventImageView.hidden=true;
        myCell.taskImageView.hidden=true;
        
        if (indexPath.item%5 == 0){
            //myCell.dayLabel.text= [arrStaticTime objectAtIndex:indexPath.item/5];
            myCell.dayLabel.text= [self timeString:indexPath.item];
            myCell.dayLabel.hidden=NO;
        }else{
            
            myCell.dayLabel.hidden=YES;
            if ([self isTimeAvailable:indexPath.item]) {
                //myCell.backgroundColor=[self isCellColorable:(int)indexPath.item];
                
                if ([self isEvent:indexPath.item]) {
                   myCell.eventImageView.hidden=false;
                    myCell.eventImageView.backgroundColor=[self isCellColorable:(int)indexPath.item];
                } else {
                    myCell.taskImageView.hidden=false;
                    myCell.taskImageView.backgroundColor=[self isCellColorable:(int)indexPath.item];
                }
                
            }else
                myCell.backgroundColor=[UIColor whiteColor];
            
        }
        
      //  myCell.backgroundColor=[UIColor redColor];
        
    } else{
        
        myCell.eventImageView.hidden=true;
        myCell.taskImageView.hidden=true;
        
        if (indexPath.item%(eventCount+1) == 0){
            //myCell.dayLabel.text= [arrStaticTime objectAtIndex:indexPath.item/5];
            myCell.dayLabel.text= [self timeString:indexPath.item];
            myCell.dayLabel.hidden=NO;
        }else{
            
            myCell.dayLabel.hidden=YES;
            if ([self isTimeAvailable:indexPath.item]) {
                //myCell.backgroundColor=[self isCellColorable:(int)indexPath.item];
                
                if ([self isEvent:indexPath.item]) {
                    myCell.eventImageView.hidden=false;
                    myCell.eventImageView.backgroundColor=[self isCellColorable:(int)indexPath.item];
                } else {
                    myCell.taskImageView.hidden=false;
                    myCell.taskImageView.backgroundColor=[self isCellColorable:(int)indexPath.item];
                }
                
            }else
            myCell.backgroundColor=[UIColor whiteColor];
            
        }
    }
    //myCell.backgroundColor=[UIColor grayColor];
    //myCell.cellImage.image = [UIImage imageNamed:@"EmailIcon"];
    //myCell.cellImage.contentMode = UIViewContentModeScaleAspectFit;
    dispatch_async(dispatch_get_main_queue(), ^{
        
       // [myCell.layer insertSublayer:[self cellBackgroundColor:myCell.bounds] atIndex:0];
    });
    
    return myCell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    int eventCount= (int)arrCollectionCellInformation.count;
    
    if (eventCount <= 4){
       if (indexPath.item % 5 ==0) {
           return;
       }
    }
    
    if (indexPath.item % (eventCount+1) <= eventCount){
        
        NSInteger objIndex = NSNotFound;
        
        if (([self getColumnIndex:indexPath.item]-1) < eventCount) {
            
            NSMutableDictionary *cellInformation = [NSMutableDictionary dictionaryWithDictionary:[arrCollectionCellInformation objectAtIndex:[self getColumnIndex:indexPath.item]-1]];
            [cellInformation removeObjectForKey:@"endDateAvailable"];
            
           objIndex= [_eventsByDate indexOfObject:cellInformation];
        }
        
        if (objIndex!=NSNotFound)
            [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:objIndex inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
           // [_tableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:objIndex inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        
        
    }
    
}

-(NSString *)timeString:(NSUInteger)index{
    
    //arrCollectionCellInformation = [self getEventArray:[NSDate date]];
//    int eventCount= (int)arrCollectionCellInformation.count;
    
//    if (eventCount<=4)
//        index=index/5;
//    else
//        index=index/(eventCount+1);
    
    //int hourTime= [[arrStaticTime objectAtIndex:index]intValue]; //24 hour time
    int hourTime= [[arrStaticTime objectAtIndex:[self getRowIndex:index]]intValue];

    if (hourTime<10)
        return [NSString stringWithFormat:@"0%d:00 AM",hourTime];
    else if (hourTime<12)
        return[NSString stringWithFormat:@"%d:00 AM",hourTime];
    else if (hourTime==12)
        return[NSString stringWithFormat:@"%d:00 PM",hourTime];
    else if (hourTime!=24)
        return (hourTime%12<10)?[NSString stringWithFormat:@"0%d:00 PM",hourTime%12]:[NSString stringWithFormat:@"%d:00 PM",hourTime%12];
    else
        return @"";
}

-(int)getColumnIndex:(NSUInteger)cellIndex{
    
    int eventCount= (int)arrCollectionCellInformation.count;
    
    if (eventCount<=4)
       return cellIndex%5;
    else
       return cellIndex%(eventCount+1);

}

-(int)getRowIndex:(NSUInteger)cellIndex{
    
    int eventCount= (int)arrCollectionCellInformation.count;
    
    if (eventCount<=4)
        return (int)cellIndex/5;
    else
        return (int)cellIndex/(eventCount+1);
}

-(bool)isTimeAvailable:(NSUInteger)index{
    
    if (arrCollectionCellInformation.count==0)
        return false;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    int eventCount= (int)arrCollectionCellInformation.count;
    /*
//    if (eventCount<=4)
//        index=index%5;
//    else
//        index=index%(eventCount+1);
//    
//    
//    
/   index--;*/
    
    int columnIndex = [self getColumnIndex:index];
    columnIndex--;
    
    if (columnIndex<eventCount) {
        
        int hourTime= [[arrStaticTime objectAtIndex:[self getRowIndex:index]]intValue]; //24 hour time
        
        NSDictionary *dict=[arrCollectionCellInformation objectAtIndex:columnIndex];
        
        NSString * strEndHour=[dict objectForKey:@"endTime"];
        NSString * strStartHour=[dict objectForKey:@"startTime"];
        
        NSDate *startDate = [dateFormatter dateFromString:[dict objectForKey:@"eventStartDate"]];
        NSDate *endDate = [dateFormatter dateFromString:[dict objectForKey:@"eventEndDate"]];

        if (startDate) {
            
            NSComparisonResult result=[[ self getDate:dayViewDate] compare:[ self getDate:startDate]];
            
            if (result==NSOrderedSame)
            {
                return [self isCurrentTime:hourTime Between:strStartHour andEnd:strEndHour];
            }
          return true;
            
        }
        
        if (endDate) {
            
            NSComparisonResult result=[[ self getDate:endDate]compare:[ self getDate:dayViewDate]];
            
            if (result==NSOrderedSame)
            {
                return [self isCurrentTime:hourTime Between:strStartHour andEnd:strEndHour];
            }
            
            return true;
        }
    
        //return false;
    }
    
   

    return false;
}

-(NSDate *)getDate:(NSDate *)date{
    
    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* components = [calendar components:flags fromDate:date];
    
    NSDate* dateOnly = [calendar dateFromComponents:components];

    return dateOnly;
}

-(BOOL)isCurrentTime:(int)hourTime Between:(NSString *)startHour   andEnd:(NSString *)endHour{
    
    int endH=0,startH = 0;
    
    if (![Validator isNullString:endHour]) {
        
        if (endHour.length>=2) {
            
            endH=[[endHour substringToIndex:2]intValue];
        }
    }
    
    if (![Validator isNullString:startHour]) {
        
        if (startHour.length>=2) {
            
            startH=[[startHour substringToIndex:2]intValue];
        }
    }
    
    if (startH==0 && endH==0)
        return true;
    
    
    if (hourTime==endH || hourTime==startH)
        return true;
    else if (hourTime>startH && hourTime<endH)
        return true;
//    else if (hourTime<endH)
//        return true;
    else
        return false;
}

-(BOOL)isEvent:(NSUInteger)index{
    
    
    if (arrCollectionCellInformation.count==0)
        return false;
    
    
//    int eventCount= (int)arrCollectionCellInformation.count;
//    
//    if (eventCount<=4)
//        index=index%5;
//    else
//        index=index%(eventCount+1);
//    
//    index=index-1;
    
    NSDictionary *dict=[arrCollectionCellInformation objectAtIndex:([self getColumnIndex:index]-1)];
    
    if ([[dict objectForKey:@"eventType"]isEqualToString:@"Event"])
        return true;
    else
        return false;
    
}

-(UIColor *)isCellColorable:(int)index{
    
    //2

    int eventCount= (int)arrCollectionCellInformation.count;
    
    if ( eventCount ==0)
        return [UIColor whiteColor];
    
    
//    if (eventCount<=4)
//        index=index%5;
//    else
//        index=index%eventCount+1;
//    
//    index=index-1;
    
    NSDictionary *dict=[arrCollectionCellInformation objectAtIndex:([self getColumnIndex:index]-1)];

    return (UIColor *)[dict objectForKey:@"color"];

    
    //return [UIColor whiteColor];
}

-(NSArray *)generateArrayofIndex:(int)index{
    
    NSMutableArray *temp=[NSMutableArray array];
    
    for (int i=1; i<=60; i=i+5) {
        [temp addObject:[NSNumber numberWithInt:i]];
    }
    return temp;
}


#ifdef USE_EVENTKIT_DATA_SOURCE

- (NSArray *)dayView:(MADayView *)dayView eventsForDate:(NSDate *)startDate {
    return [self.eventKitDataSource dayView:dayView eventsForDate:startDate];
}

#else
- (NSArray *)dayView:(MADayView *)dayView eventsForDate:(NSDate *)startDate {
    date = startDate;
    
    NSArray *arr = [NSArray arrayWithObjects: self.event, self.event, self.event,
                    self.event, self.event, self.event, self.event,  self.event, self.event, nil];
    static size_t generateAllDayEvents;
    
    generateAllDayEvents++;
    
    if (generateAllDayEvents % 4 == 0) {
        ((MAEvent *) [arr objectAtIndex:0]).title = @"All-day events test";
        ((MAEvent *) [arr objectAtIndex:0]).allDay = YES;
        
        ((MAEvent *) [arr objectAtIndex:1]).title = @"All-day events test";
        ((MAEvent *) [arr objectAtIndex:1]).allDay = YES;
    }
    return arr;
}
#endif

- (MAEvent *)event {
    static int counter;
    static BOOL flag;
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:[NSString stringWithFormat:@"number %i", counter++] forKey:@"test"];
    
    unsigned int r = arc4random() % 24;
    int rr = arc4random() % 3;
    
    MAEvent *event = [[MAEvent alloc] init];
    event.backgroundColor = ((flag = !flag) ? [UIColor purpleColor] : [UIColor brownColor]);
    event.textColor = [UIColor whiteColor];
    event.allDay = NO;
    event.userInfo = dict;
    
    if (rr == 0) {
        event.title = @"Event lorem ipsum es dolor test. This a long text, which should clip the event view bounds.";
    } else if (rr == 1) {
        event.title = @"Foobar.";
    } else {
        event.title = @"Dolor test.";
    }
    
    NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:date];
    [components setHour:r];
    [components setMinute:0];
    [components setSecond:0];
    
    event.start = [CURRENT_CALENDAR dateFromComponents:components];
    
    [components setHour:r+rr];
    [components setMinute:0];
    
    event.end = [CURRENT_CALENDAR dateFromComponents:components];
    
    return event;
}

- (MAEventKitDataSource *)eventKitDataSource {
    if (!eventKitDataSource) {
        eventKitDataSource = [[MAEventKitDataSource alloc] init];
    }
    return eventKitDataSource;
}

/* Implementation for the MADayViewDelegate protocol */

- (void)dayView:(MADayView *)dayView eventTapped:(MAEvent *)event {
    
    NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:event.start];
    NSString *eventInfo = [NSString stringWithFormat:@"Hour %li. Userinfo: %@", (long)[components hour], [event.userInfo objectForKey:@"test"]];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:event.title
                                                    message:eventInfo delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}


-(void)didSaveEvent{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self getEvents];
    });
}


- (IBAction)actionFowardDate:(id)sender {
    
    dayViewDate=[dayViewDate dateByAddingTimeInterval:60*60*24*1];
    _lblDate.text=[self dayViewDateString:dayViewDate];
    arrCollectionCellInformation = [self getEventArray:dayViewDate];
    
    _collectionview.hidden=NO;
    [_collectionview reloadData];
}

-(NSString *)dayViewDateString:(NSDate *)date{
    
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"MM/dd/yyyy";
    }
    
    return [dateFormatter stringFromDate:date];
}
- (IBAction)actionBackwardDate:(id)sender {
    
     dayViewDate=[dayViewDate dateByAddingTimeInterval:-60*60*24*1];
     arrCollectionCellInformation = [self getEventArray:dayViewDate];
     _lblDate.text=[self dayViewDateString:dayViewDate];
    _collectionview.hidden=NO;
    [_collectionview reloadData];
}

- (IBAction)reloadList:(id)sender {
    [self getEvents];
}

-(CAGradientLayer *)cellBackgroundColor:(CGRect )cellBounds{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = cellBounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor whiteColor] CGColor], (id)[[UIColor blackColor] CGColor], nil];
    //[view.layer insertSublayer:gradient atIndex:0];
    return gradient;
}

-(NSMutableArray *)addRandomColor:(NSMutableArray *)events{
    
    NSMutableArray *temp=[NSMutableArray array];
    
    for (NSDictionary *dict in events) {
        NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
        [tempDict setDictionary:dict];
        [tempDict setObject:[self getColor] forKey:@"color"];
        [temp addObject:tempDict];
    }
    
    return temp;
}

-(UIColor *)getColor{
    
    float r= arc4random()%255;
    float g= arc4random()%255;
    float b= arc4random()%255;
    
    return [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:1];
}


@end
