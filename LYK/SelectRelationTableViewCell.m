//
//  SelectRelationTableViewCell.m
//  LYK
//
//  Created by Subhapam on 02/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import "SelectRelationTableViewCell.h"

@implementation SelectRelationTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
