//
//  GamificationViewController.h
//  LYK
//
//  Created by Subhapam on 13/12/15.
//  Copyright © 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GamificationDelegate <NSObject>

-(void)shouldDismissGamificationController;

@end

@interface GamificationViewController : UIViewController

@property (nonatomic, weak) id<GamificationDelegate> delegate;

@end
