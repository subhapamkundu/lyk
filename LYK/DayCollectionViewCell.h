//
//  DayCollectionViewCell.h
//  LYK
//
//  Created by Gouranga Sasmal on 22/07/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DayCollectionViewCell : UICollectionViewCell

//@property (weak,nonatomic) IBOutlet UIImageView *cellImage;
@property (weak, nonatomic) IBOutlet UIImageView *eventImageView;
@property (weak, nonatomic) IBOutlet UIImageView *taskImageView;
@property (weak,nonatomic) IBOutlet UILabel *dayLabel;

@end
