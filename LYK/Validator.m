//
//  Validator.m
//  LYK
//
//  Created by Subhapam on 18/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "Validator.h"

@implementation Validator

+ (BOOL) validateEmailId:(NSString *)emailId{
    BOOL stricterFilter = false;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailId];
}

+ (NSString *) validatePhoneNumber:(NSString *)phoneNumber withCountryCode:(NSString *)countryCode{
    if(phoneNumber.length == 10){
        if([phoneNumber characterAtIndex:0] > '6' && [phoneNumber characterAtIndex:0] <= '9'){
            NSString *numberRegEx = @"[0-9]{10}";
            NSPredicate *numberTestPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
            if ([numberTestPredicate evaluateWithObject:phoneNumber] == true) {
                NSString * plistPath = [[NSBundle mainBundle] pathForResource:@"DiallingCodes" ofType:@"plist"];
                NSDictionary *diallingCodesDictionary = [NSDictionary dictionaryWithContentsOfFile:plistPath];
                NSString *dialingCode = [diallingCodesDictionary objectForKey:countryCode?:@""];
                // check if country code exists in list
                [NSString stringWithFormat:@"%@%@",dialingCode?:@"",phoneNumber];
                return phoneNumber;
                //                return [dialingCode stringByAppendingString:phoneNumber]?[dialingCode stringByAppendingString:phoneNumber]:phoneNumber;
                //            if(dialingCode || [allTrim(dialingCode) isEqualToString:@""]) {
                //                return nil;
                //            } else {
                //                return [dialingCode stringByAppendingString:phoneNumber]?[dialingCode stringByAppendingString:phoneNumber]:phoneNumber;
                //            }
            }
        }
    }
    return nil;
}

+ (BOOL) validateZipcode:(NSString *)zipCode{
    NSString *zipcodeRegEx = @"[0-9]{6}";
    NSPredicate *zipcodeTestPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", zipcodeRegEx];
    return [zipcodeTestPredicate evaluateWithObject:zipCode];
}

#pragma mark-
#pragma mark  Null String Exception Handel

+(BOOL)isNullString:(NSString*)_inputString{
    NSString *InputString=@"";
    InputString=[NSString stringWithFormat:@"%@",_inputString];
    if((InputString == nil) ||(InputString ==(NSString *)[NSNull null])||([InputString isEqual:nil])||([InputString isKindOfClass:[NSNull class]])||([InputString length] == 0)||[allTrim( InputString ) length] == 0||([InputString isEqualToString:@""])||([InputString isEqualToString:@"(NULL)"])||([InputString isEqualToString:@"<NULL>"])||([InputString isEqualToString:@"<null>"]||([InputString isEqualToString:@"(null)"])||([InputString isEqualToString:@""])))
        return YES;
    else
        return NO ;
    
}
+(NSString *)checkNullStringAndReplace:(NSString*)_inputString{
    NSString *InputString=@"";
    InputString=[NSString stringWithFormat:@"%@",_inputString];
    if((InputString == nil) ||(InputString ==(NSString *)[NSNull null])||([InputString isEqual:nil])||([InputString isKindOfClass:[NSNull class]])||([InputString length] == 0)||[allTrim( InputString ) length] == 0||([InputString isEqualToString:@""])||([InputString isEqualToString:@"(NULL)"])||([InputString isEqualToString:@"<NULL>"])||([InputString isEqualToString:@"<null>"]||([InputString isEqualToString:@"(null)"])||([InputString isEqualToString:@""])))
        return @"";
    else
        return _inputString ;
    
}

@end
