//
//  DashboardViewController.m
//  LYK
//
//  Created by Subhapam on 19/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "DashboardViewController.h"
#import "MPFlipViewController.h"
#import "ContentViewController.h"
#import "HomeNavController.h"
#import "HomeTabBarController.h"
#import "DetailContentViewController.h"
#import "SelectRelationTableViewCell.h"

#define MOVIE_MIN		-1

@interface DashboardFlipDataSet : NSObject

@property (nonatomic) NSInteger maxNewsId;
@property (nonatomic) NSInteger maxInsightId;
@property (nonatomic) NSInteger maxQuestionId;
@property (nonatomic) NSInteger maxOfferId;
@property (nonatomic) NSInteger maxProductId;
@property (nonatomic) NSMutableArray *contents;

-(BOOL)parseDataSetWithDetails:(NSDictionary *)json;

@end

@interface DashboardFlipDataSet ()<MenuTransitionDelegate>
{
    
}
@end

@implementation DashboardFlipDataSet

-(BOOL)parseDataSetWithDetails:(NSDictionary *)json{
    _maxNewsId = [[json objectForKey:@"maxNewsId"] integerValue];
    _maxInsightId = [[json objectForKey:@"maxInsightId"] integerValue];
    _maxQuestionId = [[json objectForKey:@"maxQuestionId"] integerValue];
    _maxOfferId = [[json objectForKey:@"maxOfferId"] integerValue];
    _maxProductId = [[json objectForKey:@"maxProductId"] integerValue];
    for (NSDictionary *details in [json objectForKey:@"contents"]) {
        NSDictionary *parsedDetails = nil;
        DebugLog(@"card type : %@",[details objectForKey:@"type"]);
        if([[details objectForKey:@"type"] isEqualToString:@"news"]){
            parsedDetails = [NSDictionary dictionaryWithObjectsAndKeys:
                             [[details objectForKey:@"content"] objectForKey:@"newsId"]?:@"", @"id",
                             [[details objectForKey:@"content"] objectForKey:@"userId"]?:@"", @"userId",
                             [[details objectForKey:@"content"] objectForKey:@"newsTitle"]?:@"", @"statement",
                             [[details objectForKey:@"content"] objectForKey:@"newsImageUrl"]?:@"", @"imageURL",
                             [[details objectForKey:@"content"] objectForKey:@"newsDescription"]?:@"", @"desc",
                             [[details objectForKey:@"content"] objectForKey:@"newsLink"]?:@"", @"linkURL",
                             @"news", @"type",nil];
        } else if ([[details objectForKey:@"type"] isEqualToString:@"blog"]){
            parsedDetails = [NSDictionary dictionaryWithObjectsAndKeys:
                             [[details objectForKey:@"content"] objectForKey:@"blogId"]?:@"", @"id",
                             [[details objectForKey:@"content"] objectForKey:@"userId"]?:@"", @"userId",
                             [[details objectForKey:@"content"] objectForKey:@"blogTitle"]?:@"", @"statement",
                             [[details objectForKey:@"content"] objectForKey:@"blogImageUrl"]?:@"", @"imageURL",
                             [[details objectForKey:@"content"] objectForKey:@"blogDescription"]?:@"", @"desc",
                             [[details objectForKey:@"content"] objectForKey:@"blogLink"]?:@"", @"linkURL",
                             @"blog", @"type",nil];
        } else if ([[details objectForKey:@"type"] isEqualToString:@"insight"]){
            parsedDetails = [NSDictionary dictionaryWithObjectsAndKeys:
                             [[details objectForKey:@"content"] objectForKey:@"insightId"]?:@"", @"id",
                             [[details objectForKey:@"content"] objectForKey:@"userId"]?:@"", @"userId",
                             [[details objectForKey:@"content"] objectForKey:@"statement"]?:@"", @"statement",
                             [[details objectForKey:@"content"] objectForKey:@"insightImageUrl"]?:@"", @"imageURL",
                             [[details objectForKey:@"content"] objectForKey:@"recoTxt"]?:@"", @"desc",
                             [[details objectForKey:@"content"] objectForKey:@"recoLink"]?:@"", @"linkURL",
                             @"insight", @"type", nil];
        } else if ([[details objectForKey:@"type"] isEqualToString:@"question"]){
            parsedDetails = [NSDictionary dictionaryWithObjectsAndKeys:
                             [[details objectForKey:@"content"] objectForKey:@"id"]?:@"", @"id",
                             [[details objectForKey:@"content"] objectForKey:@"userId"]?:@"", @"userId",
                             [[details objectForKey:@"content"] objectForKey:@"subject"]?:@"", @"statement",
                             [[details objectForKey:@"content"] objectForKey:@"questionImageUrl"]?:@"", @"imageURL",
                             [[details objectForKey:@"content"] objectForKey:@"contentBody"]?:@"", @"desc",
                             [[details objectForKey:@"content"] objectForKey:@"url"]?:@"", @"linkURL",
                             @"question", @"type", nil];
        } else if ([[details objectForKey:@"type"] isEqualToString:@"offer"]){
            NSDictionary *itemDetail = [[[details objectForKey:@"content"] objectForKey:@"itemDetail"] objectAtIndex:0];
            parsedDetails = [NSDictionary dictionaryWithObjectsAndKeys:
                             [[details objectForKey:@"content"] objectForKey:@"id"]?:@"", @"id",
                             [[details objectForKey:@"content"] objectForKey:@"userId"]?:@"", @"userId",
                             [[details objectForKey:@"content"] objectForKey:@"statement"]?:@"", @"statement",
                             [[details objectForKey:@"content"] objectForKey:@"offerImageUrl"]?:@"", @"imageURL",
                             [itemDetail objectForKey:@"ItemDesc"]?:@"", @"desc",
                             [[details objectForKey:@"content"] objectForKey:@"offerUrl"]?:@"", @"linkURL",
                             @"offer", @"type", nil];
        } else if ([[details objectForKey:@"type"] isEqualToString:@"product"]){
            parsedDetails = [NSDictionary dictionaryWithObjectsAndKeys:
                             [[details objectForKey:@"content"] objectForKey:@"productId"]?:@"", @"id",
                             [[details objectForKey:@"content"] objectForKey:@"userId"]?:@"", @"userId",
                             [[details objectForKey:@"content"] objectForKey:@"itemDesc"]?:@"", @"statement",
                             [[details objectForKey:@"content"] objectForKey:@"productImageUrl"]?:@"", @"imageURL",
                             [[details objectForKey:@"content"] objectForKey:@"offer"]?:@"", @"desc",
                             [[details objectForKey:@"content"] objectForKey:@"productItemUrl"]?:@"", @"linkURL",
                             @"product", @"type", nil];
        }
        if(!_contents)
            _contents = [NSMutableArray array];
        if(parsedDetails){
            [_contents insertObject:parsedDetails atIndex:[_contents count]];
        }
    }
    return YES;
}

@end

@interface DashboardViewController ()<MPFlipViewControllerDelegate, MPFlipViewControllerDataSource, UITableViewDataSource, UITableViewDelegate>
{
    UIActivityIndicatorView *activityIndicator;
    NSMutableDictionary *socialUserDetails;
    NSUInteger lastOffer;
    NSUInteger lastQuestion;
    NSUInteger lastNews;
    NSUInteger lastInsight;
    NSUInteger lastProduct;
    NSTimer *autoNextPageTimer;
    NSTimer *haltTimer;
    ContentViewController *previousPage;
    ContentViewController *currentPage;
    ContentViewController *nextPage;
    DashboardFlipDataSet *dataSet;
    BOOL shouldStopFlipping;
    NSMutableDictionary *selectedUsers;
}

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (assign, nonatomic) int previousIndex;
@property (assign, nonatomic) int tentativeIndex;
@property (strong, nonatomic) MPFlipViewController *flipViewController;
@property (weak, nonatomic) IBOutlet UIView *shareView;
@property (weak, nonatomic) IBOutlet UITextView *shareMessageTextView;
@property (weak, nonatomic) IBOutlet UITableView *relationTableView;

- (IBAction)viewDetails:(id)sender;
- (IBAction)cancelShare:(id)sender;
- (IBAction)shareElement:(id)sender;
- (IBAction)likeCurrentItem:(id)sender;



@end

@implementation DashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.hidesBackButton = YES;
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"Back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(goBack)];
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:((HomeNavController *)self.navigationController) action:@selector(toggleMenuWithDelegate)];
    
    [self getDataSet];
    
    self.previousIndex = MOVIE_MIN;
    
    // Configure the page view controller and add it as a child view controller.
    CGRect pageViewRect = self.contentView.bounds;
    self.flipViewController = [[MPFlipViewController alloc] initWithOrientation:[self flipViewController:nil orientationForInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation]];
    self.flipViewController.delegate = self;
    self.flipViewController.dataSource = self;
    self.flipViewController.view.frame = pageViewRect;
    [self addChildViewController:self.flipViewController];
    [self.contentView addSubview:self.flipViewController.view];
    [self.flipViewController didMoveToParentViewController:self];
    
    UISwipeGestureRecognizer *moveToTabBarControllerSwipegesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(goToTabBarController)];
    
    [_contentView addGestureRecognizer:moveToTabBarControllerSwipegesture];
    
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]
                                                          initWithTarget:self action:@selector(handleSingleTap:)];
    
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    [_flipViewController.view addGestureRecognizer:singleTapGestureRecognizer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pauseFlipping) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    _shareMessageTextView.layer.cornerRadius = 3.0f;
    _shareMessageTextView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _shareMessageTextView.layer.borderWidth = 1.0f;
    
    NSArray *actionButtons = @[
                               [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"AddFamily"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(addMemberAction)]
                               ];
    self.parentViewController.navigationItem.rightBarButtonItems = actionButtons;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self startFlipping];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    ((HomeTabBarController *)self.parentViewController).menuTransitionDelegate = self;
//    shouldStopFlipping = NO;
//    if(!shouldStopFlipping)
//        autoNextPageTimer = [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(flipArticles) userInfo:nil repeats:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if([autoNextPageTimer isValid]){
        [autoNextPageTimer invalidate];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    shouldStopFlipping = YES;
    if([autoNextPageTimer isValid]){
        [autoNextPageTimer invalidate];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)applicationDidBecomeActive{
    shouldStopFlipping = NO;
    [self startFlipping];
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if([segue.identifier isEqualToString:HomeToDetailSegue]){
         NSDictionary *currentContentDetails = [dataSet.contents objectAtIndex:currentPage.movieIndex];
         [((DetailContentViewController *)segue.destinationViewController) setURL:[currentContentDetails objectForKey:@"linkURL"]];
         [((DetailContentViewController *)segue.destinationViewController) setImage:currentPage.imageView.image];
         ((DetailContentViewController *)segue.destinationViewController).itemDetails = currentContentDetails;
         // pass details to view controller.
     }
 }

-(void) handleSingleTap:(UITapGestureRecognizer *)sender{
    NSLog(@"Touched");
    [((HomeTabBarController *)self.parentViewController) closeMenuIfOpen];
    NSDictionary *currentContentDetails = [dataSet.contents objectAtIndex:currentPage.movieIndex];
    if([currentContentDetails objectForKey:@"linkURL"] && ![allTrim([currentContentDetails objectForKey:@"linkURL"]) isEqualToString:@""]){
        [self performSegueWithIdentifier:HomeToDetailSegue sender:self];
        [self pauseFlipping];
    }
}

-(void)goBack{
    //[self disableTextFieldValidation];
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

#pragma mark - Drawer Menu Transition delegate

-(void)menuWillShow{
    self.view.userInteractionEnabled = false;
}

-(void)menuDidShow{
    
}

-(void)menuDidHide{
    self.view.userInteractionEnabled = true;
}


- (void)contentViewWithIndex:(int)index
{
    NSLog(@"Present index:%d",index);
    if(index > 0){
        if(_previousIndex < _tentativeIndex){
            if(index > [dataSet.contents count]-5){
                [self getDataSet];
            }
            previousPage = currentPage;
            currentPage = nextPage;
            [self createNextPageForIndex:index];
        } else {
            nextPage = currentPage;
            currentPage = previousPage;
            [self createPreviousPageForIndex:index];
//            UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]
//                                                                  initWithTarget:self action:@selector(handleSingleTap:)];
//            
//            singleTapGestureRecognizer.numberOfTapsRequired = 1;
//            [_contentView addGestureRecognizer:singleTapGestureRecognizer];
        }
    } else if(index == 0){
        previousPage = nil;
        [self createCurrentPageForIndex:index];
        [self createNextPageForIndex:index];
    }
    //page.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

-(void)goToTabBarController{
    
}

-(void)pauseFlipping{
    shouldStopFlipping = YES;
    if([autoNextPageTimer isValid]){
        [autoNextPageTimer invalidate];
    }
}

-(void)startFlipping{
//    shouldStopFlipping = NO;
//    if(![autoNextPageTimer isValid])
//        autoNextPageTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:_flipViewController selector:@selector(gotoNextPage) userInfo:nil repeats:YES];
}


#pragma mark - MPFlipViewControllerDelegate protocol

- (void)flipViewController:(MPFlipViewController *)flipViewController didFinishAnimating:(BOOL)finished previousViewController:(UIViewController *)previousViewController transitionCompleted:(BOOL)completed
{
    if (completed)
    {
        self.previousIndex = self.tentativeIndex;
    }
}

- (MPFlipViewControllerOrientation)flipViewController:(MPFlipViewController *)flipViewController orientationForInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        return UIInterfaceOrientationIsPortrait(orientation)? MPFlipViewControllerOrientationVertical : MPFlipViewControllerOrientationHorizontal;
    else
        return MPFlipViewControllerOrientationHorizontal;
}

#pragma mark - MPFlipViewControllerDataSource protocol

-(void)didReceiveUserInteractionInForwardDirection{
    if(![autoNextPageTimer isValid])
        [self startFlipping];
    else
        [self pauseFlipping];
}

-(void)didReceiveUserInteractionInBackwardDirection{
    [self pauseFlipping];
}


- (UIViewController *)flipViewController:(MPFlipViewController *)flipViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    int index = self.previousIndex;
    index--;
    if (index < 0)
        return nil; // reached beginning, don't wrap
    self.tentativeIndex = index;
    [self contentViewWithIndex:index];
    return currentPage;
}

- (UIViewController *)flipViewController:(MPFlipViewController *)flipViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    int index = self.previousIndex;
    index++;
    if (index >= [dataSet.contents count])
        return nil;
    self.tentativeIndex = index;
    [self contentViewWithIndex:index];
    return currentPage;
}

-(void)getDataSet{
    if(!activityIndicator){
        activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
        activityIndicator.layer.cornerRadius = 05;
        activityIndicator.opaque = NO;
        activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        activityIndicator.center = self.view.center;
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
        [self.view addSubview: activityIndicator];
    }
    self.view.userInteractionEnabled = false;
    [activityIndicator startAnimating];
    [[RequestController sharedInstance] getHomeListWithMaxCredentials:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [[UserData sharedInstance] getUserId],@"userId",
      [NSNumber numberWithUnsignedInteger:15],@"setLength",
      [NSNumber numberWithUnsignedInteger:lastOffer],@"lastOffers",
      [NSNumber numberWithUnsignedInteger:lastQuestion],@"lastQustn",
      [NSNumber numberWithUnsignedInteger:lastNews],@"lastNews",
      [NSNumber numberWithUnsignedInteger:lastInsight],@"lastInsight",
      [NSNumber numberWithUnsignedInteger:lastProduct],@"lastProduct",nil] successHandler:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            NSLog(@"data set: %@", response);
            if([response objectForKey:@"token"]){
                [[ApplicationData sharedInstance] setAuthToken:[response objectForKey:@"token"]];
            } else {
                //[[HelperModule sharedInstance] showNetworkToast:YES message:@"Token not validated" animated:YES];
            }
            [self parseFlipDataSetWithJSON:response];
            lastOffer = [dataSet maxOfferId];
            lastQuestion = [dataSet maxQuestionId];
            lastNews = [dataSet maxNewsId];
            lastInsight = [dataSet maxInsightId];
            lastProduct = [dataSet maxProductId];
            if(!currentPage){
                self.previousIndex = 0;
                [self contentViewWithIndex:0];
                [self.flipViewController setViewController:currentPage direction:MPFlipViewControllerDirectionForward animated:NO completion:nil];
                //To Do: Commented as crashing at this point after sign in. 
                //self.view.gestureRecognizers = self.flipViewController.gestureRecognizers;
            }
        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            NSLog(@"%@",error);
        });
    }];

}

-(BOOL)parseFlipDataSetWithJSON:(NSDictionary *)json{
    BOOL flag = NO;
    if(!dataSet)
        dataSet= [[DashboardFlipDataSet alloc] init];
    flag = [dataSet parseDataSetWithDetails:[json objectForKey:@"response"]];
    return flag;
}

-(void)createNextPageForIndex:(NSInteger)index{
    if((index + 1) >= [dataSet.contents count]){
        return;
    }
    nextPage = (ContentViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    NSDictionary *contentDetails = [dataSet.contents objectAtIndex:index+1];
    nextPage.movieIndex = index+1;
    
    [nextPage setContentType:[contentDetails objectForKey:@"type"]];
    [nextPage setContentId:[contentDetails objectForKey:@"id"]];

    if([contentDetails objectForKey:@"imageURL"]){
        if([[contentDetails objectForKey:@"imageURL"] isEqualToString:@""])
            [nextPage setContentImageWithURL:[NSURL URLWithString:@"http://topwalls.net/wallpapers/2012/06/Parent-and-Child-Family-Children-Grassland-Chilean-960x640.jpg"]];
        else
            [nextPage setContentImageWithURL:[NSURL URLWithString:[contentDetails objectForKey:@"imageURL"]]];
    }
    if(![[contentDetails objectForKey:@"type"] isEqualToString:@"question"]){
        if([[contentDetails objectForKey:@"desc"] isEqualToString:@""])
            [nextPage setContentDescription:[contentDetails objectForKey:@"Welcome to LyK"]];
        else
            [nextPage setContentDescription:[contentDetails objectForKey:@"desc"]];
        
        if([[contentDetails objectForKey:@"statement"] isEqualToString:@""])
            [nextPage setContentTitle:[contentDetails objectForKey:@"Welcome to LyK"]];
        else
            [nextPage setContentTitle:[contentDetails objectForKey:@"statement"]];
    } else {
        if([[contentDetails objectForKey:@"desc"] isEqualToString:@""])
            [nextPage setContentDescription:@""];
        else
            [nextPage setContentDescription:@""];
        
        if([[contentDetails objectForKey:@"statement"] isEqualToString:@""])
            [nextPage setContentTitle:[contentDetails objectForKey:@"Welcome to LyK"]];
        else
            [nextPage setContentTitle:[contentDetails objectForKey:@"desc"]];
    }
}

-(void)createCurrentPageForIndex:(NSInteger)index{
    currentPage = (ContentViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    NSDictionary *currentContentDetails = [dataSet.contents objectAtIndex:index];
    currentPage.movieIndex = index;
    
    [currentPage setContentType:[currentContentDetails objectForKey:@"type"]];
    [currentPage setContentId:[currentContentDetails objectForKey:@"id"]];
    
    if([currentContentDetails objectForKey:@"imageURL"]){
        if([[currentContentDetails objectForKey:@"imageURL"] isEqualToString:@""])
            [currentPage setContentImageWithURL:[NSURL URLWithString:@"http://topwalls.net/wallpapers/2012/06/Parent-and-Child-Family-Children-Grassland-Chilean-960x640.jpg"]];
        else
            [currentPage setContentImageWithURL:[NSURL URLWithString:[currentContentDetails objectForKey:@"imageURL"]]];
    }
//    if([[currentContentDetails objectForKey:@"desc"] isEqualToString:@""])
//        [currentPage setContentDescription:[currentContentDetails objectForKey:@"Welcome to LyK"]];
//    else
//        [currentPage setContentDescription:[currentContentDetails objectForKey:@"desc"]];
//    if([[currentContentDetails objectForKey:@"statement"] isEqualToString:@""])
//        [currentPage setContentTitle:[currentContentDetails objectForKey:@"Welcome to LyK"]];
//    else
//        [currentPage setContentTitle:[currentContentDetails objectForKey:@"statement"]];
    
    if(![[currentContentDetails objectForKey:@"type"] isEqualToString:@"question"]){
        if([[currentContentDetails objectForKey:@"desc"] isEqualToString:@""])
            [currentPage setContentDescription:[currentContentDetails objectForKey:@"Welcome to LyK"]];
        else
            [currentPage setContentDescription:[currentContentDetails objectForKey:@"desc"]];
        
        if([[currentContentDetails objectForKey:@"statement"] isEqualToString:@""])
            [currentPage setContentTitle:[currentContentDetails objectForKey:@"Welcome to LyK"]];
        else
            [currentPage setContentTitle:[currentContentDetails objectForKey:@"statement"]];
    } else {
        if([[currentContentDetails objectForKey:@"desc"] isEqualToString:@""])
            [currentPage setContentDescription:@""];
        else
            [currentPage setContentDescription:@""];
        
        if([[currentContentDetails objectForKey:@"statement"] isEqualToString:@""])
            [currentPage setContentTitle:[currentContentDetails objectForKey:@"Welcome to LyK"]];
        else
            [currentPage setContentTitle:[currentContentDetails objectForKey:@"desc"]];
    }
    
    
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]
                                                          initWithTarget:self action:@selector(handleSingleTap:)];
    
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    [_contentView addGestureRecognizer:singleTapGestureRecognizer];
}

-(void)createPreviousPageForIndex:(NSInteger)index{
    if((index - 1) < 0){
        previousPage = nil;
        return;
    }
    previousPage = (ContentViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    NSDictionary *contentDetails = [dataSet.contents objectAtIndex:index-1];
    previousPage.movieIndex = index-1;
    
    [previousPage setContentType:[contentDetails objectForKey:@"type"]];
    [previousPage setContentId:[contentDetails objectForKey:@"id"]];
    
    if([contentDetails objectForKey:@"imageURL"]){
        if([[contentDetails objectForKey:@"imageURL"] isEqualToString:@""])
            [previousPage setContentImageWithURL:[NSURL URLWithString:@"http://topwalls.net/wallpapers/2012/06/Parent-and-Child-Family-Children-Grassland-Chilean-960x640.jpg"]];
        else
            [previousPage setContentImageWithURL:[NSURL URLWithString:[contentDetails objectForKey:@"imageURL"]]];
    }
    if([[contentDetails objectForKey:@"desc"] isEqualToString:@""])
        [previousPage setContentDescription:[contentDetails objectForKey:@"Welcome to LyK"]];
    else
        [previousPage setContentDescription:[contentDetails objectForKey:@"desc"]];
    
    if([[contentDetails objectForKey:@"statement"] isEqualToString:@""])
        [previousPage setContentTitle:[contentDetails objectForKey:@"Welcome to LyK"]];
    else
        [previousPage setContentTitle:[contentDetails objectForKey:@"statement"]];
    
    if(![[contentDetails objectForKey:@"type"] isEqualToString:@"question"]){
        if([[contentDetails objectForKey:@"desc"] isEqualToString:@""])
            [previousPage setContentDescription:[contentDetails objectForKey:@"Welcome to LyK"]];
        else
            [previousPage setContentDescription:[contentDetails objectForKey:@"desc"]];
        
        if([[contentDetails objectForKey:@"statement"] isEqualToString:@""])
            [previousPage setContentTitle:[contentDetails objectForKey:@"Welcome to LyK"]];
        else
            [previousPage setContentTitle:[contentDetails objectForKey:@"statement"]];
    } else {
        if([[contentDetails objectForKey:@"desc"] isEqualToString:@""])
            [previousPage setContentDescription:@""];
        else
            [previousPage setContentDescription:@""];
        
        if([[contentDetails objectForKey:@"statement"] isEqualToString:@""])
            [previousPage setContentTitle:[contentDetails objectForKey:@"Welcome to LyK"]];
        else
            [previousPage setContentTitle:[contentDetails objectForKey:@"desc"]];
    }
}

-(void)flipArticles{
    if(!shouldStopFlipping){
        [_flipViewController gotoNextPage];
    }
}


- (IBAction)viewDetails:(id)sender {
    NSLog(@"Touched");
    NSDictionary *currentContentDetails = [dataSet.contents objectAtIndex:currentPage.movieIndex];
    if([currentContentDetails objectForKey:@"linkURL"] && ![allTrim([currentContentDetails objectForKey:@"linkURL"]) isEqualToString:@""]){
        [self performSegueWithIdentifier:HomeToDetailSegue sender:self];
        [self pauseFlipping];
    }
}

- (IBAction)cancelShare:(id)sender {
    [selectedUsers removeAllObjects];
    _shareMessageTextView.text = @"";
    _shareView.hidden = true;
    [self startFlipping];
}

- (IBAction)shareElement:(id)sender {
    NSDictionary *content = [dataSet.contents objectAtIndex:currentPage.movieIndex];
    NSString *cardType = [content objectForKey:@"type"];
    NSString *cardDataId = [content objectForKey:@"id"];
    NSString *comment = [_shareMessageTextView text];
    NSString *shareTime = [[NSDate date] description];
    NSString *lykDisLyk = @"Y";
    [activityIndicator startAnimating];
    [activityIndicator stopAnimating];
    [[HelperModule sharedInstance] showNetworkToast:YES message:@"Article successfully shared" animated:YES];
}

- (IBAction)likeCurrentItem:(id)sender {
    UIButton *likeBtn = sender;
    likeBtn.selected = true;
}

-(void)shareCurrentItem{
    _shareView.hidden = !_shareView.hidden;
    if(!_shareView.hidden){
        [self pauseFlipping];
        [_relationTableView reloadData];
    } else {
        [self startFlipping];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[UserData sharedInstance] getMembersArray] count];
}

-(SelectRelationTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectRelationTableViewCell *cell = (SelectRelationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"selectRelationCellIdentifier"];
    if(!cell){
        cell = [[SelectRelationTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"selectRelationCellIdentifier"];
    }
    Relationship *relation = [[[UserData sharedInstance] getMembersArray] objectAtIndex:indexPath.row];
    if([selectedUsers objectForKey:[relation getRelationshipId]]){
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleSelected"]];
    } else {
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleDeselected"]];
    }
    [cell.nameLabel setText:[relation getUserName]];
    [cell.detailsLabel setText:[relation getEmail]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectRelationTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Relationship *relation = [[[UserData sharedInstance] getMembersArray] objectAtIndex:indexPath.row];
    if(!selectedUsers)
        selectedUsers = [NSMutableDictionary dictionary];
    if(![selectedUsers objectForKey:[relation getRelationshipId]]){
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleSelected"]];
        [selectedUsers setObject:relation forKey:[relation getRelationshipId]];
    } else {
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleDeselected"]];
        [selectedUsers removeObjectForKey:[relation getRelationshipId]];
    }
    
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"Select Relations";
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectZero];
}


-(void)externalShare{
    NSString *shareText = @"This text I am sharing";
    NSArray *itemsToShare = @[shareText,[NSURL URLWithString:@"http://www.google.com"]];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[];
    activityVC.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Activity Status: %@ : %@", activityType, @"OK");
            [self startFlipping];
        });
        if (completed){
            NSLog(@"The Activity: %@ was completed", activityType);
        } else {
            NSLog(@"The Activity: %@ was NOT completed", activityType);
        }
                       
    };
    [self presentViewController:activityVC animated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self pauseFlipping];
        });
    }];
    
}

-(void)internalShare{
    
}

-(void)addMemberAction{
    [((HomeTabBarController *)self.parentViewController) showAddFamilyActionView];
}


@end
