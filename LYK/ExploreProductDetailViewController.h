//
//  ExploreProductDetailViewController.h
//  LYK
//
//  Created by Subhapam on 22/01/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreProductDetailViewController : UIViewController

@property (nonatomic, weak) NSDictionary *info;

@end
