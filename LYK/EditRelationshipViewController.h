//
//  EditRelationshipViewController.h
//  LYK
//
//  Created by Subhapam on 05/12/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditRelationshipViewController : UIViewController

-(void)showAddMemberView;

@end
