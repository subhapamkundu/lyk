//
//  DetailContentViewController.h
//  LYK
//
//  Created by Subhapam on 13/12/15.
//  Copyright © 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailContentViewController : UIViewController

@property (weak, nonatomic) NSDictionary *itemDetails;


-(void)setURL:(NSString *)url;
-(void)setImage:(UIImage*)thumbnailImage;

@end
