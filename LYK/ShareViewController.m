//
//  ShareViewController.m
//  LYK
//
//  Created by Subhapam on 05/02/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//
//
#import "ShareViewController.h"
#import "SelectRelationTableViewCell.h"

@interface ShareViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableDictionary *selectedUsers;
    UIActivityIndicatorView *activityIndicator;
}

@property (weak, nonatomic) IBOutlet UITextView *shareTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)share:(id)sender;



@end

@implementation ShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _shareTitle.layer.cornerRadius = 3.0f;
    _shareTitle.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _shareTitle.layer.borderWidth = 1.0f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)shareCurrentItem{
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[UserData sharedInstance] getMembersArray] count];
}

-(SelectRelationTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectRelationTableViewCell *cell = (SelectRelationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"selectRelationCellIdentifier"];
    if(!cell){
        cell = [[SelectRelationTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"selectRelationCellIdentifier"];
    }
    Relationship *relation = [[[UserData sharedInstance] getMembersArray] objectAtIndex:indexPath.row];
    if([selectedUsers objectForKey:[relation getRelationshipId]]){
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleSelected"]];
    } else {
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleDeselected"]];
    }
    [cell.nameLabel setText:[relation getUserName]];
    [cell.detailsLabel setText:[relation getEmail]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectRelationTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Relationship *relation = [[[UserData sharedInstance] getMembersArray] objectAtIndex:indexPath.row];
    if(!selectedUsers)
        selectedUsers = [NSMutableDictionary dictionary];
    if(![selectedUsers objectForKey:[relation getRelationshipId]]){
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleSelected"]];
        [selectedUsers setObject:relation forKey:[relation getRelationshipId]];
    } else {
        [cell.checkImageView setImage:[UIImage imageNamed:@"CircleDeselected"]];
        [selectedUsers removeObjectForKey:[relation getRelationshipId]];
    }
    
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"Select Relations";
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (IBAction)share:(id)sender {
    self.view.userInteractionEnabled = false;
    [activityIndicator startAnimating];
    
    NSDictionary *shareCardData = [NSDictionary dictionaryWithObjectsAndKeys:
                                [selectedUsers allKeys]?:@[], @"shareWithUser",
                                [_itemDetails objectForKey:@"id"],@"cardDataId",
                                [_itemDetails objectForKey:@"type"],@"cardType",
                                _shareTitle.text,@"comment",
                                @"Y", @"lykDisLyk", nil];
    [[RequestController sharedInstance] shareCardWithDetails:shareCardData successHandler:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            NSLog(@"data set: %@", response);
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"Shared successfully" animated:YES];
            [self.navigationController popViewControllerAnimated:YES];
        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            NSLog(@"%@",error);
            [[HelperModule sharedInstance] showNetworkToast:YES message:[error.userInfo objectForKey:NetworkRequest_ErrorInfo]?:@"No Response" animated:YES];
        });
    }];
}
@end
