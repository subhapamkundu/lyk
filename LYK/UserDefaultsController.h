//
//  UserDefaultsController.h
//  LYK
//
//  Created by Subhapam kundu on 4/2/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaultsController : NSObject

+(void)saveSOSListByUserToUserDefaults:(NSMutableDictionary *)sosListByUser deleteExisting:(BOOL)shouldDeleteExisting;
+(void)saveSOSListForUserToUserDefaults:(NSMutableDictionary *)sosListForUser deleteExisting:(BOOL)shouldDeleteExisting;
+(NSDictionary *)getSOSListByUserFromUserDefaults;
+(NSDictionary *)getSOSListForUserFromUserDefaults;

+(void)saveSafetyLocationsByUserToUserDefaults:(NSMutableDictionary *)safetyLocationsByUser deleteExisting:(BOOL)shouldDeleteExisting;
+(void)saveSafetyLocationsForUserToUserDefaults:(NSMutableDictionary *)safetyLocationsForUser deleteExisting:(BOOL)shouldDeleteExisting;
+(NSDictionary *)getSafetyListByUserFromUserDefaults;
+(NSDictionary *)getSafetyListForUserFromUserDefaults;

@end
