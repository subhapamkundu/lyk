//
//  NSString+LYK.h
//  LYK
//
//  Created by Subhapam on 01/02/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (LYK)


- (CGSize)getHeightHavingWidth:(CGFloat)widthValue andFont:(UIFont *)font;


@end
