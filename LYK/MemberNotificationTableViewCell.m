//
//  MemberNotificationTableViewCell.m
//  LYK
//
//  Created by Subhapam on 13/11/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "MemberNotificationTableViewCell.h"

@implementation MemberNotificationTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)acceptRelationship:(id)sender{
    NSLog(@"acceptRelationship");
    if([_memberNotificationTableViewCellDelegate respondsToSelector:@selector(shouldAcceptRelationshipWithDetails:)]){
        [_memberNotificationTableViewCellDelegate performSelector:@selector(shouldAcceptRelationshipWithDetails:) withObject:_relationDetails];
    }
}

- (IBAction)rejectRelationship:(id)sender{
    NSLog(@"rejectRelationship");
    if([_memberNotificationTableViewCellDelegate respondsToSelector:@selector(shouldRejectRelationshipWithDetails:)]){
        [_memberNotificationTableViewCellDelegate performSelector:@selector(shouldRejectRelationshipWithDetails:) withObject:_relationDetails];
    }
}


@end
