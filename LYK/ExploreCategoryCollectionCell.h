//
//  ExploreCategoryCollectionCell.h
//  LYK
//
//  Created by Subhapam on 11/02/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreCategoryCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;
@property (weak, nonatomic) IBOutlet UILabel *categoryName;


@end
