//
//  UserProfileViewController.m
//  LYK
//
//  Created by Subhapam on 14/11/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "UserProfileViewController.h"
#import <UIImageView+AFNetworking.h>
#import "UserProfileTableViewCell.h"

#define UserProfileTableViewCellIdentifier  @"UserProfileTableViewCellIdentifier"

@interface UserProfileViewController ()<UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIAlertViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource>
{
    UIActivityIndicatorView *activityIndicator;
    BOOL isDPUpdated;
    BOOL isSettingsViewVisible;
    UIDatePicker *datePicker;
    UITextField *countryCodeTextField;
    UIPickerView *countryCodePickerView;
    NSMutableDictionary *tableData;
}

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet LYKTextField *dobTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *emailTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *fnameTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *mnameTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *lnameTextField;
@property (weak, nonatomic) IBOutlet LYKTextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordBtn;
@property (weak, nonatomic) IBOutlet UIButton *updatePrefBtn;
@property (weak, nonatomic) IBOutlet UIButton *editRelationshipBtn;
@property (weak, nonatomic) IBOutlet UIView *settingsContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *settingsIndicatorImageViewLeft;
@property (weak, nonatomic) IBOutlet UIImageView *settingsIndicatorImageViewRight;
@property (weak, nonatomic) IBOutlet UIButton *updateStatusBtn;


- (IBAction)toggleSettingsSegmentAction:(id)sender;
- (IBAction)saveUserProfileDetails:(id)sender;
- (IBAction)changePasswordAction:(id)sender;
- (IBAction)updatePrefAction:(id)sender;
- (IBAction)editRelationshipAction:(id)sender;
- (IBAction)editStatus:(id)sender;

@end

@implementation UserProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    UITapGestureRecognizer *tpgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(back)];
//    [self.view addGestureRecognizer:tpgr];
//    self.navigationItem = [[UINavigationBar alloc] initWithFrame:CGRectZero];
//    [self.view addSubview:_navigationBar];
//    [self.navigationBar pushNavigationItem:self.navigationItem animated:NO];
//    self.navigationBar.frame = CGRectMake(0, self.tableView.contentOffset.y, self.tableView.frame.size.width, self.topLayoutGuide.length + 44);
    
    UserData *userData = [UserData sharedInstance];
    _dobTextField.text = [userData getDOB]?:@"";
    _emailTextField.text = [userData getEmailId]?:@"";
    _fnameTextField.text = [userData getUserFirstName]?:@"";
    _lnameTextField.text = [userData getUserLastName]?:@"";
    _mnameTextField.text = [userData getUserMiddleName]?:@"";
    _phoneTextField.text = [userData getUserPhoneNumber]?:@"";
    _phoneTextField.leftPadding = [NSNumber numberWithInt:70];
    
    [_profileImageView setImageWithURL:[NSURL URLWithString:[userData getProfilePicURL]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    
    UITapGestureRecognizer *tpgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showCameraActionActionsheet)];
    [_profileImageView addGestureRecognizer:tpgr];
    _profileImageView.layer.cornerRadius = _profileImageView.bounds.size.width/2;
    _profileImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _profileImageView.layer.borderWidth = 1.0f;
    
    _changePasswordBtn.layer.cornerRadius = 5.0f;
    _changePasswordBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _changePasswordBtn.layer.borderWidth = 1.0f;
    
    _updatePrefBtn.layer.cornerRadius = 5.0f;
    _updatePrefBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _updatePrefBtn.layer.borderWidth = 1.0f;
    
    _editRelationshipBtn.layer.cornerRadius = 5.0f;
    _editRelationshipBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _editRelationshipBtn.layer.borderWidth = 1.0f;
    
    countryCodeTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 60, _phoneTextField.bounds.size.height)];
    countryCodeTextField.textAlignment = NSTextAlignmentCenter;
    countryCodeTextField.borderStyle = UITextBorderStyleLine;
    countryCodeTextField.minimumFontSize = 10.0f;
    countryCodeTextField.inputView = countryCodePickerView;
    _phoneTextField.leftViewMode = UITextFieldViewModeAlways;
    _phoneTextField.leftView = countryCodeTextField;
//    countryCodeTextField.text = [[userData getUserPhoneNumber] isEqualToString:@""]?[[ApplicationData sharedInstance] getCurrentCountryCode]:[userData getUserPhoneNumberCountryCode];
    countryCodeTextField.text = [[ApplicationData sharedInstance] getCurrentCountryCode]?:[userData getUserPhoneNumberCountryCode];
    [self createCountryCodePickerView];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavHeader"]];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _profileImageView.layer.cornerRadius = _profileImageView.bounds.size.width/2;
    _profileImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _profileImageView.layer.borderWidth = 1.0f;
    
    [self createDatePicker];
    _dobTextField.inputView = datePicker;
    _dobTextField.delegate = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark -
#pragma mark - UITextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == _dobTextField) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSDate *dob = [dateFormatter dateFromString:textField.text];
        if(dob)
            datePicker.date = dob;
        else
            textField.text = [self formatDate:datePicker.date];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    
    if(textField.keyboardType == UIKeyboardTypeEmailAddress){
        if([Validator validateEmailId:textField.text]){
            ;
        } else {
            textField.backgroundColor = [UIColor colorWithRed:180.0f/255.0f green:0.0f blue:0.0f alpha:0.3f];
        }
    }
    if(textField.keyboardType == UIKeyboardTypeNamePhonePad){
        if([Validator validatePhoneNumber:textField.text withCountryCode:@"in"]){
            ;
        } else {
            textField.backgroundColor = [UIColor colorWithRed:180.0f/255.0f green:0.0f blue:0.0f alpha:0.3f];
        }
    }
    if(!tableData){
        tableData = [NSMutableDictionary dictionary];
    }
    [tableData setObject:textField.text forKey:[NSNumber numberWithInteger:textField.tag]];
}

#pragma mark -
#pragma mark - UITableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

-(UserProfileTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:UserProfileTableViewCellIdentifier];
    if (cell == nil) {
        cell = [[UserProfileTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:UserProfileTableViewCellIdentifier];
    }
    cell.dataTextField.delegate = self;
    cell.dataTextField.tag = indexPath.row;
    if(tableData)
        cell.dataTextField.text = [tableData objectForKey:[NSNumber numberWithInteger:indexPath.row]]?:BlankText;
    switch (indexPath.row) {
        case 0:{
            cell.leftIconImageView.image = [UIImage imageNamed:@"UserProfileEmailIcon"];
            cell.dataTextField.inputView = datePicker;
        }
            break;
        case 1:{
            cell.leftIconImageView.image = [UIImage imageNamed:@"UserProfileEmailIcon"];
        }
            break;
        case 2:{
            cell.leftIconImageView.image = [UIImage imageNamed:@"UserProfileEmailIcon"];
        }
            break;
        case 3:{
            cell.leftIconImageView.image = [UIImage imageNamed:@"UserProfileEmailIcon"];
        }
            break;
        case 4:{
            cell.leftIconImageView.image = [UIImage imageNamed:@"UserProfileEmailIcon"];
        }
            break;
        
        default:
            break;
    }
    
    return cell;
}

#pragma mark -
#pragma mark - Action

-(void)back{
    isDPUpdated = false;
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)showCameraActionActionsheet{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Reset Profile Image" destructiveButtonTitle:nil otherButtonTitles:@"Open Camera", @"Open Camera Roll", @"Get Image From URL", nil];
        [actionSheet showInView:self.view];
        actionSheet.tag = 0;
    } else {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Reset Profile Image" destructiveButtonTitle:nil otherButtonTitles:@"Open Camera Roll", @"Get Image From URL", nil];
        [actionSheet showInView:self.view];
        actionSheet.tag = 0;
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(actionSheet.tag == 0){
        switch (buttonIndex) {
            case 0: // Open Camera
            {
                if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                    picker.delegate = self;
                    picker.allowsEditing = YES;
                    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    [self presentViewController:picker animated:YES completion:NULL];
                } else {
                    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                    imagePicker.delegate = self;
                    imagePicker.allowsEditing = YES;
                    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
                    [self presentViewController:imagePicker animated:YES completion:NULL];
                }
            }
                break;
            case 1: // Open Photo Library
            {
                if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                    
                } else {
                    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                    picker.delegate = self;
                    picker.allowsEditing = YES;
                    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    [self presentViewController:picker animated:YES completion:NULL];
                }
            }
                break;
            case 2: // Get Image from URL
            {
                if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                    // cancel
                } else {
                    
                }
            }
                break;
            case 3: // Cancel - Undo Image change
            {
                
            }
                break;
                
            default:
                break;
        }
    } else if(actionSheet.tag == 1){
        switch (buttonIndex) {
            case 0:{
                [self changePasswordAction:nil];
            }
                break;
            case 1:{
                [self updatePrefAction:nil];
            }
                break;
//            case 2:{
//                [self editRelationshipAction:nil];
//            }
//                break;
                
            default:
                break;
        }
    } else if (actionSheet.tag == 2){
        if(buttonIndex != actionSheet.cancelButtonIndex){
            [_updateStatusBtn setTitle:[actionSheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateNormal];
            [[RequestController sharedInstance] changePasswordRequest:[NSDictionary dictionaryWithObjectsAndKeys:[actionSheet buttonTitleAtIndex:buttonIndex],@"status", nil] successHandler:^(NSDictionary *response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    [[HelperModule sharedInstance] showNetworkToast:YES message:@"Status changed" animated:YES];
                    [[UserData sharedInstance] setStatus:_updateStatusBtn.currentTitle];
                });
            } failureHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    [[HelperModule sharedInstance] showNetworkToast:YES message:@"Status updation failed." animated:YES];
                });
            }];
        }
        
    }
}

- (IBAction)toggleSettingsSegmentAction:(id)sender {
//    CGRect presentRect = _settingsContainerView.frame;
//    if(!isSettingsViewVisible){
//        CGRect updatedRect = CGRectMake(0, self.view.frame.size.height - presentRect.size.height, presentRect.size.width, presentRect.size.height);
//        [UIView beginAnimations:nil context:nil];
//        [UIView setAnimationDuration:0.2];
//        _settingsContainerView.frame = updatedRect;
//        [UIView commitAnimations];
//    } else {
//        CGRect updatedRect = CGRectMake(0, self.view.frame.size.height - (presentRect.size.height - 138), presentRect.size.width, presentRect.size.height);
//        [UIView beginAnimations:nil context:nil];
//        [UIView setAnimationDuration:0.2];
//        _settingsContainerView.frame = updatedRect;
//        [UIView commitAnimations];
//    }
    isSettingsViewVisible = !isSettingsViewVisible;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Settings" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Change Password", @"Update Preferences", nil];
    [actionSheet showInView:self.view];
    actionSheet.tag = 1;
}

- (IBAction)saveUserProfileDetails:(id)sender {
    
    
    
    NSLog(@"saveUserProfileDetails");
    if([allTrim([tableData objectForKey:@0]) isEqualToString:@""] || [allTrim(_fnameTextField.text) isEqualToString:@""]){
        [[HelperModule sharedInstance] showNetworkToast:YES message:@"First name can't be left blank" animated:YES];
        return;
    }
    if([allTrim([tableData objectForKey:@0]) isEqualToString:@""] || [allTrim(_lnameTextField.text) isEqualToString:@""]){
        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Last name can't be left blank" animated:YES];
        return;
    }
    if([allTrim([tableData objectForKey:@0]) isEqualToString:@""] || [allTrim(_dobTextField.text) isEqualToString:@""]){
        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Date of birth can't be left blank" animated:YES];
        return;
    }
    if([allTrim([tableData objectForKey:@0]) isEqualToString:@""] || ![Validator validatePhoneNumber:_phoneTextField.text withCountryCode:@"in"]){
        [[HelperModule sharedInstance] showNetworkToast:YES message:@"Phone number is not valid" animated:YES];
        return;
    }
    
    if ([self ischangedAnyThingInUserdata]) {
        
        if(!activityIndicator){
            activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.center.x - 50, self.view.center.y - 50, 100, 100)];
            activityIndicator.layer.cornerRadius = 05;
            activityIndicator.opaque = NO;
            activityIndicator.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
            activityIndicator.center = self.view.center;
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
            [activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
            [self.view addSubview: activityIndicator];
        }
        self.view.userInteractionEnabled = false;
        [activityIndicator startAnimating];
        if(isDPUpdated){
            [[RequestController sharedInstance] uploadImageWithData:UIImageJPEGRepresentation(_profileImageView.image, 1.0) successHandler:^(NSDictionary *response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString *imageURL = [[response objectForKey:Key_Response] objectForKey:@"userImageUrl"];
                    [self updateUserDetailsWithImageURL:imageURL];
                });
            } failureHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.view.userInteractionEnabled = true;
                    [activityIndicator stopAnimating];
                    [[HelperModule sharedInstance] showNetworkToast:YES message:@"Image upload failed." animated:YES];
                });
            }];
        } else {
            [self updateUserDetailsWithImageURL:nil];
        }
    }
    
    
}

-(void)updateUserDetailsWithImageURL:(NSString *)imageURL{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:_dobTextField.text forKey:@"dateOfBirth"];
    [dict setObject:_fnameTextField.text forKey:@"firstName"];
    [dict setObject:_mnameTextField.text forKey:@"middleName"];
    [dict setObject:_lnameTextField.text forKey:@"lastName"];
    [dict setObject:_phoneTextField.text forKey:@"contactNo"];
    [dict setObject:countryCodeTextField.text forKey:@"countrycode"];
   // [dict setObject:[NSString stringWithFormat:@"%@-%@",countryCodeTextField.text,_phoneTextField.text] forKey:@"contactNo"];
    if(imageURL)
        [dict setObject:imageURL forKey:@"userImageUrl"];
    [[RequestController sharedInstance] updateProfileWithDetails:dict successHandler:^(NSDictionary *response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"User profile updated." animated:YES];
            isDPUpdated = NO;
            [[UserData sharedInstance] setFirstName:_fnameTextField.text];
            [[UserData sharedInstance] setEmailId:_emailTextField.text];
            [[UserData sharedInstance] setMiddleName:_mnameTextField.text];
            [[UserData sharedInstance] setLastName:_lnameTextField.text];
            [[UserData sharedInstance] setPhoneNumber:_phoneTextField.text];
            [[UserData sharedInstance] setPhoneNumberCountryCode:countryCodeTextField.text];
        });
    } failureHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.view.userInteractionEnabled = true;
            [activityIndicator stopAnimating];
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"Profile updation failed." animated:YES];
        });
    }];
}

- (IBAction)changePasswordAction:(id)sender{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Change Password" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [av setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    
    // Alert style customization
    [[av textFieldAtIndex:0] setSecureTextEntry:YES];
    [[av textFieldAtIndex:1] setSecureTextEntry:YES];
    [[av textFieldAtIndex:0] setPlaceholder:@"Old Password"];
    [[av textFieldAtIndex:1] setPlaceholder:@"New Password"];
    [av show];
}

-(BOOL)ischangedAnyThingInUserdata{
    
    UserData *userData = [UserData sharedInstance];
    
    NSMutableDictionary *storedUserInformation=[NSMutableDictionary dictionary];
    
    [storedUserInformation setObject:[userData getUserFirstName] forKey:@"fname"];
    [storedUserInformation setObject:[userData getUserLastName] forKey:@"lname"];
    [storedUserInformation setObject:[userData getUserMiddleName] forKey:@"mname"];
    [storedUserInformation setObject:[userData getUserPhoneNumber] forKey:@"phone"];
    [storedUserInformation setObject:[userData getDOB] forKey:@"dob"];
    
    NSMutableDictionary *tempUserInformation=[NSMutableDictionary dictionary];
    
    [tempUserInformation setObject:_fnameTextField.text forKey:@"fname"];
    [tempUserInformation setObject:_lnameTextField.text forKey:@"lname"];
    [tempUserInformation setObject:_mnameTextField.text forKey:@"mname"];
    [tempUserInformation setObject:_phoneTextField.text forKey:@"phone"];
    [tempUserInformation setObject:_dobTextField.text forKey:@"dob"];

    if ([storedUserInformation isEqualToDictionary:tempUserInformation]) {
         [[HelperModule sharedInstance] showNetworkToast:YES message:@"Nothing to change" animated:YES];
        return NO;
    }
    
    return YES;
}

- (IBAction)updatePrefAction:(id)sender{
    [self performSegueWithIdentifier:@"EditProfileToInterestsSegueIdentifier" sender:nil];
}

- (IBAction)editRelationshipAction:(id)sender{
    [self performSegueWithIdentifier:@"EditProfileToRelationshipSegueIdentifier" sender:nil];
}

- (IBAction)editStatus:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Settings" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Available", @"Busy", @"Driving", @"Heading Home", @"In A Meeting", @"With Family !", nil];
    [actionSheet showInView:self.view];
    actionSheet.tag = 2;
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    _profileImageView.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    isDPUpdated = YES;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex != alertView.cancelButtonIndex){
        NSString *oldPwd = [alertView textFieldAtIndex:0].text;
        NSString *newPwd = [alertView textFieldAtIndex:1].text;
        
        if([oldPwd isEqualToString:@""]){
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"Enter old password" animated:YES];
            return;
        }
        if([newPwd isEqualToString:@""]){
            [[HelperModule sharedInstance] showNetworkToast:YES message:@"Enter new password" animated:YES];
            return;
        }
        
        [[RequestController sharedInstance] changePasswordRequest:[NSDictionary dictionaryWithObjectsAndKeys:oldPwd,@"curPass",newPwd,@"newPass", nil] successHandler:^(NSDictionary *response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [[HelperModule sharedInstance] showNetworkToast:YES message:@"Password changed" animated:YES];
            });
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.view.userInteractionEnabled = true;
                [activityIndicator stopAnimating];
                [[HelperModule sharedInstance] showNetworkToast:YES message:@"Password updation failed." animated:YES];
            });
        }];
    }
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [[ApplicationData sharedInstance] getCountryCodeList].count;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSDictionary *countryDetails = [[[ApplicationData sharedInstance] getCountryCodeList] objectAtIndex:row];
    countryCodeTextField.text = [countryDetails objectForKey:@"dial_code"];
}

- (UIView*)pickerView:(UIPickerView *)thePickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    NSDictionary *countryDetails = [[[ApplicationData sharedInstance] getCountryCodeList] objectAtIndex:row];
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, thePickerView.bounds.size.width, 44)];
    UILabel* l1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, thePickerView.bounds.size.width - 80, 44)];
//    l1.font = [UIFont sytemFontOfSize:22]; // choose desired size
//    l1.tag = VIEWTAG_LINE1;
    l1.text = [NSString stringWithFormat:@"%@ (%@)",[countryDetails objectForKey:@"name"],[countryDetails objectForKey:@"code"]];
    l1.textAlignment = NSTextAlignmentLeft;
    [v addSubview: l1];
    
    UILabel* l2 = [[UILabel alloc] initWithFrame:CGRectMake(thePickerView.bounds.size.width - 100, 0, 80, 44)];
//    l2.font = [UIFont sytemFontOfSize:14]; // choose desired size
//    l2.tag = VIEWTAG_LINE2;
    l2.text = [NSString stringWithFormat:@"%@",[countryDetails objectForKey:@"dial_code"]];
    l2.textAlignment = NSTextAlignmentRight;
    [v addSubview: l2];
    
    return v;
}


- (void)createDatePicker {
    
    UIView *pickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 216)];
    pickerView.backgroundColor = [UIColor colorWithRed:109.0/255.0 green:110.0/255.0 blue:120.0/255.0 alpha:1];
    
    datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    
    NSString *maxDateString = @"01-Jan-1956";
    NSString *minDateString = @"31-Dec-2020";
    // the date formatter used to convert string to date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // the specific format to use
    dateFormatter.dateFormat = @"dd-MMM-yyyy";
    // converting string to date
    NSDate *theMaximumDate = [dateFormatter dateFromString: maxDateString];
    NSDate *theMinimumDate = [dateFormatter dateFromString: minDateString];
    // repeat the same logic for theMinimumDate if needed
    
    // here you can assign the max and min dates to your datePicker
    [datePicker setMaximumDate:[NSDate date]]; //the min age restriction
    [datePicker setMinimumDate:theMaximumDate]; //the max age restriction (if needed, or else dont use this line)
    
    // set the mode
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    
    // update the textfield with the date everytime it changes with selector defined below
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventAllEditingEvents];
}

-(void)createCountryCodePickerView{
    countryCodePickerView = [[UIPickerView alloc] init];
    [countryCodePickerView setDataSource: self];
    [countryCodePickerView setDelegate: self];
    countryCodePickerView.showsSelectionIndicator = YES;
    countryCodeTextField.inputView = countryCodePickerView;
}

-(void)updateTextField:(id)sender {
    UIDatePicker *picker = (UIDatePicker*)sender;
//    [tableData setObject:[self formatDate:picker.date] forKey:[NSNumber numberWithInt:5]];
    _dobTextField.text = [self formatDate:picker.date];
}

- (NSString *)formatDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}



@end
