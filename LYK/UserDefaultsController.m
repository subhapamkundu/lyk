//
//  UserDefaultsController.m
//  LYK
//
//  Created by Subhapam kundu on 4/2/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import "UserDefaultsController.h"

#define SUITE_NAME                  @"group.lyk.LYKExtension"
#define SOSLISTBYUSER               @"SOSLISTBYUSER"
#define SOSLISTFORUSER              @"SOSLISTFORUSER"
#define UNSAFEREGIONLISTBYUSER      @"UNSAFEREGIONLISTBYUSER"
#define UNSAFEREGIONLISTFORUSER     @"UNSAFEREGIONLISTFORUSER"

@implementation UserDefaultsController

+(void)saveSOSListByUserToUserDefaults:(NSMutableDictionary *)sosListByUser deleteExisting:(BOOL)shouldDeleteExisting{
    NSUserDefaults *sharedDefaults = [UserDefaultsController getUserDefaultsInstance];
    NSMutableDictionary *savedUserDefaultsList = [[sharedDefaults objectForKey:SOSLISTBYUSER] mutableCopy];
    if(shouldDeleteExisting){
        savedUserDefaultsList = sosListByUser;
    } else {
        for (NSString *sosId in [sosListByUser allKeys]) {
            [savedUserDefaultsList setObject:[sosListByUser objectForKey:sosId] forKey:sosId];
        }
    }
    [sharedDefaults setObject:savedUserDefaultsList forKey:SOSLISTBYUSER];
    [sharedDefaults synchronize];
}

+(void)saveSOSListForUserToUserDefaults:(NSMutableDictionary *)sosListForUser deleteExisting:(BOOL)shouldDeleteExisting{
    NSUserDefaults *sharedDefaults = [UserDefaultsController getUserDefaultsInstance];
    NSMutableDictionary *savedUserDefaultsList = [[sharedDefaults objectForKey:SOSLISTFORUSER] mutableCopy];
    if(shouldDeleteExisting){
        savedUserDefaultsList = sosListForUser;
    } else {
        for (NSString *sosId in [sosListForUser allKeys]) {
            [savedUserDefaultsList setObject:[sosListForUser objectForKey:sosId] forKey:sosId];
        }
    }
    [sharedDefaults setObject:savedUserDefaultsList forKey:SOSLISTFORUSER];
    [sharedDefaults synchronize];
}


+(NSDictionary *)getSOSListByUserFromUserDefaults{
    NSUserDefaults *sharedDefaults = [UserDefaultsController getUserDefaultsInstance];
    NSMutableDictionary *savedUserDefaultsList = [[sharedDefaults objectForKey:SOSLISTBYUSER] mutableCopy];
    return savedUserDefaultsList;
}

+(NSDictionary *)getSOSListForUserFromUserDefaults{
    NSUserDefaults *sharedDefaults = [UserDefaultsController getUserDefaultsInstance];
    NSMutableDictionary *savedUserDefaultsList = [[sharedDefaults objectForKey:SOSLISTFORUSER] mutableCopy];
    return savedUserDefaultsList;
}

+(void)saveSafetyLocationsByUserToUserDefaults:(NSMutableDictionary *)safetyLocationsByUser deleteExisting:(BOOL)shouldDeleteExisting{
    NSUserDefaults *sharedDefaults = [UserDefaultsController getUserDefaultsInstance];
    NSMutableDictionary *savedUserDefaultsList = [[sharedDefaults objectForKey:UNSAFEREGIONLISTBYUSER] mutableCopy];
    if(shouldDeleteExisting){
        savedUserDefaultsList = safetyLocationsByUser;
    } else {
        for (NSString *regionId in [safetyLocationsByUser allKeys]) {
            [savedUserDefaultsList setObject:[safetyLocationsByUser objectForKey:regionId] forKey:regionId];
        }
    }
    [sharedDefaults setObject:savedUserDefaultsList forKey:UNSAFEREGIONLISTBYUSER];
    [sharedDefaults synchronize];
}

+(void)saveSafetyLocationsForUserToUserDefaults:(NSMutableDictionary *)safetyLocationsForUser deleteExisting:(BOOL)shouldDeleteExisting{
    NSUserDefaults *sharedDefaults = [UserDefaultsController getUserDefaultsInstance];
    NSMutableDictionary *savedUserDefaultsList = [[sharedDefaults objectForKey:UNSAFEREGIONLISTFORUSER] mutableCopy];
    if(shouldDeleteExisting){
        savedUserDefaultsList = safetyLocationsForUser;
    } else {
        for (NSString *regionId in [safetyLocationsForUser allKeys]) {
            [savedUserDefaultsList setObject:[safetyLocationsForUser objectForKey:regionId] forKey:regionId];
        }
    }
    [sharedDefaults setObject:savedUserDefaultsList forKey:UNSAFEREGIONLISTFORUSER];
    [sharedDefaults synchronize];
}

+(NSDictionary *)getSafetyListByUserFromUserDefaults{
    NSUserDefaults *sharedDefaults = [UserDefaultsController getUserDefaultsInstance];
    NSMutableDictionary *savedUserDefaultsList = [[sharedDefaults objectForKey:UNSAFEREGIONLISTBYUSER] mutableCopy];
    return savedUserDefaultsList;
}

+(NSDictionary *)getSafetyListForUserFromUserDefaults{
    NSUserDefaults *sharedDefaults = [UserDefaultsController getUserDefaultsInstance];
    NSMutableDictionary *savedUserDefaultsList = [[sharedDefaults objectForKey:UNSAFEREGIONLISTFORUSER] mutableCopy];
    return savedUserDefaultsList;
}

+(NSUserDefaults *)getUserDefaultsInstance{
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:SUITE_NAME];
    return sharedDefaults;
}

@end
