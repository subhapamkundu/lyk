//
//  LYKKeychainController.m
//  LYK
//
//  Created by Subhapam Kundu on 03/09/15.
//  Copyright (c) 2015 Subhapam Kundu. All rights reserved.
//

#import "KeychainController.h"

#define LYKKeychainIdentifier    @"LYKUserApp"


@implementation KeychainController

-(void)saveCredentialsInKeychainWithUsername:(NSString *)username password:(NSString *)password {
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, LYKKeychainIdentifier, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, nil];
    
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
    
    // Create dictionary of parameters to add
    NSData* passwordData = [password dataUsingEncoding:NSUTF8StringEncoding];
    dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword), kSecClass, LYKKeychainIdentifier, kSecAttrServer, passwordData, kSecValueData, username, kSecAttrAccount, nil];
    
    // Try to save to keychain
    err = SecItemAdd((__bridge CFDictionaryRef) dict, NULL);
}

-(NSString *)getUsernameFromKeychain {
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, LYKKeychainIdentifier, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
    
    // Look up server in the keychain
    NSDictionary* found = nil;
    CFDictionaryRef foundCF;
    OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
    if(!err){
        found = (__bridge NSDictionary*)(foundCF);
        if (!found)
            return @"";
        else
            return (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
    }
    return @"";
}

-(NSString *)getPasswordFromKeychain {
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, LYKKeychainIdentifier, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
    
    // Look up server in the keychain
    NSDictionary* found = nil;
    CFDictionaryRef foundCF;
    OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) dict, (CFTypeRef*)&foundCF);
    if(!err){
        found = (__bridge NSDictionary*)(foundCF);
        if (!found)
            return @"";
        else
            return [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
    }
    return @"";
}

-(void)clearKeyChain{
    // Create dictionary of search parameters
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, LYKKeychainIdentifier, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
    // Remove any old values from the keychain
    OSStatus err = SecItemDelete((__bridge CFDictionaryRef) dict);
    DebugLog(@"%s >> %d",__PRETTY_FUNCTION__, err);
}

@end
