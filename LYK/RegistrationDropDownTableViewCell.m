//
//  RegistrationDropDownTableViewCell.m
//  LYK
//
//  Created by Subhapam on 19/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "RegistrationDropDownTableViewCell.h"

@interface RegistrationDropDownTableViewCell ()

- (IBAction)toggleShowListAction:(id)sender;

@end

@implementation RegistrationDropDownTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    
    
}

- (IBAction)toggleShowListAction:(id)sender{
    if([_dropDownDelegate respondsToSelector:@selector(toggleDropDownAction:)]){
        [_dropDownDelegate performSelector:@selector(toggleDropDownAction:) withObject:sender];
    }
}

@end
