//
//  DropDown.h
//  LYK
//
//  Created by Subhapam on 19/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DropDownDelegate

-(void)didSelectItem:(NSString *)selectedItem atIndex:(NSNumber *)index;

@end


@interface DropDown : UIView

@property (nonatomic, weak) id <DropDownDelegate> selectorDelegate;

-(void)hideDropDown:(UIButton *)btn;
-(id)showDropDown:(UIButton *)btn height:(CGFloat *)height arr:(NSArray *)arr direction:(NSString *)direction;

@end
