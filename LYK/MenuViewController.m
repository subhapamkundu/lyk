//
//  PickJiMenuViewController.m
//  PickJi
//
//  Created by Subhapam kundu on 15/07/15.
//  Copyright (c) 2015 Subhapam kundu. All rights reserved.
//

#import "MenuViewController.h"
#import <UIImageView+AFNetworking.h>

#define Header_Height       100.0f

@interface MenuViewController () <UITableViewDelegate, UITableViewDataSource>
{
    NSArray *menuItems;
    ScreenState state;
}

@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userEmailLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *userStatusLabel;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    menuItems = @[@{@"Add Family":@"AddFamily"}, @{@"Edit Profile":@"Profile"}, @{@"Notifications":@"Notifications"}, @{@"Play Demo":@"Demo"}, @{@"About Us":@"AboutUs"}, @{@"Privacy Policy":@"Privacy"}, @{@"FAQ":@"Faq"}, @{@"Sign Out":@"Logout"}];
    state = ScreenStateNone;
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MenuBackground"]];
    [tempImageView setFrame:_menuTableView.frame];
    
    _menuTableView.backgroundView = tempImageView;
    _menuTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
//    [_profileImageView setImageWithURL:[NSURL URLWithString:[[UserData sharedInstance] getProfilePicURL]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
//    
//    [_userNameLabel setText:[[UserData sharedInstance] getUserName]];
//    [_userEmailLabel setText:[[UserData sharedInstance] getEmailId]];
    
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swippedLeftToCloseMenu)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    
    _profileImageView.layer.cornerRadius = _profileImageView.frame.size.width/2;
    _profileImageView.layer.borderWidth = 2.0f;
    _profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self updateMenuDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuItems count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuTableViewCell"];
    NSDictionary *menuItem = [menuItems objectAtIndex:indexPath.row];
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"menuTableViewCell"];
        cell.textLabel.text = [[menuItem allKeys] objectAtIndex:0];
        cell.imageView.image = [UIImage imageNamed:[[menuItem allValues] objectAtIndex:0]];
        
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuTableViewCell"];
    NSDictionary *menuItem = [menuItems objectAtIndex:indexPath.row];
//    if(!cell){
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"menuTableViewCell"];
//        
//        
//    }
    cell.textLabel.text = [[menuItem allKeys] objectAtIndex:0];
    cell.imageView.image = [UIImage imageNamed:[[menuItem allValues] objectAtIndex:0]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundColor = [UIColor clearColor];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
        state = indexPath.row;
        switch (state) {
//            case ScreenStateProducts:{
//                if([_menuDelegate respondsToSelector:@selector(didSelectProductsAction)]){
//                    [_menuDelegate performSelector:@selector(didSelectProductsAction)];
//                }
//            } break;
//            case ScreenStateUpgrade:{
//                if([_menuDelegate respondsToSelector:@selector(didSelectUpgradeAction)]){
//                    [_menuDelegate performSelector:@selector(didSelectUpgradeAction)];
//                }
//            }
//                break;
            case ScreenStateAddFamily:{
                if([_menuDelegate respondsToSelector:@selector(didSelectAddFamily)]){
                    [_menuDelegate performSelector:@selector(didSelectAddFamily)];
                }
            } break;
            case ScreenStateAccount:{
                if([_menuDelegate respondsToSelector:@selector(didSelectAccountAction)]){
                    [_menuDelegate performSelector:@selector(didSelectAccountAction)];
                }
            } break;
            case ScreenStateNotifications:{
                if([_menuDelegate respondsToSelector:@selector(didSelectNotificationAction)]){
                    [_menuDelegate performSelector:@selector(didSelectNotificationAction)];
                }
            } break;
            case ScreenStateDemo:{
                if([_menuDelegate respondsToSelector:@selector(didSelectPlayDemo)]){
                    [_menuDelegate performSelector:@selector(didSelectPlayDemo)];
                }
            } break;
//            case ScreenStateActivityLog:{
//                if([_menuDelegate respondsToSelector:@selector(didSelectActivityLogAction)]){
//                    [_menuDelegate performSelector:@selector(didSelectActivityLogAction)];
//                }
//            } break;
//            case ScreenStateFeedback:{
//                if([_menuDelegate respondsToSelector:@selector(didSelectFeedbackAction)]){
//                    [_menuDelegate performSelector:@selector(didSelectFeedbackAction)];
//                }
//            } break;
            case ScreenStateAboutUs:{
                if([_menuDelegate respondsToSelector:@selector(didSelectAboutUs)]){
                    [_menuDelegate performSelector:@selector(didSelectAboutUs)];
                }
            } break;
                
            case ScreenStatePrivacyPolicy:{
                if([_menuDelegate respondsToSelector:@selector(didSelectPrivacyPolicy)]){
                    [_menuDelegate performSelector:@selector(didSelectPrivacyPolicy)];
                }
            } break;
                
            case ScreenStateFAQ:{
                if([_menuDelegate respondsToSelector:@selector(didSelectFAQ)]){
                    [_menuDelegate performSelector:@selector(didSelectFAQ)];
                }
            } break;
            case ScreenStateLogout:{
                if([_menuDelegate respondsToSelector:@selector(didSelectLogoutAction)]){
//                    [[RequestController sharedInstance] sendLogoutRequestWithUserId:[[UserData sharedInstance] getUserId] successHandler:^(NSDictionary *response) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                        });
//                    } failureHandler:^(NSError *error) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            self.view.userInteractionEnabled = true;
//                            //[[[PJKeychainController alloc] init] clearKeyChain];
//                        });
//                    }];
                    //[[UserData sharedInstance] clearSharedInstance];
                    [_menuDelegate performSelector:@selector(didSelectLogoutAction)];
                    state = ScreenStateNone;
                }
            }break;
                
            default:
                break;
        }
        [self resetAllCellHighlightPattern];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

-(void)resetAllCellHighlightPattern{
    for (int index = 0; index < [menuItems count]; index++) {
        UITableViewCell *cell = [_menuTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
//        if(index == state)
//            cell.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
//        else
//            cell.backgroundColor = [UIColor clearColor];
    }
}

-(void)swippedLeftToCloseMenu{
    if([_menuDelegate respondsToSelector:@selector(shouldCloseMenu)]){
        [_menuDelegate performSelector:@selector(shouldCloseMenu)];
    }
}

-(void)updateMenuDetails{
    [_profileImageView setImageWithURL:[NSURL URLWithString:[[UserData sharedInstance] getProfilePicURL]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    [_userStatusLabel setText:[[UserData sharedInstance] getUserStatus]];
    [_userNameLabel setText:[[UserData sharedInstance] getUserName]];
    [_userEmailLabel setText:[[UserData sharedInstance] getEmailId]];
}

@end
