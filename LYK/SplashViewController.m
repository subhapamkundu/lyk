//
//  SplashViewController.m
//  LYK
//
//  Created by Subhapam on 16/10/15.
//  Copyright (c) 2015 Subhapam. All rights reserved.
//

#import "SplashViewController.h"

@interface SplashViewController ()<UIAlertViewDelegate>
{
    NSTimer *animationTimer;
}
@property (weak, nonatomic) IBOutlet UIImageView *splashImageView;

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[RequestController sharedInstance] startMonitoringNetwork];
    [LocationController sharedInstance];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration=4.5;
    transformAnimation.toValue=[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.1, 1.1, 1.1)];
    transformAnimation.removedOnCompletion = FALSE;
    [_splashImageView.layer addAnimation:transformAnimation forKey:@"transform"];
    [self performSelector:@selector(loadAuthorizeViewController_) withObject:nil afterDelay:3.0];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark Private Methods
#pragma mark -


-(void)loadAuthorizeViewController_{
    [self performSegueWithIdentifier:SplashToLoginSegue sender:self];
    [_splashImageView.layer removeAnimationForKey:@"transform"];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    exit(0);
}


@end
