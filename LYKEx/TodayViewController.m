//
//  TodayViewController.m
//  LYKEx
//
//  Created by Subhapam kundu on 3/28/16.
//  Copyright © 2016 Subhapam. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import "UserDefaultsController.h"
#import <CoreLocation/CoreLocation.h>
#import <AudioToolbox/AudioServices.h>

@interface TodayViewController () <NCWidgetProviding, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate>
{
    NSMutableArray *sosListByUser;
    NSMutableArray *sosListForUser;
//    NSMutableArray *safetyRegionList;
//    CLLocationManager *locationManager;
//    CLLocation *lastUpdatedLocation;
}
//@property (strong, nonatomic) NSMutableArray *enteredSafetyRegions;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

- (IBAction)refreshEmergencyContactsAction:(id)sender;

@end

@implementation TodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"Extension loaded");
    [self refreshDetailsFromUserDefaults];
//    locationManager = [[CLLocationManager alloc] init];
//    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
//    locationManager.delegate = self;
//    // iOS 8 and higher
//    if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
//        // TODO: Remember to add the NSLocationWhenInUseUsageDescription plist key
//        [locationManager requestAlwaysAuthorization];
//    }
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9) {
//        locationManager.allowsBackgroundLocationUpdates = YES;
//    }
//    [locationManager startUpdatingLocation];
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshDetailsFromUserDefaults)
                                                 name:NSUserDefaultsDidChangeNotification
                                               object:nil];

}

-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData

    completionHandler(NCUpdateResultNewData);
}

-(UIEdgeInsets)widgetMarginInsetsForProposedMarginInsets:(UIEdgeInsets)defaultMarginInsets {
    return UIEdgeInsetsMake(0, 10, 0, 10);
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSLog(@"numberOfSectionsInTableView");
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"numberOfRowsInSection");
    if(section == 0){
        return [sosListByUser count];
    } else {
        return [sosListForUser count];
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"cellForRowAtIndexPath");
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"exCellIdentifier"];
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"exCellIdentifier"];
    }
    UIImageView *callButton = [[UIImageView alloc] init];
    [callButton setImage:[UIImage imageNamed:@"CallActive"]];
    [callButton setFrame:CGRectMake(0, 0, 40,40)];
    cell.accessoryView = callButton;
    cell.textLabel.textColor = [UIColor whiteColor];
    if(indexPath.section == 0){
        NSDictionary *sosDetails = [sosListByUser objectAtIndex:indexPath.row];
        //            cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[[sosDetails objectForKey:@"createdBy"]objectForKey:@"imageUrl"]?:@""]];
        cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width/2;
        cell.imageView.clipsToBounds = true;
        cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)",[sosDetails objectForKey:@"sosName"]?:@"", [sosDetails objectForKey:@"mobileNo"]?:@""];
    } else {
        NSDictionary *sosDetails = [sosListForUser objectAtIndex:indexPath.row];
        //            cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[[sosDetails objectForKey:@"createdBy"]objectForKey:@"imageUrl"]?:@""]];
        cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width/2;
        cell.imageView.clipsToBounds = true;
        cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)",[sosDetails objectForKey:@"sosName"]?:@"", [sosDetails objectForKey:@"mobileNo"]?:@""];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *numberToCall = nil;
    if(indexPath.section == 0){
        numberToCall = [[[[sosListByUser objectAtIndex:indexPath.row] objectForKey:@"mobileNo"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
    } else {
        numberToCall = [[[[sosListForUser objectAtIndex:indexPath.row] objectForKey:@"mobileNo"] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet]] componentsJoinedByString:@""];
    }
    if(numberToCall){
        NSLog(@"number To Call: %@",numberToCall);
        NSString *phoneNumber = [@"tel://" stringByAppendingString:numberToCall];
        [self.extensionContext openURL:[NSURL URLWithString:phoneNumber] completionHandler:nil];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 1)];
}

-(void)refreshDetailsFromUserDefaults{
    [sosListByUser removeAllObjects];
    [sosListForUser removeAllObjects];
    [self refreshSOSListFromUserDefaults];
//    [self refreshSafetyListFromUserDefaults];
}

-(void)refreshSOSListFromUserDefaults{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    NSDictionary *sosList = [UserDefaultsController getSOSListByUserFromUserDefaults];
    for (NSString *sosId in [sosList allKeys]) {
        if(!sosListByUser)
            sosListByUser = [NSMutableArray array];
        [sosListByUser addObject:[sosList objectForKey:sosId]];
    }
    
    sosList = [UserDefaultsController getSOSListForUserFromUserDefaults];
    for (NSString *sosId in [sosList allKeys]) {
        if(!sosListForUser)
            sosListForUser = [NSMutableArray array];
        [sosListForUser addObject:[sosList objectForKey:sosId]];
    }
    NSLog(@"sosListByUser: %@",sosListByUser);
    NSLog(@"sosListForUser: %@",sosListForUser);
    [_tableview reloadData];
}

//-(void)refreshSafetyListFromUserDefaults{
//    NSLog(@"%s",__PRETTY_FUNCTION__);
//    NSDictionary *safetyList = [UserDefaultsController getSafetyListByUserFromUserDefaults];
//    for (NSString *sosId in [safetyList allKeys]) {
//        if(!safetyRegionList)
//            safetyRegionList = [NSMutableArray array];
//        [safetyRegionList addObject:[safetyList objectForKey:sosId]];
//    }
//    
//    safetyList = [UserDefaultsController getSafetyListForUserFromUserDefaults];
//    for (NSString *sosId in [safetyList allKeys]) {
//        if(!safetyRegionList)
//            safetyRegionList = [NSMutableArray array];
//        [safetyRegionList addObject:[safetyList objectForKey:sosId]];
//    }
//    NSLog(@"safetyRegionList: %@",safetyRegionList);
////    [self playSound:0];
////    [locationManager startMonitoringForRegion:[[CLCircularRegion alloc] initWithCenter: radius: identifier:""]];
//}
//
//-(void)updateUnsafeRegionForMonitoringWithCurrentLocation:(CLLocation *)currentLocation{
//    NSMutableArray *tempArr = [NSMutableArray array];
//    for (NSDictionary *region in safetyRegionList) {
//        NSMutableDictionary *tempRegion = [NSMutableDictionary dictionaryWithDictionary:region];
//        CLLocationCoordinate2D centre = CLLocationCoordinate2DMake([[region objectForKey:@"centreX"] doubleValue], [[region objectForKey:@"centreY"] doubleValue]);
//        CLLocationDistance dist = [currentLocation distanceFromLocation:[[CLLocation alloc] initWithLatitude:centre.latitude longitude:centre.longitude]];
//        [tempRegion setObject:[NSNumber numberWithDouble:dist] forKey:@"dist"];
//        if(tempArr.count){
//            for (int index = 0; index < tempArr.count; index++) {
//                if(dist < [[[tempArr objectAtIndex:index] objectForKey:@"dist"] doubleValue]){
//                    [tempArr insertObject:tempRegion atIndex:index];
//                    break;
//                }
//                if(index == tempArr.count){
//                    [tempArr addObject:tempRegion];
//                }
//            }
//            
//        } else {
//            [tempArr addObject:tempRegion];
//        }
//    }
//    safetyRegionList = tempArr;
//    int maxUnsafeRegionMonitored = (safetyRegionList.count > 20) ? 20 : (int)safetyRegionList.count;
//    NSArray *currentMonitoredRegions = [locationManager.monitoredRegions allObjects];
//    for (CLRegion *reg in currentMonitoredRegions) {
//        [locationManager stopMonitoringForRegion:reg];
//    }
//    
//    for (int index = 0; index < maxUnsafeRegionMonitored; index++) {
//        CLLocationCoordinate2D centre = CLLocationCoordinate2DMake([[[safetyRegionList objectAtIndex:index] objectForKey:@"centreX"] doubleValue], [[[safetyRegionList objectAtIndex:index] objectForKey:@"centreY"] doubleValue]);
//        CLLocationDistance radius = [[[safetyRegionList objectAtIndex:index] objectForKey:@"radius"] doubleValue];
//        CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:centre radius:radius identifier:[[safetyRegionList objectAtIndex:index] objectForKey:@"placeId"]];
//        region.notifyOnEntry = true;
//        region.notifyOnExit = true;
//        [locationManager startMonitoringForRegion:region];
//    }
//}
//
//- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
//    CLLocation *curLocation = [locations lastObject];
//    if(lastUpdatedLocation && [curLocation distanceFromLocation:lastUpdatedLocation] > 300){
//        [self updateUnsafeRegionForMonitoringWithCurrentLocation:curLocation];
//    } else {
//    }
//    lastUpdatedLocation = curLocation;
//    // Output the time the location update was received
//    
//}
//
//- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
//}
//
//-(void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region{
//}
//
//-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region{
//    if(!_enteredSafetyRegions){
//        _enteredSafetyRegions = [NSMutableArray array];
//    }
//    [_enteredSafetyRegions addObject:region.identifier];
//    [self playSound:[region.identifier intValue]];
//}
//
//-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region{
//    [_enteredSafetyRegions removeObject:region.identifier];
//}
//
//- (void)playSound:(UInt32)ssID{
////    SystemSoundID ssID = 0;
//    NSURL *audioPath = [[NSBundle mainBundle] URLForResource:@"alarm1" withExtension:@"aiff"];
//    OSStatus error = AudioServicesCreateSystemSoundID((__bridge CFURLRef)audioPath, &ssID);
//    if (error == kAudioServicesNoError){
//        AudioServicesAddSystemSoundCompletion(ssID, NULL, NULL, (AudioServicesSystemSoundCompletionProc)MyAudioServicesSystemSoundCompletionProc, (__bridge void * _Nullable)(self));
//        AudioServicesPlaySystemSound(ssID);
//    }
//}
//
//void MyAudioServicesSystemSoundCompletionProc (SystemSoundID  ssID, void *clientData) {
//    TodayViewController *obj = (__bridge TodayViewController *)clientData;
//    if(obj.enteredSafetyRegions.count){
//        AudioServicesAddSystemSoundCompletion(ssID, NULL, NULL, (AudioServicesSystemSoundCompletionProc)MyAudioServicesSystemSoundCompletionProc, clientData);
//        AudioServicesPlaySystemSound(ssID);
////        SystemSoundID ssID = 0;
////        NSURL *audioPath = [[NSBundle mainBundle] URLForResource:@"alarm" withExtension:@"wav"];
////        AudioServicesCreateSystemSoundID((__bridge CFURLRef)audioPath, &ssID);
////        AudioServicesAddSystemSoundCompletion(ssID, NULL, NULL, (AudioServicesSystemSoundCompletionProc)MyAudioServicesSystemSoundCompletionProc, clientData);
////        AudioServicesPlaySystemSound(ssID);
//    } else {
//        AudioServicesDisposeSystemSoundID(ssID);
//    }
//    
//}


- (IBAction)refreshEmergencyContactsAction:(id)sender {
    [self refreshDetailsFromUserDefaults];
}
@end
